<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetAdditionalCSS($this->GetTemplate()->GetFolder()."/styler/jquery.formstyler.css");
$APPLICATION->SetAdditionalCSS($this->GetTemplate()->GetFolder()."/styler/jquery.formstyler.theme.css");
$APPLICATION->AddHeadScript($this->GetTemplate()->GetFolder()."/styler/jquery.formstyler.min.js");
?>