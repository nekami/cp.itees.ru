BX.ready(function(){
    var ajax = window.ajaxTarget;
    var changeGrades = new Array();
    var changeHour = new Array();
    var canEdit = window.canEdit;
    
    var buttonClass = "ui-btn-main";
    if(canEdit !== "Y")
    {
        buttonClass = "hidden";
    }
    $('.change_grade').each(function(){
        var id = $(this).data('user');
        
        changeGrades[id] = new BX.PopupWindow("change_grade_"+id, null,{
            content: BX('change_form_container_grade_'+id),
            closeIcon: {right: "20px", top: "10px"},
            titleBar: {content: BX.create("span", {html: "�������� �����"})},
            zIndex: 0,
            offsetLeft: 0,
            offsetTop: 0,
            draggable: {restrict: false},
            overlay: {},
            buttons: [
                    new BX.PopupWindowButton({
                        text: "��������",
                        className: buttonClass,
                        events: {click: function(){
                            if($(this.popupWindow.contentContainer).find('select[name="change_grade"]').val() === null)
                            {
                                alert("�������� �����");
                            }
                            else if($(this.popupWindow.contentContainer).find('input[name="change_hours"]').val() == "")
                            {
                                alert("�������� �����������");
                            }
                            else
                            {
                                $.ajax({
                                    method: "POST",
                                    url: ajax,
                                    data: $(this.popupWindow.contentContainer).find('form').serialize()
                                }).done(function(){
                                        location.reload();
                                });
                            }
                        }}
                }),
                new BX.PopupWindowButton({
                        text: "������",
                        className: "webform-button-link-cancel",
                        events: {click: function(){
                                this.popupWindow.close();
                        }}
                }) 
            ]
        });
    });
    
    $('.change_hour').each(function(){
        var id = $(this).data('user');
        
        changeHour[id] = new BX.PopupWindow("change_hour_"+id, null,{
            content: BX('change_form_container_hour_'+id),
            closeIcon: {right: "20px", top: "10px"},
            titleBar: {content: BX.create("span", {html: "�������� �����������"})},
            zIndex: 0,
            offsetLeft: 0,
            offsetTop: 0,
            draggable: {restrict: false},
            overlay: {},
            buttons: [
                new BX.PopupWindowButton({
                    text: "��������",
                    className: buttonClass,
                    events: {click: function(){
                        if($(this.popupWindow.contentContainer).find('input[name="change_hours"]').val() == "")
                        {
                            alert("�������� �����������");
                        }
                        else
                        {
                             $.ajax({
                                method: "POST",
                                url: ajax,
                                data: $(this.popupWindow.contentContainer).find('form').serialize()
                            }).done(function(){
                                location.reload();
                            });
                        } 
                    }}
                }),
                new BX.PopupWindowButton({
                    text: "������",
                    className: "webform-button-link-cancel",
                    events: {click: function(){
                            this.popupWindow.close();
                    }}
                }) 
            ]
        });
        
    });
    
    $('body').on('click', '.change_grade', function(){
	changeGrades[$(this).data('user')].show();
    });
    
     $('body').on('click', '.change_hour', function(){
	changeHour[$(this).data('user')].show();
    });
    
    $('body').on('click', '.jq-selectbox', function(){
	   $('.jq-selectbox__dropdown').width('');
	   $('.jq-selectbox__dropdown').css('top', '31px');
    });
    
    $('body').on("change", "select[name='change_grade']",(function(){
	   $(this).parents('form').find('input[name="change_hours"]').val($(this).find('option:selected').data("hours"));
    }));
   
});