BX.ready(function(){
	$('body').on("change", "select[name='change_grade']",(function(){
	   $(this).parents('form').find('input[name="change_hours"]').val($(this).find('option:selected').data("hours"));
   }));
	
   $('body').on('click', '.company-department-employee, .finder-box-item', function(){
	   $.ajax({
		   method: "POST",
		   url: $('#content_grade').data('url'),
		   data: {user_id: $(this).find('.intranet-hidden-input').val()},
	   }).done(function(resp){
		   $('#content_grade').html(resp);
	   });
   }); 
});