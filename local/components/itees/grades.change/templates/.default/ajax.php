<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arResult["ERROR"]):?>
	<div class="grade_error"><?=$arResult["ERROR"]?></div>
<?endif?>
<?if($arResult["MESSAGE"]):?>
	<div class="grade_message"><?=$arResult["MESSAGE"]?></div>
<?endif?>

<?if($arResult["CURENT_USER"]):?>
	<div class="user-header">
		<div>
			<h1><?=$arResult["CURENT_USER"]["NAME"]." ".$arResult["CURENT_USER"]["LAST_NAME"]." - ".$arResult["GRADES"][$arResult["CURENT_USER"]["GRADE"]]["NAME"]?></h1>	
		</div>
		
		<span class="change_grade" data-user="<?=$arResult["CURENT_USER"]["ID"]?>"><b><?=GetMessage('CHANGE_GRADE')?></b></span>	
		<span class="change_hour" data-user="<?=$arResult["CURENT_USER"]["ID"]?>"><b><?=GetMessage('CHANGE_COEF')?></b></span>
	
		<div class="change_form_container_wrap">
			<span class="change_form_container" id="change_form_container_grade_<?=$arResult["CURENT_USER"]["ID"]?>">
					<?if($arResult["EDITE"] == "Y"):?>
							<form class="change_form">
							  <p><b><?=GetMessage('GRADE_CHOOSE_GRADE')?></b></p>
							  <div class="grade-name">
								  <span><?=GetMessage('GRADE_GRADE')?></span>
								  <select name="change_grade" class="select_change" required>
										<option value="" disabled selected><?=GetMessage('GRADE_CHOOSE_GRADE')?></option>
									<?foreach($arResult["GRADES"] as $grade):?>
										<option value="<?=$grade["ID"]?>" <?=$arResult["GRADES"][$arResult["CURENT_USER"]["GRADE"]]["ID"]==$grade["ID"]?"selected":""?> data-hours="<?=$grade["PROPERTY_RATE_CHANGE_HOURS_VALUE"]?>"><?=$grade["NAME"]?> (<?=$grade["SECTION"]?>, <?=GetMessage('GRADE_SALARY')?>: <?=$grade["SALARY"]?> <?=GetMessage('GRADE_RUB')?>, <?=GetMessage('GRADE_COEF')?>: <?=$grade["PROPERTY_RATE_CHANGE_HOURS_VALUE"]?>%)</option>
									<?endforeach?>
								  </select>
							  </div>
							  <div class="grade-name">
									<span><?=GetMessage('GRADE_COEF_SM')?></span>
									<input name="change_hours" type="text" value="<?=($arResult["CURENT_USER"]["GRADE_RATE"])?$arResult["CURENT_USER"]["GRADE_RATE"]:$arResult["GRADES"][$arResult["CURENT_USER"]["GRADE"]]["PROPERTY_RATE_CHANGE_HOURS_VALUE"]?>"></input>
							  </div>
							  <div class="grade-name"><span><?=GetMessage('GRADE_DATE_ACTIVE_FROM')?></span><input name="change_date" type="text" value="<?=date("d.m.Y")?>" onclick="BX.calendar({node: this, field: this, bTime: false});" class="crm-entity-widget-content-input"></input></div>
							  <input name="user_id" value="<?=$arResult["CURENT_USER"]["ID"]?>" hidden></input>
							</form>
					<?else:?>
						<div class="change_form_error"><?=GetMessage('GRADE_PERMISION_WARNING')?></div>
					<?endif?>
				</span>
				
				<span class="change_form_container" id="change_form_container_hour_<?=$arResult["CURENT_USER"]["ID"]?>">
					<?if($arResult["EDITE"] == "Y"):?>
						<?if(isset($arResult["CURENT_USER"]["GRADE"])):?>
							<form class="change_form">
							  <p><b><?=GetMessage('GRADE_SET_COEF')?></b></p>
							  <input name="change_grade" value="<?=$arResult["CURENT_USER"]["GRADE"]?>" hidden></input>
							  <div class="grade-name"><span><?=GetMessage('GRADE_COEF_SM')?></span><input name="change_hours" type="text" value="<?=($arResult["CURENT_USER"]["GRADE_RATE"])?$arResult["CURENT_USER"]["GRADE_RATE"]:$arResult["GRADES"][$arResult["CURENT_USER"]["GRADE"]]["PROPERTY_RATE_CHANGE_HOURS_VALUE"]?>"></input></div>
							  <div class="grade-name"><span><?=GetMessage('GRADE_DATE_ACTIVE_FROM')?></span><input name="change_date" type="text" value="<?=date("d.m.Y")?>" onclick="BX.calendar({node: this, field: this, bTime: false});"></input></div>
							  <input name="user_id" value="<?=$arResult["CURENT_USER"]["ID"]?>" hidden></input>
							</form>
						<?else:?>
							<div class="change_form_error"><?=GetMessage('GRADE_ERROR_NO_GRADE')?></div>
						<?endif?>
					<?else:?>
						<div class="change_form_error"><?=GetMessage('GRADE_PERMISION_WARNING')?></div>
					<?endif?>
				</span>
			</div>
	</div>
	
	
	<script>
		$(document).ready(()=>{
			$('.select_change').styler();
		});
		var ajaxTarget = "<?=$_SERVER["SCRIPT_NAME"]?>";
		var curPage = "<?=$APPLICATION->getCurPage();?>";
		var canEdit = "<?=$arResult["EDITE"] ? $arResult["EDITE"] : "N"?>";
		window.mess = {
			CHANGE_GRADE: "<?=GetMessage('CHANGE_GRADE')?>",
			GRADE_CHOOSE_GRADE: "<?=GetMessage('GRADE_CHOOSE_GRADE')?>",
			GRADE_CHANGE_BUTTON: "<?=GetMessage('GRADE_CHANGE_BUTTON')?>",
			GRADE_CHOOSE_COEF: "<?=GetMessage('GRADE_CHOOSE_COEF')?>",
			GRADE_CANCELL: "<?=GetMessage('GRADE_CANCELL')?>",
			CHANGE_COEF: "<?=GetMessage('CHANGE_COEF')?>",
		};
	</script>
	<script src="<?=$this->GetFolder()."/menu.js"?>"></script>
<?endif?>

<?if($arResult["GRADE_HISTORY"]):?>
<div class="grade-history-container">
	<table class="grade-history">
		<caption class="webform-additional-fields"><h2><?=GetMessage('GRADE_HISTORY')?></h2></caption>
		<thead>
			<th class="bx-name-col"><?=GetMessage('GRADE_USER_NAME')?></th>
			<th class="bx-name-col"><?=GetMessage('GRADE_DATE_ACTIVE_FROM')?></th>
			<th class="bx-name-col"><?=GetMessage('GRADE_GRADE')?></th>
			<th class="bx-name-col"><?=GetMessage('GRADE_COEF')?></th>
		</thead>
		<tbody>
		<?foreach($arResult["GRADE_HISTORY"] as $grade_history):?>
			<tr>
				<td><?=$arResult["CURENT_USER"]["NAME"]." ".$arResult["CURENT_USER"]["LAST_NAME"]?></td>
				<td><?=$grade_history["PROPERTY_DATE_VALUE"]?></td>
				<td><?=$arResult["GRADES"][$grade_history["PROPERTY_GRADE_VALUE"]]["NAME"]?></td>
				<td class="<?if(empty($grade_history["PROPERTY_RATE_CHANGE_HOURS_VALUE"])){echo "gray";}?>"><?=$grade_history["PROPERTY_RATE_CHANGE_HOURS_VALUE"]?$grade_history["PROPERTY_RATE_CHANGE_HOURS_VALUE"]:$arResult["GRADES"][$grade_history["PROPERTY_GRADE_VALUE"]]["PROPERTY_RATE_CHANGE_HOURS_VALUE"]?></td>
			</tr>
		<?endforeach?>
		</tbody>
	</table>
</div>
<?endif?>
