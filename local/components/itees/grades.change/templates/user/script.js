BX.ready(function(){
    const ajax = window.ajaxTarget;
    let changeGrades = [];
    let changeHour = [];
    const canEdit = window.canEdit;
	const mess = window.mess;
    
    let buttonClass = "ui-btn-main";
    if(canEdit !== "Y")
    {
        buttonClass = "hidden";
    }
    $('.change_grade').each(function(){
        let id = $(this).data('user');
        
        changeGrades[id] = new BX.PopupWindow("change_grade_"+id, null,{
            content: BX('change_form_container_grade_'+id),
            closeIcon: {right: "20px", top: "10px"},
            titleBar: {content: BX.create("span", {html: mess.CHANGE_GRADE})},
            zIndex: 0,
            offsetLeft: 0,
            offsetTop: 0,
            draggable: {restrict: false},
            overlay: {},
            buttons: [
                    new BX.PopupWindowButton({
                        text: mess.GRADE_CHANGE_BUTTON,
                        className: buttonClass,
                        events: {click: function(){
                            if($(this.popupWindow.contentContainer).find('select[name="change_grade"]').val() === null)
                            {
                                alert(mess.GRADE_CHOOSE_GRADE);
                            }
                            else if($(this.popupWindow.contentContainer).find('input[name="change_hours"]').val() == "")
                            {
                                alert(mess.GRADE_CHOOSE_COEF);
                            }
                            else
                            {
                                $.ajax({
                                    method: "POST",
                                    url: ajax,
                                    data: $(this.popupWindow.contentContainer).find('form').serialize()
                                }).done(function(){
                                        location.reload();
                                });
                            }
                        }}
                }),
                new BX.PopupWindowButton({
                        text: mess.GRADE_CANCELL,
                        className: "webform-button-link-cancel",
                        events: {click: function(){
                                this.popupWindow.close();
                        }}
                }) 
            ]
        });
    });
    
    $('.change_hour').each(function(){
        let id = $(this).data('user');
        
        changeHour[id] = new BX.PopupWindow("change_hour_"+id, null,{
            content: BX('change_form_container_hour_'+id),
            closeIcon: {right: "20px", top: "10px"},
            titleBar: {content: BX.create("span", {html: mess.CHANGE_COEF})},
            zIndex: 0,
            offsetLeft: 0,
            offsetTop: 0,
            draggable: {restrict: false},
            overlay: {},
            buttons: [
                new BX.PopupWindowButton({
                    text: mess.GRADE_CHANGE_BUTTON,
                    className: buttonClass,
                    events: {click: function(){
                        if($(this.popupWindow.contentContainer).find('input[name="change_hours"]').val() == "")
                        {
                            alert(mess.GRADE_CHOOSE_COEF);
                        }
                        else
                        {
                             $.ajax({
                                method: "POST",
                                url: ajax,
                                data: $(this.popupWindow.contentContainer).find('form').serialize()
                            }).done(function(){
                                location.reload();
                            });
                        } 
                    }}
                }),
                new BX.PopupWindowButton({
                    text: mess.GRADE_CANCELL,
                    className: "webform-button-link-cancel",
                    events: {click: function(){
                            this.popupWindow.close();
                    }}
                }) 
            ]
        });
        
    });
    
    $('body').on('click', '.change_grade', function(){
		changeGrades[$(this).data('user')].show();
    });
    
    $('body').on('click', '.change_hour', function(){
		changeHour[$(this).data('user')].show();
    });
    
    $('body').on('click', '.jq-selectbox', function(){
	   $('.jq-selectbox__dropdown').width('');
	   $('.jq-selectbox__dropdown').css('top', '31px');
    });
    
    $('body').on("change", "select[name='change_grade']",(function(){
	   $(this).parents('form').find('input[name="change_hours"]').val($(this).find('option:selected').data("hours"));
    }));
   
});