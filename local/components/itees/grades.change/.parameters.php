<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$userGroups = array();
$db_groups = CGroup::GetList(($by="c_sort"), ($order="desc"));
while($arGroup = $db_groups->Fetch())
	$userGroups[$arGroup["ID"]] = $arGroup["NAME"];

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME" => "��� ���������",
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "",
			"REFRESH" => "Y",
		),
		"IBLOCK_GRADE_HISTORY_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "�������� ������� �������",
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '',
			"REFRESH" => "Y",
		),
		"IBLOCK_GRADE_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "�������� ������",
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '',
			"REFRESH" => "Y",
		),
		"CACHE_TIME" => Array("DEFAULT"=>3600),
		"CACHE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => "��������� ����� �������",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		)
	),
);
?>
