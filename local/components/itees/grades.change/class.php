<?
use Bitrix\Main,
	Bitrix\Iblock,
	Bitrix\Main\Localization\Loc as Loc;
	
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

Loc::loadMessages(__FILE__);
CJSCore::Init(array("jquery"));

if(!CModule::IncludeModule("iblock"))
	{
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

class ChangeGrades extends CBitrixComponent
{	
	public function executeComponent()
	{
		$this->getRigths();
		$this->getCurUser();
		$this->getGradesList();
		$this->changeGradeRequest();
		$this->getGradesHistory();
		$this->userSearch(1, 'search_user', 'user_id', 'user_selector');
		$this->setWorkTemplate();
	}
	
	protected function getRigths()
	{
		GLOBAL $USER;
		if($USER->isAdmin())
			$this->arResult["EDITE"] = "Y";
	}
	
	protected function getGradesList()
	{
		//�������� ������ �������
		$arSelect = Array("ID", "ACTIVE", "SORT", "NAME", "DETAIL_PAGE_URL", "CREATED_DATE", "PROPERTY_RATE_CHANGE_HOURS", "PROPERTY_RATE_SALARY");
		$arFilter = Array("IBLOCK_ID"=>IntVal($this->arParams["IBLOCK_GRADE_ID"]), "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array("active_from"=>"desc"), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$this->arResult["GRADES"][$arFields["ID"]] = $arFields;
		}
		
		foreach($this->arResult["GRADES"] as &$grade)
		{
			$res = CIBlockSection::GetByID($grade["IBLOCK_SECTION_ID"]);
			if($ar_res = $res->GetNext())
			   $grade["SECTION"] = $ar_res['NAME'];
		   
		    CModule::includeModule('ws.projectsettings');
			$salary = WS_PSettings::getFieldValue('BASE_SALARY');
			$grade["SALARY"] = $salary * $grade["PROPERTY_RATE_SALARY_VALUE"];
		}
	}
	
	protected function getCurUser()
	{
		//������������� �������� ��� ���������� ������������
		if(isset($_REQUEST["user_id"]) || isset($this->arParams["USER_ID"]))
		{
			$userId = isset($_REQUEST["user_id"]) ? intval($_REQUEST["user_id"]) : intval($this->arParams["USER_ID"]);
			$rsUser = CUser::GetByID($userId);
			$curUser = $rsUser->Fetch();
			$this->arResult["CURENT_USER"] = $curUser;
		}
	}
	
	protected function changeGradeRequest()
	{
		//������ �� ��������� ������ ������������
		if($_REQUEST["change_grade"] && $_REQUEST["user_id"])
		{
			GLOBAL $USER;
			if($USER->isAdmin())
			{
				if(!empty($_REQUEST["change_grade"]))
				{
					GLOBAL $DB,
					       $USER;
					$userId = intval($_REQUEST["user_id"]);
					$newGrade = intval($_REQUEST["change_grade"]);
						
						/*$user = new CUser;
						$fields = array("UF_GRADE_ID" => $newGrade);
						if ($user->Update($userId, $fields))
						{
							$this->arResult["MESSAGE"] = "����� ������������ �������.";
						}
						else
						{
							$this->arResult["MESSAGE"] = "�� ���������� �������� ����� ������������.";
						}*/
						
					$gradeEl = new CIBlockElement;
					$PROP = array();
					$PROP["USER"] = $userId;
					$PROP["GRADE"] = $newGrade;
					$PROP["RATE_CHANGE_HOURS"] = intval($_REQUEST["change_hours"]);
					$PROP["DATE"] = $_REQUEST["change_date"];
						
					$userName = $this->arResult["CURENT_USER"]["NAME"]." ".$this->arResult["CURENT_USER"]["LAST_NAME"];
					$gradeSection = GetIBlockSection($this->arResult["GRADES"][$newGrade]["IBLOCK_SECTION_ID"])["NAME"];
					$gradeHistorySectionArr = CIBlockSection::GetList(Array("sort"=>"asc"), Array("IBLOCK_ID"=>IntVal($this->arParams["IBLOCK_GRADE_HISTORY_ID"]), "NAME"=>$gradeSection), true);
					$gradeHistorySection = $gradeHistorySectionArr->GetNext();
					if(!$gradeHistorySection)
					{
						$tmpSec = new CIBlockSection;
						$gradeHistorySection["ID"] = $tmpSec->Add(Array(
							  "ACTIVE" => "Y",
							  "IBLOCK_ID" => $this->arParams["IBLOCK_GRADE_HISTORY_ID"],
							  "NAME" => $gradeSection
							  )
						);
					}
					
					$arAddGradeArray = Array(
						  "MODIFIED_BY"    => $USER->GetID(), // ������� ������� ������� �������������
						  "IBLOCK_SECTION_ID" => $gradeHistorySection["ID"],
						  "IBLOCK_ID"      => IntVal($this->arParams["IBLOCK_GRADE_HISTORY_ID"]),
						  "PROPERTY_VALUES"=> $PROP,
						  "NAME"           => $userName,
						  "ACTIVE"         => "Y"
					);
					if($gradeEl->Add($arAddGradeArray))
						$this->arResult["MESSAGE"] = "����� ������������ �������.";
					else
						$this->arResult["ERROR"] = $gradeEl->LAST_ERROR;
				}
				else
					$this->arResult["ERROR"] = "�� ������ �����";
			}
			else
				$this->arResult["ERROR"] = "� ��� ��� ���� ��� �������������� ������� � ������������� ����������� - ���������� � �������������� �������, ���� ��� ���������� ��� �������";
		}
	}
	
	protected function getGradesHistory()
	{
		//�������� ������ �� ������� �������
		if($this->arResult["CURENT_USER"]["ID"])
		{
			$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_USER", "PROPERTY_DATE", "PROPERTY_RATE_CHANGE_HOURS", "PROPERTY_GRADE");
			$arFilter = Array("IBLOCK_ID"=>IntVal($this->arParams["IBLOCK_GRADE_HISTORY_ID"]), "PROPERTY_USER"=>$this->arResult["CURENT_USER"]["ID"], "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array("PROPERTY_DATE"=>"desc", "created"=>"desc"), $arFilter, false, false, $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$this->arResult["GRADE_HISTORY"][] = $arFields;
			}
			foreach($this->arResult["GRADE_HISTORY"] as $cur)
			{
				if(MakeTimeStamp($cur["PROPERTY_DATE_VALUE"])<=time())
				{
					$this->arResult["CURENT_USER"]["GRADE"] = $cur["PROPERTY_GRADE_VALUE"];
					$this->arResult["CURENT_USER"]["GRADE_RATE"] = $cur["PROPERTY_RATE_CHANGE_HOURS_VALUE"];
					break;
				}
			}
			
		}
	}
	
	protected function userSearch($ID, $searchInputID, $dataInputID, $componentName, $siteID = '', $nameFormat = '', $delay = 0, array $options = null)
	{
            $searhHTML = '';
            CCrmComponentHelper::RegisterScriptLink('/bitrix/js/crm/common.js');

            if(!is_array($options))
            {
                $options = array();
            }

            $ID = strval($ID);
            $searchInputID = strval($searchInputID);
            $dataInputID = strval($dataInputID);
            $componentName = strval($componentName);

            $siteID = strval($siteID);
            if($siteID === '')
            {
                $siteID = SITE_ID;
            }

            $nameFormat = strval($nameFormat);
            if($nameFormat === '')
            {
                $nameFormat = CSite::GetNameFormat(false);
            }

            $delay = intval($delay);
            if($delay < 0)
            {
                $delay = 0;
            }

            if(!isset($options['RENDER_SEARCH_INPUT']) || $options['RENDER_SEARCH_INPUT'])
            {
                $searhHTML .= '<input type="text" id="'.htmlspecialcharsbx($searchInputID).'" style="width:200px;">';
            }

            if(!isset($options['RENDER_DATA_INPUT']) || $options['RENDER_DATA_INPUT'])
            {
                $searhHTML .= '<input type="hidden" id="'.htmlspecialcharsbx($dataInputID).'" name="'.htmlspecialcharsbx($dataInputID).'">';
            }

            $searhHTML .= '<script type="text/javascript">'.
                'BX.ready(function(){'.
                'BX.CrmUserSearchPopup.deletePopup("'.$ID.'");'.
                'BX.CrmUserSearchPopup.create("'.$ID.'", { searchInput: BX("'.CUtil::JSEscape($searchInputID).'"), dataInput: BX("'.CUtil::JSEscape($dataInputID).'"), componentName: "'.CUtil::JSEscape($componentName).'", user: {}}, '.$delay.');'.
                '});</script>';

            $GLOBALS['APPLICATION']->IncludeComponent(
                'itees:intranet.user.selector.new',
                '',
                array(
                    'MULTIPLE' => 'N',
                    'NAME' => $componentName,
                    'INPUT_NAME' => $searchInputID,
                    'SHOW_EXTRANET_USERS' => 'NONE',
                                    "GET_FULL_INFO" => "Y",
                    'POPUP' => 'Y',
                    'SITE_ID' => $siteID,
                    'NAME_TEMPLATE' => $nameFormat,
                                    'SHOW_INACTIVE_USERS' => 'Y'
                ),
                null,
                array('HIDE_ICONS' => 'Y')
            );
		
            $this->arResult["USER_SEARCH"] = $searhHTML;
	}
	
	protected function setWorkTemplate()
	{
		if(isset($this->arResult["CURENT_USER"]) && !isset($this->arParams["USER_ID"]))
		{
			GLOBAL $APPLICATION;
			$APPLICATION->RestartBuffer();
			$this->IncludeComponentTemplate("ajax");
			die();
		}else{
			$this->IncludeComponentTemplate();
		}
	}
}
?>
