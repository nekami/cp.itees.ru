<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

\CBitrixComponent::includeComponentClass('itees:ibstructure');

class IBtoJsonComponent extends \IBStructureComponent {
    const APP24_IB_ID = 584;
    const APPROLE_IB_ID = 586;
    const APPPLACEMENT_IB_ID = 585;
	
	static $app = false;
    static $role = false;

    /**
     * ������������� ������� ��� ��������� ��������
     */
    function formQuestionFilter($data) {
        try {
            $filter = [];

            if($data['POLLS_ID']) {
                $filter['IBLOCK_SECTION_ID'] = $data['POLLS_ID'];
            }

            $filter['IBLOCK_ID'] = $this->arParams['IBLOCK_ID'];
            $filter['ACTIVE'] = 'Y';

            return [
                'data' => $filter,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }
	
	/**
     * ������������� �������
     */
	static function init() {
		try {
			$request = Application::getInstance()->getContext()->getRequest();
			$arRequest = $request->getQueryList()->toArray();
			self::$app = $arRequest['app'];
			self::$role = $arRequest['role'];

            return [
                'data' => true,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
	}

    /**
     * ������������� ������� ��� ��������� �������
     */
    function formPollFilter() {
        try {
			$app = self::$app;
            $role = self::$role;
			
            if(!$app) {
                throw new Exception(Loc::getMessage('POLL_STRUCTURE.ERROR.NO_APP24_CODE'));
            }

            $requestFieldsConformity = [
                'app' => [
                    'FIELD' => 'UF_APP24',
                    'REQUEST_CONF' => 'app',
                    'IB_ID' => self::APP24_IB_ID,
                    'IB_VALUE_FIELD' => 'CODE',
                    'IB_SELECT_FIELDS' => ['ID'],
                ],
                'role' => [
                    'FIELD' => 'UF_APP_ROLE',
                    'REQUEST_CONF' => 'role',
                    'IB_ID' => self::APPROLE_IB_ID,
                    'IB_VALUE_FIELD' => 'PROPERTY_ROLE_ID',
                    'IB_SELECT_FIELDS' => ['ID'],
                ],
            ];

            $filter = [];
            $fieldData = $requestFieldsConformity['app'];
            $appData = self::getElements([
                'IBLOCK_ID' => $fieldData['IB_ID'],
                $fieldData['IB_VALUE_FIELD'] => $app
            ], $fieldData['IB_SELECT_FIELDS']);

            if($appData['error']) {
                throw new Exception($appData['error']);
            }

            $arAppData = reset($appData['data']);

            if(!$arAppData) {
                throw new Exception(Loc::getMessage('POLL_STRUCTURE.ERROR.NO_APP24_EXIST'));
            }

            $filter[$fieldData['FIELD']] = $arAppData['ID'];

            if($role) {
                $fieldData = $requestFieldsConformity['role'];
                $roleData = self::getElements([
                    'IBLOCK_ID' => $fieldData['IB_ID'],
                    $fieldData['IB_VALUE_FIELD'] => $role,
                    'PROPERTY_APP24' => $arAppData['ID']
                ], $fieldData['IB_SELECT_FIELDS']);

                if(!$roleData['error'] && count($roleData['data'])) {
                    $arRoleData = reset($roleData['data']);

                    $filter[$fieldData['FIELD']] = $arRoleData['ID'];
                }
            }

            $filter['IBLOCK_ID'] = $this->arParams['IBLOCK_ID'];
            $filter['ACTIVE'] = 'Y';

            return [
                'data' => $filter,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * ������������� �� ������
     * @param array $arr
     * @return bool
     */
    static function isAssoc(array $arr) {
        if ([] === $arr) {
            return false;
        }
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * ��������� ������������ ����� �������
     * @return array
     */
    private static function getQuestionFieldConformity() {
        return [
            'IBLOCK_SECTION_ID' => 'POLL_ID',
            'PREVIEW_TEXT' => 'QUESTION',
            'PREVIEW_TEXT_TYPE' => 'QUESTION_TYPE',
            'CODE' => 'CODE',
            'SORT' => 'PRIORITY',
            'PROPERTY_ANSWER_TYPE_VALUE' => 'ANSWER_TYPE',
            'PROPERTY_ANSWER_TYPE_VALUE_ID' => 'ANSWER_TYPE',
            'PROPERTY_ANSWERS_VALUE' => 'ANSWERS',
            'PROPERTY_ANSWERS_VALUE_ID' => 'ANSWERS',
        ];
    }

    /**
     * ��������� ������������ ����� ������
     * @return array
     */
    private static function getPollFieldConformity() {
        return [
            'CODE' => 'CODE',
            'UF_PRIORITY' => 'PRIORITY',
            // 'UF_APP24' => 'APP24',
            // 'UF_APP_ROLE' => 'APP_ROLE',
            'UF_NAME' => 'NAME',
            'UF_PLACEMENTS' => 'PLACEMENTS',
            'UF_SHOW_AFTER_CLOSE' => 'SHOW_AFTER_CLOSE',
        ];
    }

    /**
     * ������������ ������� ������
     * @param $sections
     * @return array
     */
    private static function formPollData($sections) {
        $result = [];
        $pollFieldConformity = self::getPollFieldConformity();
		$placementId = [];
		
        foreach($sections as $section) {
            foreach($section as $field => $value) {
                if($pollFieldConformity[$field]) {
                    $result[$section['ID']][$pollFieldConformity[$field]] = $value;
                }
            }
			
			if(is_array($result[$section['ID']]['PLACEMENTS'])) {
				$placementId = array_merge($placementId, $result[$section['ID']]['PLACEMENTS']);
			}
        }
		
		if(!empty($placementId)) {
			$placementData = self::getElements([
				'ID' => $placementId,
				'IBLOCK_ID' => self::APPPLACEMENT_IB_ID,
				'PROPERTY_APP24.CODE' => self::$app 
			], ['ID', 'CODE']);
			$placementCodes = [];

			if(!$placementData['error'] && $placementData['data']) {
				foreach($placementData['data'] as $placement) {
					$placementCodes[$placement['ID']] = $placement['CODE'];
				}
			}
			
			foreach($result as &$poll) {
				foreach($poll['PLACEMENTS'] as $index => $placementId) {
					if($placementCodes[$placementId]) {
						$poll['PLACEMENTS'][$index] = $placementCodes[$placementId];
					} else {
						unset($poll['PLACEMENTS'][$index]);
					}
				}
			}
		}

        return $result;
    }

    /**
     * ������������ ������� �������
     * @param $elements
     * @return array
     */
    private static function formQuestionData($elements) {
        $result = [];
        $questionFieldConformity = self::getQuestionFieldConformity();

        foreach($elements as $element) {
            foreach($element as $field => $value) {
                if($questionFieldConformity[$field]) {
                    if(preg_match('/^PROPERTY_(\w+?)/', $field)) {
                        if(preg_match('/^PROPERTY_(\w+?)_VALUE_ID$/', $field)) {
                            $prop = $questionFieldConformity[$field];

                            if(isset($result[$element['ID']][$prop])) {
                                if(!self::isAssoc($result[$element['ID']][$prop])) {
                                    $result[$element['ID']][$prop][] = [
                                        'ID' => $value,
                                        'VALUE' => $element['PROPERTY_' . $prop . '_VALUE']
                                    ];
                                } elseif($result[$element['ID']][$prop]['ID'] !== $value) {
                                    $result[$element['ID']][$prop] = [$result[$element['ID']][$prop], [
                                        'ID' => $value,
                                        'VALUE' => $element['PROPERTY_' . $prop . '_VALUE']
                                    ]];
                                }
                            } else {
                                $result[$element['ID']][$prop] = [
                                    'ID' => $value,
                                    'VALUE' => $element['PROPERTY_' . $prop . '_VALUE']
                                ];
                            }
                        }
                    } else {
                        $result[$element['ID']][$questionFieldConformity[$field]] = $value;
                    }
                }

            }
        }

        return $result;
    }

    /**
     * ��������� �������� ���������
     * @param $sections
     * @return $elements
     */
    static function formResult($sections, $elements) {
        try {
            $pollsData = self::formPollData($sections);
            $questionData = self::formQuestionData($elements);

            $result = [];

            foreach($questionData as &$question) {
                if($question['POLL_ID'] && $pollsData[$question['POLL_ID']]) {
                    $pollCode = $pollsData[$question['POLL_ID']]['CODE'];

                    if(!$result[$pollCode]) {
                        $result[$pollCode] = $pollsData[$question['POLL_ID']];
                    }

                    unset($question['POLL_ID']);
                    $result[$pollCode]['QUESTIONS'][$question['CODE']] = $question;
                }
            }

            return [
                'data' => $result,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    public function executeComponent() {
        try {
            if(!$this->arParams['IBLOCK_ID']) {
                throw new Exception(Loc::getMessage('POLL_STRUCTURE.ERROR.NO_IB_ID'));
            }
			
			$initRes = self::init();
			
			if($initRes['error']) {
                throw new Exception($initRes['error']);
            }
            
            $pollFilter = $this->formPollFilter();

            if($pollFilter['error']) {
                throw new Exception($pollFilter['error']);
            }

            $arSectionFilter = $pollFilter['data'];


            $arSectionFields = [
                'ID',
                'CODE',
                'UF_PRIORITY',
                'UF_APP24',
                'UF_APP_ROLE',
                'UF_PLACEMENTS',
                'UF_NAME',
                'UF_SHOW_AFTER_CLOSE',
            ];

            $arSectionSelect = $arSectionFields;
            $sections = self::getSectionsTree($arSectionFilter, $arSectionSelect);

            if($sections['error']) {
                throw new Exception($sections['error']);
            }

            $sections = $sections['data'];
			
            $questionFilter = $this->formQuestionFilter(['POLLS_ID' => array_map(function($section) {return $section['ID'];}, $sections)]);

            if($questionFilter['error']) {
                throw new Exception($questionFilter['error']);
            }

            $arFilter = $questionFilter['data'];

            $arFields = [
                'ID',
                'CODE',
                'SORT',
                'IBLOCK_SECTION_ID',
                'PREVIEW_TEXT',
            ];
            $arProperties = [
                'ANSWER_TYPE',
                'ANSWERS'
            ];
            
            $arSectionSelect = array_merge($arFields, array_map(function($property) {return 'PROPERTY_' . $property;}, $arProperties));
            
            $elements = self::getElements($arFilter, $arSectionSelect);

            if($elements['error']) {
                throw new Exception($elements['error']);
            }

            $elements = $elements['data'];

            $result = self::formResult($sections, $elements);
			
			echo '<pre>';
			print_r($result);

            if($result['error']) {
                throw new Exception($result['error']);
            }

            $this->arResult = $result['data'];

            $this->IncludeComponentTemplate();
        } catch(\Exception $e) {
            ShowError($e->getMessage());
            return false;
        }
    }
}