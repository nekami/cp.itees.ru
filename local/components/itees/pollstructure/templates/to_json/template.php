<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if(ob_get_level()) {
	ob_end_clean();
}

$json = \Bitrix\Main\Web\Json::encode($arResult);

if($errorCode = json_last_error()) {
	echo "{\"error\": {\"code\": $errorCode, \"msg\": \"Error($errorCode)\"}, \"data\": false}";
} else {
	echo "{\"error\": false, \"data\": $json}";
}

exit;