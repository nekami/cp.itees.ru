var defColor = '#f6f6f6';
var game = new Game();

function Block(type) {
	var CX = 4;
	var CY = 1;
	var angle = 0;
	var c;
	
	switch(type) {
		case 0:
			c = ['green'];
			this.form = [c, c, c, c];
			break;
		case 1:
			c = ['CornflowerBlue'];
			this.form = [[],[c, c], c, c];
			break;
		case 2:
			c = ['Gold'];
			this.form = [c, c, [c, c]];
			break;
		case 3:
			c = ['Firebrick'];
			this.form = [[], [c, c], [c, c]];
			break;
		case 4:
			c = ['gray'];
			this.form = [[], c, [c, c], [, c]];
			break;
		case 5:
			c = ['purple'];
			this.form = [[, c], [c, c], c];
			break;
		case 6:
			c = ['tan'];
			this.form = [c, [c, c], c];
			break;
	}
	
	this.show = function() {
		var xr, yr, cell;
		
		for(var x in this.form) {
			for(var y in this.form[x]) {
				xr = GetXR(angle, x, y);
				yr = GetYR(angle, x, y);
				cell = document.getElementById((+xr+CX)+'_'+(+yr+CY));
				if(cell == null || cell.getAttribute('data') == '1') {
					return false;
				}
			}
		}
		
		for(var x in this.form) {
			for(var y in this.form[x]) {
				xr = GetXR(angle, x, y);
				yr = GetYR(angle, x, y);
				cell = document.getElementById((+xr+CX)+'_'+(+yr+CY));
				cell.setAttribute('data', '1');
				cell.style.background = this.form[x][y];
			}
		}
		
		return true;
	}
	
	this.showInNextBlock = function() {
		var cell;
		for(var x in this.form) {
			for(var y in this.form[x]) {
				cell = document.getElementById('next_'+(+x+1)+'_'+(+y+1));
				cell.style.background = this.form[x][y];
			}
		}
	}
	
	this.hide = function() {
		var xr, yr;
		for(var x in this.form) {
			for(var y in this.form[x]) {
				xr = GetXR(angle, x, y);
				yr = GetYR(angle, x, y);
				
				cell = document.getElementById((+xr+CX)+'_'+(+yr+CY));
				
				if(cell != null) {
					cell.setAttribute('data', '0');
					cell.style.background = defColor;
				}
			}
		}
		return true;
	}
	
	this.moveDown = function() {
		this.hide();
		CY++;
		if(!this.show()) {
			CY--;
			this.show();
			return false;
		}
		return true;
	}
	
	this.moveLeft = function() {
		this.hide();
		CX--;
		if(!this.show()) {
			CX++;
			this.show();
			return false;
		}
		return true;
	}
	
	this.moveRight = function() {
		this.hide();
		CX++;
		if(!this.show()) {
			CX--;
			this.show();
			return false;
		}
		return true;
	}
	
	this.rotate = function() {
		var tmp = angle;
		this.hide();
		switch(angle) {
			case 0:
				angle = 90;
				break;
			case 90:
				angle = 180;
				break;
			case 180:
				angle = 270;
				break;
			case 270:
				angle = 0;
				break;
			default:
				return false;
		}
		if(!this.show()) {
			angle = tmp;
			this.show();
			return false;
		}
		return true;
	}
	
	function GetXR(_angle, x, y) {
		switch(_angle) {
			case 90:
				return 2-y;
			case 180:
				return 3-x;
			case 270:
				return +y+1;
			default:
				return +x;
		}
	}
	
	function GetYR(_angle, x, y) {
		switch(_angle) {
			case 90:
				return +x-1;
			case 180:
				return 1-y;
			case 270:
				return 2-x;
			default:
				return +y;
		}
	}
}

function Game() {
	var level = 1;
	var score = 0;
	var bStarted = false;
	var bPause = false;
	var block;
	var nextBlock;
	var timeInterval;
	var audio;
	var lineSound;
	var letsPlaySound;
	var gameOverSound;
	
	this.start = function() {
		if(bStarted)
			return false;
			
		var _this = this;
		document.getElementById('game-over-message').style.display = 'none';
		bStarted = true;
		level = 1;
		score = 0;
		audio = document.getElementById('music-sound');
		audio.currentTime = 0;
		audio.play();
		lineSound = document.getElementById('drop-sound');
		letsPlaySound = document.getElementById('lets-play-sound');
		gameOverSound = document.getElementById('game-over-sound');
		document.getElementById('start').parentNode.style.display = 'none';
		document.getElementById('stop').parentNode.style.display = 'block';
		document.getElementById('pause').parentNode.style.display = 'block';
		clearField();
		setLevel(level);
		showScore(score);
		block = getNextBlock();
		window.setTimeout(autoMoveBlock, timeInterval);
		window.onkeydown = checkKeys;
		
		function autoMoveBlock() {
			if(!bPause) {
				if(!bStarted)
					return false;
				
				if(!block.moveDown()) {
					checkLines();
					block = getNextBlock();
					if(!block) {
						audio.currentTime = 0;
						audio.pause();
						gameOverSound.play();
						document.getElementById('game-over-message').style.display = 'block';
						bStarted = false;
						bPause = false;
						document.getElementById('start').parentNode.style.display = 'block';
						document.getElementById('stop').parentNode.style.display = 'none';
						document.getElementById('pause').innerHTML = 'Pause';
						document.getElementById('pause').parentNode.style.display = 'none';
						return false;
					}
				}
			}
			window.setTimeout(autoMoveBlock, timeInterval);
		}
		
		function getNextBlock() {
			var blockType = getRandomInt(0, 6);
			var tmp;
			if(typeof(nextBlock) == 'undefined') {
				tmp = new Block(blockType);
				blockType = getRandomInt(0, 6);
			}
			else {
				tmp = nextBlock;
			}
			nextBlock = new Block(blockType);
			clearNextBlockField();
			nextBlock.showInNextBlock();
			if(tmp.show())
				return tmp;
			else
				return false;
		}
		
		function checkLines() {
			var count, cell;
			for(var y = 1; y < 19; y++) {
				for(var x = 1; x < 11; x++) {
					if(x == 1)
						count = 0;
					
					cell = document.getElementById(x + '_' + y);
					if(cell.getAttribute('data') == '1')
						count++;
						
					if(count == 10) {
						lineSound.play();
						score += 1;
						var tmpLvl = level;
						level = Math.ceil(score/10);
						if(level > tmpLvl) {
							setLevel(level);
							changeMusic(level);
						}
						showScore(score);
						clearLine(y);
					}
				}
			}
			
			function changeMusic(_level) {
				audio.pause();
				letsPlaySound.play();
				audio = document.createElement('audio');
				audio.setAttribute('loop', 'loop');
				audio.innerHTML = '<source src="/bitrix/components/itees/tetris/templates/.default/sound/basshunter_tetris.ogg" type="audio/ogg"></source><source src="/bitrix/components/itees/tetris/templates/.default/sound/basshunter_tetris.mp3" type="audio/mp3"></source>';
				window.setTimeout(musicPlay, 2000);
				
				function musicPlay() {
					audio.play();
				}
			}
			
			function clearLine(lineNumber) {
				var data, color, topCell, bottomCell;
				for(var y = lineNumber; y > 1; y--) {
					for(var x = 1; x < 11; x++) {
						topCell = document.getElementById(x + '_' + (+y-1));
						bottomCell = document.getElementById(x + '_' + y);
						
						data = topCell.getAttribute('data');
						color = topCell.style.background;
						
						bottomCell.setAttribute('data', data);
						bottomCell.style.background = color;
					}
				}
			}
		}
		
		function setLevel(_level) {
			timeInterval = 100 + 1000/(_level + 1);
			showLevel(_level);
		}
		
		function showScore(_score) {
			document.getElementById('score').innerHTML = _score;
		}
		
		function showLevel(_level) {
			document.getElementById('level').innerHTML = _level;
			
			if(_level > 1) {
				var tetrisDiv = document.getElementById('tetris-game');
				var newLevelDiv = document.createElement('div');
				newLevelDiv.setAttribute('class', 'new-level-message');
				newLevelDiv.innerHTML = 'Level ' + _level;
				tetrisDiv.insertBefore(newLevelDiv, tetrisDiv.firstChild);
				newLevelDiv.style.display = 'block';
				window.setTimeout(function() {blink(10, 50)}, 500);
				bPause = true;
			}
			
			function blink(count, time) {
				newLevelDiv.style.display = 'block';
				window.setTimeout(hide, time);
				if(count > 0) {
					window.setTimeout(function() {blink(count - 1, time)}, time*2);
				}
				else {
				bPause = false;
					newLevelDiv.parentNode.removeChild(newLevelDiv);
				}
				
				function hide() {
					newLevelDiv.style.display = 'none';
				}
			}
		}
		
		function checkKeys(e) {
			var KeyID = e.keyCode;
			switch(KeyID) {
				case 32:
					_this.pause();
					e.preventDefault();
					break;
				case 37:
					if(block != null && !bPause)
						block.moveLeft();
						e.preventDefault();
					break;
				case 38:
					if(block != null && !bPause)
						block.rotate();
						e.preventDefault();
					break;
				case 39:
					if(block != null && !bPause)
						block.moveRight();
						e.preventDefault();
					break;
				case 40:
					if(block != null && !bPause)
						block.moveDown();
						e.preventDefault();
					break;
			}
			//return false;
		}
	}
	
	this.stop = function() {
		level = 1;
		document.getElementById('level').innerHTML = level;
		score = 0;
		document.getElementById('score').innerHTML = score;
		bStarted = false;
		bPause = false;
		block = null;
		document.getElementById('start').parentNode.style.display = 'block';
		document.getElementById('stop').parentNode.style.display = 'none';
		document.getElementById('pause').innerHTML = 'Pause';
		document.getElementById('pause').parentNode.style.display = 'none';
		audio.currentTime = 0;
		audio.pause();
		clearField();
		clearNextBlockField();
	}
	
	this.pause = function() {
		if(!bPause) {
			bPause = true;
			document.getElementById('pause').innerHTML = 'Resume';
			audio.pause();
		}
		else {
			document.getElementById('pause').innerHTML = 'Pause';
			bPause = false;
			audio.play();
		}
	}
	
	function clearField() {
		for(var y = 1; y < 19; y++) {
			for(var x = 1; x < 11; x++) {
				cell = document.getElementById(x + '_' + y);
				cell.style.background = defColor;
				cell.setAttribute('data', '0');
			}
		}
	}
	
	function clearNextBlockField() {
		for(var y = 1; y < 3; y++) {
			for(var x = 1; x < 5; x++) {
				cell = document.getElementById('next_' + x + '_' + y);
				cell.style.background = defColor;
			}
		}
	}
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}