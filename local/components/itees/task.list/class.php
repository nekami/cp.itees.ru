<?
use Bitrix\Main\Entity;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Tasks\TaskTable;

class CTaskListComponent extends CBitrixComponent
{
    private $pagerLimit = 20;
    private $navClients;
    
    private function setNavClients()
    {
        $this->navClients = new PageNavigation("nav-tasks");
        $this->navClients->allowAllRecords(true)
            ->setPageSize($this->pagerLimit)
            ->initFromUri();
    }
    
    private function getResult()
    {
        $this->setNavClients();
        
        $rsTasks = TaskTable::getList(array(
            'order' => array(
                'COMPANY.TITLE' => 'ASC',
                //'DEADLINE_YEAR' => 'DESC',
                //'DEADLINE_MONTH' => 'DESC',
            ),
            'select' => array(
                'CLIENT_TITLE' => 'COMPANY.TITLE',
                //'DEADLINE_YEAR',
                //'DEADLINE_MONTH',
                new Entity\ExpressionField('TASK_COUNT', 'COUNT(*)')
            ),
            'group' => array('COMPANY.TITLE'),
            'offset' => $this->navClients->getOffset(),
            'limit' => $this->navClients->getLimit(),
            'runtime' => array(
                new Entity\ReferenceField(
                    'COMPANY',
                    'Bitrix\Crm\CompanyTable',
                    array('=this.UF_CLIENT' => 'ref.ID'),
                    array('join_type' => 'LEFT')
                ),
                //new Entity\ExpressionField('DEADLINE_YEAR', 'YEAR(DEADLINE)', array('DEADLINE')),
                //new Entity\ExpressionField('DEADLINE_MONTH', 'MONTH(DEADLINE)', array('DEADLINE'))
            )
        ));
        
        while ($arTaskGroup = $rsTasks->fetch())
        {
            if(empty($arTaskGroup['CLIENT_TITLE']))
            {
                $arTaskGroup['CLIENT_TITLE'] = '������ �� ���������';
            }
            
            $arTaskGroups[] = $arTaskGroup;
        }
        
        $arResult['CLIENTS'] = $arTaskGroups;
        
        return $arResult;
    }
    
    public function executeComponent()
    {
        if ($this->startResultCache())
        {
            $this->arResult = $this->getResult();
            $this->includeComponentTemplate();
        }
    }
};