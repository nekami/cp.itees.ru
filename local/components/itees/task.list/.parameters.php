<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$arComponentParameters = Array(
	'GROUPS' => Array(
		'OVERALL' => Array(
			'NAME' => GetMessage('COMMON_SETTINGS'),
		),
	),
	'PARAMETERS' => Array(
		'CELL_SIZE' => Array(
			'PARENT' => 'OVERALL',
			'NAME' => GetMessage('CELL_SIZE'),
			'TYPE' => 'STRING',
			'DEFAULT' => '30',
		),
	),
);
?>
