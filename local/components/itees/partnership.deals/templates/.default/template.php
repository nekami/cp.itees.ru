<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addString("<link href='//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'/>");
Asset::getInstance()->addString("<script type='text/javascript' src='//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js'></script>");
Asset::getInstance()->addString("<script type='text/javascript' src='//cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js'></script>");
Asset::getInstance()->addString("<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>");
Asset::getInstance()->addString("<script type='text/javascript' src='//cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js'></script>");

$index = 1;
$categories = [
	8 => '����������24',
	9 => '�����24'
];

echo '<table id="partners-table">
	<thead>
		<tr>
			<th>#</th>
			<th>ID</th>
			<th>���������</th>
			<th>�����</th>
			<th>�����������</th>
			<th>������</th>
			<th>��������</th>
		</tr>
	</thead>
	<tbody>
';

foreach($arResult['DEALS'] as $deals) {
	foreach($deals as $deal) {
		echo sprintf("<tr class='js-deal'>
			<td>${index}</td>
			<td>${deal['ID']}</td>
			<td>${deal['TITLE']}</td>
			<td>${deal['OPPORTUNITY']}</td>
			<td>${categories[$deal['CATEGORY_ID']]}</td>
			<td><a target='_blank' href='/partnership/?partner=${arResult['PARTNER']}&client=${deal[$component::B24_PORTAL_FIELD]}'>${deal[$component::B24_PORTAL_FIELD]}</a></td>
			<td>%s</td>
		</tr>", $deal['CATEGORY_ID'] == $component::B24_O24_CATEGORY ? 
		"<button data-deal='${deal['ID']}' data-partner='${arResult['PARTNER']}' class='js-delete-client'>�������</button>" : 
		'');
		
		$index++;
	}
}

echo '</tbody>
</table>';
?>
<script>
$(document).ready(function () {
	let $partnersTable = $('#partners-table').DataTable({
		dom: 'Bfrtip',
		buttons: [{
			extend: 'excel',
			text: '������� � excel',
			exportOptions: {
				columns: [0,1,2,3,4,5]
			}
		}]
	});
   
	$('#partners-table').on('click', '.js-delete-client', function() {
		if(!confirm('�� �������?')) {
			return;
		}
	   
		BX.ajax.runComponentAction('itees:partnership.deals', 'deleteClient', {
			mode: 'class',
			data: {
				partnerId: $(this).data('partner'),
				dealId: $(this).data('deal'),
			},
		})
		.then((response) => {
			if (response.status === 'success') {
				location.reload();
			}
		});
	});
});
</script>