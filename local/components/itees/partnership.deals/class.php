<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

Loader::includeModule("iblock");
Loader::includeModule("crm");

class PartnershipDeals extends \CBitrixComponent implements Controllerable
{
	const PARTNERS_IB_ID = 607;
	const B24_PORTAL_FIELD = 'UF_CRM_1505301771';
	const B24_APP_FIELD = 'UF_CRM_1505484613';
	const B24_O24_CATEGORY = 9;
	const B24_APP24_CATEGORY = 8;
	const APPS = [
		'itees.salescriptpro'
	];
	
	protected $partner = null;
	protected $client = null;
	
    // ������������ �����
    public function configureActions() {
       // ���������� ������� ��-��������� (ActionFilter\Authentication � ActionFilter\HttpMethod)
       // ����������������� ������� ��������� � ����� /bitrix/modules/main/lib/engine/actionfilter/
	   
        return [
            'deleteClient' => [ // Ajax-�����
                'prefilters' => [],
            ],
        ];
    }

    // Ajax-������ ������ ���� � ���������� Action
    public function deleteClientAction($partnerId, $dealId) {
		if(!$partnerId || !$dealId) {
            return false;
		}
		
		$partnerDataRes = $this->getPartnerData($partnerId);
		
		if($partnerDataRes['error']) {
			return false;
		}
		
		$partnerData = $partnerDataRes['data'];
		$wrongDeals = $partnerData['WRONG_DEALS'];
		
		array_push($wrongDeals, $dealId);
		
		$this->updateWrongDeals($partnerId, $wrongDeals);
    }
	
	protected function updateWrongDeals($partnerId, $wrongDeals) {
		try {
			(new CIBlockElement)->Update($partnerId, ['DETAIL_TEXT' => serialize($wrongDeals)]);

            return [
                'data' => true,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
	}
	
	protected function init() {
		try {
			$request = Application::getInstance()->getContext()->getRequest();
			$arRequest = $request->getQueryList()->toArray();
			$this->partner = $arRequest['partner'];
			$this->client = $arRequest['client'];
			
			if(!$this->partner) 
				throw new Exception('�� ���������� ������ ��� ���������� �������!');

            return [
                'data' => true,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
	}
	
	protected function getPartnerData($partnerId) {
		try {
			$arFilter = ['IBLOCK_ID' => self::PARTNERS_IB_ID, 'ID' => $partnerId];
			$arSelect = ['PROPERTY_CLIENTS', 'DETAIL_TEXT'];
			$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
			$clients = [];
			$wrongDeals = [];
			
			while($client = $res->Fetch()) {
				$wrongDeals = $client['DETAIL_TEXT'] ? unserialize($client['DETAIL_TEXT']) : [];
				array_push($clients, $client['PROPERTY_CLIENTS_VALUE']);
			}
			
            return [
                'data' => [
					'CLIENTS' => $clients, 
					'WRONG_DEALS' => $wrongDeals
				],
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
	}
	
	protected function getClientDeals($clients, $wrongDeals) {
		try {
			$result = \Bitrix\Crm\DealTable::getList([
				'order' => ['ID' => 'DESC'],
				'filter' => [
					'CATEGORY_ID' => [self::B24_APP24_CATEGORY, self::B24_O24_CATEGORY], 
					self::B24_PORTAL_FIELD => $clients,
					self::B24_APP_FIELD => self::APPS,
					'!ID' => $wrongDeals
				],
				'select' => ['ID', 'TITLE', 'OPPORTUNITY', 'CATEGORY_ID', self::B24_PORTAL_FIELD]
			]);
			$deals = [];
			
			while($deal = $result->fetch()) {
				array_push($deals, $deal);
			}
			
            return [
                'data' => $deals,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
	}
	
	protected function formDeals($deals) {
		try {
			$result = [];
			
			foreach($deals as $deal) {
				$result[$deal[self::B24_PORTAL_FIELD]] ?? ($result[$deal[self::B24_PORTAL_FIELD]] = []);
				
				array_push($result[$deal[self::B24_PORTAL_FIELD]], $deal);
			}
			
            return [
                'data' => $result,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
	}

    public function executeComponent() {
		$initRes = $this->init();
		
		if($initRes['error']) {
			ShowError($initRes['error']);
            return false;
		}

		$partnerDataRes = $this->getPartnerData($this->partner);
		
		if($partnerDataRes['error']) {
			ShowError($partnerDataRes['error']);
			return false;
		}
		
		$partnerData = $partnerDataRes['data'];
		
		$clients = [$this->client];
		
		if(!$this->client) {
			$clients = $partnerData['CLIENTS'];
		}
		
		$dealsRes = $this->getClientDeals($clients, $partnerData['WRONG_DEALS']);
		
		if($dealsRes['error']) {
			ShowError($dealsRes['error']);
            return false;
		}
		
		$deals = $dealsRes['data'];
		$deals = $this->formDeals($deals);
		
		if($deals['error']) {
			ShowError($deals['error']);
            return false;
		}
		
		$this->arResult['DEALS'] = $deals['data'];
		$this->arResult['PARTNER'] = $this->partner;
        $this->includeComponentTemplate();
    }
}