<?
define('STOP_STATISTICS',    true);
define('NO_AGENT_CHECK',     true);
define('PUBLIC_AJAX_MODE', true);
define('DisableEventsCheck', true);

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
require_once 'class.php';

if($_REQUEST["ajax"] == "y" && isset($_REQUEST["newDefault"]))
{
	SupportOptionsButton::setNewDefault($_REQUEST["newDefault"]);
}

if($_REQUEST["ajax"] == "y" && isset($_REQUEST["statusId"]) && $_REQUEST["action"] == "status")
{
	SupportOptionsButton::setFinalStatus($_REQUEST["statusId"]);
}
	