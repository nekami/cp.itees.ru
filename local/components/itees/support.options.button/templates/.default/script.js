;(function(window){
// Calbacks for destination
   window.BXEvDestSetLinkName = function(name)
    {
        name = name.split('_');
        name = name[name.length - 1];
            if (BX.SocNetLogDestination.getSelectedCount(name) <= 0)
                    BX('feed-event-dest-add-link-'+name, 1).innerHTML = "�������� ������";
            else
                    BX('feed-event-dest-add-link-'+name, 1).innerHTML = "�������� ���";
    };
console.log(BX.SocNetLogDestination);
    window.BXEvDestSelectCallback = function(item, type, search)
    {
        var formName = arguments[4];
        var group = formName.split('_');
        group = group[group.length-1];
            var
                    type1 = type,
                    prefix = 'S';

            if (type == 'sonetgroups')
                    prefix = 'SG';
            else if (type == 'groups')
            {
                    prefix = 'UA';
                    type1 = 'all-users';
            }
            else if (type == 'users')
                    prefix = 'U';
            else if (type == 'department')
                    prefix = 'DR';

            BX('feed-event-dest-item-'+group).appendChild(
                    BX.create("span", { attrs : { 'data-id' : item.id }, props : { className : "feed-event-destination feed-event-destination-"+type1 }, children: [
                            BX.create("input", { attrs : { 'type' : 'hidden', 'name' : 'EVENT_PERM[' + prefix + '][]', 'value' : item.id }}),
                            BX.create("span", { props : { 'className' : "feed-event-destination-text" }, html : item.name}),
                            BX.create("span", { props : { 'className' : "feed-event-del-but"}, events : {'click' : function(e){BX.SocNetLogDestination.deleteItem(item.id, type, formName);BX.PreventDefault(e)}, 'mouseover' : function(){BX.addClass(this.parentNode, 'feed-event-destination-hover')}, 'mouseout' : function(){BX.removeClass(this.parentNode, 'feed-event-destination-hover')}}})
                    ]})
            );

            BX.onCustomEvent('OnDestinationLivefeedChanged', [item]);
            BX('feed-event-dest-input-'+group).value = '';
            BXEvDestSetLinkName(formName);
    };

    // remove block
    window.BXEvDestUnSelectCallback = function(item, type, search)
    {
        var formName = arguments[3];
        var group = formName.split('_');
        group = group[group.length-1];
            var elements = BX.findChildren(BX('feed-event-dest-item-'+group), {attribute: {'data-id': ''+item.id+''}}, true);
            if (elements != null)
            {
                    for (var j = 0; j < elements.length; j++)
                            BX.remove(elements[j]);
            }
            BX('feed-event-dest-input-'+group).value = '';
            BX.onCustomEvent('OnDestinationLivefeedChanged');
            BXEvDestSetLinkName(formName);
        console.log(BX.SocNetLogDestination);
    };
    window.BXEvDestOpenDialogCallback = function()
    {
        var formName = arguments[0];
        var group = formName.split('_');
        group = group[group.length-1];
            BX.style(BX('feed-event-dest-input-box-'+group), 'display', 'inline-block');
            BX.style(BX('feed-event-dest-add-link-'+group), 'display', 'none');
            BX.focus(BX('feed-event-dest-input-'+group));
    };

    window.BXEvDestCloseDialogCallback = function()
    {
        var formName = arguments[0];
        var group = formName.split('_');
        group = group[group.length-1];
            if (!BX.SocNetLogDestination.isOpenSearch() && BX('feed-event-dest-input-'+group).value.length <= 0)
            {
                    BX.style(BX('feed-event-dest-input-box-'+group), 'display', 'none');
                    BX.style(BX('feed-event-dest-add-link-'+group), 'display', 'inline-block');
                    BXEvDestDisableBackspace();
            }
    };

    window.BXEvDestCloseSearchCallback = function()
    {
        var formName = arguments[0];
        var group = formName.split('_');
        group = group[group.length-1];
            if (!BX.SocNetLogDestination.isOpenSearch() && BX('feed-event-dest-input-'+group).value.length > 0)
            {
                    BX.style(BX('feed-event-dest-input-box-'+group), 'display', 'none');
                    BX.style(BX('feed-event-dest-add-link-'+group), 'display', 'inline-block');
                    BX('feed-event-dest-input-'+group).value = '';
                    BXEvDestDisableBackspace();
            }

    };
    window.BXEvDestDisableBackspace = function()
    {
            if (BX.SocNetLogDestination.backspaceDisable || BX.SocNetLogDestination.backspaceDisable != null)
                    BX.unbind(window, 'keydown', BX.SocNetLogDestination.backspaceDisable);

            BX.bind(window, 'keydown', BX.SocNetLogDestination.backspaceDisable = function(e)
            {
                    if (e.keyCode == 8)
                    {
                            BX.PreventDefault(e);
                            return false;
                    }
            });
            setTimeout(function()
            {
                    BX.unbind(window, 'keydown', BX.SocNetLogDestination.backspaceDisable);
                    BX.SocNetLogDestination.backspaceDisable = null;
            }, 5000);
    };

    window.BXEvDestSearchBefore = function(event)
    {console.log(arguments);
            return BX.SocNetLogDestination.searchBeforeHandler(event, {
                    formName: 'support_groups_diag',
                    inputId: 'feed-event-dest-input-diag'
            });
    };
    window.BXEvDestSearch = function(event)
    {console.log(arguments);
            return BX.SocNetLogDestination.searchHandler(event, {
                    formName: "support_groups_diag",
                    inputId: 'feed-event-dest-input-diag',
                    linkId: 'feed-event-dest-add-link-diag',
                    sendAjax: true
            });
    };
})(window);

