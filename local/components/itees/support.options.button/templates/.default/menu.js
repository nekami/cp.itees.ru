BX.ready(function(){
    var menuItemsOptions = [];
    var supUsers = [];
    var arResult = window.arResult;
    arResult["SUPPORT_USERS"].forEach(function(item, i){
            var className = "";
            if(item["ID"] === arResult["CUR_DEAFULT"]){
                    className = 'menu-popup-item-accept';
            } else {
                    className = 'menu-popup-item-none';
            };
            supUsers[item["ID"]] ={
                    text: item["LAST_NAME"]+" "+item["NAME"],
                    value: item["ID"],
                    className: className,
                    onclick: function(event, menuItem)
                    {
                            var post = {};
                            post['newDefault'] = menuItem.value;
                            post['ajax'] = 'y';
                            BX.ajax.post(
                                    arResult["SCRIPT_PATH"] + "/ajax.php",
                                    post,
                                    function (){
                                            menuItem.menuWindow.menuItems.forEach(function(item, i){
                                                    if(BX.hasClass(item.layout.item, "menu-popup-item-accept"))
                                                    {
                                                            BX.removeClass(item.layout.item, "menu-popup-item-accept");
                                                            BX.addClass(item.layout.item, "menu-popup-item-none");
                                                    }
                                            });
                                            BX.addClass(menuItem.layout.item, "menu-popup-item-accept");
                                            BX.removeClass(menuItem.layout.item, "menu-popup-item-none");
                                    }
                            );
                    }
            }
    });
    menuItemsOptions.push({
            tabId: "popupMenuOptionsDef",
            text: BX.message('supButText'),
            className: "menu-popup-item-none",
            onclick: function(event, item)
            {
            },
            items: supUsers,

    });

    menuItemsOptions.push({
            tabId: "popupMenuOptions",
            delimiter: true
    });
    
    //Категории обращений
    var cats = [];
    arResult["CATEGORYES"].forEach(function(item, i){
        cats[item["ID"]] = {
            text: item["NAME"],
            value: item["ID"],
            className: 'menu-popup-item-none',
            onclick: function(event, menuItem)
            {
                window.open('/bitrix/admin/ticket_dict_edit.php?find_type=C&ID='+item["ID"]+'&lang=ru');
            }
        };
    });

    menuItemsOptions.push({
            tabId: "popupMenuOptionsCat",
            text: BX.message('catButText'),
            className: "menu-popup-item-none",
            onclick: function(event, item)
            {
                window.open('/bitrix/admin/ticket_dict_list.php?lang=ru&find_type=C');
            },
            items: cats,
    });

    menuItemsOptions.push({
            tabId: "popupMenuOptions",
            delimiter: true
    });

    var status = [];
    arResult["STATUSES"].forEach(function(item, i){
            var className = "";
            if(item["IS_FINAL"] == 1){
                    className = 'menu-popup-item-accept';
            } else {
                    className = 'menu-popup-item-none';
            };
            status[i] = {
                    text: item["NAME"],
                    value: item["ID"],
                    className: className,
                    onclick: function(event, item)
                    {
                            var post = {};
                            post['statusId'] = item.value;
                            post['ajax'] = 'y';
                            post['action'] = 'status';
                            BX.ajax.post(
                                    arResult["SCRIPT_PATH"] + "/ajax.php",
                                    post,
                                    function (){
                                            if (BX.hasClass(item.layout.item, "menu-popup-item-accept"))
                                            {
                                                    BX.removeClass(item.layout.item, "menu-popup-item-accept");
                                                    BX.addClass(item.layout.item, "menu-popup-item-none");
                                            }
                                            else
                                            {
                                                    BX.addClass(item.layout.item, "menu-popup-item-accept");
                                                    BX.removeClass(item.layout.item, "menu-popup-item-none");
                                            }
                                    }
                            );
                    }
            };
    });
    menuItemsOptions.push({
            tabId: "popupMenuOptionsStatus",
            text: BX.message('statButText'),
            className: "menu-popup-item-none",
            onclick: function(event, item)
            {
            },
            items: status,
    });

    var buttonRect = BX("support-popupMenuOptions").getBoundingClientRect();
    var menu = BX.PopupMenu.create(
            "popupMenuOptions",
            BX("support-popupMenuOptions"),
            menuItemsOptions,
            {
                    closeByEsc: true,
                    offsetLeft: buttonRect.width / 2,
                    angle: true
            }
    );

    BX.bind(BX("support-popupMenuOptions"), "click", BX.delegate(function()
    {
            if (BX.data(BX("support-popupMenuOptions"), "disabled") !== true)
            {
                    menu.popupWindow.show();
            }
    }, this));
    menu.popupWindow.autoHide = false;

    BX.bind(document, 'click', function(e) {
            if(!BX.findParent(e.target, {"class" : "menu-popup"}) && BX.style(BX(menu.popupWindow.uniquePopupId), "display") != "none" && !BX.findParent(e.target, {property: {"id" : "support-popupMenuOptions"}}) && e.target != BX("support-popupMenuOptions"))
            {
                    menu.close();
            }
    });
    
    BX.bind(BX('support_groups_btn_send'), 'click', function(e) {console.log(BX.SocNetLogDestination);
        e.preventDefault();
        var groupsForm = window.groups;
        BX.ajax.post(
            arResult["SCRIPT_PATH"] + "/ajax.php",
            groupsForm,
            function (){
                BX.addClass(BX('support-group-wrap'), "hidden");
            }
        );
    });
    
    BX.bind(BX('sup-group-control-btn'), 'click', function(e) {
        e.preventDefault();
        BX.toggleClass(BX('support-group-wrap'), "hidden");
    });
    
    BX.bind(BX('sup-group-control-btn-close'), 'click', function(e) {
        e.preventDefault();
        BX.addClass(BX('support-group-wrap'), "hidden");
    });
			
});
