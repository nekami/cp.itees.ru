<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array("popup", "SocNetLogDestination"));
?>

<?if($arResult["ACCES"] == "Y"):?>
    <div id="support-popupMenuOptions" data-url="<?=$arResult['SCRIPT']?>" class="support-button-cogwheel">
            <span class="support-button-icon"></span>
    </div>
            <!-- Destination - "Attendees" -->
    <?
    /*$arGroups = ["diag", "work", "self"];
    ?>
            
    <div><button class="ui-btn" id="sup-group-control-btn"><?=GetMessage('SUP_BTN_GROUP_CONTROL')?></button></div>
    <div id="support-group-wrap" class="hidden">
        <div class="feed-event-destination-block">
            <?foreach($arGroups as $groups):?>
                <div class="feed-event-destination-title"><?=GetMessage("SUP_BTN_GROUP_".$groups)?>:</div>
                <div class="feed-event-destination-wrap" id="feed-event-dest-cont-<?=$groups?>">
                        <span id="feed-event-dest-item-<?=$groups?>"></span>
                <span class="feed-add-destination-input-box" id="feed-event-dest-input-box-<?=$groups?>">
                        <input type="text" value="" class="feed-add-destination-inp" id="feed-event-dest-input-<?=$groups?>">
                </span>
                        <a href="#" class="feed-add-destination-link" id="feed-event-dest-add-link-<?=$groups?>"></a>
                        <script type="text/javascript">
                                BXSocNetLogDestinationDisableBackspace = null;
                                BX.SocNetLogDestination.init({
                                        name : "support_groups_<?=$groups?>",
                                        searchInput : BX('feed-event-dest-input-<?=$groups?>'),
                                        extranetUser :  false,
                                        bindMainPopup : { 'node' : BX('feed-event-dest-cont-<?=$groups?>'), 'offsetTop' : '5px', 'offsetLeft': '15px'},
                                        bindSearchPopup : { 'node' : BX('feed-event-dest-cont-<?=$groups?>'), 'offsetTop' : '5px', 'offsetLeft': '15px'},
                                        callback : {
                                                select : BXEvDestSelectCallback,
                                                unSelect : BXEvDestUnSelectCallback,
                                                openDialog : BXEvDestOpenDialogCallback,
                                                closeDialog : BXEvDestCloseDialogCallback,
                                                openSearch : BXEvDestOpenDialogCallback,
                                                closeSearch : BXEvDestCloseSearchCallback
                                        },
                                        items : {
                                                sonetgroups : <?=CUtil::PhpToJSObject($arResult["GROUPS"]);?>,
                                        },
                                        itemsLast : {
                                                sonetgroups : {}
                                        },
                                        itemsSelected : {},
                                        destSort : []
                                });
                                BX.bind(BX('feed-event-dest-input-<?=$groups?>'), 'keyup', BXEvDestSearch);
                                BX.bind(BX('feed-event-dest-input-<?=$groups?>'), 'keydown', BXEvDestSearchBefore);
                                BX.bind(BX('feed-event-dest-add-link-<?=$groups?>'), 'click', function(e){BX.SocNetLogDestination.openDialog("support_groups_<?=$groups?>"); BX.PreventDefault(e); });
                                BX.bind(BX('feed-event-dest-cont-<?=$groups?>'), 'click', function(e){BX.SocNetLogDestination.openDialog("support_groups_<?=$groups?>"); BX.PreventDefault(e);});
                                BXEvDestSetLinkName("support_groups_<?=$groups?>");

                        </script>
                </div>
            <?endforeach;?>
        </div>
        <div class="support-group-wrap-footer">
            <button class="ui-btn" id="support_groups_btn_send"><?=GetMessage('SUP_BTN_GROUP_BTN_SEND')?></button>
            <a href="#" id="sup-group-control-btn-close">�������</a>
        </div>
    </div>
    <?*/?>
<pre><?/*print_r($arResult)*/?></pre>
	<script type="text/javascript">
                window.groups = {groupDiag:[], groupWork:[], groupSelf:[]};
		window.arResult = <?=CUtil::PhpToJSObject($arResult);?>;
		BX.message({
			supButText: "<?=GetMessageJS('SUP_BTN_DEFAULT')?>",
			catButText: "<?=GetMessageJS('SUP_BTN_CAT')?>",
			statButText: "<?=GetMessageJS('SUP_BTN_STATUS')?>",	
			groupButText: "<?=GetMessageJS('SUP_BTN_GROUP')?>",
                        groupsAddOne: "<?=GetMessageJS('SUP_BTN_GROUPS_ADD')?>",
                        groupsAddMore: "<?=GetMessageJS('SUP_BTN_GROUPS_ADD_MORE')?>",
                        LM_POPUP_TAB_LAST: "���������",
                        LM_POPUP_TAB_SG: "������",
                        LM_POPUP_TAB_LAST_CONTACTS: "��������� ��������",
                        LM_POPUP_TAB_LAST_COMPANIES: "��������� ��������",
                        LM_POPUP_TAB_LAST_LEADS: "��������� ����",
                        LM_POPUP_TAB_LAST_DEALS: "��������� ������",
                        LM_POPUP_TAB_LAST_USERS: "��������� ������������",
                        LM_POPUP_TAB_LAST_CRMEMAILS: "��������� ���������",
                        LM_POPUP_TAB_LAST_SG: "��������� ������",
                        LM_EMPTY_LIST: "�����",
		});	
	</script>
	<script type="text/javascript" src="<?=$this->GetFolder()?>/menu.js"></script>
<?endif?>