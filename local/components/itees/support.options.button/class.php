<?
use Bitrix\Main\ModuleManager,
	Bitrix\Main\Localization\Loc as Loc;
	
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/component.php';

if (!ModuleManager::isModuleInstalled("support"))
{
	ShowError(GetMessage("MODULE_NOT_INSTALL"));
	return;
}

Loc::loadMessages(__FILE__);

class SupportOptionsButton extends CBitrixComponent
{	
	public function executeComponent()
	{
		$this->prepare();
		$this->IncludeComponentTemplate();
	}
	
	private function prepare()
	{
        GLOBAL $APPLICATION;
		$this->arResult["SCRIPT_PATH"] = $this->GetPath();
		//�������� ������ ������������� ���������
		$filter = array(
			"GROUPS_ID" => $this->arParams["SUPPORT_GROUP"],
			"ACTIVE" => "Y",
		);
		$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter);
		while ($arUser = $rsUsers->Fetch())
		{
			$this->arResult["SUPPORT_USERS"][] = $arUser;
		}
		
		$this->arResult["SCRIPT"] = $this->GetPath()."/class.php";
		
		$curDefaultUser = \Bitrix\Main\Config\Option::get("support", "DEFAULT_RESPONSIBLE_ID");
		$this->arResult["CUR_DEAFULT"] = $curDefaultUser;
		
		//�������� ������ ��������
		$arFilter = Array(
			"LID" => SITE_ID,
			"TYPE" => "S",
		);
		$by = "s_c_sort";
		$sort = "asc";
		$rsStatus = CTicketDictionary::GetList($by, $sort, $arFilter, $is_filtered); 
		while($arRes = $rsStatus->GetNext()) {
			$this->arResult["STATUSES"][] = $arRes;
		}
		foreach($this->arResult["STATUSES"] as  &$status)
		{
			$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields ("TEH_SUPPORT_STATUS", $status["ID"]);
			$status["IS_FINAL"] = $arUF["UF_TS_STATUS_FINAL"]["VALUE"];
		}
                
        //�������� ������ ���������
		$arFilter = Array(
			"LID" => SITE_ID,
			"TYPE" => "C",
		);
		$by = "s_c_sort";
		$sort = "asc";
		$rsStatus = CTicketDictionary::GetList($by, $sort, $arFilter, $is_filtered); 
		while($arRes = $rsStatus->GetNext()) {
			$this->arResult["CATEGORYES"][] = $arRes;
		}
                
                //��������� ������
                $groups = CSocNetGroup::GetList(array("ID" => "DESC"), array("ACTIVE"=>"Y"));
                while($group = $groups->GetNext())
                {
                    $this->arResult["GROUPS"]["SG".$group["ID"]]["id"] = "SG".$group["ID"];
                    $this->arResult["GROUPS"]["SG".$group["ID"]]["entityId"] = $group["ID"];
                    $this->arResult["GROUPS"]["SG".$group["ID"]]["name"] = $group["NAME"];
                    $this->arResult["GROUPS"]["SG".$group["ID"]]["desc"] = $group["~DESCRIPTION"];
                }
		
		GLOBAL $USER;
		if($USER->isAdmin() /*|| array_intersect(array(), $USER->GetUserGroupArray())*/)
			$this->arResult["ACCES"] = "Y";
	}
	
	public static function setNewDefault($newDef)
	{
		\Bitrix\Main\Config\Option::set("support", "DEFAULT_RESPONSIBLE_ID", $newDef);
	}
	
	public static function setFinalStatus($statusId)
	{
		$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("TEH_SUPPORT_STATUS", $statusId);
		if(!isset($arUF["UF_TS_STATUS_FINAL"]["VALUE"]) || $arUF["UF_TS_STATUS_FINAL"]["VALUE"] == "0")
			$GLOBALS["USER_FIELD_MANAGER"]->Update("TEH_SUPPORT_STATUS", $statusId, Array ("UF_TS_STATUS_FINAL" => "1"));
		else
			$GLOBALS["USER_FIELD_MANAGER"]->Update("TEH_SUPPORT_STATUS", $statusId, Array ("UF_TS_STATUS_FINAL" => "0"));
	}
	
	private function setTemplate()
	{
		
	}
}
?>
