<?php
/**
 * Зависимые компоненты:
 * itees:pollstructure
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

class IBStructureComponent extends \CBitrixComponent {
    /**
     * Получение дерева разделов
     * @param $arFilter
     * @return $arSelect
     */
    static function getSectionsTree($arFilter, $arSelect) {
        try {
            \Bitrix\Main\Loader::includeModule("iblock");
            $obSect = \CIBlockSection::GetList(['left_margin' => 'asc'], $arFilter, false, $arSelect);

            $result = [];

            while($arSect = $obSect->Fetch()) {
                array_push($result, $arSect);
            }

            return [
                'data' => $result,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * Формирует итоговую структуру
     * @param $sections
     * @return $elements
     */
    static function formResult($sections, $elements) {
		// не сделано
        try {
            $result = [
				'SECTIONS' => $sections,
				'ELEMENTS' => $elements,
			];

            return [
                'data' => $result,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * Получение элементов
     * @param $arFilter
     * @return $arSelect
     */
    static function getElements($arFilter, $arSelect) {
        try {
            \Bitrix\Main\Loader::includeModule("iblock");
            $obEl = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);

            $result = [];

            while($arEl = $obEl->Fetch()) {
                array_push($result, $arEl);
            }

            return [
                'data' => $result,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    public function executeComponent() {
        try {
            if(!$this->arParams['IBLOCK_ID']) {
                throw new Exception(Loc::getMessage('IB_STRUCTURE.ERROR.NO_IB_ID'));
            }
            
            $arFilter = $GLOBALS[$this->arParams['FILTER_NAME']];
            $arFilter['IBLOCK_ID'] = $this->arParams['IBLOCK_ID'];
            $arSectionFilter = $GLOBALS[$this->arParams['FILTER_SECTION_NAME']];
            $arSectionFilter['IBLOCK_ID'] = $this->arParams['IBLOCK_ID'];
            $arFields = $this->arParams['FIELD_CODE'];
            $arProperties = $this->arParams['PROPERTY_CODE'];
            $arSectionFields = $this->arParams['SECTION_FIELD_CODE'];

            $arSectionSelect = $arSectionFields;
            $sections = self::getSectionsTree($arSectionFilter, $arSectionSelect);

            if($sections['error']) {
                throw new Exception($sections['error']);
            }

            $sections = $sections['data'];
            
            $arSectionSelect = array_merge($arFields, array_map(function($property) {return 'PROPERTY_' . $property;}, $arProperties));
            
            $elements = self::getElements($arFilter, $arSectionSelect);

            if($elements['error']) {
                throw new Exception($elements['error']);
            }

            $elements = $elements['data'];
			
            $result = self::formResult($sections, $elements);

            if($result['error']) {
                throw new Exception($result['error']);
            }

            $this->arResult = $result['data'];

            $this->IncludeComponentTemplate();
        } catch(\Exception $e) {
            ShowError($e->getMessage());
            return false;
        }
    }
}