<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if(ob_get_level()) {
	ob_end_clean();
}

$json = json_encode($arResult, JSON_PRETTY_PRINT );

if($errorCode = json_last_error()) {
	echo "{\"error\": {\"code\": $errorCode}, \"data\": false}";
} else {
	echo $json;
}

exit;