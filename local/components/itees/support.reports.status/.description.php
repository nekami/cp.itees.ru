<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => getMessage("SUPPORT_REPORT_STATUS_NAME"),
	"DESCRIPTION" => getMessage("SUPPORT_REPORT_STATUS_DESC"),
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "utility",
	),
);
?>