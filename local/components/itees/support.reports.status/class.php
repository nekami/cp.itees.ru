<?
use \Bitrix\Main,
	\Bitrix\Main\ModuleManager,
	\Bitrix\Main\Localization\Loc as Loc,
	\Bitrix\Iblock,
	\Bitrix\Iblock\Component;
	
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
{
	ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
	return;
}

class SupportReportStatus extends CBitrixComponent
{	
	public function executeComponent()
	{
		$this->prepare();
		$this->IncludeComponentTemplate();
	}
	
	private function prepare()
	{
        GLOBAL $APPLICATION;
		$this->arResult['COMPONENT_PARAMS']['AJAX'] = $this->GetPath()."/ajax.php";
		$this->arResult['COMPONENT_PARAMS']['REPORT_STATUS_IBLOCK'] = $this->arParams["REPORT_STATUS_IBLOCK"];
		$this->arResult['COMPONENT_PARAMS']['REPORT_IBLOCK_ID'] = $this->arParams["REPORT_IBLOCK_ID"];
		$this->arResult['COMPONENT_PARAMS']['REPORT_STATUS_CODE'] = $this->arParams["REPORT_STATUS_CODE"];
		
		$arStatusParams = self::getInfo($this->arParams);
		$this->arResult["STATUSES"] = $arStatusParams["INFOS"];
		$this->arResult["HTML"] = self::RenderProgressControl($arStatusParams);
	}
	
	private static function getInfo($arParams)
	{
		//ID финального статуса
		$finalId = 0;
		$db_enum_list = CIBlockProperty::GetPropertyEnum("STAGE", Array(), Array("IBLOCK_CODE"=>$arParams["REPORT_STATUS_IBLOCK"], "EXTERNAL_ID"=>"FINAL"));
		if($ar_enum_list = $db_enum_list->GetNext())
		{
			$finalId = $ar_enum_list["ID"]; 
		}
		
		//Все статусы
		$statuses = array();
		$arSelect = Array("ID", "NAME", "PROPERTY_COLOR", "PROPERTY_STAGE", "SORT");
		$arFilter = Array("IBLOCK_CODE"=>$arParams["REPORT_STATUS_IBLOCK"], "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$inf = array(
				"STATUS_ID" => $arFields["ID"],
				"NAME" => $arFields["NAME"],
				"SORT" => $arFields["SORT"],
				"COLOR" => $arFields["PROPERTY_COLOR_VALUE"]
			);
			$statuses[$arFields["ID"]] = $inf;
			if ($arFields["PROPERTY_STAGE_ENUM_ID"] === $finalId)
				$finalId = $arFields["ID"];
		}
		
		//Текущий статус
		$curId = 0;
		$arSelect = Array("ID", "NAME", "PROPERTY_".$arParams["REPORT_STATUS_CODE"]);
		$arFilter = Array("IBLOCK_CODE"=>$arParams["REPORT_IBLOCK_CODE"], "ID"=>$arParams["REPORT_ID"], "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$curId = $arFields["PROPERTY_REPORT_STATUS_VALUE"];
		}
		
		$arStatusParams = array(
			"PREFIX" => "SUPPORT_REPORT_LIST_PROGRESS_BAR_",
			"ENTITY_ID" => $arParams["REPORT_ID"],
			"CURRENT_ID" => $curId,
			"INFOS" => $statuses,
			"FINAL_ID" => $finalId,
		);
		
		return $arStatusParams;
	}
	
	private static function RenderProgressControl($arParams)
    {
        if(!is_array($arParams))
        {
            return '';
        }

        $infos = isset($arParams['INFOS']) ? $arParams['INFOS'] : null;

        if(!is_array($infos) || empty($infos))
        {
            return '';
        }

        $finalID = isset($arParams['FINAL_ID']) ? $arParams['FINAL_ID'] : '';

        $currentInfo = null;
        $currentID = isset($arParams['CURRENT_ID']) ? $arParams['CURRENT_ID'] : '';
        if($currentID !== '' && isset($infos[$currentID]))
        {
            $currentInfo = $infos[$currentID];
        }
        $currentSort = is_array($currentInfo) && isset($currentInfo['SORT']) ? intval($currentInfo['SORT']) : -1;

        $finalInfo = null;
        if($finalID !== '' && isset($infos[$finalID]))
        {
            $finalInfo = $infos[$finalID];
        }
        $finalSort = is_array($finalInfo) && isset($finalInfo['SORT']) ? intval($finalInfo['SORT']) : -1;

        $isSuccessful = $currentSort === $finalSort;
        $isFailed = $currentSort > $finalSort;

        $defaultProcessColor = "#ace9fb";
        $defaultSuccessColor = "#dbf199";
        $defaultFailureColor = "#ffbebd";

        $stepHtml = '';
        $color = isset($currentInfo['COLOR']) ? $currentInfo['COLOR'] : '';
        if($color === '')
        {
            $color = $defaultProcessColor;
            if($isSuccessful)
            {
                $color = $defaultSuccessColor;
            }
            elseif($isFailed)
            {
                $color = $defaultFailureColor;
            }
        }

        $finalColor = isset($finalInfo['COLOR']) ? $finalInfo['COLOR'] : '';
        if($finalColor === '')
        {
            $finalColor = $isSuccessful ? $defaultSuccessColor : $defaultFailureColor;
        }

        foreach($infos as $info)
        {
            $ID = isset($info['STATUS_ID']) ? $info['STATUS_ID'] : '';
            $sort = isset($info['SORT']) ? (int)$info['SORT'] : 0;
			$enableCustomColors = isset($info['COLOR']) ? 1 : 0;

            if($sort > $finalSort)
            {
                break;
            }
			
			if($enableCustomColors)
            {
                $stepHtml .= '<td class="crm-list-stage-bar-part"';
                if($sort <= $currentSort)
                {
                    $stepHtml .= ' style="background:'.$color.'"';
                }
                $stepHtml .= '>';
            }
            else
            {
                $stepHtml .= '<td class="crm-list-stage-bar-part';
                if($sort <= $currentSort)
                {
                    $stepHtml .= ' crm-list-stage-passed';
                }
                $stepHtml .= '">';
            }
			
            $stepHtml .= '<div data-status-id='.htmlspecialcharsbx(strtolower($ID)).' class="crm-list-stage-bar-block  crm-stage-'.htmlspecialcharsbx(strtolower($ID)).'"><div class="crm-list-stage-bar-btn"></div></div></td>';
        }

        $wrapperStyle = '';
        $wrapperClass = '';
        if($enableCustomColors)
        {
            if($isSuccessful || $isFailed)
            {
                $wrapperStyle = 'style="background:'.$finalColor.'"';
            }
        }
        else
        {
            if($isSuccessful)
            {
                $wrapperClass = ' crm-list-stage-end-good';
            }
            elseif($isFailed)
            {
                $wrapperClass =' crm-list-stage-end-bad';
            }
        }

        $prefix = isset($arParams['PREFIX']) ? $arParams['PREFIX'] : '';
        $entityID = isset($arParams['ENTITY_ID']) ? intval($arParams['ENTITY_ID']) : 0;
        $controlID = isset($arParams['CONTROL_ID']) ? $arParams['CONTROL_ID'] : '';

        if($controlID === '')
        {
            $controlID = $entityTypeName !== '' && $entityID > 0 ? "{$prefix}{$entityTypeName}_{$entityID}" : uniqid($prefix);
        }

        $isReadOnly = isset($arParams['READ_ONLY']) ? (bool)$arParams['READ_ONLY'] : false;
        $legendContainerID = isset($arParams['LEGEND_CONTAINER_ID']) ? $arParams['LEGEND_CONTAINER_ID'] : '';
        $displayLegend = $legendContainerID === '' && (!isset($arParams['DISPLAY_LEGEND']) || $arParams['DISPLAY_LEGEND']);
        $legendHtml = '';
        if($displayLegend)
        {
            $legendHtml = '<div class="crm-list-stage-bar-title">'.htmlspecialcharsbx(isset($infos[$currentID]) && isset($infos[$currentID]['NAME']) ? $infos[$currentID]['NAME'] : $currentID).'</div>';
        }

        $conversionScheme = null;
        if(isset($arParams['CONVERSION_SCHEME']) && is_array($arParams['CONVERSION_SCHEME']))
        {
            $conversionScheme = array();
            if(isset($arParams['CONVERSION_SCHEME']['ORIGIN_URL']))
            {
                $conversionScheme['originUrl'] = $arParams['CONVERSION_SCHEME']['ORIGIN_URL'];
            }
            if(isset($arParams['CONVERSION_SCHEME']['SCHEME_NAME']))
            {
                $conversionScheme['schemeName'] =  $arParams['CONVERSION_SCHEME']['SCHEME_NAME'];
            }
            if(isset($arParams['CONVERSION_SCHEME']['SCHEME_CAPTION']))
            {
                $conversionScheme['schemeCaption'] =  $arParams['CONVERSION_SCHEME']['SCHEME_CAPTION'];
            }
            if(isset($arParams['CONVERSION_SCHEME']['SCHEME_DESCRIPTION']))
            {
                $conversionScheme['schemeDescription'] =  $arParams['CONVERSION_SCHEME']['SCHEME_DESCRIPTION'];
            }
        }

        return $registrationScript.'<div data-report-id="'.htmlspecialcharsbx($arParams["ENTITY_ID"]).'" '
			.'class="crm-list-stage-bar'.$wrapperClass.'" '.$wrapperStyle.' id="'.htmlspecialcharsbx($controlID).'"><table class="crm-list-stage-bar-table"><tr>'
            .$stepHtml
            .'</tr></table>'
            .'</div>'.$legendHtml;
    }
	
	public static function rerenderStatus($arParams)
	{
		$arStatusParams = self::getInfo($arParams);
		return self::RenderProgressControl($arStatusParams);
	}
}
?>
