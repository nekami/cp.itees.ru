<?
define('STOP_STATISTICS',    true);
define('NO_AGENT_CHECK',     true);
define('PUBLIC_AJAX_MODE', true);
define('DisableEventsCheck', true);

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
require_once 'class.php';

if (!\Bitrix\Main\Loader::includeModule('iblock'))
{
	ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
	return;
}

if($_REQUEST["ajax"] == "Y" && isset($_REQUEST["reportId"]) && isset($_REQUEST["newStatus"]))
{
	$reportId = $_REQUEST["reportId"];
	
	CIBlockElement::SetPropertyValuesEx($reportId, false, array($_REQUEST["REPORT_STATUS_CODE"] => $_REQUEST["newStatus"]));
	
	$arParams = Array(
		"REPORT_ID" => $_REQUEST["reportId"],
		"REPORT_IBLOCK_ID" => $_REQUEST["REPORT_IBLOCK_ID"],
		"REPORT_STATUS_CODE" => $_REQUEST["REPORT_STATUS_CODE"],
		"REPORT_STATUS_IBLOCK" => $_REQUEST["REPORT_STATUS_IBLOCK"]
	);
	
	$return = SupportReportStatus::rerenderStatus($arParams);
	echo $return;
}
	