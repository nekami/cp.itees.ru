BX.ready(function(){
	var statuses = window.statuses;
	var bar = '.crm-list-stage-bar-block';
	$(document).on( 'mouseenter', bar, hintCreate );
	$(document).on( 'mouseleave', bar, hintDelete );
	$(document).on( 'click', bar, sendStatus );
	
	function hintCreate() 
	{
		
		let statusId = $(this).data('statusId');
		hint = new BX.PopupWindow(
			"report_status_hint",
			BX(this),
			{
				content: "<b>"+statuses[statusId].NAME+"</b>", 
				autoHide: true, 
				angle: true,
			}
		);
		hint.offsetLeft = 15;
		hint.offsetTop = 12;
		hint.show();
	}

	function hintDelete()
	{
		$('#report_status_hint').remove();
	}

	function sendStatus()
	{
		var componentParms = window.params;
		let reportId = $(this).parents('.crm-list-stage-bar').data('reportId');
		let cont = $(this).parents('.crm-list-stage-bar').parent();
		$.ajax({
			method: "POST",
			url: componentParms.AJAX,
			data: {
				ajax: "Y",
				reportId: reportId,
				REPORT_STATUS_IBLOCK: componentParms.REPORT_STATUS_IBLOCK,
				REPORT_IBLOCK_ID: componentParms.REPORT_IBLOCK_ID,
				REPORT_STATUS_CODE: componentParms.REPORT_STATUS_CODE,
				newStatus: $(this).data('statusId'),
			},
		}).done(function(resp){
			hintDelete();
			cont.html(resp);
		});
	}
	
});

