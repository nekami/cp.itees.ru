<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"REPORT_ID" => Array(
			"NAME" => GetMessage("SUPPORT_REPORT_STATUS_REPORT_ID"),
			"PARENT" => "BASE",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"REPORT_IBLOCK_ID" => Array(
			"NAME" => GetMessage("SUPPORT_REPORT_STATUS_REPORT_IBLOCK_ID"),
			"PARENT" => "BASE",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"REPORT_STATUS_CODE" => Array(
			"NAME" => GetMessage("SUPPORT_REPORT_STATUS_REPORT_STATUS_CODE"),
			"PARENT" => "BASE",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"REPORT_STATUS_IBLOCK" => Array(
			"NAME" => GetMessage("SUPPORT_REPORT_STATUS_IBLOCK"),
			"PARENT" => "BASE",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
	),
);
?>
