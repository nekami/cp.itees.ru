<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Config\Option, 
	Bitrix\Main\Application;

class AdvertisingSignature extends CBitrixComponent
{
    public function executeComponent()
    {
		global $USER;
		if (!hasAdvertisingSignatureAccess())
		{
			LocalRedirect(CComponentEngine::MakePathFromTemplate("/company/personal/user/#user_id#/", array("user_id" => $USER->GetID())));
		}
		
		if($this->request->getPost('SESSID') == bitrix_sessid() && !empty($this->request->getPost('SUBMIT'))) {
			
			Option::set("main", "advertising_signature", $this->request->getPost('VALUE'));
			Option::set("main", "advertising_signature_active", $this->request->getPost('ACTIVE'));
			
			$context = Application::getInstance()->getContext();
			$server = $context->getServer();
			
			LocalRedirect($server->getRequestUri());
		}
		
		$this->arResult['VALUE'] = Option::get("main", "advertising_signature");
		$this->arResult['ACTIVE'] = Option::get("main", "advertising_signature_active");
		
		$this->includeComponentTemplate();
    }
};