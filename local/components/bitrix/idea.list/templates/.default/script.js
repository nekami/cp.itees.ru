$(function() 
{	
// ������ � CRM-���������		
	BX.FlagPopup = {}; // ������� ���������/��������� ���������� ����
	
	BX.addCustomEvent("onAfterPopupShow", BX.delegate(function(ParamsPopupShow)
	{
		// ���� �������� ��������� ������ ���������� ����
		uniquePopupId = ParamsPopupShow.uniquePopupId;
			
		if (uniquePopupId = uniquePopupId.match(/CRM-crm-uf-crm-blog-post(\d+)-uf-crm-blog-post-open-popup/i))
	    {
			if (BX.FlagPopup[uniquePopupId[1]] != "Y") 
			{
				BX.FlagPopup[uniquePopupId[1]] = "Y";
				BX.IdIdea = uniquePopupId[1];
				
				obCrm['crm-uf-crm-blog-post'+uniquePopupId[1]+'-uf-crm-blog-post-open'].AddOnSaveListener(function()
				{					
					UpdateCrmObjectIdea();
				});
			}
		}
	}, this));		
	
	BX.addCustomEvent("onPopupClose", BX.delegate(function(ParamsPopupClose)
	{		
	   uniquePopupId = ParamsPopupClose.uniquePopupId;
	   
	   if (uniquePopupId = uniquePopupId.match(/CRM-crm-uf-crm-blog-post(\d+)-uf-crm-blog-post-open-popup/i))
	   {			
			BX.FlagPopup[uniquePopupId[1]] = "N";
	   }		
    }, this));
	
	// ���������� crm-��������		   
	BX.addCustomEvent("onCrmSelectedItem", BX.delegate(function(ParamsSelectedItem)
	{
		if (typeof ParamsSelectedItem.fieldUid !== 'undefined') BX.IdIdea = ParamsSelectedItem.fieldUid;
		if (typeof BX.UF_CRM_BLOG_POST[BX.IdIdea] !== 'undefined' && BX.UF_CRM_BLOG_POST[BX.IdIdea].indexOf(ParamsSelectedItem.id) == -1)
		{
			BX.UF_CRM_BLOG_POST[BX.IdIdea].push(ParamsSelectedItem.id);
		}		
	}, this)); 		   
	
	
	// �������� crm-��������
	BX.addCustomEvent("onCrmUnSelectedItem", BX.delegate(function(ParamsUnSelectedItem)
	{
		if (typeof ParamsUnSelectedItem.fieldUid !== 'undefined') BX.IdIdea = ParamsUnSelectedItem.fieldUid;
		
		// �������� �� ������� crm-��������
		if (BX.UF_CRM_BLOG_POST[BX.IdIdea])
		{
			var indexCrmObject = BX.UF_CRM_BLOG_POST[BX.IdIdea].indexOf(ParamsUnSelectedItem.id);
			
			if (indexCrmObject !== -1) 
			{ 
				BX.UF_CRM_BLOG_POST[BX.IdIdea].splice(indexCrmObject, 1); 				
			} 
		}
				
		if (BX.FlagPopup[BX.IdIdea] != "Y")
		{	
			UpdateCrmObjectIdea();
		}
		
	}, this));	
	
	
	function UpdateCrmObjectIdea()
	{		
		if (BX.IdIdea && BX.UF_CRM_BLOG_POST[BX.IdIdea])
		{	
			$.ajax({ 
				url: '/local/components/bitrix/idea.list/ajax.php', 
				data: {'ACTION': 'UPDATE_CRM_OBJECT', 'IdIdea': BX.IdIdea, 'IdCrmEssence': BX.UF_CRM_BLOG_POST[BX.IdIdea]}, 
				method: 'get', 
				dataType: 'json',					  
				success: function (msg) 
				{					
					if (msg.error) console.log("������ ������� � ���� ������"); 	
					else 
					{
						$('[data-count-client="UF_CLIENT_BLOG_POST_'+BX.IdIdea+'"]').html(msg.count_client);
					}
				},
				error: function () { console.log("������ ajax-�������"); },
			});
		}
	}
	
	BX.addCustomEvent("IdeaOnLifeSearch", BX.delegate(function(ParamsIdeaSidebar)
	{
		// ����������������� ������ �������� ����� ������
		$.each(BX.ParamsIdeaSidebar, function(id, val) 
	    {				
			new IdeaSidebar(val);			
		});	
		
    }, this));	
	
});