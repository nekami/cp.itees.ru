(function() {

/* ������ �� ��������� ���� */

	IdeaSidebar = function(parameters)
	{		
		this.layout = {
			stagesWrap: BX(parameters.htmlStagesWrapId),
			stages: BX(parameters.htmlStagesId)
		};
		this.parameters = parameters || {};
		this.ideaId = this.parameters.ideaId;
		this.stageId = parseInt(this.parameters.stageId);
		this.stages = this.parameters.stages || {};		
		this.initStages();
	};

/* ������������� ����� �� �������� ������� ����  */
IdeaSidebar.prototype.initStages = function()
{
	if (this.layout.stages && this.stages)
	{
		var stagesShowed = this.stages.length > 0;

		BX.cleanNode(this.layout.stages);
		
		var canChange = this.parameters.can;

		for (var i=0, c=this.stages.length; i<c; i++)
		{
			this.layout.stages.appendChild(
				this.stages[i].TEXT_LAYOUT = BX.create("div", {
					
					attrs: {
						"data-stageId": this.stages[i].ID,
						title: this.stages[i].VALUE
					},
					
					props: {
						className: "task-section-status-step"
					},
					
					text: this.stages[i].VALUE,
					
					events:
						canChange
							? {
							click: BX.delegate(this.setStageHadnler, this),
						}
							: null,
					style:
						!canChange
							? {
							cursor: "default"
						}
							: null
					
				})
			);
		}

		if (stagesShowed)
		{
			BX.show(this.layout.stagesWrap);

			if (this.stageId > 0)
			{
				this.setStage(this.stageId);
			}
			else
			{
				this.setStage(this.stages[0].ID);
			}
		}
		else
		{
			BX.hide(this.layout.stagesWrap);
		}
	}
};


IdeaSidebar.prototype.getStageData = function(stageId)
{
	stageId = parseInt(stageId);

	if (this.stages)
	{
		for (var id in this.stages)
		{
			if (parseInt(this.stages[id].ID) === stageId)
			{
				return this.stages[id];
			}
		}
	}

	return null;
};

/* */
IdeaSidebar.prototype.setStageHadnler = function()
{
	var stageId = BX.data(BX.proxy_context, "stageId");
	this.setStage(stageId);
	this.saveStage(stageId);
};

/* ���������� ������� � �� */
IdeaSidebar.prototype.saveStage = function(stageId)
{
	stageId = parseInt(stageId);
	if (stageId === this.stageId)
	{
		return;
	}
	this.stageId = stageId;
	
	var StageObject = {};
	StageObject.IdIdea = this.ideaId;			
	StageObject.StageId = this.stageId;
	
	$('input[name="UF_STATUS"]').val(this.stageId);
	
	if(BX.QueryDB)
	{
		$.ajax({ 
			url: '/local/components/bitrix/idea.list/ajax.php', 
			data: {'ACTION': 'UPDATE_STAGE', 'STAGE_OBJECT': StageObject}, 
			method: 'get', 
			dataType: 'json',					  
			success: function (msg) 
			{					
				if (msg.error) console.log("������ ������� � ���� ������"); 	
				else StageObject = {};
			},
			error: function () { console.log("������ ajax-�������"); }
		});	
	}	
};

/*  */
IdeaSidebar.prototype.setStage = function(stageId)
{
	var stage = this.getStageData(stageId);
	stageId = parseInt(stageId);

	if (this.stages && stage)
	{
		var color = "#" + stage["XML_ID"];
		var clearAll = true;
		var layout;
		for (var i=0, c=this.stages.length; i<c; i++)
		{
			layout = this.stages[i].TEXT_LAYOUT;
			if (clearAll)
			{
				layout.style.color = this.calculateTextColor(color);
				layout.style.backgroundColor = color;
				layout.style.borderBottomColor = color;
			}
			else
			{
				layout.style.backgroundColor = "";
				layout.style.borderBottomColor = "#" + this.stages[i].XML_ID;
			}
			if (parseInt(this.stages[i].ID) === stageId)
			{
				clearAll = false;
			}
		}
	}
};

/* ��������� ����� ����� */
IdeaSidebar.prototype.calculateTextColor = function(baseColor)
{
	var defaultColors = [
		"00c4fb",
		"47d1e2",
		"75d900",
		"ffab00",
		"ff5752",
		"468ee5",
		"1eae43"
	];
	var r, g, b;

	if (BX.util.in_array(baseColor.toLowerCase(), defaultColors))
	{
		return "#fff";
	}
	else
	{
		var c = baseColor.split("");
		if (c.length== 3){
			c= [c[0], c[0], c[1], c[1], c[2], c[2]];
		}
		c = "0x" + c.join("");
		r = ( c >> 16 ) & 255;
		g = ( c >> 8 ) & 255;
		b =  c & 255;
	}

	var y = 0.21 * r + 0.72 * g + 0.07 * b;
	return ( y < 145 ) ? "#fff" : "#333";
};


	IdeaSidebar.prototype.setStatus = function(status, time)
	{
		var statusName = BX("task-detail-status-name");
		var statusDate = BX("task-detail-status-date");

		statusName.innerHTML = BX.message("TASKS_STATUS_" + status);
		statusDate.innerHTML = (status != 4 && status != 5 ?
			BX.message("TASKS_SIDEBAR_START_DATE") + " " : "") +
			BX.util.htmlspecialchars(time);
	};

}).call(this);