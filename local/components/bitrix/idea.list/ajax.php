<? require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("blog");

function UpdateIdea($arIdea) 
{
	$error = false;
	
	if (empty(CBlogPost::Update($arIdea['IdIdea'], $arIdea["arField"]))) $error = true;

	echo json_encode(array('error' => $error, 'count_client' => $arIdea["arField"]["UF_CLIENT_BLOG_POST"]));	
}

// ���������� ������� 
if (!empty($_GET['STAGE_OBJECT']) && ($_GET['ACTION'] == 'UPDATE_STAGE'))
{
	UpdateIdea(
	[ 
		'IdIdea' =>$_GET['STAGE_OBJECT']['IdIdea'], 
		'arField' => 
		[
			'UF_STATUS' => $_GET['STAGE_OBJECT']['StageId']
		] 
	]);
}

// ���������� crm-�������� � ���������� ��������
if (!empty($_GET['IdIdea']) && ($_GET['ACTION'] == 'UPDATE_CRM_OBJECT'))
{
	// ������ ���������� ��������
	$CountClient = 0;
	$CrmEssence = [];
	if (!empty($_GET['IdCrmEssence'])) {
		$CountClient = GetCountClient($_GET['IdCrmEssence']);
		$CrmEssence  = $_GET['IdCrmEssence'];
	}
	
	UpdateIdea(
	[ 
		'IdIdea' =>$_GET['IdIdea'], 
		'arField' => 
		[
			'UF_CRM_BLOG_POST'    => $CrmEssence,
			"UF_CLIENT_BLOG_POST" => $CountClient
		] 
	]);
}
?>