<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if(!empty($arResult['LESSON']['CREATED_BY'])) {
	
	$user = \Bitrix\Main\UserTable::getList([
		'filter' => ['ID' => $arResult['LESSON']['CREATED_BY']],
		'select' => ['ID', 'NAME', 'LAST_NAME', 'LOGIN']
	])->fetch();
	
	$arResult['LESSON']['AUTHOR_FULL_NAME'] = CUser::FormatName(CSite::GetDefaultNameFormat(), $user, true);
}