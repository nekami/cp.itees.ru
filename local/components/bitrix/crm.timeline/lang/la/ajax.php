<?
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_TIMELINE_DOCUMENT_DELETED"] = "Documento eliminado";
$MESS["CRM_WAIT_ACTION_INVALID_BEFORE_PARAMS"] = "La fecha de finalizaci�n de espera debe estar m�s all� de la fecha actual. La fecha que ingres� puede estar en el pasado o demasiado lejos en el futuro.";
$MESS["CRM_WAIT_ACTION_INVALID_REQUEST_DATA"] = "La solicitud contiene par�metros incorrectos";
$MESS["CRM_WAIT_ACTION_ITEM_NOT_FOUND"] = "El elemento no fue encontrado.";
?>