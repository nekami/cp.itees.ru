<?
$MESS["CRM_TIMELINE_DOCUMENT_DELETED"] = "Dokument gel�scht";
$MESS["CRM_PERMISSION_DENIED"] = "Zugriff verweigert";
$MESS["CRM_WAIT_ACTION_INVALID_REQUEST_DATA"] = "Anfrage enth�lt nicht korrekte Parameter.";
$MESS["CRM_WAIT_ACTION_ITEM_NOT_FOUND"] = "Element wurde nicht gefunden.";
$MESS["CRM_WAIT_ACTION_INVALID_BEFORE_PARAMS"] = "Das Datum des Warteschlu�es muss hinter dem aktuellen Datum liegen. Das von Ihnen eingegebene Datum liegt evtl. in Vergangenheit oder aber zu weit in Zukunft.";
?>