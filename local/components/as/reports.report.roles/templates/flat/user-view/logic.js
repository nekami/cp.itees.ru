BX.namespace('Tasks.Component');

var key = false;
(function(){

    BX.Tasks.Component.TaskDetailPartsUserView = BX.Tasks.UserItemSet.extend({
        sys: {
            code: 'user-view'
        },
        options: {
            preRendered: true,
            autoSync:   true,
            role: false,
            multiple: false,
            useSearch: true,
            ajaxKey: false
        },
        methods: {

            construct: function()
            {
	            this.callConstruct(BX.Tasks.UserItemSet);

                this.syncOnDelay = BX.debounce(this.syncOnDelay, 800);
            },

            bindEvents: function()
            {
                this.callMethod(BX.Tasks.UserItemSet, 'bindEvents');


                if(this.option('role') == 'AUDITORS')
                {
                    this.bindDelegateControl('toggle-auditor', 'click', BX.delegate(this.onToggleImAuditor, this));
                }

            },

            onToggleImAuditor: function()
            {
                var imAuditor = BX.hasClass(this.scope(), 'imauditor');
                var user = this.option('user');
                if(!user)
                {
                    return;
                }

                var value = this.extractItemValue(user);
                if(!value)
                {
                    return;
                }

                var ctx = this;
	            var taskId = parseInt(this.option('taskId'));

                var q = this.getQuery();
                if(q && parseInt(this.option('taskId')))
                {


	                if(imAuditor)
	                {
		                BX.Tasks.confirm(BX.message('TASKS_TTDP_TEMPLATE_USER_VIEW_LEAVE_AUDITOR_CONFIRM'), function(way){
			                if(way)
			                {
				                action.call();
			                }
		                });
	                }
	                else
	                {
		                action.call();
	                }
                }

            },

            processItemAfterCreate: function(value, parameters)
            {
                $(".task-detail-sidebar-info-user").each(function() {
                    var id = $( this ).attr('data-item-value');
                    if(id!=0)$( this).show();
                });
                parameters = parameters || {};
                if(!parameters.loadInitial) // work only for items that added lately, not with .load() inside .construct()
                {
                    var item = this.getItem(value);
                    if(item)
                    {
                        //debugger;
                        if(item.data().AVATAR == '')
                        {
                            var avatarNode = item.control('avatar');
                            if(BX.type.isElementNode(avatarNode))
                            {
                                avatarNode.style = '';
                            }
                        }
                    }
                }
            },

            // sync on popup close
            onClose: function()
            {
	            if(this.vars.changed)
	            {
		            this.syncAllIfCan();
	            }
	            this.vars.changed = false;
            },
            // and sync on item deleted by pressing "delete" button
            onItemDeleteClicked: function(node)
            {
                var usr = BX.findParent(BX.findParent( node )).getAttribute('data-item-value');
                var ids = '';
                for (var k in this.vars.items)
                {
                    if( usr == this.vars.items[k].value()){
                        continue;
                    }
                    else    ids += this.vars.items[k].value()+";";

                }
                if(Object.keys(this.vars.items).length==1)ids = "0;";

                if(this.sendAjax(report_id,ids,"add")){

                }
                var value = this.doOnItem(node, this.deleteItem);
            },
            syncOnDelay: function()
            {
                this.syncAllIfCan();
            },
            sendAjax: function(report_id,usr,action)
            {
                BX.ajax(
                    {url:    "/dev/ajax/report_auditor_handler.php",
                        method: "POST",
                        //async: false,
                        data: {"report_id":report_id,"AUDITOR":usr,"action":action},
                        onsuccess:function (data) {}
                    }
                );
            },

            syncAll: function()
            {
                var q = this.getQuery();
                if(q && parseInt(this.option('taskId')) && this.option('role'))
                {
                    var arg = [];
                    var ids = '';
                    for (var k in this.vars.items)
                    {
                        var itemData = BX.clone(this.vars.items[k].data());

                        ids += this.vars.items[k].value()+";";
                        arg.push({
	                        ID: this.vars.items[k].value(),
	                        NAME: itemData.NAME,
	                        LAST_NAME: itemData.LAST_NAME,
	                        EMAIL: itemData.EMAIL
                        });
                    }

                    if(this.sendAjax(report_id,ids,"add")){}
                }
            },

            getItemClass: function()
            {
                return BX.Tasks.Util.ItemSet.Item;
            },

	        attachRightsCheck: function(taskId, q)
	        {
		        var toTasks = this.option('pathToTasks');

		        q.add('task.checkcanread', {
			        id: taskId
		        }, {}, function(error, data){

			        if(!data.RESULT.READ) // we lost task access, sadly leaving
			        {
				        if(toTasks)
				        {
					        window.document.location = toTasks;
				        }
				        else
				        {
					        window.document.location.reload();
				        }
			        }
		        });
	        }
        }
    });

})();