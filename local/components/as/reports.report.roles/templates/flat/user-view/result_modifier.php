<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Tasks\Util\Type;

foreach($arResult['ROWS'] as &$manager) {
    $role = (string)$manager['TEMPLATE_DATA']['ROLE'];
    if ($role == '' || !in_array($role, array('RESPONSIBLE', 'ORIGINATOR', 'AUDITORS', 'ACCOMPLICES', 'CHIEF', 'BUH'))) {
        $role = 'RESPONSIBLE';
    }
    $manager['TEMPLATE_DATA']['ROLE'] = $role;

    $manager['TEMPLATE_DATA']['MULTIPLE'] = $manager['TEMPLATE_DATA']['MULTIPLE'] == true || $manager['TEMPLATE_DATA']['MULTIPLE'] == 'Y';
    $manager['TEMPLATE_DATA']['AUTO_SYNC'] = $manager['TEMPLATE_DATA']['AUTO_SYNC'] == true || $manager['TEMPLATE_DATA']['AUTO_SYNC'] == 'Y';

    $manager['TEMPLATE_DATA']['EDITABLE'] =
        !$arParams["PUBLIC_MODE"] &&
        intval($manager['TEMPLATE_DATA']['TASK_ID']) &&
        $manager['TEMPLATE_DATA']['TASK_CAN_EDIT'];

    $manager['TEMPLATE_DATA']['EDITABLE_OR_AUDITOR'] = $manager['TEMPLATE_DATA']['EDITABLE'] || $role == 'AUDITORS';
    $manager['TEMPLATE_DATA']['EMPTY_LIST'] =
        !Type::isIterable($manager['TEMPLATE_DATA']['ITEMS']['DATA']) ||
        count($manager['TEMPLATE_DATA']['ITEMS']['DATA']) < 1;

    if (!Type::isIterable($manager['TEMPLATE_DATA']['ITEMS']['DATA'])) {
        $manager['TEMPLATE_DATA']['ITEMS']['DATA'] = array();
    } else {
        $currentUser = $GLOBALS['USER']->getId();
        $needCurrentUser = $role == 'AUDITORS' && Type::isIterable($manager['TEMPLATE_DATA']['USER']);

        $formattedUsers = array();
        foreach ($manager['TEMPLATE_DATA']['ITEMS']['DATA'] as $i => $item) {
            $formattedUsers[$item['ID']] = $item;
        }

        if ($needCurrentUser) {
            $formattedUsers[$currentUser] = \Bitrix\Tasks\Util\User::extractPublicData($manager['TEMPLATE_DATA']['USER']);
        }

        $arParams["PATH_TO_USER_PROFILE"] = \Bitrix\Tasks\Integration\Socialnetwork\Task::addContextToURL($arParams["PATH_TO_USER_PROFILE"], $manager['TEMPLATE_DATA']['TASK_ID']);

        foreach ($formattedUsers as $i => $item) {
            $formattedUsers[$i]['AVATAR'] = \Bitrix\Tasks\UI::getAvatar($item['PERSONAL_PHOTO'], 58, 58);
            $formattedUsers[$i]['URL'] = CComponentEngine::MakePathFromTemplate($arParams["PATH_TO_USER_PROFILE"], array("user_id" => $item["ID"]));
            $formattedUsers[$i]['NAME_FORMATTED'] = CUser::FormatName($arParams["NAME_TEMPLATE"], $item, true, false);

            $userType = 'employee';
            if ($item['IS_EMAIL_USER']) {
                $userType = 'mail';
            } else if ($item['IS_EXTRANET_USER']) {
                $userType = 'extranet';
            }

            $formattedUsers[$i]['USER_TYPE'] = $userType;
        }

        $tmp = array();
        foreach ($manager['TEMPLATE_DATA']['ITEMS']['DATA'] as $i => $item) {
            $tmp[intval($item['ID'])] = $formattedUsers[intval($item['ID'])];
        }

        $manager['TEMPLATE_DATA']['ITEMS']['DATA'] = $tmp;

        if ($manager['TEMPLATE_DATA']['EDITABLE_OR_AUDITOR']) {
            $manager['TEMPLATE_DATA']['ITEMS']['DATA'][] = array(
                'ID' => '{{VALUE}}',
                'NAME_FORMATTED' => '{{DISPLAY}}',
                'WORK_POSITION' => '{{WORK_POSITION}}',
                'URL' => CComponentEngine::MakePathFromTemplate($arParams["PATH_TO_USER_PROFILE"], array("user_id" => '{{VALUE}}')),
                'AVATAR' => '{{AVATAR}}',
                'USER_TYPE' => '{{USER_TYPE}}',
            );
        }

        if ($needCurrentUser) {
            $manager['TEMPLATE_DATA']['USER'] = $formattedUsers[$currentUser];
        }
    }
    /*
    $imAuditor = false;
    foreach($manager['TEMPLATE_DATA']['ITEMS']['DATA'] as $item)
    {
        if($item['ID'] == $GLOBALS['USER']->getId())
        {
            $imAuditor = true;
            break;
        }
    }
    $manager['TEMPLATE_DATA']['IM_AUDITOR'] = $imAuditor;
    $manager['TEMPLATE_DATA']['CAN_ADD_MAIL_USERS'] = \Bitrix\Main\ModuleManager::isModuleInstalled("mail");
    */
}