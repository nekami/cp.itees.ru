<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */


/*************************************************************************
 * Processing of received parameters
 *************************************************************************/
if (!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 180;
global $USER;
/*
if(!is_array($arParams["IBLOCKS"]))
	$arParams["IBLOCKS"] = array($arParams["IBLOCKS"]);

$arIBlockFilter = array();
foreach($arParams["IBLOCKS"] as $IBLOCK_ID)
{
	$IBLOCK_ID=intval($IBLOCK_ID);
	if($IBLOCK_ID>0)
		$arIBlockFilter[]=$IBLOCK_ID;
}

if(empty($arIBlockFilter))
{
	if(!CModule::IncludeModule("iblock"))
	{
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	$rsIBlocks = CIBlock::GetList(array("sort" => "asc"), array(
		"type" => $arParams["IBLOCK_TYPE"],
		"LID" => SITE_ID,
		"ACTIVE" => "Y",
	));
	if($arIBlock = $rsIBlocks->Fetch())
		$arIBlockFilter[] = $arIBlock["ID"];
}

unset($arParams["IBLOCK_TYPE"]);
$arParams["PARENT_SECTION"] = intval($arParams["PARENT_SECTION"]);
$arParams["IBLOCKS"] = $arIBlockFilter;
*/

if (!empty($arParams["REPORT_ID"])
    //&&    $this->StartResultCache(false, ($arParams["CACHE_GROUPS"] === "N" ? false : $USER->GetGroups()) )
) {
    if (!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }
    //SELECT
    $arSelect = array(
        "ID",
        "IBLOCK_ID",
        "CODE",
        "IBLOCK_SECTION_ID",
        "NAME",
        "DATE_CREATE",
        "CREATED_BY",
        "CREATED_USER_NAME",
        "TIMESTAMP_X",
        "MODIFIED_BY",
        "PREVIEW_PICTURE",
        "DETAIL_PICTURE",
        "DETAIL_PAGE_URL",
        "PROPERTY_AUDITORS",
        //"*","UF_*","PROPERTY_*"
    );
    //WHERE
    $arFilter = array(
        "IBLOCK_ID" => 59,//reports
        "ID" => $arParams["REPORT_ID"],
        "ACTIVE_DATE" => "Y",
        "ACTIVE" => "Y",
        "CHECK_PERMISSIONS" => "Y",
    );
    if ($arParams["PARENT_SECTION"] > 0) {
        $arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
        $arFilter["INCLUDE_SUBSECTIONS"] = "Y";
    }
    //ORDER BY
    $arSort = array(
        "RAND" => "ASC",
    );
    //EXECUTE
    $rsIBlockElement = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
    $rsIBlockElement->SetUrlTemplates($arParams["DETAIL_URL"]);


    $managers = array();


    if ($arResult = $rsIBlockElement->GetNext()) {
        if(!empty($arResult["PROPERTY_AUDITORS_VALUE"])){

            $managers["AUDITORS"] = $arResult["PROPERTY_AUDITORS_VALUE"];
        }


    }

    if(empty($managers["AUDITORS"])) $managers["AUDITORS"]=Array(0);    //to show auditors panel



// установим строковый параметр
    //COption::SetOptionString("itees.performance_sheet", "general_buh", 1125);
// получим строковый параметр
    $buh = COption::GetOptionString("itees.performance_sheet", "general_buh", "DEFAULT_VALUE");
    //1125
    //general_buh


    $arResult1 = Array(
        "LOGGED_IN_USER" => $USER->GetID(),
        "DEFER_LOAD" => "N",
        "NAME_TEMPLATE" => "#LAST_NAME# #NAME#",
        "IS_IFRAME" => "",
        "INNER_HTML" => "N",
        "BLOCKS" => Array(),
        "BLOCK" => "user-view",
        "TEMPLATE_FOLDER" => "/bitrix/components/bitrix/tasks.task.detail.parts/templates/flat/user-view/",
        "EXTENSION_ID" => "b77fd41e1e5567606ab911321bbf96e5"
    );

    //see functions init.php
    $managers["BUH"] = $buh;
    $managers["RESPONSIBLE"] = $arResult["CREATED_BY"];
    $managers["CHIEF"] = getBitrixUserManager($arResult["CREATED_BY"])[0];
    $managers = array_reverse($managers);


        foreach($managers as $role=>$manager_id){

            $tmp = Array(
                "TEMPLATE_DATA" => Array
                (
                    //"ROLE" => "ORIGINATOR",
                    //"ROLE" => "RESPONSIBLE",
                    "ROLE" => $role,
                    "ITEMS" => Array
                    (
                        "DATA" => Array
                        (

                        ),

                    ),
                    "TASK_CAN_EDIT" => 0,
                    "TASK_ID" => 1,
                    "EDITABLE" => 1
                )
            );

            if(is_array($manager_id)){




                foreach($manager_id as $m_i=>$man){

                    $rsUser = CUser::GetByID($man);
                    $arUser = $rsUser->Fetch();
                    $tmp['TEMPLATE_DATA']['ITEMS']['DATA'][$man] = $arUser;
                    $tmp['TEMPLATE_DATA']['TASK_CAN_EDIT'] = 1;
                    $tmp['TEMPLATE_DATA']['MULTIPLE'] = 1;

            }





            }else{
                $rsUser = CUser::GetByID($manager_id);
                $arUser = $rsUser->Fetch();


                $tmp['TEMPLATE_DATA']['ITEMS']['DATA'][$manager_id] = $arUser;



            }
            $arResult1["ROWS"][] = $tmp;
        }
        $arResult = array_merge($arResult, $arResult1);


    $this->IncludeComponentTemplate();
    /*
    $this->SetResultCacheKeys(array("ROWS"));
    if (empty($arResult)) {
        $this->AbortResultCache();
    }
    */
}


//C_AS::dmp($arResult);

?>
