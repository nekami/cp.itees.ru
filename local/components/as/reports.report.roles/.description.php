<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("T_REPORT_ROLES_NAME"),
	"DESCRIPTION" => GetMessage("T_REPORT_ROLES_DESC"),
	"ICON" => "/images/photo_view.gif",
	"CACHE_PATH" => "Y",
	"SORT" => 40,
	"PATH" => array(
		"ID" => "crm",
		'CHILD' => array(
			'ID' => 'report',
			'NAME' => GetMessage('CRM_REPORT'),
			"SORT" => 20,
		)

	),
);

?>