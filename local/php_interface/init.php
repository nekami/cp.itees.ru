<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
// ��������� �������� ���� ��� �������� �������
IncludeModuleLangFile(__FILE__);

$landing = strpos(\Bitrix\Main\Context::getCurrent()->getRequest()->getHeader('referer'), 'cp.itees.ru') === false ? true : false;

include @($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/itees.php");
include @($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/config.php");
include @($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/forGrade.php");// �������� �� ����������
include @($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/GradeSocialNetwork.php");// ���������� �������� � ���� ���. ����
include_once @($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/include/classes/domains_monitor.php");//������� �������
include_once @($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/include/classes/keys.php");//����� ������� ���������� � ������������ ������ ��������
include @($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/include/property_user_group.php");
include @($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/include/property_crm_client.php");
include @($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/include/property_crm_product.php");
include_once @($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/journal_leads.php");
include @($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/uf_iblock_element.php');
include @($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/uf_iblock_element_list.php');
include @($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/uf_ticket.php');
include @($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/functions.php');
include @($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/handlers_main.php');
include @($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/handlers_support.php');
include @($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/handlers_tasks.php');
include @($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/handlers_idea.php');
include @(__DIR__ . '/bitrix/php_interface/include/handlers_deals.php');
include @($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/handlers_billing.php');
include @($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/b24_expand_tariff_handlers.php');
include @($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/agents.php");
include @($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/App24/PollsService.php");
include @($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/App24/Partnership.php");

include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/custom_connectors/index.php');

include_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/learnaccess.php');

include_once(__DIR__ . "/include/tp_status_hendler.php");

$eventManager = Bitrix\Main\EventManager::getInstance();

AddEventHandler('main', 'OnEpilog', Array('TimeComment', 'AddTimeCommentFormClientSideCode'));
AddEventHandler("main", "OnBeforeEventAdd", "OnBeforeEventAddHandler");
AddEventHandler("main", 'OnFileCopy', 'OnFileCopyHandler');
AddEventHandler('main', 'OnEndBufferContent', 'OnEndBufferContentHandler');

AddEventHandler('crm', 'OnBeforeCrmWebformResultAdd', 'OnBeforeCrmWebformResultAddHandler');
AddEventHandler("crm", "OnBeforeCrmLeadAdd", "OnBeforeCrmLeadAddHandler");
AddEventHandler("crm", "OnBeforeCrmInvoiceUpdate", "OnBeforeCrmInvoiceUpdateHandler");
AddEventHandler("crm", "OnBeforeCrmInvoiceAdd", "OnBeforeCrmInvoiceUpdateHandler");
AddEventHandler("crm", "OnAfterCrmInvoiceDelete", "OnAfterCrmInvoiceDeleteHandler");
AddEventHandler("crm", "OnAfterCrmControlPanelBuild", "OnBeforeCrmControlPanelBuildHandler");

AddEventHandler("crm", "OnAfterCrmDealAdd", "OnAfterCrmDealB24Handler");
AddEventHandler("crm", "OnAfterCrmDealUpdate", "OnAfterCrmDealB24Handler");

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "OnBeforeIBlockElementUpdateHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", "OnBeforeIBlockElementDeleteHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "OnBeforeIBlockElementAddHandler");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAddHandler");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "OnAfterIBlockElementUpdateHandler");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "changeSectionElement");
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "addLicense");

AddEventHandler("sale", "OnBeforeOrderAdd", "OnBeforeOrderAddHandler");
AddEventHandler("sale", "OnOrderAdd", "OnOrderAddHandler");
AddEventHandler("sale", "OnSaleBeforeStatusOrder", "OnSaleBeforeStatusOrderHandler");
AddEventHandler("iblock", "OnBeforeOrderUpdate", "OnBeforeOrderUpdateHandler");
AddEventHandler('sale', 'OnSaleStatusOrder', 'OnSaleStatusOrderHandler');

AddEventHandler("forum", "onBeforeMessageAdd", "onBeforeMessageAddHandler");

AddEventHandler("tasks", "OnBeforeTaskElapsedTimeAdd", Array("TaskEditTime", "OnBeforeTaskElapsedTimeAddHandler"));
AddEventHandler("tasks", "OnBeforeTaskElapsedTimeUpdate", Array("TaskEditTime", "OnBeforeTaskElapsedTimeUpdateHandler"));
AddEventHandler("tasks", "OnBeforeTaskUpdate", Array("TaskEditTime", "OnBeforeTaskUpdateHandler"));
AddEventHandler("tasks", "OnBeforeTaskAdd", Array("TaskEditTime", "OnBeforeTaskAddHandler"));
AddEventHandler('tasks', 'OnTaskElapsedTimeAdd', Array('TimeComment', 'ShowTimeCommentForm'));

$eventManager->addEventHandler("main", "OnBeforeMailSend", "OnBeforeMailSendHandler");
$eventManager->addEventHandler("socialnetwork", "OnParseSocNetComponentPath", "OnFillSocNetFeaturesList");
$eventManager->addEventHandler("socialnetwork", "OnFillSocNetMenu", "AddSocNetMenu");
$eventManager->addEventHandler("socialnetwork", "OnFillSocNetFeaturesList", "OnParseSocNetComponentPath");


if (!$landing) {
	include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/class/timecomment.php');
	$eventManager->addEventHandler('main', 'OnEpilog', 'JsInclude');
}

//����������� ���������� js
function JsInclude() {
	
	$arJsConfig = [ 
		'learning_search' => [
			'js' => '/dev/js/learning_search.js'
		] 
	]; 

	foreach ($arJsConfig as $ext => $arExt) { 
		\CJSCore::RegisterExt($ext, $arExt); 
	}
	
	CUtil::InitJSCore(array_keys($arJsConfig));
}

//������ �������� � ������ �� ��������� ��� �������� �����
function OnAfterCrmInvoiceDeleteHandler($ID)
{
    CModule::IncludeModule('iblock');
    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 59, "PROPERTY_INVOICE_ID" => $ID), false, false, array("ID"));
    if ($arElem = $res->GetNext(false, false))
        CIBlockElement::SetPropertyValuesEx($arElem['ID'], 59, array('INVOICE_ID' => '', 'REPORT_PASS_CLIENT' => 'N'));
}

function OnBeforeCrmInvoiceUpdateHandler(&$arFields)
{
    $inn = $arFields['INVOICE_PROPERTIES'][8];
    $kpp = $arFields['INVOICE_PROPERTIES'][9];

    if ($arFields['UF_COMPANY_ID']) {

        $res = CCrmCompany::GetList(Array(), Array('ID' => $arFields['UF_COMPANY_ID']), Array());
        if ($arCompany = $res->GetNext()) {
            $company_inn = $arCompany['UF_CRM_INN'];
            $company_kpp = $arCompany['UF_CRM_1422281639'];
            if ($company_inn) {
                $arFields['INVOICE_PROPERTIES'][8] = $arFields['PR_INVOICE_8'] = $company_inn;
            }
        }

        if ($inn && empty($company_inn)) {
            $CCrmCompany = new CCrmCompany();
            $arCompanyFields['UF_CRM_INN'] = (string)$inn;
            $CCrmCompany->Update($arFields['UF_COMPANY_ID'], $arCompanyFields);
        }

        if ($kpp && empty($company_kpp)) {
            $CCrmCompany = new CCrmCompany();
            $arCompanyFields['UF_CRM_1422281639'] = (string)$kpp;
            $CCrmCompany->Update($arFields['UF_COMPANY_ID'], $arCompanyFields);
        }
    }
}

function onBeforeMessageAddHandler($arFields, $strUploadDir)
{
    if ($arFields['FORUM_ID'] == 37 && $arFields['POST_MESSAGE'] != $arFields["XML_ID"]) {

        $already_sended_users = Array();
        $already_sended_users[] = $arFields['AUTHOR_ID'];    //exclude author
        CModule::IncludeModule('crm');
        CModule::IncludeModule('im');
        $report = preg_replace("/REPORT_/", "", $arFields["XML_ID"]);

        //--notyfy managers////////
        if (CModule::IncludeModule("iblock")) {
            $rs = CIBlockElement::GetList(
                array(),
                array(
                    "IBLOCK_ID" => 59,
                    "ID" => $report),
                false,
                false,
                array("ID", "NAME", "CREATED_BY", "PROPERTY_AUDITORS")
            );

            $repName = '';
            if ($ar = $rs->GetNext()) {

                $managers = array();
                $repName = $ar["NAME"];

                if (!empty($ar["PROPERTY_AUDITORS_VALUE"])) {
                    $managers["AUDITORS"] = $ar["PROPERTY_AUDITORS_VALUE"];
                }
                //see functions init.php
                $managers["CHIEF"] = getBitrixUserManager($ar["CREATED_BY"])[0];
                $managers["RESPONSIBLE"] = $ar["CREATED_BY"];

                $buh = COption::GetOptionString("itees.performance_sheet", "general_buh", "DEFAULT_VALUE");
                $managers["BUH"] = $buh;

                foreach ($managers as $role => $manager_id) {

                    if ($arFields["AUTHOR_ID"] == $manager_id) continue;
                    if (is_array($manager_id)) {        //multiple
                        foreach ($manager_id as $m_i => $man) {
                            if (in_array($man, $already_sended_users)) {
                                continue;
                            }
                            $already_sended_users[] = $man;
                            if ($arFields["AUTHOR_ID"] == $man) continue;
                            $arMessageFields = array(
                                "TO_USER_ID" => $man,
                                "FROM_USER_ID" => 0,
                                "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                                "NOTIFY_MODULE" => "im",
                                "NOTIFY_MESSAGE" => '��������� ���������� � ������ <a href="/itees/support_reports/reports/' . $report . '/">' . $repName . '</a> c� ��������� �������: ' . $arFields['POST_MESSAGE'],
                            );

                            CIMNotify::Add($arMessageFields);
                        }
                    } else {        //single
                        if (in_array($manager_id, $already_sended_users)) {
                            continue;
                        }
                        $already_sended_users[] = $manager_id;
                        $arMessageFields = array(
                            "TO_USER_ID" => $manager_id,
                            "FROM_USER_ID" => 0,
                            "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                            "NOTIFY_MODULE" => "im",
                            "NOTIFY_MESSAGE" => '��������� ���������� � ������ <a href="/itees/support_reports/reports/' . $report . '/">' . $repName . '</a> c� ��������� �������: ' . $arFields['POST_MESSAGE'],
                        );

                        CIMNotify::Add($arMessageFields);
                    }
                }
            }
        }

        preg_match_all('/\[USER\=([0-9]+)\]/', $arFields['POST_MESSAGE'], $arMatches);
        if (isset($arMatches[1])) {
            foreach ($arMatches[1] as $user_id) {
                if (in_array($user_id, $already_sended_users)) {
                    continue;
                }
                $already_sended_users[] = $user_id;
                $arMessageFields = array(
                    "TO_USER_ID" => $user_id,
                    "FROM_USER_ID" => 0,
                    "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                    "NOTIFY_MODULE" => "im",
                    "NOTIFY_MESSAGE" => '��� ��������� � ���������� � ������ <a href="/itees/support_reports/reports/' . $report . '/">' . $repName . '</a> c� ��������� �������: ' . $arFields['POST_MESSAGE'],
                );

                CIMNotify::Add($arMessageFields);
            }
        }
    }
}

function OnBeforeOrderUpdateHandler($ID, $arFields)
{
    global $APPLICATION;

    $arOrder = CSaleOrder::GetByID($ID);
    if ($arOrder['STATUS_ID'] == 'D') {
        CModule::IncludeModule('im');
        $arMessageFields = array(
            "TO_USER_ID" => $GLOBALS['USER']->GetId(),
            "FROM_USER_ID" => 0,
            "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
            "NOTIFY_MODULE" => "crm",
            "NOTIFY_MESSAGE" => '�� ������� �������� ������ �����! ��������� �������� ������ ������������ ������',
        );
        CIMNotify::Add($arMessageFields);

        $APPLICATION->throwException('�� ������� �������� ������ �����! ��������� �������� ������ ������������ ������');
        return false;
    }

    if (isset($arFields['STATUS_ID']) && $arFields['STATUS_ID'] != 'N' && $arFields['STATUS_ID'] != 'D') {
        CModule::IncludeModule('crm');
        CModule::IncludeModule('im');

        $INN = GetOrderPropertyValue($ID, 'INN');
        if (empty($INN)) {

            $arMessageFields = array(
                "TO_USER_ID" => $GLOBALS['USER']->GetId(),
                "FROM_USER_ID" => 0,
                "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                "NOTIFY_MODULE" => "crm",
                "NOTIFY_MESSAGE" => '�� ������� �������� ������ �����! � ������ ' . $arFields['STATUS_ID'] . ' ����� ���� ���������� ������ ����� � ����������� ���',
            );
            CIMNotify::Add($arMessageFields);

            $APPLICATION->throwException('� ������ ' . $arFields['STATUS_ID'] . ' ����� ���� ���������� ������ ����� � ����������� ���');
            return false;
        }
    }
}

function OnSaleBeforeStatusOrderHandler($ID, $val)
{
    global $APPLICATION;

    $arOrder = CSaleOrder::GetByID($ID);

    if ($arOrder['STATUS_ID'] == 'D') {
        CModule::IncludeModule('im');
        $arMessageFields = array(
            "TO_USER_ID" => $GLOBALS['USER']->GetId(),
            "FROM_USER_ID" => 0,
            "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
            "NOTIFY_MODULE" => "crm",
            "NOTIFY_MESSAGE" => '�� ������� �������� ������ �����! ��������� �������� ������ ������������ ������',
        );
        CIMNotify::Add($arMessageFields);
        $APPLICATION->throwException('�� ������� �������� ������ �����! ��������� �������� ������ ������������ ������');
        return false;
    }
}


//�������, ������� '_' � ����� ����� ��� ����������� ������ �� ��
function OnFileCopyHandler(&$arFile, $fileName)
{
    if ((substr($arFile['ORIGINAL_NAME'], -1) == '_') && (substr($fileName, -1) != '_')) {
        $arFile['ORIGINAL_NAME'] = substr($arFile['ORIGINAL_NAME'], 0, strlen($arFile['ORIGINAL_NAME']) - 1);
        $arFile['MODULE_ID'] = 'iblock';
    }
    return $arFile;
}


//�������� ��������� � ��������� ������ �����
function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
{
    if ("TICKET_CHANGE_FOR_TECHSUPPORT" == $event || "TICKET_NEW_FOR_TECHSUPPORT" == $event ||
        "TICKET_NEW_FOR_AUTHOR" == $event || "TICKET_CHANGE_BY_AUTHOR_FOR_AUTHOR" == $event ||
        "TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR" == $event || "TICKET_MESS_COPY_FOR_TECHSUPPORT" == $event) {
        $arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("SUPPORT", $arFields["ID"]);
        $companyID = $arUF["UF_COMPANY_ID"]["VALUE"];

        if (CModule::IncludeModule('crm')) {
            $arCompany = CCrmCompany::GetByID($companyID);

            $arFields["COMPANY_NAME"] = $arCompany["TITLE"];
        }
    }

    if ("TICKET_CHANGE_FOR_TECHSUPPORT" == $event || "TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR" == $event || "TICKET_CHANGE_BY_AUTHOR_FOR_AUTHOR" == $event) {
        if (preg_match("/��������� �������/", $arFields["WHAT_CHANGE"])) {
            if ("TICKET_CHANGE_FOR_TECHSUPPORT" == $event) {
                $event = "TICKET_CLOSE";
            } else {
                $event = "TICKET_CLOSE_FOR_AUTHOR";
            }
        }
    }

    if ($event == "TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR") {
        $arFields['FILES_LINKS_2'] = preg_replace("~(http|https|ftp|ftps)://(.*?)(\s|\n|[,.?!](\s|\n)|$)~", '<a href="$1://$2">$1://$2</a>$3', $arFields['FILES_LINKS']);

        $rsUser = CUser::GetByID($arFields['MESSAGE_AUTHOR_USER_ID']);
        if ($arUser = $rsUser->Fetch()) {
            $arFields['USER_PHOTO'] = CFile::GetPath($arUser['PERSONAL_PHOTO']);
        }

        if (!$arFields['USER_PHOTO']) {
            $arFields['USER_PHOTO'] = 'http://www.itees.ru/local/images/cp/support.jpg';
        }

        $arFields['WHAT_CHANGE'] = preg_replace('/\<|\>/', '', $arFields['WHAT_CHANGE']);

    }

    //����� �������� ����� �� ����� �� �� �������������� ������������� �������� ����������� �������� �� �������
    //������ �� ��� �������� ������ � ������ ��, �������� ������ �� ���������� ��������
    // (/ticket_show_file/ticket_show_file.php)
    $arFields['FILES_LINKS'] = str_replace('bitrix/tools/ticket_show_file', 'ticket_show_file/ticket_show_file', $arFields['FILES_LINKS']);
    $arTicket['IMAGE_LINK'] = str_replace('bitrix/tools/ticket_show_file', 'ticket_show_file/ticket_show_file', $arFields['IMAGE_LINK']);


    if (isset($_REQUEST['bbcode_val']) && $_REQUEST['bbcode_val'] == true) {
        if (!empty($arFields['MESSAGE_BODY']))    //bbcode convert
        {

            $obParser = new CTextParser;
            $arFields["MESSAGE_BODY"] = $obParser->convertText($arFields["MESSAGE_BODY"]);
            $arFields["MESSAGE_BODY"] = $obParser->convert4mail($arFields["MESSAGE_BODY"]);
            $arFields["MESSAGE_BODY"] = HTMLToTxt($arFields["MESSAGE_BODY"],
                "",
                array("'<a[^>]*?>'si"),
                false);

        }
    }
}


function OnBeforeCrmControlPanelBuildHandler(&$items)
{
    //��������� ������ ���� � ������� CRM
    $items[] = array('ID' => 'LOG', 'NAME' => '������ �������� �����', 'TITLE' => '������ �������� �����', 'URL' => '/crm/configs/log/', 'ICON' => 'settings');
    $items[] = array('ID' => 'SCRIPTS', 'NAME' => '��������� ��������', 'TITLE' => '��������� ��������', 'URL' => '/crm/configs/scripts/', 'ICON' => 'settings');
}


function OnBeforeOrderAddHandler($arFields)
{
    if ($_GET['type'] == 'sale' && $_GET['mode'] == 'import') {
        $arFields['USER_ID'] = '514';
    }
}

function OnOrderAddHandler($ID, $arFields)
{
    if (CModule::IncludeModule('iblock')) {
        $el = new CIBlockElement;
        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => 75,
            "NAME" => $ID,
            "ACTIVE" => "Y",
        );
        $el->Add($arLoadProductArray);
    }

    if ($_GET['type'] == 'sale' && $_GET['mode'] == 'import') {
        global $DB;
        $strSql = "SELECT * FROM b_xml_tree";
        $res = $DB->Query($strSql);
        while ($arRes = $res->Fetch()) {
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/b_xml_tree.txt", print_r($arRes, true), FILE_APPEND);
        }
    }
}


function AddValueFieldToTask($task_id, $ar)
{
    $obTask = new CTasks;
    $obTask->Update($task_id, $ar);
}

function GetChildsCustom($task_id, &$arChilds = Array())
{
    $res = CTasks::GetList(
        Array(),
        Array("PARENT_ID" => $task_id)
    );

    while ($arTask = $res->GetNext()) {
        $arChilds[] = $arTask["ID"];
        GetChildsCustom($arTask["ID"], $arChilds);
    }
}


function OnAfterIBlockElementAddHandler(&$arFields)
{
    SetNoGarantSumm($arFields);

    if (CModule::IncludeModule("tasks") && $arFields["IBLOCK_ID"] == 59) {
        if (!empty($arFields["PROPERTY_VALUES"][288])) {
            foreach ($arFields["PROPERTY_VALUES"][288] as $arTask) {
                $arTFields = Array(
                    "UF_REPORTS" => Array($arFields["ID"])
                );
                $obTask = new CTasks;
                $obTask->Update($arTask["VALUE"], $arTFields);
            }
        }

        $res = CIBlockElement::GetList(Array(), Array('ID' => $arFields['ID']), false, false, Array("ID", "PROPERTY_CLIENT_ID"));
        while ($arFields2 = $res->GetNext()) {
            if ($arFields2['PROPERTY_CLIENT_ID_VALUE']) {
                CModule::IncludeModule("crm");

                $res2 = CCrmCompany::GetList(Array(), Array("ID" => $arFields2['PROPERTY_CLIENT_ID_VALUE'], "CHECK_PERMISSIONS" => "N"), $arSelect = Array("TITLE"));
                if ($arTask = $res2->GetNext()) {
                    $CLIENT_NAME = $arTask["TITLE"];
                    CIBlockElement::SetPropertyValuesEx($arFields2['ID'], 59, Array("CLIENT_NAME" => $CLIENT_NAME));
                }
            }
        }
    }
}

function OnBeforeIBlockElementAddHandler(&$arFields)
{
    global $APPLICATION;

    if ($arFields["IBLOCK_ID"] == 64)    //SEO:��������� �������
    {
        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "NAME" => $arFields["NAME"]), false, false, Array("ID"));
        if ($arRes = $res->GetNext()) {
            $APPLICATION->ThrowException("����� ��������� ������� ��� ����������");
            return false;
        }

        if (count($arFields["PROPERTY_VALUES"][385])) {
            foreach ($arFields["PROPERTY_VALUES"][385] as $k => &$baseId) {
                $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "ID" => $baseId), false, false, Array("ID", "PROPERTY_SYNONYM"));
                if ($arRes2 = $res->GetNext()) {
                    if (intval($arRes2["PROPERTY_SYNONYM_VALUE"])) {
                        $baseId = $arRes2["PROPERTY_SYNONYM_VALUE"];
                    }
                }
            }
        }
    } elseif ($arFields["IBLOCK_ID"] == 67)    //SEO:�����
    {
        $flag = false;
        foreach ($arFields["PROPERTY_VALUES"][392] as $k => &$se) {
            if ($se && $se['VALUE']) {
                $flag = true;
            }
        }

        if (!$flag) {
            $arFilter = array(
                "IBLOCK_CODE" => "seo_search_engines",
                "ACTIVE" => "Y",
                "ACTIVE_DATE" => "Y",
                "PROPERTY_SYNONYM" => false
            );

            $arSelect = array(
                "ID",
            );

            $db_elements = CIblockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
            while ($arElement = $db_elements->GetNext()) {
                $arFields["PROPERTY_VALUES"][392][] = $arElement['ID'];
            }
        }
    } elseif ($arFields["IBLOCK_ID"] == 71) {
        $domain = $arFields['NAME'];
        if (strlen($domain)) {
            $db_elements = CIblockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "NAME" => $domain), false, false, Array('ID'));
            if ($arElement = $db_elements->GetNext()) {
                $APPLICATION->ThrowException("����� ����� ��� ����������");
                return false;
            }
        }
    } elseif ($arFields["IBLOCK_ID"] == 59)    //���������:������
    {
        if (CModule::IncludeModule('crm') && CModule::IncludeModule('iblock') && CModule::IncludeModule('socialnetwork')) {
            $res = CSocNetGroup::GetList(
                array(),
                array('ID' => $arFields["PROPERTY_VALUES"]["GROUP_ID"]),
                false,
                false,
                array('DATE_CREATE', 'UF_DEAL')
            );
            while ($arGroup = $res->Fetch()) {
                $date = $arGroup['DATE_CREATE'];

                $y = date('Y', strtotime($date));

                if ($y >= 2016)    //������� ���, ����� ������ ���� ��������� ������, ���� �� ����������� ������
                {
                    if (
                        empty($arGroup['UF_DEAL'])
                        &&
                        !empty($arFields["PROPERTY_VALUES"]["PROJECT"])
                    ) {    //������ �� �������� ������
                        // ��� ������ ������ �������� ������ ��� ������� �� �������� (����� ����������� �������� �������� PROJECT � ������)
                        $APPLICATION->ThrowException("� 2016 ������ ������� �����, � �� ������������ �������.");
                        return false;
                    }
                }
            }
        }

    }

    if (CModule::IncludeModule("tasks") && CModule::IncludeModule("iblock") && $arFields["IBLOCK_ID"] == 59 && !empty($arFields["PROPERTY_VALUES"][288])) {
        foreach ($arFields["PROPERTY_VALUES"][288] as $arTask) {
            if (intval($arTask["VALUE"]) > 0) {
                $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 59, "PROPERTY_TASK_ID" => intval($arTask["VALUE"])), false, false, Array("ID", "PROPERTY_TASK_ID"));
                if ($arRes = $res->GetNext()) {
                    $APPLICATION->ThrowException("������ �" . $arTask["VALUE"] . " ��� ������������ � ������ �" . $arRes["ID"]);
                    return false;
                }
            }
        }
    }
}


function OnBeforeIBlockElementDeleteHandler($ID)
{
    CModule::IncludeModule("iblock");
    CModule::IncludeModule("tasks");

    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 64, "ID" => $ID), false, false, Array("ID"));
    if ($ar_res = $res->GetNext()) {
        $arFilter = Array("IBLOCK_ID" => 70, "PROPERTY_search_engine" => $ar_res['ID']);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("ID"));
        while ($arPhrase = $res->GetNext()) {
            CIBlockElement::Delete($arPhrase["ID"]);
        }
    }

    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 70, "ID" => $ID), false, false, Array("ID"));
    if ($ar_res = $res->GetNext()) {
        $arFilter = Array("IBLOCK_ID" => 69, "PROPERTY_phrase" => $ar_res['ID']);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("ID"));
        while ($arRep = $res->GetNext()) {
            CIBlockElement::Delete($arRep["ID"]);
        }
    }

    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 67, "ID" => $ID), false, false, Array("ID"));
    if ($ar_res = $res->GetNext()) {
        $arFilter = Array("IBLOCK_ID" => 70, "PROPERTY_site" => $ar_res['ID']);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("ID"));
        while ($arPhrase = $res->GetNext()) {
            CIBlockElement::Delete($arPhrase["ID"]);
        }
    }

    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 59, "ID" => $ID), false, false, Array("ID", "PROPERTY_TASK_ID"));
    if ($arRes = $res->GetNext()) {
        foreach ($arRes["PROPERTY_TASK_ID_VALUE"] as $task_id) {
            $arTFields = Array(
                "UF_REPORTS" => Array()
            );
            $obTask = new CTasks;
            $obTask->Update($task_id, $arTFields);
        }
    }
}

function SetNoGarantSumm($arFields)
{
    if ($arFields['IBLOCK_ID'] == 62 && $arFields['CODE'] == 'nogarant_time') {
        if (isset($arFields['PROPERTY_VALUES']['price']) && intval($arFields['PROPERTY_VALUES']['price']) == 0) {
            $summ = 0;
            $res = CIBlockElement::GetList(Array(), Array('IBLOCK_ID' => $arFields['IBLOCK_ID'], 'PROPERTY_report' => $arFields['PROPERTY_VALUES']['report'], '!PROPERTY_garant' => 'Y', '!CODE' => Array('garant_time', 'nogarant_time')), false, false, Array());
            while ($ob = $res->GetNextElement()) {
                $arProps = $ob->GetProperties();
                $summ += $arProps['price']['VALUE'];
            }

            CIBlockElement::SetPropertyValuesEx($arFields['ID'], $arFields['IBLOCK_ID'], array('price' => number_format($summ, 2, '.', '')));
        }
    }
}

function OnAfterIBlockElementUpdateHandler(&$arFields)
{
    SetNoGarantSumm($arFields);

    if ($arFields['IBLOCK_ID'] == 59) {
        $c = 0;
        $res = CIBlockElement::GetList(Array(), Array("ID" => $arFields["ID"]), false, false, Array("ID", "PROPERTY_TASK_ID"));
        while ($ar_res = $res->GetNext()) {
            foreach ($ar_res['PROPERTY_TASK_ID_VALUE'] as $task_id) {
                if ($task_id > 0)
                    $c++;
            }
        }

        if ($c === 0) {
            CIBlockElement::Delete($arFields["ID"]);
        }

        $res = CIBlockElement::GetList(Array(), Array('ID' => $arFields['ID']), false, false, Array("ID", "PROPERTY_CLIENT_ID"));
        while ($arFields2 = $res->GetNext()) {
            if ($arFields2['PROPERTY_CLIENT_ID_VALUE']) {
                CModule::IncludeModule("crm");

                $res2 = CCrmCompany::GetList(Array(), Array("ID" => $arFields2['PROPERTY_CLIENT_ID_VALUE'], "CHECK_PERMISSIONS" => "N"), $arSelect = Array("TITLE"));
                if ($arTask = $res2->GetNext()) {
                    $CLIENT_NAME = $arTask["TITLE"];
                    CIBlockElement::SetPropertyValuesEx($arFields2['ID'], 59, Array("CLIENT_NAME" => $CLIENT_NAME));
                }
            }
        }
    }
}

function SetGarant($el_id)
{
    if (intval($el_id) > 0) {
        CModule::IncludeModule('iblock');
        global $USER;

        $arLoadProductArray = Array(
            "MODIFIED_BY" => $USER->GetID(),
            "SORT" => '10',
        );

        $el = new CIBlockElement;
        $el->Update($el_id, $arLoadProductArray);
        CIBlockElement::SetPropertyValueCode($el_id, "garant", "Y");

        $res = CIBlockElement::GetProperty(62, $el_id, Array(), array("CODE" => "report"));
        $report = $res->GetNext();

        $res = CIBlockElement::GetProperty(62, $el_id, Array(), array("CODE" => "elapsed_time"));
        $elapsed_time = $res->GetNext();

        $res = CIBlockElement::GetProperty(62, $el_id, Array(), array("CODE" => "price"));
        $price = $res->GetNext();

        $res = CIBlockElement::GetProperty(62, $el_id, Array(), array("CODE" => "hour_price"));
        $hour_price = $res->GetNext();

        $res2 = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "CODE" => "garant_time", "PROPERTY_report" => $report['VALUE']), false, false, Array("ID", "PROPERTY_elapsed_time"));
        if ($ar_fields = $res2->GetNext()) {
            CIBlockElement::SetPropertyValueCode($ar_fields["ID"], "elapsed_time", $ar_fields["PROPERTY_ELAPSED_TIME_VALUE"] + $elapsed_time['VALUE']);
            $res3 = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "CODE" => "nogarant_time", "PROPERTY_report" => $report['VALUE']), false, false, Array("ID", "PROPERTY_elapsed_time", "PROPERTY_price", "PROPERTY_summ_changed_by_hands"));
            if ($ar_fields3 = $res3->GetNext()) {
                CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "elapsed_time", intval($ar_fields3["PROPERTY_ELAPSED_TIME_VALUE"]) - intval($elapsed_time['VALUE']));
                if ($ar_fields3["PROPERTY_SUMM_CHANGED_BY_HANDS_VALUE"] != '��') {
                    if (floatval($price['VALUE']) > 0) {
                        CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "price", number_format(floatval($ar_fields3["PROPERTY_PRICE_VALUE"]) - floatval($price['VALUE']), 2, '.', ''));
                    } elseif (floatval($hour_price['VALUE']) > 0) {
                        CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "price", number_format(floatval($ar_fields3["PROPERTY_PRICE_VALUE"]) - floatval($hour_price['VALUE']) * intval($elapsed_time['VALUE']) / 60, 2, '.', ''));
                    }
                }
            }
        } else {
            $el = new CIBlockElement;
            $PROP = array();
            $PROP['report'] = $report['VALUE'];
            $PROP['elapsed_time'] = $elapsed_time['VALUE'];

            $arLoadProductArray = Array(
                "MODIFIED_BY" => $USER->GetID(),
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID" => 62,
                "PROPERTY_VALUES" => $PROP,
                "NAME" => "����� �� ����������� �������",
                "CODE" => 'garant_time',
                "ACTIVE" => "Y",
                "SORT" => "30"
            );

            $el->Add($arLoadProductArray);

            $res3 = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "CODE" => "nogarant_time", "PROPERTY_report" => $report['VALUE']), false, false, Array("ID", "PROPERTY_elapsed_time", "PROPERTY_price", "PROPERTY_summ_changed_by_hands"));
            if ($ar_fields3 = $res3->GetNext()) {
                CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "elapsed_time", intval($ar_fields3["PROPERTY_ELAPSED_TIME_VALUE"]) - intval($elapsed_time['VALUE']));
                if ($ar_fields3["PROPERTY_SUMM_CHANGED_BY_HANDS_VALUE"] != '��') {
                    if (floatval($price['VALUE']) > 0) {
                        CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "price", number_format(floatval($ar_fields3["PROPERTY_PRICE_VALUE"]) - floatval($price['VALUE']), 2, '.', ''));
                    } elseif (floatval($hour_price['VALUE']) > 0) {
                        CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "price", number_format(floatval($ar_fields3["PROPERTY_PRICE_VALUE"]) - floatval($hour_price['VALUE']) * intval($elapsed_time['VALUE']) / 60, 2, '.', ''));
                    }
                }
            }
        }
    }
}

function SetNoGarant($el_id)
{
    if (intval($el_id) > 0) {
        CModule::IncludeModule('iblock');
        global $USER;

        $res = CIBlockElement::GetProperty(62, $el_id, Array(), array("CODE" => "report"));
        if ($report = $res->GetNext()) {
            $res1 = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "CODE" => "nogarant_time", "PROPERTY_report" => $report['VALUE']), false, false, Array("ID", "SORT"));
            if ($ar_fields1 = $res1->GetNext()) {
                $sort = $ar_fields1['SORT'] - 1;
            } else {
                $res2 = CIBlockElement::GetList(Array('SORT' => 'DESC'), Array("IBLOCK_ID" => 62, "PROPERTY_report" => $report['VALUE']), false, Array("nPageSize" => 1), Array("ID", "SORT"));
                if ($ar_fields2 = $res2->GetNext()) {
                    $sort = $ar_fields2['SORT'] + 10;
                }
            }

            $arLoadProductArray = Array(
                "MODIFIED_BY" => $USER->GetID(),
                "SORT" => $sort,
            );

            $el = new CIBlockElement;
            $el->Update($el_id, $arLoadProductArray);
            CIBlockElement::SetPropertyValueCode($el_id, "garant", "N");

            $res = CIBlockElement::GetProperty(62, $el_id, Array(), array("CODE" => "elapsed_time"));
            $elapsed_time = $res->GetNext();

            $res = CIBlockElement::GetProperty(62, $el_id, Array(), array("CODE" => "price"));
            $price = $res->GetNext();

            $res = CIBlockElement::GetProperty(62, $el_id, Array(), array("CODE" => "hour_price"));
            $hour_price = $res->GetNext();

            $res2 = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "CODE" => "nogarant_time", "PROPERTY_report" => $report['VALUE']), false, false, Array("ID", "PROPERTY_elapsed_time"));
            if ($ar_fields = $res2->GetNext()) {
                CIBlockElement::SetPropertyValueCode($ar_fields["ID"], "elapsed_time", $ar_fields["PROPERTY_ELAPSED_TIME_VALUE"] + $elapsed_time['VALUE']);
                $res3 = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "CODE" => "garant_time", "PROPERTY_report" => $report['VALUE']), false, false, Array("ID", "PROPERTY_elapsed_time", "PROPERTY_price", "PROPERTY_summ_changed_by_hands"));
                if ($ar_fields3 = $res3->GetNext()) {
                    CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "elapsed_time", intval($ar_fields3["PROPERTY_ELAPSED_TIME_VALUE"]) - intval($elapsed_time['VALUE']));
                    if ($ar_fields3["PROPERTY_SUMM_CHANGED_BY_HANDS_VALUE"] != '��') {
                        if (floatval($price['VALUE']) > 0) {
                            CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "price", number_format(floatval($ar_fields3["PROPERTY_PRICE_VALUE"]) - floatval($price['VALUE']), 2, '.', ''));
                        } elseif (floatval($hour_price['VALUE']) > 0) {
                            CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "price", number_format(floatval($ar_fields3["PROPERTY_PRICE_VALUE"]) - floatval($hour_price['VALUE']) * intval($elapsed_time['VALUE']) / 60, 2, '.', ''));
                        }
                    }
                }
            } else {
                $el = new CIBlockElement;
                $PROP = array();
                $PROP['report'] = $report['VALUE'];
                $PROP['elapsed_time'] = $elapsed_time['VALUE'];

                $arLoadProductArray = Array(
                    "MODIFIED_BY" => $USER->GetID(),
                    "IBLOCK_SECTION_ID" => false,
                    "IBLOCK_ID" => 62,
                    "PROPERTY_VALUES" => $PROP,
                    "NAME" => "����� �� ������������� �������",
                    "CODE" => 'nogarant_time',
                    "ACTIVE" => "Y",
                    "SORT" => $sort + 10
                );

                $el->Add($arLoadProductArray);

                $res3 = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "CODE" => "garant_time", "PROPERTY_report" => $report['VALUE']), false, false, Array("ID", "PROPERTY_elapsed_time", "PROPERTY_price", "PROPERTY_summ_changed_by_hands"));
                if ($ar_fields3 = $res3->GetNext()) {
                    CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "elapsed_time", intval($ar_fields3["PROPERTY_ELAPSED_TIME_VALUE"]) - intval($elapsed_time['VALUE']));
                    if ($ar_fields3["PROPERTY_SUMM_CHANGED_BY_HANDS_VALUE"] != '��') {
                        if (floatval($price['VALUE']) > 0) {
                            CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "price", number_format(floatval($ar_fields3["PROPERTY_PRICE_VALUE"]) - floatval($price['VALUE']), 2, '.', ''));
                        } elseif (floatval($hour_price['VALUE']) > 0) {
                            CIBlockElement::SetPropertyValueCode($ar_fields3["ID"], "price", number_format(floatval($ar_fields3["PROPERTY_PRICE_VALUE"]) - floatval($hour_price['VALUE']) * intval($elapsed_time['VALUE']) / 60, 2, '.', ''));
                        }
                    }
                }
            }
        }
    }
}

function OnBeforeIBlockElementUpdateHandler(&$arFields)
{
    global $APPLICATION;
    if ($arFields["IBLOCK_ID"] == 64) {
        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "NAME" => $arFields["NAME"], "!ID" => $arFields['ID']), false, false, Array("ID"));
        if ($arRes = $res->GetNext()) {
            $APPLICATION->ThrowException("��������� ������� � ����� ��������� ��� ����������");
            return false;
        }

        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "PROPERTY_SYNONYM" => $arFields['ID']), false, false, Array("ID", "NAME"));
        if ($arRes = $res->GetNext()) {
            if (count($arFields["PROPERTY_VALUES"][385])) {
                foreach ($arFields["PROPERTY_VALUES"][385] as $k => $baseId) {
                    if (!empty($baseId['VALUE'])) {
                        $APPLICATION->ThrowException("�������� ��������! ��������� ������� �������� ������� ��� " . $arRes["NAME"]);
                        return false;
                    }
                }
            }
        }

        if (count($arFields["PROPERTY_VALUES"][385])) {
            foreach ($arFields["PROPERTY_VALUES"][385] as $k => &$baseId) {
                $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "ID" => $baseId), false, false, Array("ID", "PROPERTY_SYNONYM"));
                if ($arRes2 = $res->GetNext()) {
                    if (intval($arRes2["PROPERTY_SYNONYM_VALUE"])) {
                        $baseId = $arRes2["PROPERTY_SYNONYM_VALUE"];
                    }
                }
            }
        }
    }


    if ($arFields["IBLOCK_ID"] == 62) {
        $arProp = $arFields["PROPERTY_VALUES"][298];
        if (is_array($arProp)) {
            reset($arProp);
            $val = current($arProp);

            if ($val['VALUE'] == "Y") {
                $res = CIBlockElement::GetProperty(62, $arFields['ID'], Array(), array("CODE" => "garant"));
                if ($ob = $res->GetNext()) {
                    if ($ob['VALUE'] != "Y") {
                        SetGarant($arFields['ID']);
                        $arFields['SORT'] = "10";
                    }
                }
            }
        }
    } elseif ($arFields["IBLOCK_ID"] == 71) {
        $domain = $arFields['NAME'];
        if (strlen($domain)) {
            $db_elements = CIblockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "NAME" => $domain, "!ID" => $arFields["ID"]), false, false, Array('ID'));
            if ($arElement = $db_elements->GetNext()) {
                $APPLICATION->ThrowException("����� ����� ��� ����������");
                return false;
            }
        }
    }

    if (CModule::IncludeModule("tasks") && CModule::IncludeModule("iblock") && $arFields["IBLOCK_ID"] == 59 && !empty($arFields["PROPERTY_VALUES"][288])) {
        foreach ($arFields["PROPERTY_VALUES"][288] as $arTask) {
            if (intval($arTask["VALUE"]) > 0) {
                $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 59, "PROPERTY_TASK_ID" => $arTask["VALUE"], "!ID" => $arFields["ID"]), false, false, Array("ID", "PROPERTY_TASK_ID"));
                if ($arRes = $res->GetNext()) {
                    $APPLICATION->ThrowException("������ �" . $arTask["VALUE"] . " ��� ������������ � ������ �" . $arRes["ID"]);
                    return false;
                }
            }
        }

        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 59, "ID" => $arFields["ID"]), false, false, Array("ID", "PROPERTY_CLIENT_ID", "PROPERTY_TASK_ID"));
        if ($arRes = $res->GetNext()) {
            // ������� �������� �������� "�����" � ������, ���� ��� ��������� �� ������
            foreach ($arRes["PROPERTY_TASK_ID_VALUE"] as $task_id) {
                $flag = false;
                foreach ($arFields["PROPERTY_VALUES"][288] as $arTask) {
                    if ($task_id == $arTask["VALUE"]) {
                        $flag = true;
                    }
                }
                if ($flag == false) {
                    $arTFields = Array(
                        "UF_REPORTS" => Array()
                    );
                    $obTask = new CTasks;
                    $obTask->Update($task_id, $arTFields);
                }
            }
        }

        // ��������� �������� �������� "�����" � ������, ���� ��� ������������ � �����
        foreach ($arFields["PROPERTY_VALUES"][288] as $arTask) {
            $arTFields = Array(
                "UF_REPORTS" => Array($arFields["ID"])
            );
            $obTask = new CTasks;
            $obTask->Update($arTask["VALUE"], $arTFields);

            $arTasks[] = $arTask["VALUE"];
        }

        $arPrintFormTasks = Array();
        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "PROPERTY_report" => $arFields["ID"]), false, false, Array("ID", "PROPERTY_task_id"));
        while ($arRes = $res->GetNext()) {
            $arPrintFormTasks[] = $arRes["PROPERTY_TASK_ID_VALUE"];
        }
    }
}


function checkCyrillicSymbols($text)
{
    if (preg_match("/[�-��-�]+/", $text)) {
        return true;
    } else {
        return false;
    }
}

function OnBeforeCrmWebformResultAddHandler($WEB_FORM_ID, &$arFields, &$arrVALUES)
{
    if ($_REQUEST['form_id'] == 4) {    //crm.webform http://www.itees.ru/test/as_index.php


        $dbRes = CIBlockSection::GetList(
            array('active_from' => 'desc'),
            array(
                'IBLOCK_ID' => COption::GetOptionInt('intranet', 'iblock_structure'),
                'NAME' => 'HR-�����'//,   'UF_HEAD' => 478,
            ),
            false,
            array('UF_*')
        );
        $structure = array();
        while ($arRes = $dbRes->Fetch()) {
            $structure[] = $arRes;
        }
        if (!empty($structure[0]['UF_HEAD'])) {
            $arFields['ASSIGNED_BY_ID'] = $structure[0]['UF_HEAD'];
        }
    }
}

function OnBeforeCrmLeadAddHandler(&$arFields)
{

    if (checkCyrillicSymbols($arFields['TITLE']) or checkCyrillicSymbols($arFields['COMMENTS']) or checkCyrillicSymbols($arFields['NAME']) or checkCyrillicSymbols($arFields['FULL_NAME'])) {

    } else {
        if ($arFields['SOURCE_ID'] != 7 && $arFields['SOURCE_ID'] != 6)
            return false;
    }
}

function __AddWPButton()
{
    if ($GLOBALS["USER"]->IsAdmin()) {
        $GLOBALS["APPLICATION"]->SetAdditionalCSS("/bitrix/wizards/bitrix/portal/css/panel.css");
        $arMenu = Array(
            Array(
                "ACTION" => "jsUtils.Redirect(arguments, '" . CUtil::JSEscape("/bitrix/admin/wizard_install.php?lang=ru&wizardName=bitrix:portal&" . bitrix_sessid_get()) . "')",
                "ICON" => "wizard",
                "TITLE" => "��������� ������ ����� ������� � ��������� �������",
                "TEXT" => '<b>������ ���������</b>',
            ),
            Array(
                "ACTION" => "jsUtils.Redirect(arguments, '" . CUtil::JSEscape("/bitrix/admin/wizard_install.php?lang=ru&wizardName=bitrix:portal_clear&" . bitrix_sessid_get()) . "')",
                "ICON" => "wizard-clear",
                "TITLE" => "��������� ������ ������� ����-������",
                "TEXT" => '������ �������',
            ),
        );
        $GLOBALS["APPLICATION"]->AddPanelButton(array(
            "HREF" => "/bitrix/admin/wizard_install.php?lang=ru&wizardName=bitrix:portal&" . bitrix_sessid_get(),
            "ID" => "portal_wizard",
            "ICON" => "icon-wizard",
            "ALT" => "��������� ������ ����� ������� � ��������� �������",
            "TEXT" => '������ ���������',
            "MAIN_SORT" => 325,
            "SORT" => 20,
            "MENU" => $arMenu,
        ));

    }
}

function get_rus_month($number = false)
{
    if ($number === false) {
        $number = date("n");
    }

    switch (intval($number)) {
        case 1:
            $m = '������';
            break;
        case 2:
            $m = '�������';
            break;
        case 3:
            $m = '����';
            break;
        case 4:
            $m = '������';
            break;
        case 5:
            $m = '���';
            break;
        case 6:
            $m = '����';
            break;
        case 7:
            $m = '����';
            break;
        case 8:
            $m = '������';
            break;
        case 9:
            $m = '��������';
            break;
        case 10:
            $m = '�������';
            break;
        case 11:
            $m = '������';
            break;
        case 12:
            $m = '�������';
            break;
        default:
            $m = false;
    }
    return $m;
}

function addTaskToReport($task_id, $report_id)
{
    if (!CModule::IncludeModule('iblock') || !CModule::IncludeModule('tasks'))
        return false;

    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 59, "ID" => $report_id), false, false, Array("ID", "PROPERTY_TASK_ID"));
    if ($arRes = $res->GetNext()) {
        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 59, "PROPERTY_TASK_ID" => $task_id), false, false, Array("ID"));
        if ($res->GetNext()) {
            return false;
        } else {
            $arReportTasks = $arRes['PROPERTY_TASK_ID_VALUE'];
            if (!in_array($task_id, $arReportTasks)) {
                $res = CTasks::GetList(
                    Array("ID" => "DESC"),
                    Array("ID" => $task_id),
                    Array("ID", "TITLE", "GROUP_ID", "DESCRIPTION", "UF_CLIENT")
                );

                if ($arTask = $res->GetNext()) {
                    $arReportTasks[] = $task_id;

                    $arTFields = Array(
                        "UF_REPORTS" => Array($report_id)
                    );
                    $obTask = new CTasks;
                    $obTask->Update($task_id, $arTFields);

                    CIBlockElement::SetPropertyValuesEx($report_id, 59, Array('TASK_ID' => $arReportTasks));

                    $res = CIBlockElement::GetList(Array("SORT" => "DESC"), Array("IBLOCK_ID" => 62, "PROPERTY_report" => $report_id), false, false, Array("ID", "SORT"));
                    if ($arRes = $res->GetNext()) {
                        $res = CTaskElapsedTime::GetList(
                            Array(),
                            Array("TASK_ID" => $arTask['ID'])
                        );

                        $taskTime = 0;

                        while ($arTime = $res->GetNext()) {
                            $taskTime += $arTime['SECONDS'];
                        }

                        $el = new CIBlockElement;

                        $PROP = array();
                        $PROP["report"] = $report_id;
                        $PROP["task_id"] = $task_id;
                        $PROP["task_name"] = $arTask['TITLE'];
                        $PROP["task_name_print"] = $arTask['TITLE'];
                        $PROP["garant"] = ($arTask['UF_TASK_GARANT'] == 1) ? 'Y' : 'N';
                        $PROP["elapsed_time"] = ceil($taskTime / 60);

                        $arLoadProductArray = Array(
                            "MODIFIED_BY" => $GLOBALS['USER']->GetID(),
                            "IBLOCK_SECTION_ID" => false,
                            "IBLOCK_ID" => 62,
                            "PROPERTY_VALUES" => $PROP,
                            "NAME" => $arTask['TITLE'],
                            "ACTIVE" => "Y",
                            "SORT" => $arRes['SORT'] - 1,
                        );

                        $el->Add($arLoadProductArray);

                        $nogarant_time = 0;
                        $res2 = CIBlockElement::GetList(Array("SORT" => "DESC"), Array("IBLOCK_ID" => 62, "PROPERTY_report" => $report_id, "PROPERTY_garant" => "N", "!CODE" => 'nogarant_time'), false, false, Array("ID", "SORT", "PROPERTY_elapsed_time"));
                        while ($arRes2 = $res2->GetNext()) {
                            $modulo = ($arRes2["PROPERTY_ELAPSED_TIME_VALUE"]) % 5;
                            $correction = 0;
                            if ($modulo > 0)
                                $correction = -$modulo + 5;
                            $nogarant_time += $arRes2["PROPERTY_ELAPSED_TIME_VALUE"] + $correction;
                        }

                        $res3 = CIBlockElement::GetList(Array("SORT" => "DESC"), Array("IBLOCK_ID" => 62, "PROPERTY_report" => $report_id, "CODE" => 'nogarant_time'), false, false, Array("ID", "SORT", "PROPERTY_elapsed_time"));
                        if ($arRes3 = $res3->GetNext()) {
                            CIBlockElement::SetPropertyValuesEx($arRes3['ID'], 62, array('elapsed_time' => $nogarant_time));
                        }
                    }

                    return true;

                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    } else {
        return false;
    }
}


function addLicense(&$arFields)
{

    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/' . 'test_l.txt', print_r($_REQUEST, true));
}

function changeSectionElement(&$arFields)
{
    global $USER;
    if ($arFields['IBLOCK_ID'] == 73) {
        CIBlockElement::SetElementSection(
            $arFields['ID'],
            array($_POST['changedSection'])
        );
        CIBlockElement::SetPropertyValueCode(
            $arFields['ID'],
            "MANAGER",
            $USER->GetId()
        );
    }
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/' . 'test.txt', print_r($arFields, true));
}

//���������� $_SERVER["HTTP_HOST"] ��� cron
if (empty($_SERVER["HTTP_HOST"])) {
    $_SERVER["HTTP_HOST"] = 'cp.itees.ru';
}

//������ ���������� id ������ ������������
function GetUserGrade($userId, $date)
{
    $date = ConvertDateTime($date, "YYYY-MM-DD");

    CModule::IncludeModule("iblock");

    $res = CIBlockElement::GetList(
        array("PROPERTY_DATE" => "DESC"),
        array(
            "IBLOCK_CODE" => "user_grade",
            "<=PROPERTY_DATE" => $date,
            "PROPERTY_USER" => $userId,
            "ACTIVE" => "Y"
        ),
        false,
        false,
        array("ID", "PROPERTY_GRADE")
    );
    if ($arRes = $res->GetNext(false, false))
        return $arRes['PROPERTY_GRADE_VALUE'];


    return false;
}

//������� ���������� ����������� ��������� ����
function GetUserGradeTimeFactor($userId, $date)
{
    if (!empty($date))
        $date = ConvertDateTime($date, 'YYYY-MM-DD');
    else
        $date = date('Y-m-d');

    CModule::IncludeModule('iblock');

    $dbUserGradeHistory = CIBlockElement::GetList(
        array('PROPERTY_DATE' => 'DESC'),
        array(
            'IBLOCK_CODE' => 'user_grade',
            '<=PROPERTY_DATE' => $date,
            'PROPERTY_USER' => $userId,
            'ACTIVE' => 'Y'
        ),
        false,
        false,
        array('ID', 'PROPERTY_GRADE', 'PROPERTY_RATE_CHANGE_HOURS')
    );
    if ($arUserGradeHistory = $dbUserGradeHistory->GetNext(false, false)) {
        if (!empty($arUserGradeHistory['PROPERTY_RATE_CHANGE_HOURS_VALUE']))
            //����������� � ������� ���������
            return $arUserGradeHistory['PROPERTY_RATE_CHANGE_HOURS_VALUE'] / 100;
        else
            //����������� � ������ ������� ���������
            $gradeId = $arUserGradeHistory['PROPERTY_GRADE_VALUE'];
    }

    if (empty($gradeId)) {
        $rsUsers = CUser::GetList(
            ($by = 'id'),
            ($order = 'desc'),
            array('ACTIVE' => 'Y', 'ID' => $userId),
            array('SELECT' => array('UF_GRADE_ID'), 'FIELDS' => array('ID'))
        );
        if ($arUser = $rsUsers->GetNext(false, false))
            //����������� � ������������ � ������
            $gradeId = $arUser['UF_GRADE_ID'];
    }

    if (!empty($gradeId)) {
        $res = CIBlockElement::GetList(
            array(),
            array(
                'ACTIVE' => 'Y',
                'ID' => $gradeId,
                'IBLOCK_CODE' => 'grades'
            ),
            false,
            false,
            array('ID', 'PROPERTY_RATE_CHANGE_HOURS')
        );
        if ($arFields = $res->GetNext(false, false))
            return $arFields['PROPERTY_RATE_CHANGE_HOURS_VALUE'] / 100;
    } else
        //���� ������ �� �����
        return 1;
}

function GetGradeRateById($Id)
{
    CModule::IncludeModule("iblock");
    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_CODE" => "grades", "ID" => $Id, "ACTIVE" => "Y"), false, false, Array('PROPERTY_RATE_CHANGE_HOURS'));
    if ($arRes = $res->GetNext(false, false)) {
        if (!empty($arRes['PROPERTY_RATE_CHANGE_HOURS_VALUE']))
            return $arRes['PROPERTY_RATE_CHANGE_HOURS_VALUE'];
    }

    return false;
}

function changeTicketField($ticketId, $fieldName, $fieldValue)
{
    if (!empty($ticketId) && CModule::IncludeModule("support")) {
        $ticketFields[$fieldName] = $fieldValue;    //"� �������"
        CTicket::SetTicket($ticketFields, $ticketId, "N", $NOTIFY = "N");
    }
}

// �������� �� ����� �� ��������?
function CheckReport($reportId)
{
    CModule::IncludeModule("iblock");

    $obPropChecklist = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_CODE" => "support_reports", "CODE" => "CHECKLIST"));
    while ($arPropChecklistVal = $obPropChecklist->Fetch()) {
        $arChecklist[$arPropChecklistVal['ID']] = $arPropChecklistVal["VALUE"];
    }

    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_CODE" => "support_reports", "ID" => intval($reportId)), false, false, Array("ID", "CODE", "PROPERTY_CHECKLIST"));
    while ($arReport = $res->GetNext()) {
        $arChecklistChecked = $arReport['PROPERTY_CHECKLIST_VALUE'];
    }

    if (!array_diff_key($arChecklist, $arChecklistChecked) && !empty($arChecklistChecked)) {
        return true;
    }

    return false;
}

class TaskEditTime
{

    function OnBeforeTaskAddHandler(&$arFields)
    {

        CModule::IncludeModule('iblock');
        $arGroups = false;
        $res = CIBlockElement::GetList(Array(), Array('IBLOCK_CODE' => 'support groups', 'ACTIVE' => 'Y'), false, false, Array('CODE'));
        while ($arGroup = $res->GetNext()) {
            $groupId = intval($arGroup['CODE']);

            if ($groupId > 0)
                $arGroups[$groupId] = $groupId;
        }

        if (isset($arGroups[$arFields['GROUP_ID']])) {

            $arSelect = Array("ID", "NAME", "PROPERTY_CLIENT");
            $arFilter = Array("IBLOCK_ID" => 77, "CODE" => $arFields['GROUP_ID']);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($ob = $res->GetNext()) {
                if (!empty($ob['PROPERTY_CLIENT_VALUE']))
                    $arFields['UF_CLIENT'] = $ob['PROPERTY_CLIENT_VALUE'];
            }

            if (!$arFields['UF_CLIENT']) {
                //throw new TasksException('������� �������!', TasksException::TE_ACTION_NOT_ALLOWED);
                //return false;
            }
        }
    }

    function OnBeforeTaskUpdateHandler($ID, &$arFields, &$arTaskCopy)
    {

        CModule::IncludeModule('iblock');
        $arGroups = false;
        $res = CIBlockElement::GetList(Array(), Array('IBLOCK_CODE' => 'support groups', 'ACTIVE' => 'Y'), false, false, Array('CODE'));
        while ($arGroup = $res->GetNext()) {
            $groupId = intval($arGroup['CODE']);

            if ($groupId > 0)
                $arGroups[$groupId] = $groupId;
        }

        if (isset($arGroups[$arFields['GROUP_ID']])) {

            $arSelect = Array("ID", "NAME", "PROPERTY_CLIENT");
            $arFilter = Array("IBLOCK_ID" => 77, "CODE" => $arFields['GROUP_ID']);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($ob = $res->GetNext()) {
                if (!empty($ob['PROPERTY_CLIENT_VALUE']))
                    $arFields['UF_CLIENT'] = $ob['PROPERTY_CLIENT_VALUE'];
            }

            if (!$arFields['UF_CLIENT']) {
                //throw new TasksException('������� �������!', TasksException::TE_ACTION_NOT_ALLOWED);
                //return false;
            }
        }

        if ($arFields['STATUS'] == 2) {
            if (self::IsTaskInReport($ID)) {
                throw new TasksException('�� �� ������ ����������� ������, ����������� � �����!', TasksException::TE_ACTION_NOT_ALLOWED);
                return false;
            }
        }
    }

    function OnBeforeTaskElapsedTimeUpdateHandler($ID, $arUpdatingLogItem, &$arFields)
    {

        $time_delta = floor($arUpdatingLogItem['SECONDS'] / 60) - $arFields['MINUTES'];
        if ($time_delta != 0) {
            if (self::IsTaskInReport($arUpdatingLogItem['TASK_ID'])) {
                throw new TasksException('�� �� ������ �������� ����������� ����� ��� ������, ����������� � �����!', TasksException::TE_ACTION_NOT_ALLOWED);
                return false;
            }
        }

    }

    function OnBeforeTaskElapsedTimeAddHandler(&$arFields)
    {
        if (self::IsTaskInReport($arFields['TASK_ID'])) {
            throw new TasksException('�� �� ������ �������� ����������� ����� ��� ������, ����������� � �����!', TasksException::TE_ACTION_NOT_ALLOWED);
            return false;
        }
    }

    public function IsTaskInReport($id)
    {
        if (CModule::IncludeModule("tasks") && CModule::IncludeModule("iblock")) {
            $res = CIBlockElement::GetList(
                array(),
                array('IBLOCK_ID' => 59, "PROPERTY_TASK_ID" => $id, "!PROPERTY_REPORT_PASS_CLIENT" => false),
                false,
                false,
                array('ID', 'NAME')
            );
            while ($ob = $res->GetNext()) {
                $arReport[] = $ob;
            }
            return $arReport;
        } else return false;
    }
}

class C_AS
{


    //C_AS::callStack(debug_backtrace());
    public static function callStack($stacktrace)
    {
        //print str_repeat("=", 50) ."\n";
        $i = 1;
        foreach ($stacktrace as $node) {
            if (class_exists('CUserTypeIBlockElement')) {
                $myclass = "new";
            }//C_AS::dmp($i);
            C_AS::dmp("$i. " . $myclass . basename($node['file']) . ":" . $node['function'] . "(" . $node['line'] . ")\n<br/>");
            $i++;
        }
    }


    public function dmp($vr)
    {
        $tt = date("F j, Y, g:i a");
        //file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/logs/as_log.txt', "\n" . $tt . '-->', FILE_APPEND);
        if (is_array($vr))
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/logs/as_log.txt', print_r($vr, true), FILE_APPEND);
        else
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/logs/as_log.txt', $vr, FILE_APPEND);
    }
}

function GetProjectGroups()
{
    if (CModule::IncludeModule("socialnetwork") && CModule::IncludeModule("iblock")) {
        $arGroups = false;

        $res = CIBlockElement::GetList(
            Array(),
            Array("IBLOCK_CODE" => 'support_themes', "PROPERTY_TYPE" => 5637),    //"PROPERTY_TYPE_VALUE" => '������'
            false,
            false,
            Array("ID", "NAME", "CODE", "PROPERTY_TYPE")
        );
        while ($arSupportSubject = $res->GetNext()) {
            $arSubjId[] = $arSupportSubject['CODE'];
        }

        $res = CSocNetGroup::GetList(array(), array('SUBJECT_ID' => $arSubjId), false, false, array("ID"));
        while ($arGroup = $res->Fetch()) {
            $arGroups[] = $arGroup['ID'];
        }

        return $arGroups;
    }

    return false;
}

//������� ����������� ������ ������� "������� ���������" ��� ������ ���� ������ ����������� � ������
function OnSaleStatusOrderHandler($ID, $status)
{
    if (!empty($ID)) {
        CModule::IncludeModule('crm');
        CModule::IncludeModule('iblock');

        //�����
        $obReport = CIBlockElement::GetList(
            array(),
            array('IBLOCK_CODE' => 'support_reports', 'PROPERTY_INVOICES' => $ID),
            false,
            false,
            array('ID', 'PROPERTY_INVOICES')
        );
        while ($arReport = $obReport->GetNext(false, false)) {
            $res = false;

            //����� ������
            $obReportSum = CIBlockElement::GetList(
                array(),
                array(
                    'IBLOCK_CODE' => 'reports_print',
                    'CODE' => 'nogarant_time',
                    '!PROPERTY_price' => false,
                    'PROPERTY_report' => $arReport['ID']
                ),
                false,
                false,
                array('ID', 'PROPERTY_price')
            );
            if ($arReportSum = $obReportSum->GetNext(false, false)) {
                //���������� ����� ������ ������
                $invSumPaid = 0;
                $obInvoices = CCrmInvoice::GetList(
                    array(),
                    array('ID' => $arReport['PROPERTY_INVOICES_VALUE']),
                    false,
                    false,
                    array('ID', 'SUM_PAID')
                );
                while ($arInvoice = $obInvoices->GetNext(false, false)) {
                    $invSumPaid = $invSumPaid + $arInvoice['SUM_PAID'];
                }

                if ($arReportSum['PROPERTY_PRICE_VALUE'] <= $invSumPaid && $status == 'P')
                    $res = 4612;
            }

            CIBlockElement::SetPropertyValuesEx($arReport['ID'], false, array("PAYED" => $res));
        }
    }
}


function jsondecode($sText)
{
    if (!$sText) return false;
    $sText = iconv('cp1251', 'utf8', $sText);
    $aJson = json_decode($sText, true);
    $aJson = iconvarray($aJson);
    return $aJson;
}

function iconvarray($aJson)
{
    foreach ($aJson as $key => $value) {
        if (is_array($value)) {
            $aJson[$key] = iconvarray($value);
        } else {
            $aJson[$key] = iconv('utf8', 'cp1251', $value);
        }
    }
    return $aJson;
}

function pLog($var, $url = false, $delimiter = false)
{
    if (empty($url)) {
        $url = $_SERVER["DOCUMENT_ROOT"] . "/logs/log.txt";
    }

    if (!empty($delimiter)) {
        $delimiter = "\n\n--- " . $delimiter . " ---\n\n";
    } else {
        $delimiter = "\n\n-------------------------------------------------\n\n";
    }
    file_put_contents($url, $delimiter . print_r($var, true), FILE_APPEND);
}

//���������� ������� ��� ���� ������������ �� crm �����
function OnBeforeMailSendHandler(\Bitrix\Main\Event $event)
{
	$advertisingSignatureActive = Bitrix\Main\Config\Option::get("main", "advertising_signature_active", '');

	$arParameters = $event->getParameter(0);

	if($advertisingSignatureActive == 'Y')
	{
		if(!empty($arParameters['CONTEXT']))
		{
			$callback = $arParameters['CONTEXT']->getCallback();

			if(!empty($callback))
			{
				if($callback->getModuleId() == 'crm')
				{
					
					$advertisingSignature = Bitrix\Main\Config\Option::get("main", "advertising_signature", '');

					if(!empty($advertisingSignature))
					{
						$hostname = \COption::getOptionString('main', 'server_name', '') ?: 'localhost';
						if (defined('BX24_HOST_NAME') && BX24_HOST_NAME != '')
							$hostname = BX24_HOST_NAME;
						else if (defined('SITE_SERVER_NAME') && SITE_SERVER_NAME != '')
							$hostname = SITE_SERVER_NAME;

						$doc = new DOMDocument();
						@$doc->loadHTML($advertisingSignature);

						$tags = $doc->getElementsByTagName('img');

						$arFilesSrc = [];

						foreach ($tags as $tag) {
						   $arFilesSrc[] = $tag->getAttribute('src');
						}

						if(!empty($arFilesSrc))
						{
							$attachments = array();
							foreach ($arFilesSrc as $item)
							{
								$arFile = CFile::MakeFileArray($item);

								$contentId = sprintf(
									'bxacid.%s@%s.crm',
									hash('crc32b', $arFile['size'].$arFile['name']),
									hash('crc32b', $hostname)
								);

								$attachments[] = array(
									'ID'           => $contentId,
									'NAME'         => $arFile['name'],
									'PATH'         => $arFile['tmp_name'],
									'CONTENT_TYPE' => $arFile['type'],
								);

								$advertisingSignature = str_replace(
									$item,
									sprintf('cid:%s', $contentId),
									$advertisingSignature
								);
							}

							if(!empty($attachments)) {
								$arParameters['ATTACHMENT'] = $attachments;
							}
						}

						$arParameters['BODY'] = $arParameters['BODY'] . $advertisingSignature;
					}
				}
			}
		}
	}
	return new \Bitrix\Main\EventResult($event->getEventType(), $arParameters);
}

// ��� ������������ ������ ��������������� ����������� 
// ������� �������������� ������ advertising_signature
function AddSocNetFeature(&$arSocNetFeaturesSettings) {
	
	$arParams = array(
		"allowed" => array(SONET_ENTITY_USER, SONET_ENTITY_GROUP),
		"operations" => array(
			"write" => array(SONET_ENTITY_USER => SONET_RELATIONS_TYPE_NONE, SONET_ENTITY_GROUP => SONET_ROLES_MODERATOR),
			"view" => array(SONET_ENTITY_USER => SONET_RELATIONS_TYPE_ALL, SONET_ENTITY_GROUP => SONET_ROLES_USER),
		),
		"minoperation" => "view",
   );
	
	if(hasAdvertisingSignatureAccess()) {
	   $arSocNetFeaturesSettings["advertising_signature"] = $arParams;
	}
}

// ��� ������������ ������ �������� ������� �������������� 
// �������� ��� ����������� advertising_signature
function AddSocNetMenu(&$arResult) {
	global $USER;
	if(hasAdvertisingSignatureAccess()) {
		// ������� ��� ������
		$arResult["CanView"]["advertising_signature"] = true;
		// ������ ��������
		$arResult["Urls"]["advertising_signature"] = CComponentEngine::MakePathFromTemplate("/company/personal/user/#user_id#/advertising_signature/", array("user_id" => $USER->GetID()));
		// �������� ��������
		//$arResult["Title"]["advertising_signature"] = Loc::getMessage('advertising_signature');
		$arResult["Title"]["advertising_signature"] = '������ �������';
	}
}

// ��� ������������ ������ �������� ������� ������� 
// ������������ ���������� � ������ ��� ������� ������ 
function OnParseSocNetComponentPath(&$arUrlTemplates, &$arCustomPagesPath) {
	if(hasAdvertisingSignatureAccess()) {
	   // ������ ������ ��������
	   $arUrlTemplates["advertising_signature"] = "/company/personal/user/#user_id#/advertising_signature/";
	   // ���� ������������ ����� �����, �� �������� ����� ��������
	   $arCustomPagesPath["advertising_signature"] = "/company/advertising_signature/";
	}
}

//�������� ������� � '��������� ����' �� �������� ������������
function hasAdvertisingSignatureAccess() {

	global $USER;

	$arGroup = \Bitrix\Main\GroupTable::getList([
		'filter' => ['STRING_ID' => 'ADVERTISING_SIGNATURE_EDITOR'],
		'select' => ['ID'],
		'limit' => 1,
		'cache' => ['ttl' => 3600]
	])->Fetch();

	if ($USER->IsAdmin() || in_array($arGroup['ID'], $USER->GetUserGroupArray())) {
		return true;
	}
	
	return false;
}

function OnEndBufferContentHandler(&$buffer)
{
	if(CSite::InDir('/stream/') && $GLOBALS['USER']->IsAdmin())
	{
		$buffer = str_replace(
			'</body>',
			'<script>
				$(function(){
					addPostUpdatesMessage = new BX.PopupWindow("update-message-add-form", null, {
						content: \'<label>� <input type="text" value="'.ConvertTimeStamp(time() - 3600 * 24 * 7).'" name="updatesfrom" onclick="BX.calendar({node: this, field: this, bTime: false});"></label><label> �� <input type="text" value="'.ConvertTimeStamp(time()).'" name="updatesto" onclick="BX.calendar({node: this, field: this, bTime: false});"></label>\',
						closeIcon: {right: "20px", top: "10px"},
						titleBar: {content: BX.create("span", {html: \'<h3>�������� �� �����������, ������������� </h3>\', \'props\': {\'className\': \'access-title-bar\'}})},
						zIndex: 0,
						offsetLeft: 0,
						offsetTop: 0,
						draggable: {restrict: false},
						buttons: [
							new BX.PopupWindowButton({
								text: "��������",
								className: "popup-window-button-accept",
								events: {click: function(){
									$("body").css("cursor", "wait");
									var from = $("input[name=updatesfrom]").val();
									var to = $("input[name=updatesto]").val();
									$.ajax({
										url: "/dev/ajax/updates_post.php?from="+from+"&to="+to,
										success: function(data) {
											if(data == "success")
												location.reload();
											else
												alert("�� ��������� ������ �� ������� ���������� �� �����������");
											
											$("body").css("cursor", "default");
										}
									});
								}}
							}),
							new BX.PopupWindowButton({
								text: "�������",
								className: "webform-button-link-cancel",
								events: {click: function(){
									this.popupWindow.close();
								}}
							})
						]
					}); 

					$("#feed-add-post-form-link-more").append(\'<span class="feed-add-post-form-link" data-onclick="addPostUpdatesMessage.show();" data-name="�������� �� ����������" id="feed-add-post-form-tab-update" style="display:none;"></span>\');
				});
			</script>
			</body>',
			$buffer
		);
	}
}

function PostUpdatesMessage($timeFrom = false, $timeTo = false)
{
	global $USER;
	$newID = 0;
	
	if(CModule::IncludeModule("blog") && CModule::IncludeModule("socialnetwork"))
	{
		$arBlog = CBlog::GetByOwnerID($USER->GetID());
		
		$message = GetUpdateLogForSonetMessage($timeFrom, $timeTo);

		if($message)
		{
			$arFields = array(
				"TITLE" => '���������� ������� �� ������ � '.ConvertTimeStamp($timeFrom).' �� '.ConvertTimeStamp($timeTo),
				"DETAIL_TEXT" => $message ,
				"DATE_PUBLISH" => date('d.m.Y H:i:s'),
				"PUBLISH_STATUS" => "P",
				"CATEGORY_ID" => "",
				"PATH" => "/company/personal/user/".$USER->GetID()."/blog/#post_id#/",
				"URL" => "admin-blog-s1",
				"PERMS_POST" => Array(),
				"PERMS_COMMENT" => Array (),
				"SOCNET_RIGHTS" => Array
				(
					"UA", "G2"
				),
				"=DATE_CREATE" => "now()",
				"AUTHOR_ID" => $USER->GetID(),
				"BLOG_ID" => $arBlog['ID'],
				"UF_BLOG_POST_IMPRTNT" => '1'
			);

			$newID = CBlogPost::Add($arFields);
			 
			$arFields["ID"] = $newID;
			$arParamsNotify = Array(
				"bSoNet" => true,
				"UserID" => $USER->GetID(),
				"user_id" => $USER->GetID(),
			);
			 
			CBlogPost::Notify($arFields, array(), $arParamsNotify);
		}
	}
	
	if($newID)
		return true;
	else
		return false;
}

function GetUpdateLogForSonetMessage($timeFrom = false, $timeTo = false)
{
	$arResult = Array();
	$strResult = '';
	
	if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/updater.log")
	&& is_file($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/updater.log")
	&& is_readable($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/updater.log"))
	{
		$logf = fopen($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/updater.log", "r");
		while (!feof($logf))
		{
			$buffer = fgets($logf, 8192);
			$rec = false;
			if (substr($buffer, strlen("0000-00-00 00:00:00 "), strlen("- UPD_SUCCESS -"))=="- UPD_SUCCESS -")
			{
				$rec = array(
					"S",
					substr($buffer, 0, strlen("0000-00-00 00:00:00")),
					substr($buffer, strlen("0000-00-00 00:00:00 - UPD_SUCCESS - "))
				);
			}
			elseif (substr($buffer, strlen("0000-00-00 00:00:00 "), strlen("- UPD_ERROR -"))=="- UPD_ERROR -")
			{
				$rec = array(
					"E",
					substr($buffer, 0, strlen("0000-00-00 00:00:00")),
					substr($buffer, strlen("0000-00-00 00:00:00 - UPD_ERROR - "))
				);
			}
			elseif (substr($buffer, strlen("0000-00-00 00:00:00 "), strlen("- UPD_NOTE -"))=="- UPD_NOTE -")
			{
				$rec = array(
					"N",
					substr($buffer, 0, strlen("0000-00-00 00:00:00")),
					substr($buffer, strlen("0000-00-00 00:00:00 - UPD_NOTE - "))
				);
			}
			if($rec)
			{
				$rec[3] = "";
				$pos1 = strpos($rec[2], "<br>");
				if($pos1 !== false)
				{
					$rec[3] = trim(substr($rec[2], $pos1 + 4));
					$rec[3] = str_replace('\"', '&quot;', $rec[3]);
					$rec[2] = substr($rec[2], 0, $pos1);
				}
				$arLogRecs[] = $rec;
			}
		}
		fclose($logf);

		$by = strtoupper($by);
		if($by == "SUCCESS")
			$sort = 0;
		elseif($by == "DESCRIPTION")
			$sort = 2;
		else
			$sort = 1;
		if(strtoupper($order) == "ASC")
			$ord = 1;
		else
			$ord = -1;
		usort($arLogRecs, create_function('$a, $b', 'return strcmp($a['.$sort.'], $b['.$sort.'])*('.$ord.');'));
	}
	
	if($timeFrom)
	{
		if(!$timeTo)
			$timeTo = time();
		
		foreach($arLogRecs as $k => &$arRec)
		{
			$time = MakeTimeStamp($arRec[1], "YYYY-MM-DD HH:MI:SS");

			if($time > $timeTo || $time < $timeFrom || $arRec[0] != 'S')
			{
				unset($arLogRecs[$k]);
			}
			else
			{
				if(preg_match("/Updated\: ([a-zA-Z\-\_]+) \([0-9\.]+\)/", $arRec[2], $arMatches))
				{
					$module = $arMatches[1];
					$arMess = explode('<li>', $arRec[3]);
					$message = '';
					foreach($arMess as $k => $mess)
					{
						if(!empty(trim($mess)))
						{
							$message .= '<li>'.$mess.' ('.ConvertTimeStamp($time).')</li>';
						}
					}
					
					$arResult[$module][] = Array('DATE' => ConvertTimeStamp($time), 'MESSAGE' => $message);
				}
			}
		}
	}
	
	/*if(!empty($arResult))
	{
		$strResult = '<h2>���������� ������� �� ������ � '.ConvertTimeStamp($timeFrom).' �� '.ConvertTimeStamp($timeTo).'</h2>';
	}*/
	
	foreach($arResult as $module => $arUpdates)
	{
		$strResult .= '<b>���������� ������ '.$module.'</b>';
		$strResult .= '<ol>';
		
		foreach($arUpdates as $k => $arUpdate)
		{
			$strResult .= $arUpdate['MESSAGE'];
		}
		
		$strResult .= '</ol>';
	}
	
	return $strResult;
}

//���� ������ �������� � ������ "�������" ��� �������� ���� ��������� ������
//��������� �������/��������� ����� updateB24Deal() ��������� ������� �� ��������� ������
function OnAfterCrmDealB24Handler($arFields)
{
	$endCouponDatePropCode = 'UF_CRM_1505372586';
	
	//������ ��������, ���������� ������� "�������"
	if($arFields['STAGE_ID'] == "C8:WON" || !empty($arFields[$endCouponDatePropCode]))
	{
		CModule::IncludeModule('crm');
		
		$arAgent = CAgent::GetList([], ['NAME' => 'updateB24Deal(' . $arFields['ID'] . ');'])->fetch();

		if(empty($arFields[$endCouponDatePropCode]))
		{
			$arDeal = CCrmDeal::GetListEx(
				[],
				['ID' => $arFields['ID']],
				false,
				false,
				['ID', $endCouponDatePropCode]
			)->fetch();
			
			$arFields[$endCouponDatePropCode] = $arDeal[$endCouponDatePropCode];
			unset($arDeal);
		}

		$dealCouponExpirationDate = strtotime($arFields[$endCouponDatePropCode]);
		$newAgentDate = date("d.m.Y", $dealCouponExpirationDate + 259200) . ' 00:00:00';

		//����� ����������, ������ ��� ��������� ���������, ���������� �����
		if(!empty($arAgent))
		{
			if($dealCouponExpirationDate > strtotime($arAgent['NEXT_EXEC']))
			{
				$agentId = CAgent::Update($arAgent['ID'], ['NEXT_EXEC' => $newAgentDate]);
			}
		}
		//���� ������ ��� � ���� ��������� ������ ��� �� ���������, �������� �����
		elseif(strtotime($arFields[$endCouponDatePropCode]) > mktime())
		{
			$agentId = CAgent::AddAgent(
				'updateB24Deal(' . $arFields['ID'] . ');',
				'crm',
				'N',
				0,
				$newAgentDate,
				'Y',
				$newAgentDate
			);
		}
	}
}

//����� ��������� ��� ������ ������ �� ������� ���������� �24
//����� ��������� ������ � �������� ������� � ������ "����������"
function updateB24Deal($dealId)
{
	if(!empty($dealId))
	{
		$endCouponDatePropCode = 'UF_CRM_1505372586';
		
		CModule::IncludeModule('crm');

		$arDeal = CCrmDeal::GetListEx(
			[],
			['ID' => $dealId, 'CHECK_PERMISSIONS' => 'N'],
			false,
			false,
			['ID', $endCouponDatePropCode]
		)->fetch();

		if(!empty($arDeal))
		{
			if(mktime() > strtotime($arDeal[$endCouponDatePropCode]))
			{
				$arParams = ['STAGE_ID' => 'C8:APOLOGY'];
				
				$CCrmDeal = new CCrmDeal(false);
				$res = $CCrmDeal->Update($dealId, $arParams);
			}
		}
	}
}

function p($var, $url = false, $delimiter = false) {
    if (empty($url)) {
        $url = $_SERVER["DOCUMENT_ROOT"] . "/logs/log.txt";
    }

    if (!empty($delimiter)) {
        $delimiter = "\n\n--- " . $delimiter . " ---\n\n";
    } else {
        $delimiter = "\n\n-------------------------------------------------\n\n";
    }
    file_put_contents($url, $delimiter . print_r($var, true), FILE_APPEND);
}