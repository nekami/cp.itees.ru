<?php

use \Bitrix\Main\{TaskTable, UserTable, Loader};

Loader::IncludeModule('learning');
Loader::IncludeModule('iblock');

/*
 * Класс делает полную проверку прав доступа к учебным курсам
 */

class LearnAccess {
    
    //проверка доступа к списку уроков курса
    //возвращает массив id доступных уроков курса
    static public function lessonsByParent($parentLessonId) {
        return self::lessons(self::getLessonsByParent($parentLessonId));
    }
    
    //проверка доступа к списку уроков курса
    //возвращает массив id доступных уроков курса
    static public function lessons($lessonsId = []) {
        if(!empty($lessonsId)) {
            global $USER;
            if($USER->IsAdmin()) {
                return $lessonsId;
            }
            //какие элементы из переданных создал пользователь
            $availableLessonsId = self::isLessonsCreator($lessonsId, []);

            //права по умолчанию
            $basePerm = self::permissionsFormat(
                \CLearnAccess::GetBasePermissions()
            );

            $tasks = self::getTasks();

            //департаменты пользователя
            $departments = self::getUserDepartmentId();
            //группы пользователя
            $groups = self::getUserGroups();

            foreach($lessonsId as $lessonId) {
                //права на элемент
                $lessonPerm = self::permissionsFormat(
                    \CLearnAccess::GetLessonPermissions($lessonId)
                );

                //проверка по пользователю
                if(
                    !empty($tasks[$lessonPerm['U'][self::getUserId()]]) ||
                    !empty($tasks[$basePerm['U'][self::getUserId()]])
                ) {
                    $availableLessonsId[] = $lessonId;
                    continue;
                }

                //проврека по группе
                if(!empty($groups) && (!empty($lessonPerm['G']) || !empty($basePerm['G']))) {
                    foreach($groups as $groupId) {
                        if(!empty($tasks[$basePerm['G'][$groupId]]) || !empty($tasks[$lessonPerm['G'][$groupId]])) {
                            $availableLessonsId[] = $lessonId;
                            continue 2;
                        }
                    }
                }

                //проврека по департаменту
                if(!empty($departments)) {
                    foreach($departments as $departmentId) {
                        if(
                            !empty($tasks[$basePerm['D'][$departmentId]]) ||
                            !empty($tasks[$lessonPerm['D'][$departmentId]]) ||
                            !empty($tasks[$basePerm['DR'][$departmentId]]) ||
                            !empty($tasks[$lessonPerm['DR'][$departmentId]])
                        ) {
                            $availableLessonsId[] = $lessonId;
                            continue 2;
                        }
                    }
                }

                //проврека по вложенному департаменту
                if(!empty($departments)) {
                    if(!empty($lessonPerm['DR'])) {
                        foreach($lessonPerm['DR'] as $departmentId => $perm) {
                            if(!empty($tasks[$perm])) {
                                $lessonDepartments[$lessonId][] = $departmentId;
                                $parentDepartments[] = $departmentId;
                            }
                        }
                    }
                    if(!empty($basePerm['DR'])) {
                        foreach($basePerm['DR'] as $departmentId => $perm) {
                            if(!empty($tasks[$perm])) {
                                $lessonDepartments[$lessonId][] = $departmentId;
                                $parentDepartments[] = $departmentId;
                            }
                        }
                    }
                }
            }

            $availableLessonsId = self::getCheckChildDepartments($lessonDepartments, $parentDepartments, $departments, $availableLessonsId);

            return array_unique($availableLessonsId);
        }
        return [];
    }
	
    static protected function getCheckChildDepartments($lessonDepartments = [], $parentDepartments = [], $userDepartments, $availableLessonsId = []) {

        if(!empty($userDepartments) && !empty($parentDepartments)) {

            $sectionsMargin = self::getDepartments(array_merge($parentDepartments, $userDepartments));

            foreach($lessonDepartments as $lessonId => $departments) {
                foreach($departments as $departmentId) {
                    foreach($userDepartments as $userDepartmentId) {
                        if(
                            $sectionsMargin[$departmentId]['LEFT_MARGIN'] < $sectionsMargin[$userDepartmentId]['LEFT_MARGIN'] && 
                            $sectionsMargin[$userDepartmentId]['RIGHT_MARGIN'] < $sectionsMargin[$departmentId]['RIGHT_MARGIN']
                        ) {
                            $availableLessonsId[] = $lessonId;
                            continue 3;
                        }
                    }
                }
            }
        }

        return $availableLessonsId;
    }

    static protected function getLessonsByParent($parentLessonId) {
        $lessons = \CLearnLesson::ListImmediateChilds($parentLessonId);
        return array_column($lessons, 'TARGET_NODE');
    }

    static protected function permissionsFormat($permissions) {
        foreach($permissions as $type => $permission) {
            $id = preg_replace('/[^0-9]/', '', $type);
            $perms[str_replace($id, '', $type)][$id] = $permission;
        }
        
        return $perms;
    }
    
    static protected function isLessonsCreator($lessonsId, $availableLessonsId) {

        if(!empty($lessonsId)) {
            $filter = ['LESSON_ID' => $lessonsId, 'CREATED_BY' => self::getUserId()];
            $lessons = \CLearnLesson::GetList([], $filter, ['LESSON_ID']);
            while($lesson = $lessons->fetch()) {
                $availableLessonsId[] = $lesson['LESSON_ID'];
            }
        }
        
        return array_unique($availableLessonsId);
    }
    	
    static protected function getDepartments($sectionsId) {
        $sectionsMargin = [];
        
        $sections = \CIBlockSection::GetList(
            [],
            ['IBLOCK_CODE' => 'departments', 'ID' => $sectionsId],
            false,
            ['ID', 'LEFT_MARGIN', 'RIGHT_MARGIN']
        );
        
        while($section = $sections->Fetch()) {
            $sectionsMargin[$section['ID']] = $section;
        }
        
        return $sectionsMargin;
    }
    
    static protected function getUserDepartmentId() {
        $user = UserTable::getList([
            'filter' => ['ID' => self::getUserId()],
            'select' => ['UF_DEPARTMENT'],
            'cache' => ['ttl' => 86400]
        ])->fetch();

        return $user['UF_DEPARTMENT'];
    }
    
    static protected function getTasks() {
        $tasks = [];
        $learningTasks = TaskTable::getlist([
            'filter' => [
                'MODULE_ID' => 'learning',
                '!NAME' => 'learning_lesson_access_denied'
            ],
            'select' => ['ID', 'NAME'],
            'cache' => ['ttl' => 86400]
        ]);

        while($task = $learningTasks->fetch()) {
            $tasks[$task['ID']] = $task['NAME'];
        }
        
        return $tasks;
    }
        
    static protected function getUserId() {
        global $USER;
        return $USER->GetID();        
    }
    
    static protected function getUserGroups() {
        global $USER;
        return $USER->GetUserGroupArray();
    }
}