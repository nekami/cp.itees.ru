<?
AddEventHandler("tasks", "OnBeforeTaskNotificationSend", "StopNotification");
//AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "DealsCreate");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "DealsUpdate");
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", "DealsDelete");
AddEventHandler("main", "OnEndBufferContent", "AddLinks");
AddEventHandler("iblock", "OnAfterIBlockElementSetPropertyValuesEx", "DealsUpdateStatus");

function StopNotification($fields) {
	
	$check = true;
	if (isset($fields['arChanges']['UF_CRM_TASK'])) {
		$from_value = explode(',',$fields['arChanges']['UF_CRM_TASK']['FROM_VALUE']);
		$to_value = explode(',',$fields['arChanges']['UF_CRM_TASK']['TO_VALUE']);
		if (count($from_value) != count($to_value)) {
			foreach ($to_value as $el) {
				if (preg_match('/D_[0-9]{1,}/i',$el)) {
					$check = false;
				}
			}
		}
	}
	
	if (!$check) {
		return false;
	}
}

function DealsDelete($ID) {
	global $USER;
	//if ($USER->GetID() == 1147) {
		$arSelect = Array("PROPERTY_DEAL_ID","PROPERTY_TASK_ID");
		$arFilter = Array("IBLOCK_ID"=>59, "ID"=>$ID);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if ($ob = $res->GetNext()) {
			if (isset($ob['PROPERTY_DEAL_ID_VALUE']) && $ob['PROPERTY_DEAL_ID_VALUE'] > 0) {
				if (CModule::IncludeModule('crm')) {
					$crm = new CCrmDeal;
					$crm->Delete($ob['PROPERTY_DEAL_ID_VALUE']);
				}
			}
		}
	//}
}

function DealsUpdate(&$arFields) {
	global $USER;
	//if ($USER->GetID() == 1147) {
		if($arFields["IBLOCK_ID"] == 62 && $arFields['CODE'] == 'nogarant_time') {
			if (CModule::IncludeModule('crm')) {
				
				$arSelect = Array("PROPERTY_DEAL_ID","PROPERTY_TASK_ID");
				$arFilter = Array("IBLOCK_ID"=>59, "ID"=>$arFields['PROPERTY_VALUES']['report']);
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				$ob = $res->GetNext();
				$tasks = unserialize($ob['~PROPERTY_TASK_ID_VALUE']);
				
				if (isset($ob['PROPERTY_DEAL_ID_VALUE']) && $ob['PROPERTY_DEAL_ID_VALUE'] > 0) {
					$crm = new CCrmDeal;
					$arCrmFields = array(
						"OPPORTUNITY" => $arFields['PROPERTY_VALUES']['price']
					);
					$crm->Update($ob['PROPERTY_DEAL_ID_VALUE'],$arCrmFields);
				}
			}
		}
	//}
}

function DealsUpdateStatus($id,$iblock_id,$property) {
	
	global $USER;
	//if ($USER->GetID() == 1147) {
		if ($iblock_id == 59) {
			$arSelect = Array("ID","PROPERTY_DEAL_ID","PROPERTY_ACT","PROPERTY_CHECKLIST");
			$arFilter = Array("IBLOCK_ID"=>59, "ID" => $id);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			$iblock_element_info = $res->GetNext();
			
			$arF = array('CODE' => 'CHECKLIST');
			$properties = CIBlockPropertyEnum::GetList(Array(), $arF);
			while ($prop_fields = $properties->GetNext()) {
				$property_fields[] = $prop_fields;
			}
			$property_fields_count = count($property_fields);
			$current_fields_count = count($iblock_element_info['PROPERTY_CHECKLIST_VALUE']);
			
			if ($iblock_element_info['PROPERTY_DEAL_ID_VALUE'] > 0 && CModule::IncludeModule('crm')) {
				$crm = new CCrmDeal;
				
				if(array_key_exists('CHECKLIST', $property)) {
					
					$filled_fields_count = count($property['CHECKLIST']);
					
					if (($property_fields_count - $filled_fields_count) == 0) {
						if (!empty($iblock_element_info['PROPERTY_ACT_VALUE']))
							$update_field = array("STAGE_ID" =>'C6:WON'); //���� ��������� ���� ���, ������ ���������� ������
						else
							$update_field = array("STAGE_ID" =>'C6:PREPARATION'); //������� ��������, ���� ���, ������ ������ "���������� ����������"
						
						$crm->Update($iblock_element_info['PROPERTY_DEAL_ID_VALUE'],$update_field);
					}
					else { //������� �� ��������, ������ ������ "�����"
						$update_field = array("STAGE_ID" =>'C6:NEW');
						$crm->Update($iblock_element_info['PROPERTY_DEAL_ID_VALUE'],$update_field);
					}
				}
				else {
					if (!empty($iblock_element_info['PROPERTY_ACT_VALUE'])) {
						
						if (($property_fields_count - $current_fields_count) == 0)
							$update_field = array("STAGE_ID" =>'C6:WON');
						else
							$update_field = array("STAGE_ID" =>'C6:NEW');

						$crm->Update($iblock_element_info['PROPERTY_DEAL_ID_VALUE'],$update_field); //���� ��������� ���� ��� � ���� �������, ������ ���������� ������
					}
					else {
						if (($property_fields_count - $current_fields_count) == 0)
							$update_field = array("STAGE_ID" =>'C6:PREPARATION');
						else
							$update_field = array("STAGE_ID" =>'C6:NEW');
						
						$crm->Update($iblock_element_info['PROPERTY_DEAL_ID_VALUE'],$update_field);
					}
				}
			}
		}
	//}
}


function DealsCreate(&$arFields) {
	global $USER;
	//if ($USER->GetID() == 1147) {
		if($arFields["IBLOCK_ID"] == 62 && $arFields['CODE'] == 'nogarant_time') {
			
			if (CModule::IncludeModule('crm')) {
				
				$arSelect = Array("PROPERTY_CLIENT_ID","PROPERTY_TASK_ID");
				$arFilter = Array("IBLOCK_ID"=>59, "ID"=>$arFields['PROPERTY_VALUES']['report']);
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				$ob = $res->GetNext();
				$tasks = unserialize($ob['~PROPERTY_TASK_ID_VALUE']);
				
				$obCompanys = CCrmCompany::GetListEx(
					array(),
					array('ID' => $ob['PROPERTY_CLIENT_ID_VALUE']),
					false,
					false,
					array('TITLE')
				);
				$arCompany = $obCompanys->GetNext(false, false);
				
				$crm = new CCrmDeal;
				$arCrmFields = array(
					"TITLE" => $arCompany['TITLE'].' - ��������� ����� - '.FormatDate("f Y", MakeTimeStamp(date('d.m.Y'))),
					"CLOSEDATE" => date('d.m.Y'),
					"STAGE_ID" => "C6:NEW",
					"OPPORTUNITY" => $arFields['PROPERTY_VALUES']['price'],
					"COMPANY_ID" => $ob['PROPERTY_CLIENT_ID_VALUE'],
					"AUTHOR_ID" => $arFields['MODIFIED_BY'],
					'CATEGORY_ID' => 6, //ID  ��������� ������� ���������
					"UF_REPORT_LINK2" => '##a##/itees/support_reports/reports/'.$arFields['PROPERTY_VALUES']['report'].'/##a##', //��������� � ���������
					"UF_REPORT_LINK" => $arFields['PROPERTY_VALUES']['report'] //��������� � ���������
				);
				$crmResult = $crm->Add($arCrmFields, $bUpdateSearch = true, $options = array());
				$uf_fields = array('D_'.$crmResult); //id ������ ��� ������ � ���� ����� UF_CRM_TASK
				//RewriteFile($_SERVER["DOCUMENT_ROOT"]."/logs/handlers_deals.txt", print_r($arCrmFields, true));
				
				CIBlockElement::SetPropertyValuesEx($arFields['PROPERTY_VALUES']['report'], 59, Array('DEAL_ID' => $crmResult));
				
				if (CModule::IncludeModule("tasks")) {
					$arTasks = array();
					foreach($tasks['VALUE'] as $task) {
						$rsTask = CTasks::GetByID($task);
						if ($arTask = $rsTask->GetNext()) {
							$task_obj = new CTasks;
							$arUfFields = array(
								"UF_CRM_TASK" => array_merge($arTask['UF_CRM_TASK'],$uf_fields)
							);
							$task_obj->Update($task, $arUfFields);
						}
					}
				}
			}
			//RewriteFile($_SERVER["DOCUMENT_ROOT"]."/logs/handlers_deals2.txt", print_r($arFields, true));
		}
	//}
}

function AddLinks(&$content) { //��� ������ ����� �� ������ �� ������ � �������

	preg_match('/##a##(.{1,})##a##/', $content, $arMatches);
	if ($arMatches) {
		$arMatches[0] = trim($arMatches[0],'#a');
		$content = preg_replace("/##a##(.{1,})##a##/", '<a href="'.$arMatches[0].'">������� � ������</a>', $content);
	}
	
}