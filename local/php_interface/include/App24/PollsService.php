<?php
namespace App24\Polls;
use Bitrix\Main\EventManager;
use Bitrix\Main\SystemException;
use Bitrix\Main\Loader;

Class PollsService {

    const RESULTS_IB_TYPE = 'polls_results';        //��� �� ����������� ������

    const IB_POLL_ID = 579;                         //ID �� � ��������
    const IB_APP24_ID = 584;                        //ID �� � ������������24
    const SITE_ID = 's1';                           //ID �����
    const ALL_USERS_GROUP = 2;                      //������ ������������� "��� ������������"
    const POLL_NAME_MAX_LENGTH = 255;               //����������� ����� �������� ������
    const POLL_RESULT_PROP_NAME_MAX_LENGTH = 255;   //����������� ����� �������� �� � ������������ ������
    const POLL_RESULT_PROP_ROW_COUNT = 3;           //������ ���� �������� �� � ������������ ������
    const DATE_COMPLETE_DEFAULT_VALUE = 'N';        //�������� �� ��������� ���� "���� ����������� ������"
	
	static $pollResultIbProps = [
		'DOMAIN' => [
			'NAME' => '�����',
			'TYPE' => 'S',
		],
		'EMPLOYEE' => [
			'NAME' => '���������',
			'TYPE' => 'S',
		],
		'POLL_COMPLETE_DATE' => [
			'NAME' => '���� ����������� ������',
			'TYPE' => 'S',
			'USER_TYPE' => 'Date',
		],
		'POLL' => [
			'NAME' => '�����',
			'TYPE' => 'S',
		],
		'APP24' => [
			'NAME' => '����������24',
			'TYPE' => 'E',
			'LINK_IBLOCK_ID' => self::IB_APP24_ID,
		],
	];
	
	/**
     * ����������� ��������� ������ �� utf8 � cp1251
     * @param $arr
     * @return array
     */
	static function iconvArray($arr) {
		foreach ($arr as $key => $value) {
			if (is_array($value)) {
				$arr[$key] = iconvarray($value);
			} else {
				$arr[$key] = iconv('utf8', 'cp1251', $value);
			}
		}
		return $arr;
	}
	
	/**
     * ��������� ��������� �����������
     * @param $pollName
     * @return array
     * @todo �������� �������� �� ������������� ���������� �� ����� userId + pollId + domain.
     * @todo ������� �� ������ �������.
     */
    static function dailyResultsHandler($resultData) {
        try {
			if(!is_array($resultData['results']) || !count($resultData['results'])) {
				throw new \Exception('�� �������� ����������');
			}
			
			$domain = $resultData['domain'] ? $resultData['domain'] : false;
			$appId = false;			
			
			if($resultData['app24']) {
				$arAppData = self::getAppData(['CODE' => $resultData['app24']]);
				
				if($arAppData['error']) {
                    throw new \Exception($arAppData['error']);
                }

                if(count($arAppData['data'])) {
					$appId = $arAppData['data'][0]['ID'];
                }
			}
			
			$pollCodes = [];
			$questionCodes = [];
			
			foreach($resultData['results'] as $result) {
				array_push($pollCodes, $result['POLL_CODE']);
				$questionCodes = array_merge($questionCodes, array_keys($result['RESULT']));
			}
			
			$arPolls = self::getPollsData(['CODE' =>  $pollCodes]);
			
			if($arPolls['error']) {
				throw new \Exception($arPolls['error']);
			}
			
			$arPolls = $arPolls['data'];
			
			
			$arQuestions = self::getQuestionsData(['CODE' =>  $questionCodes]);
			
			if($arQuestions['error']) {
				throw new \Exception($arQuestions['error']);
			}
			
			$arQuestions = $arQuestions['data'];
			
			$pollsInfo = [];
			$questionsInfo = [];
			
			foreach($arPolls as $arPoll) {
				$pollsInfo[$arPoll['CODE']] = $arPoll;
			}
			
			foreach($arQuestions as $arQuestion) {
				$questionsInfo[$arQuestion['CODE']] = $arQuestion;
			}
			
			$baseProps = [];
			
			if($appId) {
				$baseProps['APP24'] = $appId;
			}
			
			if($domain) {
				$baseProps['DOMAIN'] = $domain;
			}
			
			foreach($resultData['results'] as $result) {
				if(!$pollsInfo[$result['POLL_CODE']]['UF_RESULT_IB']) {
					continue;
				}
				
				$resultIbId = $pollsInfo[$result['POLL_CODE']]['UF_RESULT_IB'];
				
				$arProps = $baseProps;
				$arProps['EMPLOYEE'] = $result['USER_ID'];
				$resultName = $result['USER_ID'];
				
				if($resultData['users'][$result['USER_ID']]) {
					$userName = self::formUserName($resultData['users'][$result['USER_ID']]);
					
					$arProps['EMPLOYEE'] = $userName;
					$resultName = $userName;
				}
				
				$arProps['POLL'] = $result['POLL_CODE'];
				
				if($pollsInfo[$result['POLL_CODE']]['NAME']) {
					$arProps['POLL'] = $pollsInfo[$result['POLL_CODE']]['NAME'];
				}
				
				if($result['COMPLETE'] !== self::DATE_COMPLETE_DEFAULT_VALUE) {
					$arProps['POLL_COMPLETE_DATE'] = $result['COMPLETE'];
				}
				
				foreach($result['RESULT'] as $questionCode => $answer) {
					if(!$questionsInfo[$questionCode]['IB_RES_PROP_ID']) {
						continue;
					}
				
					$arProps[$questionsInfo[$questionCode]['IB_RES_PROP_ID']] = $answer;
				}
				
				if($domain) {
					$resultName .= ' (' . $domain . ')';
				}
				
				self::addPollResult($resultIbId, $resultName, $arProps);
			}
		} catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }
	
	/**
     * ���������� ����������� � �� � ������������
     * @param $resultIb
     * @param $resultName
     * @param $arProps
     * @return array
     */
    static function addPollResult($resultIb, $resultName, $arProps) {
        try {
			Loader::includeModule("iblock");
				
			$obPoll = new \CIBlockElement;
			$arFields = [
				'IBLOCK_ID' => $resultIb,
				'NAME' => $resultName,
				'PROPERTY_VALUES' => $arProps,
			];
			
			if($resultId = $obPoll->Add($arFields)) {
				return [
					'data' => $resultId,
					'error' => false
				];
			} else {
			   throw new \Exception($obPoll->LAST_ERROR);
			}
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }
	
	/**
     * ������������ ����� ������������
     * @param $userData
     * @return array
     */
    static function formUserName($userData) {
        return \CUser::FormatName('#LAST_NAME# #NAME# #SECOND_NAME# (#EMAIL#)', $userData);
    }

    /**
     * ��������� ������ � �����������24
     * @param $arAppData
     * @return array
     */
    static function getAppData($arAppData) {
        try {
            if(!$arAppData || !is_array($arAppData)) {
                throw new \Exception('�� ������� ������ ��� ����������!');
            }
			
			Loader::includeModule("iblock");

            $arSelect = ['ID', 'NAME'];
            $arFilter = array_merge(['IBLOCK_ID' => self::IB_APP24_ID], $arAppData);
            $obResult = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
            $arResult = [];

            while($appData = $obResult ->Fetch()) {
                array_push($arResult, $appData);
            }

            return [
                'data' => $arResult,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * ��������� ��� ������
     * @param $pollName
     * @param $arAppData
     * @return bool|string
     */
    static function formPollName($pollName, $arAppData) {
        return substr($pollName . ' (' .
            implode(', ',
                array_map(
                    function ($app) {
                        return $app['NAME'];
                    },
                    $arAppData
                )
            ) .
            ')', 0, self::POLL_NAME_MAX_LENGTH);
    }

    /**
     * ��������� �� � ������������ ������
     * @param $pollName
     * @return array
     */
    static function addPollResultIb($pollName) {
        try {
			Loader::includeModule("iblock");
			
            $arFields = [
                'ACTIVE' => 'Y',
                'NAME' => $pollName,
                'IBLOCK_TYPE_ID' => self::RESULTS_IB_TYPE,
                'SITE_ID' => [self::SITE_ID],
                'GROUP_ID' => [self::ALL_USERS_GROUP => 'W']
            ];

            $obIb = new \CIBlock;

            $resultIbId = $obIb->Add($arFields);

            if(!$resultIbId) {
                throw new \Exception($obIb->LAST_ERROR);
            }
			
			
			foreach(self::$pollResultIbProps as $propCode => $propData) {
				$callData = [
					'NAME' => $propData['NAME'],
					'CODE' => $propCode,
					'PROPERTY_TYPE' => $propData['TYPE'],
					'ROW_COUNT' => self::POLL_RESULT_PROP_ROW_COUNT,
					'IBLOCK_ID' => $resultIbId
				];
				
				if($propData['USER_TYPE']) {
					$callData['USER_TYPE'] = $propData['USER_TYPE'];
				}
				
				if($propData['LINK_IBLOCK_ID']) {
					$callData['LINK_IBLOCK_ID'] = $propData['LINK_IBLOCK_ID'];
				}
				
				$propAddResult = self::addPollResultProperty($callData);

				if($propAddResult['error']) {
					throw new \Exception($propAddResult['error']);
				}
			}
			
            return [
                'data' => $resultIbId,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * ��������� �� � ������������ ������
     * @param $ibId
     * @param $pollName
     * @return array
     */
    static function updatePollResultIb($ibId, $pollName) {
        try {
			Loader::includeModule("iblock");
			
            $arFields = [
                'NAME' => $pollName,
            ];

            $obIb = new \CIBlock;

            $result = $obIb->Update($ibId, $arFields);

            if(!$result) {
                throw new \Exception($obIb->LAST_ERROR);
            }

            return [
                'data' => $result,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * ��������� ������ ������
     * @param $data
     * @return array
     */
    static function getPollData($data) {
        try {
			Loader::includeModule("iblock");
			
			$arFilter = array_merge(['IBLOCK_ID' => self::IB_POLL_ID], $data);
            $arSelect = ['UF_RESULT_IB'];
            $rsSect = \CIBlockSection::GetList([], $arFilter, false, $arSelect);
            $result = $rsSect->Fetch();

            if($result) {
                return [
                    'data' => $result,
                    'error' => false
                ];
            } else {
                throw new \Exception('����� �� ������');
            }
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }
	
	/**
     * ��������� ������ �������
     * @param $data
     * @return array
     */
    static function getPollsData($data) {
        try {
			Loader::includeModule("iblock");
			
			$arFilter = array_merge(['IBLOCK_ID' => self::IB_POLL_ID], $data);
            $arSelect = ['UF_RESULT_IB'];
            $rsSect = \CIBlockSection::GetList([], $arFilter, false, $arSelect);
            $result = [];
			
			while($arSect = $rsSect->Fetch()) {
                array_push($result, $arSect);
            }

            return [
				'data' => $result,
				'error' => false
			];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * ��������� ������ �������
     * @param $data
     * @return array
     */
    static function getQuestionData($data) {
        try {
			Loader::includeModule("iblock");
			
			$arFilter = array_merge(["IBLOCK_ID" => self::IB_POLL_ID], $data);

            $arSelect = [
                "ID",
                "IBLOCK_ID",
                "NAME",
                "CODE",
                "PROPERTY_IB_RES_PROP_ID"
            ];

            $obQuestion = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
            $result = $obQuestion->Fetch();
            
            if($result) {
                foreach($result as $field => $val) {
                    if(preg_match('/PROPERTY_(\w+?)_VALUE/', $field)) {
                        if(preg_match('/^PROPERTY_(\w+?)_VALUE$/', $field, $matches)) {
                            $prop = $matches[1];
                            $result[$prop] = $val;
                        }

                        unset($result[$field]);
                    }
                }

                return [
                    'data' => $result,
                    'error' => false
                ];
            } else {
                throw new \Exception('������ �� ������');
            }
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }
	
	/**
     * ��������� ������ ��������
     * @param $data
     * @return array
     */
    static function getQuestionsData($data) {
        try {
			Loader::includeModule("iblock");
			
			$arFilter = array_merge(["IBLOCK_ID" => self::IB_POLL_ID], $data);

            $arSelect = [
                "ID",
                "IBLOCK_ID",
                "NAME",
                "CODE",
                "PROPERTY_IB_RES_PROP_ID"
            ];

            $obQuestion = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
            $result = [];
			
			while($arQuestion = $obQuestion->Fetch()) {
				foreach($arQuestion as $field => $val) {
                    if(preg_match('/PROPERTY_(\w+?)_VALUE/', $field)) {
                        if(preg_match('/^PROPERTY_(\w+?)_VALUE$/', $field, $matches)) {
                            $prop = $matches[1];
                            $arQuestion[$prop] = $val;
                        }

                        unset($arQuestion[$field]);
                    }
                }
				
				array_push($result, $arQuestion);
			}
			
			return [
				'data' => $result,
				'error' => false
			];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * ���������� �������� ������ �� ������ � �� � ������������
     * @param $resultIb
     * @param $questionName
     * @param $questionCode
     * @return array
     */
    static function addPollResultProperty($arFields) {
        try {
			Loader::includeModule("iblock");
			
            $obProp = new \CIBlockProperty;

            $resultPropId = $obProp->Add($arFields);

            if(!$resultPropId) {
                throw new \Exception($obProp->LAST_ERROR);
            }

            return [
                'data' => $resultPropId,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * ���������� �������� ������ �� ������ � �� � ������������
     * @param $resultPropId
     * @param $questionName
     * @param $questionCode
     * @return array
     */
    static function updatePollResultProperty($resultPropId, $questionName, $questionCode) {
        try {
			Loader::includeModule("iblock");
			
            $arFields = [
                'NAME' => $questionName,
                'CODE' => $questionCode,
            ];

            $obProp = new \CIBlockProperty;

            $resultPropUpdate = $obProp->Update($resultPropId, $arFields);

            if(!$resultPropUpdate) {
                throw new \Exception($obProp->LAST_ERROR);
            }

            return [
                'data' => $resultPropUpdate,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * �������� �������� ������ �� ������ � �� � ������������
     * @param $resultPropId
     * @return array
     */
    static function deletePollResultProperty($resultPropId) {
        try {
			Loader::includeModule("iblock");
			
            $obProp = new \CIBlockProperty;

            $resultPropDelete = $obProp->Delete($resultPropId);

            if(!$resultPropDelete) {
                throw new \Exception($obProp->LAST_ERROR);
            }

            return [
                'data' => $resultPropDelete,
                'error' => false
            ];
        } catch(\Exception $e) {
            return [
                'data' => false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * ���������� ������� "�� ���������� ������"
     * ������� �� � ������������ ������
     * @param $pollData
     * @return bool
     */
    static function onBeforePollAdd(&$pollData) {
        try {
            $iblockId = $pollData['IBLOCK_ID'];
            $pollName = $pollData['UF_NAME'];
            $arAppId = $pollData['UF_APP24'];

            if($iblockId == self::IB_POLL_ID) {
                $arAppData = self::getAppData(['ID' => $arAppId]);

                if($arAppData['error']) {
                    throw new \Exception($arAppData['error']);
                }

                if(!$arAppData['data']) {
                    throw new \Exception('���������� �� �������!');
                }

                $pollName = self::formPollName($pollName, $arAppData['data']);

                $resultIbData = self::addPollResultIb($pollName);

                if($resultIbData['error']) {
                    throw new \Exception($resultIbData['error']);
                }

                $pollData['NAME'] = $pollName;
                $pollData['UF_RESULT_IB'] = $resultIbData['data'];
            }
        } catch(\Exception $e) {
            global $APPLICATION;
            $APPLICATION -> ThrowException($e->getMessage());
            return false;
        }
    }

    /**
     * ���������� ������� "�� ���������� ������"
     * ��������� �������� �� � ������������ ������
     * @param $pollData
     * @return bool
     */
    static function onBeforePollUpdate(&$pollData) {
        try {
            $iblockId = $pollData['IBLOCK_ID'];
            $pollName = $pollData['UF_NAME'];
            $pollId = $pollData['ID'];
            $arAppId = $pollData['UF_APP24'];

            if($iblockId == self::IB_POLL_ID && $pollId) {
                $pollDataEx = self::getPollData(['ID' => $pollId]);

                if($pollDataEx['error']) {
                    throw new \Exception($pollDataEx['error']);
                }

                $resultIb = $pollDataEx['data']['UF_RESULT_IB'];

                if(!$resultIb) {
                    throw new \Exception('�� ������� �������� �������� � ������������ ������!');
                }

                $arAppData = self::getAppData(['ID' => $arAppId]);

                if($arAppData['error']) {
                    throw new \Exception($arAppData['error']);
                }

                if(!$arAppData['data']) {
                    throw new \Exception('���������� �� �������!');
                }

                $pollName = self::formPollName($pollName, $arAppData['data']);

                $resultIbData = self::updatePollResultIb($resultIb, $pollName);

                if($resultIbData['error']) {
                    throw new \Exception($resultIbData['error']);
                }

                $pollData['NAME'] = $pollName;
            }
        } catch(\Exception $e) {
            global $APPLICATION;
            $APPLICATION -> ThrowException($e->getMessage());
            return false;
        }
    }

    /**
     * ���������� ������� "�� �������� �������"
     * ���������� �������� ������ �� ������ � �� � ������������
     * @param $questionData
     * @return bool
     */
    static function onBeforeQuestionAdd(&$questionData) {
        try {
            $iblockId = $questionData['IBLOCK_ID'];
            $questionName = $questionData['NAME'];
            $questionCode = $questionData['CODE'];
            $pollId = reset($questionData['IBLOCK_SECTION']);
            
            if($iblockId == self::IB_POLL_ID) {
                if(!$pollId) {
                    throw new \Exception('�������� �������� � ����� ��������!');
                }

                $pollDataEx = self::getPollData(['ID' => $pollId]);

                if($pollDataEx['error']) {
                    throw new \Exception($pollDataEx['error']);
                }

                $resultIb = $pollDataEx['data']['UF_RESULT_IB'];

                if(!$resultIb) {
                    throw new \Exception('�� ������� �������� �������� � ������������ ������!');
                }

                $propName = substr($questionName, 0, self::POLL_RESULT_PROP_NAME_MAX_LENGTH);
                $propAddResult = self::addPollResultProperty([
					'NAME' => $propName,
					'CODE' => $questionCode,
					'PROPERTY_TYPE' => 'S',
					'ROW_COUNT' => self::POLL_RESULT_PROP_ROW_COUNT,
					'IBLOCK_ID' => $resultIb
				]);

                if($propAddResult['error']) {
                    throw new \Exception($propAddResult['error']);
                }

                $questionData['PROPERTY_VALUES']['IB_RES_PROP_ID'] = $propAddResult['data'];
            }
        } catch(\Exception $e) {
            global $APPLICATION;
            $APPLICATION -> ThrowException($e->getMessage());
            return false;
        }
    }

    /**
     * ���������� ������� "�� ���������� �������"
     * ���������� �������� ������ �� ������ � �� � ������������
     * @param $questionData
     * @return bool
     */
    static function onBeforeQuestionUpdate($questionData) {
        try {
            $iblockId = $questionData['IBLOCK_ID'];
            $questionId = $questionData['ID'];
            $questionName = $questionData['NAME'];
            $questionCode = $questionData['CODE'];
            
            if($iblockId == self::IB_POLL_ID) {
                $questionDataEx = self::getQuestionData(['ID' => $questionId]);

                if($questionDataEx['error']) {
                    throw new \Exception($questionDataEx['error']);
                }

                $resultPropId = $questionDataEx['data']['IB_RES_PROP_ID'];

                if(!$resultPropId) {
                    throw new \Exception('�� ������� �������� ID �������� ���������� �������!');
                }

                $propName = substr($questionName, 0, self::POLL_RESULT_PROP_NAME_MAX_LENGTH);
                $propAddResult = self::updatePollResultProperty($resultPropId, $propName, $questionCode);

                if($resultIbData['error']) {
                    throw new \Exception($resultIbData['error']);
                }
            }
        } catch(\Exception $e) {
            global $APPLICATION;
            $APPLICATION -> ThrowException($e->getMessage());
            return false;
        }
    }

    /**
     * ���������� ������� "�� �������� �������"
     * �������� �������� ������ �� ������ � �� � ������������
     * @param $questionId
     */
    static function onBeforeQuestionDelete($questionId) {
        try {
            $questionDataEx = self::getQuestionData(['ID' => $questionId]);

            if($questionDataEx['error']) {
                throw new \Exception($questionDataEx['error']);
            }

            $iblockId = $questionDataEx['data']['IBLOCK_ID'];

            if($iblockId == self::IB_POLL_ID) {
                $resultPropId = $questionDataEx['data']['IB_RES_PROP_ID'];

                if(!$resultPropId) {
                    throw new \Exception('�� ������� �������� ID �������� ���������� �������!');
                }

                $propAddResult = self::deletePollResultProperty($resultPropId);

                if($resultIbData['error']) {
                    throw new \Exception($resultIbData['error']);
                }
            }
        } catch(\Exception $e) {
            global $APPLICATION;
            $APPLICATION -> ThrowException($e->getMessage());
        }
    }

    /**
     * ����������� ������������ �������
     */
    static function addEventHandlers() {

        EventManager::getInstance()->addEventHandler(
            "iblock",
            "OnBeforeIBlockSectionAdd",
            array(
                "App24\\Polls\\PollsService",
                "onBeforePollAdd"
            )
        );

        EventManager::getInstance()->addEventHandler(
            "iblock",
            "OnBeforeIBlockSectionUpdate",
            array(
                "App24\\Polls\\PollsService",
                "onBeforePollUpdate"
            )
        );

        EventManager::getInstance()->addEventHandler(
            "iblock",
            "OnBeforeIBlockElementAdd",
            array(
                "App24\\Polls\\PollsService",
                "onBeforeQuestionAdd"
            )
        );

        EventManager::getInstance()->addEventHandler(
            "iblock",
            "OnBeforeIBlockElementUpdate",
            array(
                "App24\\Polls\\PollsService",
                "onBeforeQuestionUpdate"
            )
        );

        EventManager::getInstance()->addEventHandler(
            "iblock",
            "OnBeforeIBlockElementDelete",
            array(
                "App24\\Polls\\PollsService",
                "onBeforeQuestionDelete"
            )
        );
    }
}

PollsService::addEventHandlers();