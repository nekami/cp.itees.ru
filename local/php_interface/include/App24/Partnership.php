<?php
namespace App24\Partnership;
use Bitrix\Main\EventManager;

Class Partnership {
    const IB_PARTNERS_ID = 607;
	const CLIENTS_PROPERTY = 'PROPERTY_2385';
	const PORTAL_PROPERTY_ID = 2383;
	
	static function sendToApp($query_data) {	
		$link = 'http://salescriptpro.ka.app.itees.ru/server/partnership/partner_handler.php';

		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_POST => 1,
			CURLOPT_HEADER => 0,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $link,
			CURLOPT_POSTFIELDS => http_build_query($query_data),
		));

		$result = curl_exec($curl);
		curl_close($curl);
		
		$result = json_decode($result);
		// RewriteFile($_SERVER["DOCUMENT_ROOT"]."/logs/ak/040220_3.txt", print_r($result, true));

		return $result;
	}
	
    static function OnAdminListDisplay(&$list) {
        try {
			parse_str(parse_url($GLOBALS['APPLICATION']->GetCurUri())['query'], $arQuery);
			
			if($arQuery['IBLOCK_ID'] != self::IB_PARTNERS_ID) {
				return;
			}
			
			foreach($list->aRows as &$row) {
				//$regexpLink = '/>([0-9a-zA-Zа-яА-Я]+)</';
				$regexpLink = '/>(.+)</';
				
				preg_match($regexpLink, $row->aFields['ID']['view']['value'], $matches);
				$partnerId = $matches[1];
				
				preg_match($regexpLink, $row->aFields['NAME']['view']['value'], $matches);
				$partnerName = $matches[1];
				
				$row->aFields['NAME']['view']['value'] = "<a target='_blank' href='https://cp.itees.ru/partnership/?partner=${partnerId}'>" . $partnerName . '</a>';
				
				$clients = explode(' / ', $row->aFields[self::CLIENTS_PROPERTY]['view']['value']);
				
				$row->aFields[self::CLIENTS_PROPERTY]['view']['value'] = '';
				
				foreach($clients as $client) {
					$row->aFields[self::CLIENTS_PROPERTY]['view']['value'] .= "<a target='_blank' href='https://cp.itees.ru/partnership/?partner=${partnerId}&client=${client}'>" . $client . '</a><br />';
				}
				
			}
			
        } catch(\Exception $e) {
            global $APPLICATION;
            $APPLICATION -> ThrowException($e->getMessage());
        }
    }
	
	static function OnAfterPartnerAdd($arFields) {
        try {
			if($arFields['IBLOCK_ID'] != self::IB_PARTNERS_ID || !$arFields['RESULT']) {
				return;
			}
			
			self::sendToApp([
				'ACTION' => 'ADD',
				'PORTAL' => reset($arFields['PROPERTY_VALUES'][self::PORTAL_PROPERTY_ID])['VALUE'],
				'PARTNER_ID' => $arFields['ID']
			]);

        } catch(\Exception $e) {
            global $APPLICATION;
            $APPLICATION -> ThrowException($e->getMessage());
        }
    }
	
	static function OnAfterPartnerUpdate($arFields) {
        try {
			if($arFields['IBLOCK_ID'] != self::IB_PARTNERS_ID || !$arFields['RESULT']) {
				return;
			}
			
			self::sendToApp([
				'ACTION' => 'UPDATE',
				'PORTAL' => reset($arFields['PROPERTY_VALUES'][self::PORTAL_PROPERTY_ID])['VALUE'],
				'PARTNER_ID' => $arFields['ID']
			]);
			
        } catch(\Exception $e) {
            global $APPLICATION;
            $APPLICATION -> ThrowException($e->getMessage());
        }
    }
	
	static function OnAfterPartnerDelete($arFields) {
        try {
			if($arFields['IBLOCK_ID'] != self::IB_PARTNERS_ID) {
				return;
			}
			
			self::sendToApp([
				'ACTION' => 'DELETE',
				'PARTNER_ID' => $arFields['ID']
			]);
			
        } catch(\Exception $e) {
            global $APPLICATION;
            $APPLICATION -> ThrowException($e->getMessage());
        }
    }

    /**
     * Регистрация обработчиков событий
     */
    static function addEventHandlers() {

        EventManager::getInstance()->addEventHandler(
            "main",
            "OnAdminListDisplay",
            array(
                "App24\\Partnership\\Partnership",
                "OnAdminListDisplay"
            )
        );
		
		EventManager::getInstance()->addEventHandler(
            "iblock",
            "OnAfterIBlockElementAdd",
            array(
                "App24\\Partnership\\Partnership",
                "OnAfterPartnerAdd"
            )
        );
        
		EventManager::getInstance()->addEventHandler(
            "iblock",
            "OnAfterIBlockElementUpdate",
            array(
                "App24\\Partnership\\Partnership",
                "OnAfterPartnerUpdate"
            )
        );
		
		EventManager::getInstance()->addEventHandler(
            "iblock",
            "OnAfterIBlockElementDelete",
            array(
                "App24\\Partnership\\Partnership",
                "OnAfterPartnerDelete"
            )
        );
    }
}

Partnership::addEventHandlers();