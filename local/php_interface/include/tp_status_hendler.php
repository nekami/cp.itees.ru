<?
AddEventHandler("main", "OnAdminListDisplay", array("SupportTab", "supExec"));
AddEventHandler("support", "OnBeforeTicketUpdate", array("SupportTab", "OnBeforeTicketUpdateHandler"));

class SupportTab
{
    public static function supExec(&$list)
    {
        if ($_REQUEST["find_type"] == "S") {
            $GLOBALS["APPLICATION"]->SetAdditionalCSS("/local/css/support.css");

            $list->AddHeaders(array(
                Array
                (
                    "id" => "FINAL",
                    "content" => "���������",
                    "default" => true,
                    "__sort" => -1
                )
            ));

            foreach ($list->aRows as &$row) {
                $uf_status = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("TEH_SUPPORT_STATUS", $row->id);
                if ($uf_status["UF_TS_STATUS_FINAL"]["VALUE"] == 1)
                    $final = "status_final_yes";
                else
                    $final = "status_final_no";
                $finalHTML = '<div data-id=' . $row->id . ' class=' . $final . '></div>';

                $row->AddField("FINAL", $finalHTML);

                $arActions = $row->aActions;
                $arActions[] = array("SEPARATOR" => true);
                $arActions[] = array(
                    "DEFAULT" => true,
                    "TEXT" => "���������",
                    "ACTION" => $list->ActionAjaxReload("/local/ajax/support_ajax.php?ID=" . $row->id . "&FINAL=" . $final) . 'window.location.reload();',
                    "ICON" => ($final == "status_final_yes" ? "checked" : ""),
                );
                $row->AddActions($arActions);
            }
        }
    }

    public static function OnBeforeTicketUpdateHandler($arFields)
    {
        if (!empty($_REQUEST["STATUS_ID"])) {
            $arFields["STATUS_ID"] = $_REQUEST["STATUS_ID"];
        }
        return $arFields;
    }
}