<?

\Bitrix\Main\Loader::includeModule('learning');

Class LearningSearch {
    
    //������ ���� ��������� ������ ��� ���������� url
    protected static function getParentId($id, $parents) {
        if(!empty($id)) {
            $parents[] = $id;
            $lessons = \CLearnLesson::ListImmediateParents($id);
            $parents = self::getParentId($lessons[0]['PARENT_LESSON'], $parents);
        }
        return $parents;
    }

    //�������� url �������� ������
    public static function getLessonUrl($item, $itemId) {

        $url = false;
        $parents = self::getParentId($itemId[2], $parents);
        $parents = array_reverse($parents);

        if(self::isActiveLesson($parents)) {
            $parents = implode('.', $parents);
            $url = '/services/learning/course.php?COURSE_ID=' . $itemId[1] . '&LESSON_ID=' . $itemId[2] . '&LESSON_PATH=' . $parents;
        }

        return $url;
    }

    //����������� ��������� ���������� ���������� �������
    public static function encodingSearch($search) {
        return iconv(
            mb_detect_encoding($search),
            LANG_CHARSET,
            $search
        );
    }

    //��������� ���������� ��������� ���������
    protected static function isActiveLesson($itemId) {
        if(!empty($itemId)) {
            $lessons = \CLearnLesson::GetList(
                [],
                [
                    'LESSON_ID' => $itemId,
                    'ACTIVE' => 'Y',
                    'CHECK_PERMISSIONS' => 'Y'
                ],
                ['LESSON_ID']
            );

            $availableLessons = [];

            while($lesson = $lessons->fetch()) {
                $availableLessons[] = $lesson['LESSON_ID'];
            }

            if(empty(array_diff($itemId, $availableLessons))) {
                return true;
            }
        }
        return false;
    }
}