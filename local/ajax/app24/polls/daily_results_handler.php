<?
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST");
header("Access-Control-Allow-Headers: *");

use App24\Polls\PollsService;

define("NOT_CHECK_PERMISSIONS", true);
define("NO_KEEP_STATISTIC", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// $test = [
    // 'results' => [
            // 0 => [
                    // 'ID' => 261230,
                    // 'POLL_CODE' => 'sibir',
                    // 'USER_ID' => 15,
                    // 'COMPLETE' => '15.07.2019',
                    // 'CLOSE_DATE' => 'N',
                    // 'SEND_DATE' => 'N',
                    // 'RESULT' => [
                            // 'pro_sobak' => '��������',
                            // 'pro_kotov' => '�����'
                        // ]

                // ],
				
			// 1 => [
                    // 'ID' => 261230,
                    // 'POLL_CODE' => 'moskva',
                    // 'USER_ID' => 15,
                    // 'COMPLETE' => 'N',
                    // 'CLOSE_DATE' => 'N',
                    // 'SEND_DATE' => 'N',
                    // 'RESULT' => [
                            // 'sobachka' => 'lol',
                            // 'koshechka' => 'lolka'
                        // ]

                // ],
			// 2 => [
                    // 'ID' => 261230,
                    // 'POLL_CODE' => 'moskva',
                    // 'USER_ID' => 16,
                    // 'COMPLETE' => '15.07.2019',
                    // 'CLOSE_DATE' => 'N',
                    // 'SEND_DATE' => 'N',
                    // 'RESULT' => [
                            // 'sobachka' => 'lol2',
                            // 'koshechka' => 'lolka2'
                        // ]

                // ],
			// 3 => [
                    // 'ID' => 261230,
                    // 'POLL_CODE' => 'sibir',
                    // 'USER_ID' => 17,
                    // 'COMPLETE' => '15.07.2019',
                    // 'CLOSE_DATE' => 'N',
                    // 'SEND_DATE' => 'N',
                    // 'RESULT' => [
                            // 'pro_sobak' => '��������2',
                            // 'pro_kotov' => '�����2'
                        // ]

                // ],

    // ],

    // 'users' => [
            // 15 => [
                    // 'EMAIL' => 'ka@itees.ru',
                    // 'NAME' => '������',
                    // 'LAST_NAME' => '�������',
                    // 'SECOND_NAME' => ''
                // ],
			// 16 => [
                    // 'EMAIL' => 'sa@itees.ru',
                    // 'NAME' => '�����',
                    // 'LAST_NAME' => '���������',
                    // 'SECOND_NAME' => ''
                // ],
			// 17 => [
                    // 'EMAIL' => 'la@itees.ru',
                    // 'NAME' => '��������',
                    // 'LAST_NAME' => '�������',
                    // 'SECOND_NAME' => ''
                // ],

        // ],

    // 'domain' => 'itees.bitrix24.ru',
    // 'app24' => 'itees.salescriptpro'
// ];

try {
	$result = PollsService::dailyResultsHandler(PollsService::iconvArray($_POST));
	
	if($result['error']) {
		throw new Exception($result['error']);
	}
} catch(Exception $e) {
	exit($e->getMessage());
}

