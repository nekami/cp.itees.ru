<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$ID = (int)$_REQUEST["ID"];

if($_REQUEST["FINAL"] == "status_final_yes")
	$statusValue = 0;
elseif($_REQUEST["FINAL"] == "status_final_no")
	$statusValue = 1;
	
$GLOBALS["USER_FIELD_MANAGER"]->Update("TEH_SUPPORT_STATUS", $ID, Array("UF_TS_STATUS_FINAL" => $statusValue));
?>