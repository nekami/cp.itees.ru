<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if($arResult['CUSTOMIZATION']['CSS_PATH'])
{
	$this->addExternalCss($arResult['CUSTOMIZATION']['CSS_PATH']);
}
?>
<div id="imopenlines-page-placeholder" class="imopenlines-page-placeholder"></div>
<script type="text/javascript">
	<?
	$appDomain = $_GET['app_domain'] ?? null;
	$appCode = $_GET['app_code'] ?? null;
	$appName = $_GET['app_name'] ?? null;
	
	$hashCode = 'PYQBvVC5xOnWON';
	$hash = md5($appDomain.'_'.$hashCode);
	
	if($appName) {
		$appName = str_replace('20%', ' ', iconv('utf-8', 'windows-1251//TRANSLIT', $appName));
	}
	
	if(!empty($appDomain) && !empty($appCode)) :
	?>

	window.addEventListener('onBitrixLiveChat', function(event)
	{
		var
		isCheck = false,
		widget = event.detail.widget;
		
		if(widget.getUserData().id == -1) {
			widget.setUserRegisterData({
				'hash': '<?=$hash?>',
			});
		}
		
		widget.subscribe({
			type: BX.LiveChatWidget.SubscriptionType.userMessage,
			callback: function(data) {
				if(isCheck) return;
				
				BX.ajax.post(
					'/pub/dev/openline.php',
					{
						'hash': '<?=$hash?>',
						'app_domain': '<?=$appDomain?>',
						'app_code': '<?=$appCode?>',
						'app_name': '<?=$appName?>',
						'user_data': widget.getUserData(),
						'user_id': widget.getUserId(),
						'chat_id': widget.getChatId(),
					},
					function(res) {
						isCheck = true;
						console.log(res);
					}
				);
			}
		});
		
		widget.setCustomData([
			{"GRID": [
			  {
				"NAME" : "<?=$MESS['MESS_PORTAL']?>",
				"VALUE" : "[url=https://<?=$appDomain?>]<?=$appDomain?>[/url]",
				"DISPLAY" : "LINE",
			  },
			  {
				"NAME" : "<?=$MESS['MESS_APPC_CODE']?>",
				"VALUE" : "<?=$appCode?>",
			  },
			  <? if($appName): ?>
			  {
				"NAME" : "<?=$MESS['MESS_APPC_NAME']?>",
				"VALUE" : "<?=$appName?>",
			  },
			  <? endif; ?>
			]}
		]);
	});
	<? endif; ?>
	
	<?= $arResult['WIDGET_CODE']; ?>
</script>


