<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 


?>

<script>
// нужен ли запрос в БД
BX.QueryDB = '<?=$arParams["QueryDB"] ? true : false?>';
</script>

<? 
global $USER;
$arStatusList = CIdeaManagment::getInstance()->Idea()->GetStatusList(); ?>

<div class="task-detail-sidebar-item">								
	<div class="task-detail-sidebar-item-value">
		<div class="task-section task-section-status-wrap" id="ideaStagesWrap">
			<div class="task-section-status-container">				
				<input type="hidden" name="UF_STATUS" value="<?=$arParams["arUserField"]["VALUE"]?>">
				<div class="task-section-status-container-flex" id="ideaStages"></div>
			</div>
		</div>
	</div>
</div>
							
<script>
	new IdeaSidebar({
		ideaId: <?=!empty($arParams["arUserField"]["ENTITY_VALUE_ID"]) ? $arParams["arUserField"]["ENTITY_VALUE_ID"] : 0?>,
		stageId: <?=!empty($arParams["arUserField"]["VALUE"]) ? $arParams["arUserField"]["VALUE"] : ID_STATUS_FIRST ?>,
		stages: <?= \CUtil::PhpToJSObject(array_values($arStatusList), false, false, true)?>,
		htmlStagesWrapId: 'ideaStagesWrap',
		htmlStagesId: 'ideaStages',
		can: '<?=$USER->IsAdmin() ? true : false?>'
	});
</script>