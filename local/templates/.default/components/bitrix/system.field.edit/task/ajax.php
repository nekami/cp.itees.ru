<? require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("tasks");
CModule::IncludeModule("blog");

// ��������� ������������� ������
if (($_GET['ACTION'] == "GET_RESPONSIBLE_ID") && !empty($_GET['TASK_ID']))
{
	$obTasks = CTasks::GetList(array(), array("ID" => $_GET['TASK_ID']), array("ID", "RESPONSIBLE_ID"),array());

	if ($arTask = $obTasks->GetNext())
	{
		echo json_encode(array('error' => false, 'IdResponsible' => $arTask["RESPONSIBLE_ID"]));
	}
    else 
	{
		echo json_encode(array('error' => true));
	}
	
	// ������ ������ �����
	if ($_GET['ADDITIONAL_ACTION'] == "ADD_TASK")
	{
		$IdIdea = $_GET['IDEA_ID'];		
		$arTask = GetTaskByIdea($IdIdea);		
		$arTask[] = $_GET['TASK_ID'];		
		CBlogPost::Update($IdIdea, ["UF_TASK_BLOG_POST" => $arTask]);
	}	
}

// �������� ������
if (($_GET['ACTION'] == "DELETE_TASK") && !empty($_GET['TASK_ID']) && !empty($_GET['IDEA_ID']))
{	
	$IdIdea = $_GET['IDEA_ID'];
	$IdTask = $_GET['TASK_ID'];
	
	$arTask = GetTaskByIdea($IdIdea);	
	
	if(($key = array_search($IdTask, $arTask)) !== FALSE){
			 unset($arTask[$key]);
	}
	
	$error = false;
	
	
	
	
	if (empty( CBlogPost::Update($IdIdea, ["UF_TASK_BLOG_POST" => $arTask]) )) $error = true;

	echo json_encode(array('error' => $error));
	
}


function GetTaskByIdea($IdIdea)
{
	$dbPosts = CBlogPost::GetList(
        array(),
        array("ID" => $IdIdea),
		false, false,
		array("ID", "UF_TASK_BLOG_POST")
    );
	
	$arTask = [];
	if ($arPost = $dbPosts->Fetch()) 
	{ 
		if (!empty($arPost["UF_TASK_BLOG_POST"]))
			$arTask = $arPost["UF_TASK_BLOG_POST"]; 
	}
	
	return $arTask;
}
?>