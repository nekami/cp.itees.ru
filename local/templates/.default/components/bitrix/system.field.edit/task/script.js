$(function() {
		
	// ��������� ��������� ���� ��� ������ ����
	$.each(BX.arIdIdea, function(id, val) 
	{
		window['popup'+val] = new BX.PopupWindow(
			"TaskAddIdea"+val,	
			// window.document.getElementById('task-options-item-open-inner'+val), 
			$('.task-options-item-open-inner[data-id-idea="'+val+'"]')[0],
			{
				//width: 427,           
				titleBar: '',
				autoHide : true,
				offsetTop : 0,
				offsetLeft : 0,
				lightShadow : true,
				closeIcon : true,
				closeByEsc : false,            
				buttons: [
					new BX.PopupWindowButton({
						text : '�������',
						id : 'choose',
						className : 'popup-window-button popup-window-button-accept',
						events : {
							click: function() {
								window['popup'+val].close();
							}
						}
					}),
					new BX.PopupWindowButton({
						text: '������',
						id: 'cancel',
						className: 'popup-window-button popup-window-button-link popup-window-button-link-cancel',
						events: {
							click: function() {                            
								window['popup'+val].close();
							}
						}
					})
				]	
			}			
		);

		window['popup'+val].setContent($('.task_add_idea')[0]);
	});	
			
	// ��� ������� �� ������ ���������� ������	
	$(document).on('click', 'a[data-bx-id="task-item-set-open-form"]', function() {	
		
        event.preventDefault();
        window['popup'+$(this).attr("data-id-idea")].show();
    });
	
	// ������� ������ ������
	// BX.ArIdTask - ������ � ��� �������������� �������� (��. � ������)

    // ������ ��� ����� ��������� � ����� ������ � �� �����������
	// �.�. ��� on-change ��������� ������ � ������
	// � ��� ������ ������� - ����������� ��������� ������
	
	// BX.TaskObject - ������ � ��������� �������
	
	BX.addCustomEvent("on-change", BX.delegate(function(ParamsTask)
	{
		BX.TaskObject = {};
		
	    $.each(ParamsTask, function(id, val) 
	    {	
			// �������� ��������� ������ �� ������� ��������������			
			if((Object.values(BX.ArIdTask[BX.IdIdea]).indexOf(val.id) == -1) && (val.id != "")) 
			{ 				
				BX.TaskObject.IdTask = val.id;
				BX.TaskObject.NameTask = val.name;
			}		
		});	
	
    }, this)); 
	
	
	// �������, ����������� � ���, ��� ������� �� ������ ������� ��� ����
	BX.FlagAddOnSaveListener = "N";
	
	BX.addCustomEvent("onAfterPopupShow", BX.delegate(function(ParamsPopupShow)
	{		   
	   uniquePopupId  = ParamsPopupShow.uniquePopupId;
	   
	   if (PopupId = uniquePopupId.match(/TaskAddIdea(\d+)/i))
	   {	
			BX.IdIdea = PopupId[1];
   
			if (BX.FlagAddOnSaveListener != "Y") 
			{
				// ���� �� ������ �������				
				$('span#choose').click(function()
				{
					if (BX.TaskObject.IdTask != undefined)
					{
						BX.FlagAddOnSaveListener = "Y";	
						
						BX.ArIdTask[BX.IdIdea][BX.TaskObject.IdTask] = BX.TaskObject.IdTask;

						AddTaskByIdea();	
					}				
				});
			}
	   }		
	}, this));
	
	
	function AddTaskByIdea()
	{		
		// ��������� � ������� � ���� ������ � ��� ���
		if (BX.QueryDB && BX.IdIdea) 
			data = {'ACTION': 'GET_RESPONSIBLE_ID','TASK_ID': BX.TaskObject.IdTask, 'ADDITIONAL_ACTION': 'ADD_TASK', 'IDEA_ID': BX.IdIdea};
		else 
			data = {'ACTION': 'GET_RESPONSIBLE_ID','TASK_ID': BX.TaskObject.IdTask};
	
		$.ajax({ 
			url: '/local/templates/.default/components/bitrix/system.field.edit/task/ajax.php', 
			data: data,
			method: 'get', 
			dataType: 'json',					  
			success: function (answer) 
			{					
				if (answer.error) console.log("������ ������� � ���� ������"); 	
				else 
				{
					AddTaskHtml(BX.TaskObject.IdTask, BX.TaskObject.NameTask, answer.IdResponsible, BX.IdIdea);
				}
			},
			error: function () { console.log("������ ajax-�������"); }
		});
	}
		

/******������ �������� ������*******/
	
	// ������������� ����, � ������� ���� ������ ������ �������� ������
	
	$(document).on('click', ".tasks-buttonAdd", function()
	{
		BX.IdIdea = $(this).attr('data-id-idea');
	});	
	
	
	// ���������� ������ ���������� ������, ������� ����� �� �������� ��������� ������ ������������� �����
	
	BX.addCustomEvent("tasksTaskEvent", BX.delegate(function(action, params)
	{		
	   if (action == "ADD")
	   {
			AddTaskHtml(params.task.ID, params.taskUgly.name, params.taskUgly.directorId, BX.IdIdea);			
			
			if (BX.QueryDB && BX.IdIdea) 
			{				
				$.ajax({ 
					url: '/local/templates/.default/components/bitrix/system.field.edit/task/ajax.php', 
					data: {'ACTION': 'GET_RESPONSIBLE_ID','TASK_ID': params.task.ID, 'ADDITIONAL_ACTION': 'ADD_TASK', 'IDEA_ID': BX.IdIdea}, 
					method: 'get', 
					dataType: 'json',					  
					success: function (msg) 
					{					
						if (msg.error) console.log("������ ������� � ���� ������"); 
					},
					error: function () { console.log("������ ajax-�������"); }
				});				
			}
			
	   }
	   
    }, this));	


	function AddTaskHtml(IdTask, NameTask, IdResponsibleTask, IdIdea)
	{
		var TaskHtml = 	$('<span data-bx-id="task-item-set-item" data-item-value="'+IdTask+'" class="task-inline-selector-item">'
						+'<input type="hidden" name="UF_TASK_BLOG_POST[]" value="'+IdTask+'">'
						+'<span class="task-options-destination task-options-destination-all-users">'
						+'<a href="/company/personal/user/'+IdResponsibleTask+'/tasks/task/view/'+IdTask+'/" target="_blank" class="task-options-destination-text">'+NameTask+'</a>'
						+'<span data-bx-id="task-item-set-item-delete" class="task-option-inp-del" title="�������"></span>'
						+'</span>'
						+'</span>');							
								
		$('span[data-bx-id="task-item-set-items'+IdIdea+'"]').append(TaskHtml);
	}
	
		
	// ������� �� �������
	$(document).on('click', '.task-option-inp-del', function()
	{
		var IdIdea = $(this).parents(".task-options-item-open-inner").attr("data-id-idea");

		if (IdIdea != undefined)
		{
			var IdTask = $(this).parents("[data-item-value]").attr("data-item-value");
				
			$(this).parents('span[data-bx-id="task-item-set-item"]').remove();	
				
			if (BX.ArIdTask[IdIdea].indexOf(IdTask) != -1)
			{
				delete BX.ArIdTask[IdIdea][BX.ArIdTask[IdIdea].indexOf(IdTask)];
			}
				
			// ���� �� �������� ���������, �� �������� ������� ���� ����� � ������ ��������� (����� ��� ������ � ��������� �������� ����)
			if ($('span[data-bx-id="task-item-set-items'+IdIdea+'"]').children(".task-inline-selector-item").length == 0)
			{					
				$('span[data-bx-id="task-item-set-items'+IdIdea+'"]').append('<input type="hidden" name="UF_TASK_BLOG_POST[]" value="">');
			}

			if (BX.QueryDB && IdTask) 
			{			
				$.ajax({ 
					url: '/local/templates/.default/components/bitrix/system.field.edit/task/ajax.php', 
					data: {'ACTION': 'DELETE_TASK', 'TASK_ID': IdTask, 'IDEA_ID': IdIdea}, 
					method: 'get', 
					dataType: 'json',					  
					success: function (msg) 
					{					
						if (msg.error) console.log("������ ������� � ���� ������"); 
					},
					error: function () { console.log("������ ajax-�������"); }
				});				
			}
		}
	});
	

	BX.addCustomEvent("IdeaOnLifeSearch", BX.delegate(function(ParamsIdeaSidebar)
	{
		// ���������������� ���������� ���� �� ����� ������ ����� ������
		var getAllKeys = Object.keys(window);
		getAllKeys.forEach(function(keyName) 
        {
			if (keyName.indexOf('popup') !== -1) 
            {
				window[keyName].offsetLeft = 273;
            } 
        });
		
    }, this));	
	
});