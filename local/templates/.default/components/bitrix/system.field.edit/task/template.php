<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? $arParams['arUserField']['ENTITY_VALUE_ID'] = (!empty($arParams['arUserField']['ENTITY_VALUE_ID']) ? $arParams['arUserField']['ENTITY_VALUE_ID'] : 0);
?>

<script>
// список всех задач
if (!BX.ArIdTask) BX.ArIdTask = {};
BX.ArIdTask['<?=$arParams["arUserField"]["ENTITY_VALUE_ID"]?>'] = <?=CUtil::PhpToJSObject(array_keys($arResult["VALUE"]));?>;
// нужен ли запрос в БД
BX.QueryDB = '<?=$arParams["QueryDB"] ? true : false?>';
// идентификаторы идей на странице
if (!BX.arIdIdea) BX.arIdIdea = [];
BX.arIdIdea.push(<?=$arParams['arUserField']['ENTITY_VALUE_ID']?>);
</script>

		<div class="task-options-item-open-inner" data-id-idea="<?=$arParams['arUserField']['ENTITY_VALUE_ID']?>">
			<span class="task-options-destination-wrap task-item-set-empty-true">
								
				<span data-bx-id="task-item-set-items<?=$arParams['arUserField']['ENTITY_VALUE_ID']?>">	
					<? if (!empty($arResult["VALUE"])) { ?>
						<? // вывод уже закрепленных задач ?>
						<? foreach ($arResult["VALUE"] as $key => $res) { ?>					
							<span data-bx-id="task-item-set-item" data-item-value="<?=$key?>" class="task-inline-selector-item">						
								<input type="hidden" name="UF_TASK_BLOG_POST[]" value="<?=$key?>">						
								<span class="task-options-destination task-options-destination-all-users">
									<a href="/company/personal/user/<?=$res["RESPONSIBLE_ID"]?>/tasks/task/view/<?=$key?>/" target="_blank" class="task-options-destination-text"><?=$res["TITLE"]?></a>
									
									<span class="task-option-inp-del" title="<?=GetMessage("IDEA_TASK_DELETE")?>"></span>
								</span>
							</span>	
						<? } ?>
					<? } ?>
				</span>	

				<span class="task-inline-selector-item">
					<a href="javascript:void(0)" class="feed-add-destination-link" data-bx-id="task-item-set-open-form" data-id-idea="<?=$arParams['arUserField']['ENTITY_VALUE_ID']?>">
					
						<span class="task-item-set-empty-block-off">+ <?=GetMessage("IDEA_TASK_ADD")?></span>
						<span class="task-item-set-empty-block-on"><?=GetMessage("IDEA_TASK_CHANGE")?></span>
					</a>
				</span>

				<div class="hidden-soft task_add_idea"  id="TaskAddIdea_<?=$arParams['arUserField']['ENTITY_VALUE_ID']?>">
				
					<?$APPLICATION->IncludeComponent(
						"bitrix:tasks.task.selector",
						".default",
						Array(
							"MULTIPLE" => "N",
							"NAME" => "task".$arParams['arUserField']['ENTITY_VALUE_ID'],							
							"SELECT" => array('ID','TITLE','STATUS'),
							"SITE_ID" => SITE_ID
						),
						null,
						Array(
							'HIDE_ICONS' => 'Y'
						)
					); ?>	

					
				</div>
				
			</span>							
		</div>
		
		<br style="clear: both;" /><br>	