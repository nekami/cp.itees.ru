BX.CrmEntitySelector = (function ()
{
	var CrmEntitySelector = function (parameters)
	{
		this.randomString = parameters.randomString;
		this.jsObject = parameters.jsObject;
		this.fieldUid = parameters.fieldUid;
		this.fieldName = parameters.fieldName;
		this.usePrefix = parameters.usePrefix;
		this.listPrefix = parameters.listPrefix;
		this.multiple = parameters.multiple;
		this.listElement = parameters.listElement;
		this.listEntityType = parameters.listEntityType;
		this.pluralCreation = Boolean(parameters.pluralCreation);
		this.listEntityCreateUrl = parameters.listEntityCreateUrl;
		this.currentEntityType = parameters.currentEntityType;
		this.context = parameters.context;
		this.saveButton = BX(parameters.saveButton);
		this.cancelButton = BX(parameters.cancelButton);

		this.initialize();
	};

	CrmEntitySelector.prototype.initialize = function()
	{
		this.popupObject = null;
		this.popupId = 'crm-'+this.randomString+'-popup';
		this.popupBindElement = null;
		this.popupContent = '';
		this.externalRequestData = null;
		this.externalEventHandler = null;

		BX.addCustomEvent('onCrmSelectedItem', BX.proxy(this.setSelectedElement, this));
		BX.addCustomEvent('onCrmUnSelectedItem', BX.proxy(this.unsetSelectedElement, this));
	};

	CrmEntitySelector.prototype.createNewEntity = function(event)
	{
		if(this.pluralCreation)
		{
			event = event || window.event;
			this.popupBindElement = event.currentTarget;
			this.createPopup();
		}
		else
		{
			this.performExternalRequest();
		}
	};

	CrmEntitySelector.prototype.performExternalRequest = function(entityType)
	{
		if(this.popupObject)
		{
			this.popupObject.popupWindow.close();
		}

		if(entityType)
		{
			this.setCurrentEntityType(entityType);
		}

		var url = BX.util.add_url_param(this.getCreateUrl(), {
			external_context: this.context
		});

		if(!this.externalRequestData)
		{
			this.externalRequestData = {};
		}

		this.externalRequestData[this.context] = {context: this.context, wnd: window.open(url)};

		if(!this.externalEventHandler)
		{
			this.externalEventHandler = BX.delegate(this.onExternalEvent, this);
			BX.addCustomEvent(window, 'onLocalStorageSet', this.externalEventHandler);
		}
	};

	CrmEntitySelector.prototype.onExternalEvent = function(params)
	{
		var key = BX.type.isNotEmptyString(params['key']) ? params['key'] : '';
		var value = BX.type.isPlainObject(params['value']) ? params['value'] : {};
		var typeName = BX.type.isNotEmptyString(value['entityTypeName']) ? value['entityTypeName'] : '';
		var context = BX.type.isNotEmptyString(value['context']) ? value['context'] : '';

		if(key === 'onCrmEntityCreate' && typeName === this.currentEntityType.toUpperCase()
			&& this.externalRequestData && BX.type.isPlainObject(this.externalRequestData[context]))
		{
			var isCanceled = BX.type.isBoolean(value['isCanceled']) ? value['isCanceled'] : false;
			if(!isCanceled && BX.type.isPlainObject(value['entityInfo']))
			{
				if(this.multiple != 'Y')
				{
					for(var k = 0; k < this.listElement.length; k++)
					{
						this.listElement[k]['selected'] = 'N';
					}
				}
				value["entityInfo"]['selected'] = 'Y';
				var entityInfo = value["entityInfo"];
				if(this.usePrefix == 'Y')
				{
					var entityType = entityInfo['type'].toUpperCase();
					entityInfo['id'] = this.listPrefix[entityType]+'_'+entityInfo['id'];
				}
				this.listElement.push(entityInfo);
				BX[''+this.jsObject+''].initWidgetEntitySelection();
			}

			if(this.externalRequestData[context]['wnd'])
			{
				this.externalRequestData[context]['wnd'].close();
			}

			delete this.externalRequestData[context];
		}
	};

	CrmEntitySelector.prototype.createPopup = function()
	{
		var popupItems = [];
		for(var k = 0; k < this.listEntityType.length; k++)
		{
			popupItems.push({
				text : BX.message('CRM_CES_CREATE_'+this.listEntityType[k].toUpperCase()),
				onclick : 'BX["'+this.jsObject+'"].performExternalRequest("'+this.listEntityType[k]+'");'
			});
		}
		if(!BX.PopupMenu.getMenuById(this.popupId))
		{
			var buttonRect = this.popupBindElement.getBoundingClientRect();
			this.popupObject = BX.PopupMenu.create(
				this.popupId,
				this.popupBindElement,
				popupItems,
				{
					closeByEsc : true,
					angle: true,
					offsetLeft: buttonRect.width/2
				}
			);
		}
		if(this.popupObject)
		{
			this.popupObject.popupWindow.show();
		}
	};

	CrmEntitySelector.prototype.setCurrentEntityType = function(currentEntityType)
	{
		this.currentEntityType = currentEntityType;
	};

	CrmEntitySelector.prototype.getCreateUrl = function()
	{
		if(this.listEntityCreateUrl.hasOwnProperty(this.currentEntityType)) 
		{
			return this.listEntityCreateUrl[this.currentEntityType];
		}
		else 
		{
			return '';
		}
	};

	CrmEntitySelector.prototype.setSelectedElement = function(itemInfo)
	{
		var type = "";
		if (itemInfo.type === 'company' || itemInfo.type === 'deal')
			type = itemInfo.type;
		
		for (var k in this.listElement)
		{
			if (this.listElement[k].type === type && this.listElement[k].selected === "Y" && itemInfo.id !== this.listElement[k].id) 
			{
				CRM.PopupUnselectItem(
					"crm-"+this.fieldUid+"-open", 
					"crm-crm-"+this.fieldUid+"-open_"+this.fieldName+"-block-item-"+this.listElement[k].id+"-"+this.listElement[k].place, 
					"selected-crm-crm-"+this.fieldUid+"-open_"+this.fieldName+"-block-item-"+this.listElement[k].id+"-"+this.listElement[k].place
				);
				this.listElement[k].selected = 'N';
			}
		}
		
		for (var k in this.listElement)
		{
			if (itemInfo.id === this.listElement[k].id && itemInfo.place === this.listElement[k].place)
			{
				this.listElement[k].selected = 'Y';
				if ( itemInfo.type === 'deal' ) {
					$('#deal-create-button').addClass('hidden');
				}
				return;
			}
		}
		
		itemInfo.selected = "Y";
		this.listElement.push(itemInfo);
		if ( itemInfo.type === 'deal' )
			$('#deal-create-button').addClass('hidden');
	};

	CrmEntitySelector.prototype.unsetSelectedElement = function(itemInfo)
	{
		for (var k in this.listElement)
		{
			if (itemInfo.id === this.listElement[k].id && itemInfo.place === this.listElement[k].place)
			{
				this.listElement[k].selected = 'N';
				if ( itemInfo.type === 'deal' ) {
					$('#deal-create-button').removeClass('hidden');
				}
			}
		}
	};

	CrmEntitySelector.prototype.initWidgetEntitySelection = function()
	{
		if(typeof(CRM) == 'undefined')
		{
			BX.loadCSS('/bitrix/js/crm/css/crm.css');
			BX.loadScript('/bitrix/js/crm/crm.js', BX[''+this.jsObject+''].initWidgetEntitySelection());
			return;
		}

		//Переопределение методов формирующих таблицу с выбранными элементами
		if (this.fieldName === "INVOICE") {
			//Переопределяем необходимые методы CRM
			CRM.SearchChange = function(crmID)
			{
				if (!obCrm[crmID])
					return false;

				var searchValue = obCrm[crmID].PopupSearchInput.value;
				if (searchValue == '')
					return false;

				var entityType = '';
				if(obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+"_"+obCrm[crmID].name+"-tab-lead")
					entityType = 'lead';
				else if(obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+"_"+obCrm[crmID].name+"-tab-contact")
					entityType = 'contact';
				else if(obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+"_"+obCrm[crmID].name+"-tab-deal")
					entityType = 'deal';
				else if(obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+"_"+obCrm[crmID].name+"-tab-quote")
					entityType = 'quote';	
				else if(obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+"_"+obCrm[crmID].name+"-tab-order")
					entityType = 'order';
				else if(obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+"_"+obCrm[crmID].name+"-tab-company")
					entityType = 'company';
				else
					entityType = obCrm[crmID].PopupEntityType;

				var options = {
					'REQUIRE_REQUISITE_DATA': (obCrm[crmID].options.requireRequisiteData) ? 'Y' : 'N'
				};

				if (BX.type.isPlainObject(obCrm[crmID].options["searchOptions"]))
				{
					var searchOptions = obCrm[crmID].options["searchOptions"];
					for(var optionName in searchOptions)
					{
						if (searchOptions.hasOwnProperty(optionName))
							options[optionName] = searchOptions[optionName];
					}
				}

				var postData = { 'MODE' : 'SEARCH', 'VALUE' : searchValue, 'MULTI' : (obCrm[crmID].PopupPrefix? 'Y': 'N'),
					'OPTIONS': options };
				if (crmID === "new_invoice_product_button")
				{
					postData["ENTITY_TYPE"] = "INVOICE";
				}
				var postUrl = '/bitrix/components/bitrix/crm.' + entityType + '.list/list.ajax.php';
				if (entityType[0] === 'invoice') {
					postUrl = window.invoiceListUrl;
					postData['COMPANY'] = window.curClientComp;
					var arSelected = [];
					for (var item in obCrm[crmID].PopupItemSelected) {
						arSelected.push(item.substr(0, item.indexOf("-")));
					}
					postData['SELECTED'] = arSelected;
				}
				var handlers = obCrm[crmID].onBeforeSearchListeners;
				if(handlers && BX.type.isArray(handlers) && handlers.length > 0)
				{
					var data = { 'entityType':entityType, 'postData': postData };
					for(var j = 0; j < handlers.length; j++)
					{
						try
						{
							handlers[j](data);
						}
						catch(ex)
						{
						}

						postData = data['postData'];
					}
				}

				CRM.PopupShowSearchBlock(crmID, obCrm[crmID].PopupSearch[1]);

				setTimeout(function() {
					if(typeof(obCrm[crmID]) === "undefined")
					{
						return;
					}

					if (BX('crm-'+crmID+"_"+obCrm[crmID].name+'-block-search').innerHTML == ''
					&& obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+"_"+obCrm[crmID].name+"-tab-"+entityType) {
						var spanWait = document.createElement('div');
						spanWait.className="crm-block-cont-search-wait";
						spanWait.innerHTML=obCrm[crmID].PopupLocalize['wait'];
						BX('crm-'+crmID+"_"+obCrm[crmID].name+'-block-search').appendChild(spanWait);
					}
				}, 3000);
				BX.ajax({
					url: postUrl,
					method: 'POST',
					dataType: 'json',
					data: postData,
					onsuccess: function(data)
					{
						if (obCrm[crmID].PopupTabsIndexId!="crm-"+crmID+"_"+obCrm[crmID].name+"-tab-"+entityType)
							return false;

						BX('crm-'+crmID+"_"+obCrm[crmID].name+'-block-search').className = 'crm-block-cont-block crm-block-cont-block-'+entityType;
						BX('crm-'+crmID+"_"+obCrm[crmID].name+'-block-search').innerHTML = '';
						el = 0;
						for (var i in data) {
							data[i]['place'] = 'search';
							obCrm[crmID].PopupAddItem(data[i]);
							el++;
						}
						if (el == 0)
						{
							var spanWait = document.createElement('div');
							spanWait.className="crm-block-cont-search-no-result";
							spanWait.innerHTML=obCrm[crmID].PopupLocalize['noresult'];
							BX('crm-'+crmID+"_"+obCrm[crmID].name+'-block-search').appendChild(spanWait);
						}
					},
					onfailure: function(data)
					{

					}
				});
			};
			
			CRM.prototype.PopupSave = function()
			{
				var arElements = {};
				for (var i in this.PopupEntityType)
				{
					var elements = BX.findChildren(BX("crm-"+this.crmID+"_"+this.name+"-block-"+this.PopupEntityType[i]+"-selected"), {className: "crm-block-cont-block-item"});
					if (elements !== null)
					{
						var el = 0;
						arElements[this.PopupEntityType[i]] = {};
						for(var e=0; e<elements.length; e++)
						{
							var elementIdLength = "selected-crm-"+this.crmID+"_"+this.name+"-block-item-";
							var elementId = elements[e].id.substr(elementIdLength.length);

							var data =  {
								'id' : this.PopupItem[elementId]['id'],
								'type' : this.PopupEntityType[i],
								'place' : this.PopupItem[elementId]['place'],
								'title' : this.PopupItem[elementId]['title'],
								'desc' : this.PopupItem[elementId]['desc'],
								'url' : this.PopupItem[elementId]['url'],
								'image' : this.PopupItem[elementId]['image'],
								'largeImage' : this.PopupItem[elementId]['largeImage'],
								'price' : this.PopupItem[elementId]['price'],
								'statuses' : this.PopupItem[elementId]['statuses'],
								'paid' : this.PopupItem[elementId]['paid'],
								'deletable' : this.PopupItem[elementId]['deletable'],
							};

							if(typeof(this.PopupItem[elementId]['customData']) != 'undefined')
							{
								data['customData'] = this.PopupItem[elementId]['customData'];
							}
							if(typeof(this.PopupItem[elementId]['advancedInfo']) != 'undefined')
							{
								data['advancedInfo'] = this.PopupItem[elementId]['advancedInfo'];
							}

							arElements[this.PopupEntityType[i]][el] = data;

							el++;
						}
					}
				}

				var ary = this.onSaveListeners;
				if(ary.length > 0)
				{
					for(var j = 0; j < ary.length; j++)
					{
						try
						{
							ary[j](arElements);
						}
						catch(ex)
						{
						}
					}
				}

				if(!this.disableMarkup)
				{
					this.PopupCreateValue(arElements);
				}
			};
			
			CRM.prototype.PopupCreateValue = function(arElements)
			{
				var inputBox = BX("crm-"+this.crmID+"_"+this.name+"-input-box");
				var textBox = BX("crm-"+this.crmID+"_"+this.name+"-text-box");

				if(!inputBox || !textBox)
				{
					return;
				}

				inputBox.innerHTML = '';

				var textBoxNew = document.createElement('DIV');
				textBoxNew.id = textBox.id;
				textBox.parentNode.replaceChild(textBoxNew, textBox);
				textBox = textBoxNew;

				var tableObject = document.createElement('table');
				tableObject.className = "field_crm";
				tableObject.cellPadding = "0";
				tableObject.cellSpacing = "0";
				var tbodyObject = document.createElement('TBODY');

				var iEl = 0;
				for (var type in arElements)
				{
					var rowObject = document.createElement("TR");
					rowObject.className = "crmPermTableTrHeader";

					if (this.PopupEntityType.length > 1)
					{
						var cellObject = document.createElement("TD");
						cellObject.className = "field_crm_entity_type";
						cellObject.appendChild(document.createTextNode(this.PopupLocalize[type]+":"));
						rowObject.appendChild(cellObject);
					}

					cellObject = document.createElement("TD");
					cellObject.className = "field_crm_entity";

					var iTypeEl = 0;
					for (var i in arElements[type])
					{
						var addInput=document.createElement('input');
						addInput.type = 'text';
						addInput.name = this.name+(this.PopupMultiple? '[]': '');
						addInput.value = arElements[type][i]['id'];

						inputBox.appendChild(addInput);

						var addCrmLink=document.createElement('a');
						if (arElements[type][i].deletable == "N") addCrmLink.className = "crm-block-cont-item-undeletable";
						addCrmLink.href=arElements[type][i]['url'];
						addCrmLink.target="_blank";
						addCrmLink.appendChild(document.createTextNode(arElements[type][i]['title']));
						cellObject.appendChild(addCrmLink);
						
						if (arElements[type][i]['type'] === 'invoice') {
							var price = document.createElement('span');
							price.className = "crm_invoice_price";
							price.appendChild(document.createTextNode("(" + new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'RUB' }).format(arElements[type][i]['price']) + ")"));
							cellObject.appendChild(price);
						}

						var addCrmDeleteLink=document.createElement('span');
						addCrmDeleteLink.className="crm-element-item-delete";
						addCrmDeleteLink.id="deleted-crm-"+this.crmID+'_'+this.name+"-block-item-"+arElements[type][i]['id']+'-'+arElements[type][i]['place'];
						eval('BX.bind(addCrmDeleteLink, "click", function(event) { CRM.PopupUnselectItem("'+this.crmID+'", this.id.substr(8), "selected-"+this.id.substr(8)); CRM.PopupSave2("'+this.crmID+'");})');
						if (arElements[type][i].deletable != "N") cellObject.appendChild(addCrmDeleteLink);
						
						if (arElements[type][i]['type'] === 'invoice') {
							var title = document.createElement('div');
							title.className = "crm-list-stage-bar-title";
							title.appendChild(document.createTextNode(arElements[type][i]['desc']));
							cellObject.appendChild(title);
							
							var statusBar = document.createElement('div');
							statusBar.className = "crm_invoice_status_bar";
							statusBar.innerHTML = arElements[type][i]['statuses'];
							cellObject.appendChild(statusBar);
						}
						//Strongly required for user field value change event
						BX.fireEvent(addInput, "change");

						iTypeEl++;
						iEl++;
					}

					if(iTypeEl > 0)
					{
						rowObject.appendChild(cellObject);
						tbodyObject.appendChild(rowObject);
					}

				}
				if (iEl == 0)
				{
					var addInput=document.createElement('input');
					addInput.type = 'text';
					addInput.name = this.name+(this.PopupMultiple? '[]': '');
					addInput.value = '';
					inputBox.appendChild(addInput);

					//Strongly required for user field value change event
					BX.fireEvent(addInput, "change");
				}
				tableObject.appendChild(tbodyObject);
				textBox.appendChild(tableObject);

				if(this.el)
				{
					if (iEl>0)
					{
						this.el.innerHTML = this.PopupLocalize['edit'];
					}
					else
					{
						BX.cleanNode(textBox, false);

						if(BX.browser.IsIE())
						{
							// HACK: empty DIV has height in IE7 - make it collapse to zero.
							textBox.style.fontSize = '0px';
							textBox.style.lineHeight = '0px';
						}
						this.el.innerHTML = this.PopupLocalize['add'];
					}
				}
			};
			
			//Раскрывающееся меню выбора элементов
			CRM.PopupSelectItem = function(crmID, element, tab, unsave, select)
			{
				if (!obCrm[crmID])
					return false;

				var flag=element;
				if(flag.check)
				{
					if (select === undefined || select == false)
						CRM.PopupUnselectItem(crmID, element.id, "selected-"+element.id);
					return false;
				}

				elementIdLength = "crm-"+crmID+'_'+obCrm[crmID].name+"-block-item-";
				elementId = element.id.substr(elementIdLength.length);
				var addCrmItems=document.createElement('span');
				addCrmItems.className = "crm-block-cont-block-item"+(obCrm[crmID].PopupItem[elementId].deletable == "N" ? " crm-block-cont-item-undeletable" : "");
				addCrmItems.id="selected-"+element.id;

				var addCrmDelBut=document.createElement('i');
				var addCrmLink=document.createElement('a');
				addCrmLink.href=obCrm[crmID].PopupItem[elementId]['url'];
				addCrmLink.target="_blank";

				var blockWrap;
				if (tab === null)
				{
					if(obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+'_'+obCrm[crmID].name+"-tab-lead")
						blockWrap=BX("crm-"+crmID+'_'+obCrm[crmID].name+"-block-lead-selected");

					if(obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+'_'+obCrm[crmID].name+"-tab-contact")
						blockWrap=BX("crm-"+crmID+'_'+obCrm[crmID].name+"-block-contact-selected");

					if(obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+'_'+obCrm[crmID].name+"-tab-deal")
						blockWrap=BX("crm-"+crmID+'_'+obCrm[crmID].name+"-block-deal-selected");

					if(obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+'_'+obCrm[crmID].name+"-tab-quote")
						blockWrap=BX("crm-"+crmID+'_'+obCrm[crmID].name+"-block-quote-selected");

					if(obCrm[crmID].PopupTabsIndexId=="crm-"+crmID+'_'+obCrm[crmID].name+"-tab-company")
						blockWrap=BX("crm-"+crmID+'_'+obCrm[crmID].name+"-block-company-selected");

				}
				else
					blockWrap=BX("crm-"+crmID+'_'+obCrm[crmID].name+"-block-"+tab+"-selected");

				if (obCrm[crmID].PopupMultiple)
				{
					blockTitle = BX.findChild(blockWrap, { className : "crm-block-cont-right-title-count"}, true);
					blockTitle.innerHTML = parseInt(blockTitle.innerHTML)+1;
					BX.addClass(element, "crm-block-cont-item-selected");
					BX.addClass(blockWrap, "crm-added-item");
					flag.check=1;
				}
				else
				{
					for (var i in obCrm[crmID].PopupEntityType)
					{
						BX.removeClass(BX("crm-"+crmID+'_'+obCrm[crmID].name+"-block-"+obCrm[crmID].PopupEntityType[i]+"-selected"), "crm-added-item");
						elements = BX.findChildren(BX("crm-"+crmID+'_'+obCrm[crmID].name+"-block-"+obCrm[crmID].PopupEntityType[i]+"-selected"), {className: "crm-block-cont-block-item"});
						if (elements !== null)
							for (var i in elements)
								BX.remove(elements[i]);

					}
				}
				if (obCrm[crmID].PopupItem[elementId]['deletable'] != "N") blockWrap.appendChild(addCrmItems).appendChild(addCrmDelBut);

				blockWrap.appendChild(addCrmItems).appendChild(addCrmLink).innerHTML=BX.util.htmlspecialchars(obCrm[crmID].PopupItem[elementId]['title']);

				eval('BX.bind(addCrmDelBut, "click", function(event) {CRM.PopupUnselectItem("'+crmID+'", element.id, "selected-"+element.id); BX.PreventDefault(event);})');

				obCrm[crmID].PopupItemSelected[elementId] = element;

				if (!obCrm[crmID].PopupMultiple && (unsave === undefined || unsave == false))
				{
					obCrm[crmID].PopupSave();

					if (BX.PopupWindowManager._currentPopup !== null
						&& BX.PopupWindowManager._currentPopup.uniquePopupId == "CRM-"+this.crmID+"-popup")
					{
						BX.PopupWindowManager._currentPopup.close();
					}
				}

				BX.onCustomEvent(window, 'onCrmSelectedItem', [obCrm[crmID].PopupItem[elementId]]);
			};

		}
	
		//Выборка элемента из последних и поиска
		CRM.prototype.PopupAddItem = function(arParam)
		{
			if (arParam['place'] === undefined || arParam['place'] == '')
				arParam['place'] = arParam['type'];

			bElementSelected = false;
			if (this.PopupItemSelected[arParam['id']+'-'+arParam['place']] !== undefined)
				bElementSelected = true;

			itemBody = document.createElement("span");
			itemBody.id = 'crm-'+this.crmID+"_"+this.name+'-block-item-'+arParam['id']+'-'+arParam['place'];
			itemBody.className = "crm-block-cont-item"+(bElementSelected? " crm-block-cont-item-selected": "")+(arParam.deletable == "N" ? " crm-block-cont-item-undeletable" : "");
			itemBody.check=bElementSelected? 1: 0;

			if (arParam['type'] == 'contact' || arParam['type'] == 'company')
			{
				itemAvatar = document.createElement("span");
				itemAvatar.className = "crm-avatar";

				if (arParam['image'] !== undefined && arParam['image'] != '')
				{
					itemAvatar.style.background = 'url("' + arParam['image'] + '") no-repeat';
				}

				itemBody.appendChild(itemAvatar);
			}

			itemTitle = document.createElement("ins");
			itemTitle.appendChild(document.createTextNode(arParam['title']));
			itemId = document.createElement("var");
			itemId.className = "crm-block-cont-var-id";
			itemId.appendChild(document.createTextNode(arParam['id']));
			itemUrl = document.createElement("var");
			itemUrl.className = "crm-block-cont-var-url";
			itemUrl.appendChild(document.createTextNode(arParam['url']));

			var itemDesc = document.createElement("span");
			itemDesc.innerHTML = this.prepareDescriptionHtml(arParam['desc']);
			var bodyBox = document.createElement("span");
			bodyBox.className = "crm-block-cont-contact-info";
			bodyBox.appendChild(itemTitle);
			bodyBox.appendChild(itemDesc);
			bodyBox.appendChild(itemId);
			bodyBox.appendChild(itemUrl);
			itemBody.appendChild(bodyBox);
			if (arParam.deletable != "N") itemBody.appendChild(document.createElement("i"));

			bDefinedItem = false;
			if (arParam['place'] != 'search' && this.PopupItem[arParam['id']+'-'+arParam['place']] !== undefined)
				bDefinedItem = true;
			else
				this.PopupItem[arParam['id']+'-'+arParam['place']] = arParam;

			var placeHolder = BX("crm-"+this.crmID+"_"+this.name+"-block-"+arParam['place']);

			if (placeHolder !== null)
			{
				if (!bDefinedItem)
					placeHolder.appendChild(itemBody);

				CRM._bindPopupItem(this.crmID, itemBody, arParam["type"]);

				if (arParam['selected'] !== undefined && arParam['selected'] == 'Y')
					CRM.PopupSelectItem(this.crmID, itemBody, arParam['type'], true, true);
			}
		};
		
		//Событие удаления элемента из выборки
		CRM.PopupUnselectItem = function(crmID, element, selected)
		{
			elementIdLength = "crm-"+crmID+'_'+obCrm[crmID].name+"-block-item-";
			elementId = element.substr(elementIdLength.length);
			
			if (!obCrm[crmID] || obCrm[crmID].PopupItem[elementId].deletable == "N") 
				return false;

			if (obCrm[crmID].PopupMultiple)
			{
				if(BX(selected).parentNode.getElementsByTagName('span').length == 3)
					BX.removeClass(BX(selected).parentNode, "crm-added-item");

				blockTitle = BX.findChild(BX(selected).parentNode, { className : "crm-block-cont-right-title-count"}, true);
				blockTitle.innerHTML = parseInt(blockTitle.innerHTML)-1;

				obj = BX(element);
				if (obj !== null)
				{
					obj.check=0;
					BX.removeClass(obj, "crm-block-cont-item-selected");
				}
			}
			
			delete obCrm[crmID].PopupItemSelected[elementId];
			BX.remove(BX(selected));

			BX.onCustomEvent(window, 'onCrmUnSelectedItem', [obCrm[crmID].PopupItem[elementId]]);
		};

		var uid = this.fieldUid;
		var _this = this;
		CRM.Set(
			BX('crm-'+this.fieldUid+'-open'),
			this.fieldName,
			'',
			this.listElement,
			(this.usePrefix === 'Y'),
			(this.multiple === 'Y'),
			this.listEntityType,
			{
				'lead': BX.message('CRM_FF_LEAD'),
				'contact': BX.message('CRM_FF_CONTACT'),
				'company': BX.message('CRM_FF_COMPANY'),
				'deal': BX.message('CRM_FF_DEAL'),
				'quote': BX.message('CRM_FF_QUOTE'),
				'order': BX.message('CRM_FF_ORDER'),
				'invoice': BX.message('CRM_FF_INVOICE'),
				'ok': BX.message('CRM_FF_OK'),
				'cancel': BX.message('CRM_FF_CANCEL'),
				'close': BX.message('CRM_FF_CLOSE'),
				'wait': BX.message('CRM_FF_SEARCH'),
				'noresult': BX.message('CRM_FF_NO_RESULT'),
				'add': BX.message('CRM_FF_CHOISE'),
				'edit': BX.message('CRM_FF_CHANGE'),
				'search': BX.message('CRM_FF_SEARCH'),
				'last': BX.message('CRM_FF_LAST')
			}
		);
		
		obCrm['crm-'+uid+'-open'].AddOnSaveListener(function(data){
			if (typeof(data.invoice) === 'object') {
				let invoices = {};
				for (let i in data.invoice) {
					invoices[i] = {
						'id' : data.invoice[i].id,
						'type' : data.invoice[i].type,
						'price' : data.invoice[i].price,
						'paid' : data.invoice[i].paid,
					};
				}
				window.crmInvoice = invoices;
			} else {
				let compId = data.company[0] ? data.company[0].id.substr(3) : null;
				window.crmData = data;
				if (window.curClientComp !== compId && typeof updateInvoices === 'function') {
					window.curClientComp = compId;
					window.crmInvoice = null;
					updateInvoices();
				} 
			}
			if (typeof _this.saveButton.classList.add === 'function' && window.canEdit === "Y") _this.saveButton.classList.add('save');
			if (typeof _this.cancelButton.classList.add === 'function' && window.canEdit === "Y") _this.cancelButton.classList.remove('passive');
			if (typeof updateSumm === 'function' && window.canEdit === "Y") updateSumm();
		});
	};
	return CrmEntitySelector;
})();