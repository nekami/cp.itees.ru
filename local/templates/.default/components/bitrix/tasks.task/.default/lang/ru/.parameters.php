<?
$MESS['TASKS_TASK_TEMPLATE_SET_NAVCHAIN'] = '�������� ������� ���������';

$MESS['TASKS_TASK_TEMPLATE_PATH_TO_USER_TASKS'] = '���� � ������� ������������';
$MESS['TASKS_TASK_TEMPLATE_PATH_TO_GROUP_TASKS'] = '���� � ������ �����';
$MESS['TASKS_TASK_TEMPLATE_PATH_TO_GROUP_TASKS_TASK'] = '���� � ������';
$MESS['TASKS_TASK_TEMPLATE_PATH_TO_USER_PROFILE'] = '���� � ������� ������������';
$MESS['TASKS_TASK_TEMPLATE_PATH_TO_GROUP'] = '���� � ������';
$MESS['TASKS_TASK_TEMPLATE_SHOW_RATING'] = '�������� �������';
$MESS['TASKS_TASK_TEMPLATE_RATING_TYPE'] = '��� ������ ��������';
$MESS['TASKS_TASK_TEMPLATE_RATING_TYPE_CONFIG'] = '�� ���������';
$MESS['TASKS_TASK_TEMPLATE_RATING_TYPE_LIKE_TEXT'] = '�������� / �� �������� (���������)';
$MESS['TASKS_TASK_TEMPLATE_RATING_TYPE_LIKE_GRAPHIC'] = '�������� / �� �������� (�����������)';
$MESS['TASKS_TASK_TEMPLATE_RATING_TYPE_STANDART_TEXT'] = '��� �������� (���������)';
$MESS['TASKS_TASK_TEMPLATE_RATING_TYPE_STANDART_GRAPHIC'] = '��� �������� (�����������)';
$MESS['TASKS_TASK_TEMPLATE_REDIRECT_ON_SUCCESS'] = '��������� �������� �� url �������� ��� �������� ����������';

/*

$MESS ['INTL_VARIABLE_ALIASES'] = "���������� ����������";
$MESS ['INTL_TASK_TYPE'] = "��� �����";
$MESS ['INTL_OWNER_ID'] = "��� ���������";
$MESS ['INTL_TASK_VAR'] = "��� ���������� ��� ���� ������";
$MESS ['INTL_USER_VAR'] = "��� ���������� ��� ���� ������������";
$MESS ['INTL_GROUP_VAR'] = "��� ���������� ��� ���� ������";
$MESS ['INTL_VIEW_VAR'] = "��� ���������� ��� ���� �������������";
$MESS ['INTL_ACTION_VAR'] = "��� ���������� ��� ���� ��������";
$MESS ['INTL_PAGE_VAR'] = "��� ���������� ��� ���� ��������";
$MESS ['INTL_PATH_TO_GROUP_TASKS'] = "���� � ������ �����";
$MESS ['INTL_PATH_TO_GROUP_TASKS_TASK'] = "���� � ������";
$MESS ['INTL_PATH_TO_GROUP_TASKS_VIEW'] = "���� � �������������";
$MESS ['INTL_SET_NAVCHAIN'] = "�������� ������� ���������";
$MESS ['INTL_ITEM_COUNT'] = "���������� ������� �� ��������";
$MESS ['INTL_TF_ID'] = "���";
$MESS ['INTL_TF_NAME'] = "��������";
$MESS ['INTL_TF_CODE'] = "������������� ���";
$MESS ['INTL_TF_XML_ID'] = "������� ���";
$MESS ['INTL_TF_MODIFIED_BY'] = "������� ������";
$MESS ['INTL_TF_DATE_CREATE'] = "�������";
$MESS ['INTL_TF_CREATED_BY'] = "�����";
$MESS ['INTL_TF_DATE_ACTIVE_FROM'] = "������";
$MESS ['INTL_TF_DATE_ACTIVE_TO'] = "���������";
$MESS ['INTL_TF_IBLOCK_SECTION'] = "�����";
$MESS ['INTL_TF_DETAIL_TEXT'] = "�������� ������";
$MESS ['INTL_TASK_TYPE_GROUP'] = "��� ������";
$MESS ['INTL_TASK_TYPE_USER'] = "��� ������������";
$MESS ['INTL_PATH_TO_FORUM_SMILE'] = "���� � ����� �� ���������� ��� ������ ������������ ����� �����";
$MESS ['INTL_PATH_TO_PROFILE_VIEW'] = "���� � �������� ������������";
$MESS ['SHOW_RATING'] = "�������� �������";
$MESS ['SHOW_RATING_CONFIG'] = "�� ���������";
$MESS ['RATING_TYPE'] = "��� ������ ��������";
$MESS ['RATING_TYPE_CONFIG'] = "�� ���������";
$MESS ['RATING_TYPE_STANDART_TEXT'] = "�������� / �� �������� (���������)";
$MESS ['RATING_TYPE_STANDART_GRAPHIC'] = "�������� / �� �������� (�����������)";
$MESS ['RATING_TYPE_LIKE_TEXT'] = "��� �������� (���������)";
$MESS ['RATING_TYPE_LIKE_GRAPHIC'] = "��� �������� (�����������)";
*/