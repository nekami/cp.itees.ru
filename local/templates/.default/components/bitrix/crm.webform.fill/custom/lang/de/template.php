<?
$MESS["CRM_WEBFORM_FILL_ERROR_FIELD_EMPTY"] = "Sie m�ssen alle erforderlichen Felder ausf�llen";
$MESS["CRM_WEBFORM_FILL_ERROR_TITLE"] = "Achtung!";
$MESS["CRM_WEBFORM_FILL_NOT_SELECTED"] = "Nicht ausgew�hlt";
$MESS["CRM_WEBFORM_FILL_FILE_SELECT"] = "ausw�hlen";
$MESS["CRM_WEBFORM_FILL_FILE_NOT_SELECTED"] = "Es wurde keine Datei ausgew�hlt";
$MESS["CRM_WEBFORM_FILL_FIELD_ADD_OTHER"] = "Mehr Felder hinzuf�gen";
$MESS["CRM_WEBFORM_FILL_PRODUCT_TITLE"] = "Produkte ausgew�hlt";
$MESS["CRM_WEBFORM_FILL_PRODUCT_SUMMARY"] = "Gesamt";
$MESS["CRM_WEBFORM_FILL_COPYRIGHT_CHARGED_BY"] = "Erstellt mit";
$MESS["CRM_WEBFORM_FILL_FILL_AGAIN"] = "Das Formular erneut ausf�llen";
$MESS["CRM_WEBFORM_FILL_LICENCE_PROMPT"] = "Bedingungen der Lizenzvereinbarung:";
$MESS["CRM_WEBFORM_FILL_LICENCE_ACCEPT"] = "Akzeptieren";
$MESS["CRM_WEBFORM_FILL_LICENCE_DECLINE"] = "Ablehnen";
$MESS["CRM_WEBFORM_FILL_REDIRECT_DESC"] = "Sie werden weitergeleitet in";
$MESS["CRM_WEBFORM_FILL_REDIRECT_SECONDS"] = "Sek.";
$MESS["CRM_WEBFORM_FILL_REDIRECT_GO_NOW"] = "Jetzt �ffnen";
$MESS["CRM_WEBFORM_FILL_RESULT_SENT"] = "Das Formular wurde erfolgreich ausgef�llt.";
$MESS["CRM_WEBFORM_FILL_RESULT_ERROR"] = "Das Formular konnte nicht gesendet werden.";
$MESS["CRM_WEBFORM_FILL_BUTTON_DEFAULT"] = "Senden";
$MESS["CRM_WEBFORM_FILL_CALLBACK_FREE"] = "Toll free";
$MESS["CRM_WEBFORM_FILL_COPYRIGHT_BITRIX"] = "Bitrix";
$MESS["CRM_WEBFORM_FILL_LICENCE_PROMPT1"] = "Nutzungsbedingungen:";
?>