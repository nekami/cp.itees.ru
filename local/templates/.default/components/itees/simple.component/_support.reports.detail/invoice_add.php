<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

function ReportInvoiceAdd($arInvoice)
{
	if (!CModule::IncludeModule('crm') || !CModule::IncludeModule('fileman') || !CModule::IncludeModule('catalog') || !CModule::IncludeModule('sale'))
		return;

	if($arInvoice['ID'])
		$bEdit = true;
	
	global $DB, $USER, $USER_FIELD_MANAGER, $APPLICATION;
	
	$CCrmInvoice = new CCrmInvoice();
	$productDataFieldName = 'INVOICE_PRODUCT_DATA';
	$personTypeId = 0;
	$arPersonTypes = CCrmPaySystem::getPersonTypeIDs();
	if (isset($arPersonTypes['COMPANY']) && isset($arPersonTypes['CONTACT']))
	{
		$info = $CCrmInvoice::__GetCompanyAndContactFromPost($arInvoice);
		if ($info['COMPANY'] > 0) $personTypeId = $arPersonTypes['COMPANY'];
		elseif ($info['CONTACT'] > 0) $personTypeId = $arPersonTypes['CONTACT'];
		unset($info);
	}

	// Get invoice properties
	$arInvoiceProperties = array();
	$tmpArProps = $CCrmInvoice->GetProperties($arParams['ELEMENT_ID'], $personTypeId);
	if ($tmpArProps !== false)
	{
		$arInvoiceProperties = $tmpArProps;
		if ($bTaxMode && !isset($arFields['PR_LOCATION']) && isset($arInvoiceProperties['PR_LOCATION']))
			$arFields['PR_LOCATION'] = $arInvoiceProperties['PR_LOCATION']['VALUE'];
	}
	unset($tmpArProps);

	$bVarsFromForm = true;

		$companyID = $arInvoice['COMPANY'];

		$clientRequisiteId = isset($arInvoice['REQUISITE_ID']) ? (int)$arInvoice['REQUISITE_ID'] : 0;
		if ($clientRequisiteId < 0)
			$clientRequisiteId = 0;
		$clientBankDetailId = isset($arInvoice['BANK_DETAIL_ID']) ? (int)$arInvoice['BANK_DETAIL_ID'] : 0;
		if ($clientBankDetailId < 0)
			$clientBankDetailId = 0;
		if (($companyID > 0 || $contactID > 0) && $clientRequisiteId > 0)
		{
			$requisiteIdLinked = $clientRequisiteId;
			$bankDetailIdLinked = $clientBankDetailId;
		}
		else
		{
			$requisiteIdLinked = 0;
			$bankDetailIdLinked = 0;
		}

		$comments = trim($arInvoice['COMMENTS']);
		$bSanitizeComments = ($comments !== '' && strpos($comments, '<'));
		$userDescription = trim($arInvoice['USER_DESCRIPTION']);
		$bSanitizeUserDescription = ($userDescription !== '' && strpos($userDescription, '<'));
		if($bSanitizeComments || $bSanitizeUserDescription)
		{
			$sanitizer = new CBXSanitizer();
			$sanitizer->ApplyDoubleEncode(false);
			$sanitizer->SetLevel(CBXSanitizer::SECURE_LEVEL_MIDDLE);
			//Crutch for for Chrome line break behaviour in HTML editor.
			$sanitizer->AddTags(array('div' => array()));
			$sanitizer->AddTags(array('a' => array('href', 'title', 'name', 'style', 'alt', 'target')));
			if ($bSanitizeComments)
				$comments = $sanitizer->SanitizeHtml($comments);
			if ($bSanitizeUserDescription)
				$userDescription = $sanitizer->SanitizeHtml($userDescription);
			unset($sanitizer);
		}
		unset($bSanitizeComments, $bSanitizeUserDescription);

		if(empty($arInvoice['DATE_INSERT']))
			$arInvoice['DATE_INSERT'] = ConvertTimeStamp(time(), 'FULL', SITE_ID);

		$arFields = array(
			'ORDER_TOPIC' => trim($arInvoice['ORDER_TOPIC']),
			'STATUS_ID' => trim($arInvoice['STATUS_ID']),
			'DATE_INSERT' => $arInvoice['DATE_INSERT'],
			'DATE_BILL' => isset($arInvoice['DATE_BILL']) ? trim($arInvoice['DATE_BILL']) : null,
			'PAY_VOUCHER_DATE' => isset($arInvoice['PAY_VOUCHER_DATE']) ? trim($arInvoice['PAY_VOUCHER_DATE']) : null,
			'DATE_PAY_BEFORE' => trim($arInvoice['DATE_PAY_BEFORE']),
			'RESPONSIBLE_ID' => intval($arInvoice['RESPONSIBLE_ID']),
			'UF_COMPANY_ID' => $companyID,
		);
		unset($dateInsert);

		if ($bTaxMode)
		{
			$arFields['PR_LOCATION'] = $arInvoice['LOC_CITY'];
		}
		if ($bEdit)
			$arFields['ACCOUNT_NUMBER'] = trim($arInvoice['ACCOUNT_NUMBER']);
		$bStatusSuccess = CCrmStatusInvoice::isStatusSuccess($arFields['STATUS_ID']);
		if ($bStatusSuccess)
			$bStatusFailed = false;
		else
			$bStatusFailed = CCrmStatusInvoice::isStatusFailed($arFields['STATUS_ID']);
		if ($bStatusSuccess)
		{
			$arFields['PAY_VOUCHER_NUM'] = isset($arInvoice['PAY_VOUCHER_NUM']) ? substr(trim($arInvoice['PAY_VOUCHER_NUM']), 0, 20) : '';
			$arFields['DATE_MARKED'] = $statusParams['PAY_VOUCHER_DATE'] = isset($arInvoice['PAY_VOUCHER_DATE']) ? trim($arInvoice['PAY_VOUCHER_DATE']) : null;
			$arFields['REASON_MARKED'] = isset($arInvoice['REASON_MARKED_SUCCESS']) ? substr(trim($arInvoice['REASON_MARKED_SUCCESS']), 0, 255) : '';
		}
		elseif ($bStatusFailed)
		{
			$arFields['DATE_MARKED'] = isset($_REQUEST['DATE_MARKED']) ? trim($arInvoice['DATE_MARKED']) : null;
			$arFields['REASON_MARKED'] = isset($_REQUEST['REASON_MARKED']) ? substr(trim($_REQUEST['REASON_MARKED']), 0, 255) : '';
		}

		$processProductRows = array_key_exists($productDataFieldName, $arInvoice);
		$arProduct = array();
		if($processProductRows)
		{
			$arProduct = $arInvoice[$productDataFieldName];
		}

		$arProduct = CCrmInvoice::ProductRows2BasketItems($arProduct);
		$arResult['PRODUCT_ROWS'] = $arFields['PRODUCT_ROWS'] = $arProduct;
		
		// Product row settings
		$productRowSettings = array();
		$productRowSettingsFieldName = $productDataFieldName.'_SETTINGS';
		if(array_key_exists($productRowSettingsFieldName, $arInvoice))
		{
			$settingsJson = isset($arInvoice[$productRowSettingsFieldName]) ? strval($arInvoice[$productRowSettingsFieldName]) : '';
			$arSettings = strlen($settingsJson) > 0 ? CUtil::JsObjectToPhp($settingsJson) : array();
			if(is_array($arSettings))
			{
				$productRowSettings['ENABLE_DISCOUNT'] = isset($arSettings['ENABLE_DISCOUNT']) ? $arSettings['ENABLE_DISCOUNT'] === 'Y' : false;
				$productRowSettings['ENABLE_TAX'] = isset($arSettings['ENABLE_TAX']) ? $arSettings['ENABLE_TAX'] === 'Y' : false;
			}
		}
		unset($productRowSettingsFieldName, $settingsJson, $arSettings);

		// set person type field
		$arFields['PERSON_TYPE_ID'] = $personTypeId;

		// set pay system field
		$arFields['PAY_SYSTEM_ID'] = intval($arInvoice['PAY_SYSTEM_ID']);

		$USER_FIELD_MANAGER->EditFormAddFields(CCrmInvoice::GetUserFieldEntityID(), $arFields);
		if($conversionWizard !== null)
		{
			$conversionWizard->prepareDataForSave(CCrmOwnerType::Invoice, $arFields);
		}

		CCrmInvoice::__RewritePayerInfo($companyID, $contactID, $arInvoiceProperties);
		unset($companyId, $contactId);
		CCrmInvoice::rewritePropsFromRequisite($personTypeId, $requisiteIdLinked, $arInvoiceProperties);
		$formProps = array();
		if (isset($arInvoice['LOC_CITY']))
			$formProps['LOC_CITY'] = $arInvoice['LOC_CITY'];

		$tmpArInvoicePropertiesValues = $CCrmInvoice->ParsePropertiesValuesFromPost($personTypeId, $formProps, $arInvoiceProperties);
		if (isset($tmpArInvoicePropertiesValues['PROPS_VALUES']) && isset($tmpArInvoicePropertiesValues['PROPS_INDEXES']))
		{
			$arFields['INVOICE_PROPERTIES'] = $tmpArInvoicePropertiesValues['PROPS_VALUES'];
			foreach ($tmpArInvoicePropertiesValues['PROPS_INDEXES'] as $propertyName => $propertyIndex)
				if (!isset($arFields[$propertyName]))
					$arFields[$propertyName] = $tmpArInvoicePropertiesValues['PROPS_VALUES'][$propertyIndex];
		}
		unset($tmpArInvoicePropertiesValues);
		// </editor-fold>

		$DB->StartTransaction();

		$bSuccess = false;
		if ($bEdit)
		{
			$bSuccess = $CCrmInvoice->Update($arInvoice['ID'], $arFields, array('REGISTER_SONET_EVENT' => true, 'UPDATE_SEARCH' => true));
		}
		else
		{
			$recalculate = false;
			$ID = $CCrmInvoice->Add($arFields, $recalculate, SITE_ID, array('REGISTER_SONET_EVENT' => true, 'UPDATE_SEARCH' => true));
			$bSuccess = (intval($ID) > 0) ? true : false;
			if($bSuccess)
			{
				$arInvoice['ID'] = $ID;
			}
		}

		if ($bSuccess)
		{
			if ($requisiteIdLinked > 0)
			{
				\Bitrix\Crm\Requisite\EntityLink::register(
					CCrmOwnerType::Invoice, $arInvoice['ID'], $requisiteIdLinked, $bankDetailIdLinked
				);
			}
			else
			{
				\Bitrix\Crm\Requisite\EntityLink::unregister(CCrmOwnerType::Invoice, $arInvoice['ID']);
			}
		}

		// link contact to company
		if($bSuccess)
		{
			if($arFields['UF_CONTACT_ID'] > 0 && $arFields['UF_COMPANY_ID'] > 0)
			{
				$CrmContact = new CCrmContact();
				$dbRes = CCrmContact::GetListEx(
					array(),
					array('ID' => $arFields['UF_CONTACT_ID']),
					false,
					false,
					array('COMPANY_ID')
				);
				$arContactInfo = $dbRes->Fetch();
				if ($arContactInfo && intval($arContactInfo['COMPANY_ID']) <= 0)
				{
					$arContactFields = array(
						'COMPANY_ID' => $arFields['UF_COMPANY_ID']
					);

					$bSuccess = $CrmContact->Update(
						$arFields['UF_CONTACT_ID'],
						$arContactFields,
						false,
						true,
						array('DISABLE_USER_FIELD_CHECK' => true)
					);

					if(!$bSuccess)
					{
						$arResult['ERROR_MESSAGE'] = !empty($arFields['RESULT_MESSAGE']) ? $arFields['RESULT_MESSAGE'] : GetMessage('UNKNOWN_ERROR');
					}
				}
				unset($arContactInfo, $dbRes, $CrmContact);
			}
		}

		if($bSuccess)
		{
			$DB->Commit();
		}
		else
		{
			$DB->Rollback();

			$errCode = 0;
			$errMsg = '';
			$ex = $APPLICATION->GetException();
			if ($ex)
			{
				$errCode = $ex->GetID();
				$APPLICATION->ResetException();
				if (!empty($errCode))
					$errMsg = GetMessage('CRM_ERR_SAVE_INVOICE_'.$errCode);
				if ($errMsg == '')
					$errMsg = $ex->GetString();
			}
			$arResult['ERROR_MESSAGE'] = (!empty($errMsg) ? $errMsg : GetMessage('UNKNOWN_ERROR'))."<br />\n";
			unset($errCode, $errMsg);
		}


	if($arInvoice['ID'])
		return $arInvoice['ID']; 
}
?>