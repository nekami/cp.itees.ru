$(document).ready(function() {
	setHandlers();
});

function setHandlers() {
	$("textarea.input_time").change(function() {
		var nogarant = 0;
		var garant = 0;
		$("table.report tr").each(function() {
			if($("input.garant", this).val() == "Y") {
				garant += +($("textarea.time_h", this).val()) * 60 + +($("textarea.time_m", this).val());
			}
			else if($("input.garant", this).val() == "N") {
				nogarant += +($("textarea.time_h", this).val()) * 60 + +($("textarea.time_m", this).val());
			}
		});
		$("input.garant_time_h").val(Math.floor(garant/60));
		$("input.garant_time_m").val(garant%60);
		$("input.nogarant_time_h").val(Math.floor(nogarant/60));
		$("input.nogarant_time_m").val(nogarant%60);
		$("span.garant_time_h").html(Math.floor(garant/60));
		$("span.garant_time_m").html(garant%60);
		$("span.nogarant_time_h").html(Math.floor(nogarant/60));
		$("span.nogarant_time_m").html(nogarant%60);
		$("textarea.input_price").change();
	});
	
	$("textarea.input_price").change(function() {
		var totalPrice = 0;
		var arTime = [];
		var hour_price;
		var price;
		var time;
		$("table.report tr").each(function() {
			if($("input.garant", this).val() == "N") {
				price = $("textarea.input_total_price", this).val();
				hour_price = +($("textarea.input_hour_price", this).val());
				time = +($("textarea.time_h", this).val()) * 60 + +($("textarea.time_m", this).val());
				
				if(+price  > 0)
				{
					totalPrice += +price;
				}
				else
				{
					if(typeof(arTime[hour_price] !== "undefined") && arTime[hour_price] != null)
						arTime[hour_price] = +arTime[hour_price]+time;
					else
						arTime[hour_price] = +time;
				}
			}
		});
		//console.log(arTime);
		for(var key in arTime) {
			totalPrice += (+((+key)*Math.ceil(arTime[key]/60)));
		}

		$("input.nogarant_summ").val(totalPrice.toFixed(2));
		$("span.nogarant_summ").html(totalPrice.toFixed(2));
		/*var nogarant = 0;
		$("table.report tr").each(function() {
			if($("input.garant", this).val() == "N") {
				nogarant += +($("textarea.input_price", this).val());
			}
		});
		$("input.nogarant_summ").val(nogarant);
		$("span.nogarant_summ").html(nogarant);*/
		$("input[name='summ_changed_by_hands']").val('N');
	});
	
	$("span.input_comment, span.input_price, span.input_task").each(function() {
		var obj = this;
		$(obj).parent().click(function() {
			showInputArea(obj);
		});
	});
	
	//$("#table_form :input").change(function() {
		//ajaxPostReload();
	//});
	$("table.report textarea, input.nogarant_summ, input.nogarant_time_m, input.nogarant_time_h, input.garant_summ, input.garant_time_m, input.garant_time_h").each(function() {
		var _this = this;
		$(this).blur(function() {
			$(_this).hide();
			$(_this).prev("span").html(($(_this).val()).replace(/\n/g,"<br>"));
			$(_this).prev("span").show();
			if($(_this).prev("span").html() == "") {
				$(_this).prev("span").html("&nbsp;&nbsp;");
				if($(_this).prev("span").hasClass("input_date")) {
					$(_this).prev("span").html("-- -- ----<br />");
				}
			}
			else {
				if($(_this).prev("span").hasClass("input_date")) {
					$(_this).prev("span").html($(_this).prev("span").html()+"<br />");
				}
			}
		});
	});
	
	//$("textarea.input_hour_price").change(function() {
		//calcPrice($(this).parent().prev("a"));
	//});
	
	window.onkeydown = checkKeys;
}

function SummChangeByHandsHandler() {
	$("#table_form").submit(function() {
		if(+($("input[name='nogarant_summ_2']").val()) == +($("input[name='nogarant_summ']").val())) {
			$("input[name='summ_changed_by_hands']").val('N');
		}
		else {
			$("input[name='summ_changed_by_hands']").val('Y');
		}
	});
}

function checkKeys(e) {
	var KeyID = e.keyCode;
	switch(KeyID) {
		case 13: //Enter
			if($("textarea:focus").length == 1) {
				$textarea = $("textarea:focus");
				if($textarea.hasClass("input_time") || $textarea.hasClass("input_price") || $textarea.hasClass("input_date") ) {
					$textarea.blur();
					$textarea.change();
					e.preventDefault();
				}
			}
			break;
	}
}

var new_tr_counter = 0;

function ShowMenuPopupCustom(obj) {
	
	/* if(hideInputAreaBool)
		return false; */
	
	var $menu = $(obj).siblings("ul");
	$menu.show();
	$("body").prepend("<div class='back'></div>");
	
	$(".back").click(function() {
		$(".popup_menu_custom, .back").hide();
	});
	
	return false;
}

function DeleteFromReport(obj, url) {
	
	if(hideInputAreaBool)
		return false;
	
	if(confirm("�� �������, ��� ������ ������� ������ �� ������?")) {
		var ids = $(obj).attr("data-id");
			var arIds = ids.split(',');
			$(obj).siblings(".loader").show();
			//console.log(ids); return false;
			$.ajax({
				url: $(obj).attr("data-url") + ids,
				cache: false,
				success: function(result){
					//console.log(result); return false;
					if(result == "1") {
						/*for (k in arIds) {
							$("tr#t_"+arIds[k]).remove();
						}
						$(obj).parents("tr").remove();
						$(".popup_menu_custom, .back").hide();*/
						ajaxReload(url,true);
					}
					else if(result == "2") {
						location.assign('/itees/support_reports/reports/');
					}
				}
			});
	}
	return false;
}

function DeleteFromPrintForm(obj, url) {
	
	if(hideInputAreaBool)
		return false;
	
	if(confirm("�� �������, ��� ������ ������� ������ �� ������?")) {
		var ids = $(obj).attr("data-id");
		var arIds = ids.split(',');
		$(obj).siblings(".loader").show();
		
		$.ajax({
			url: $(obj).attr("data-url") + ids,
			cache: false,
			success: function(result){
				if(result == "1") {
					ajaxReload(url);
				}
			}
		});
	}

	return false;
}

function Garant(obj, url) {
	
	if(hideInputAreaBool)
		return false;
	
	var ids = $(obj).attr("data-id");
	var arIds = ids.split(',');
	$(obj).siblings(".loader").show();
	
	$.ajax({
		url: $(obj).attr("data-url") + ids,
		cache: false,
		success: function(result){
			if(result == "1") {
				ajaxReload(url);
			}
		}
	});

	return false;
}

function ajaxPostReload() {
	var fields = $("#table_form :input").serializeArray();
	fields.push({name:"save"});
	fields.push({name:"ajax"});
	//$("body").prepend("<div class='ajax_post_loader'></div>");
	$("body").css("cursor", "wait");
	$.ajax({
		type: "POST",
		data: fields,
		success: function(msg){
			$("table.report").html("");
			$("table.report").html($("table.report", msg).html());
			$(".popup_menu_custom, .back, .loader").hide();
			$("input[name='clear']").show();
			$("body").css("cursor", "default");
			//$(".ajax_post_loader").remove();
			setHandlers();
		}
	});
}

function ajaxReload(url, recalculate) {
	$.ajax({
		type: "GET",
		url: url,
		success: function(msg){
			$("table.report").html("");
			$("table.report").html($("table.report", msg).html());
			$(".popup_menu_custom, .back, .loader").hide();
			setHandlers();
			if(recalculate == true) {
				$('.input_time').change();
				setTimeout(function() {ajaxPost($('#table_form').attr('action'))}, 500);
			}
		}
	});
}

function ajaxPost(url) {
	var form_data = $('#table_form').serialize();
	form_data = form_data + '&save=���������&ajax=true';
  $.post(url, form_data, function() {});
}

function addFirstRow(obj) {
	addRow($("a.plus", $(obj).parents("tr:first").next('tr').nextAll("tr:visible").first()), 1);
}

function addRow(obj, before) {
	
	if(hideInputAreaBool)
		return false;
	
	new_tr_counter++;
	var $cur_tr = $(obj).parents("tr:first");
	var $clone = $cur_tr.clone();
	$clone.css('background', '#fff');
	$('.zero-time-alert', $clone).remove();
	var $counter = $("input.counter", $clone);
	var $garant = $("input.garant", $clone);
	if(before == 1)
		$counter.val(+$counter.val() - 0.01);
	else
		$counter.val(+$counter.val() + 0.01);
	$(".task_name, .task_id, .menu_td, .duration, .grades_time, .estimate_time", $clone).html("");
	$(":input", $clone).val("");
	$("span.input_date, span.input_task, span.input_comment, span.input_hour_price, span.input_total_price, span.input_time", $clone).hide();
	$("textarea.input_date, textarea.input_task, textarea.input_comment, textarea.input_hour_price, textarea.input_total_price, textarea.input_time", $clone).show();
	$(":input", $clone).each(function() {
		var name = $(this).attr("name");
		if(typeof(name) != 'undefined') {
			if(!name.match(/\[new\]/))
				name = name.replace(/\[[n_0-9]+\]/, '[new]['+(new_tr_counter)+']');
			else
				name = name.replace(/\[[n_0-9]+\]/, '['+(new_tr_counter)+']');
			$(this).attr("name", name);
		}
	});
	var name = $counter.attr("name");
	var garant_name = $garant.attr("name");
	if(typeof(name) != 'undefined') {
		if(!name.match(/\[new\]/))
			name = name.replace(/\[[n_0-9]+\]/, '[new]['+(new_tr_counter)+']');
		else
			name = name.replace(/\[[n_0-9]+\]/, '['+(new_tr_counter)+']');
	}
	if(typeof(garant_name) != 'undefined') {
		if(!garant_name.match(/\[new\]/))
			garant_name = garant_name.replace(/\[[n_0-9]+\]/, '[new]['+(new_tr_counter)+']');
		else
			garant_name = garant_name.replace(/\[[n_0-9]+\]/, '['+(new_tr_counter)+']');
	}
	$counter.attr("name", name);
	$garant.attr("name", garant_name);
	$(".task_id", $clone).append($counter);
	$(".task_id", $clone).append($garant);
	if($(".minus", $clone).length == 0)
		$(".plus", $clone).parent().append(" <a href='#' class='minus' onclick='delRow(this); return false;'>-</a>");
	if(before == 1)
		$cur_tr.before($clone);
	else
		$cur_tr.after($clone);
	setHandlers();
}

function delRow(obj) {
	var $cur_tr = $(obj).parents("tr:first");
	$cur_tr.remove();
}

function showInputArea(obj, h) {
	
	if(hideInputAreaBool)
		return false;
	
	var width = $(obj).parents("td").width();
	var td = $(obj).closest("td");

	if($("pre", obj).length > 0)
		var height = $("pre", obj).height() + 22;
	else
		var height = $(obj).height();
	var data = $(obj).attr("data");
	$(obj).hide();
	if(h)
		$("textarea."+data, td).height(height);
	$("textarea."+data+", input."+data, td).show().focus();
	if(width < $("textarea."+data, td).width())
		$("textarea."+data, td).width(width);
		
	//$("textarea."+data).on('resize', function() {$(this).css("cursor", "nw-resize")})
	$("textarea."+data, td).css("resize", "vertical");
}

function forAll(obj) {
	
	if(hideInputAreaBool)
		return false;
	
	if(confirm("�� ������ ��������� ������ �������� �� ���� �����?")) {
		var tr = $(obj).parents("tr");
		var garant = $("input.garant", tr).val();
		var val = $("textarea", $(obj).siblings("div")).val();
		if(garant == "N") {
			$("table.report tr").each(function() {
				if($("input.garant", this).val() == "N") {
					$("textarea.input_hour_price", this).val(val).change();
					$("span.input_hour_price", this).html(val);
				}
			});
		}
		else {
			$("table.report tr").each(function() {
				if($("input.garant", this).val() == "Y") {
					$("textarea.input_hour_price", this).val(val).change();
					$("span.input_hour_price", this).html(val);
				}
			});
		}
	}
}

function calcPrice(obj) {
	
	if(hideInputAreaBool)
		return false;
	
	var tr = $(obj).closest("tr");
	var hour_price = $("textarea", $(obj).siblings("div")).val();
	var minute_price = hour_price/60;
	var hours = $("textarea.time_h", tr).val();
	var minutes = $("textarea.time_m", tr).val();
	var price = Math.ceil((+(hours*60) + +minutes)/60 * hour_price);
	//console.log(minute_price);
	$("textarea.input_total_price", tr).val(price).change();
	$("span.input_total_price", tr).html(price);
	$("input[name='summ_changed_by_hands']").val('N');
}

function calcAllPrices() {
	$("table.report tr").each(function() {
		if($("input.garant", this).val() == "N")
			$("a.calcprice", this).click();
	});
	$("input[name='summ_changed_by_hands']").val('N');
}

function showChecklist()
{
	if($('.trig').hasClass('hide'))
	{
		$('.trig').removeClass('hide');
		$('.checklist_show').text('������ ������ ���-�����');
	}
	else
	{
		$('.trig').addClass('hide');
		$('.checklist_show').text('�������� ������ ���-�����');
	}
}

function setInputAreaBool(val)
{
	hideInputAreaBool = val;
	
	if(val)
	{
		$('.report').addClass('deactivated');
		$('.save.report_save, #report_date').attr('disabled', 'disabled');
		$('.report-name-edit').addClass('hideImp');
		$('#report_date + img').hide();
		if(!$("span").is(".disabled_save"))
			$('.save.report_save').after('<span class="disabled_save">����� �������� � �� ����� ���� ��������!</span>');
	}
	else
	{
		$('.report').removeClass('deactivated');
		$('.save.report_save, #report_date').removeAttr('disabled', 'disabled');
		$('.report-name-edit').removeClass('hideImp');
		$('#report_date + img').show();
		$('.disabled_save').remove();
	}
}

function startReportPassClient(val)
{
	if(val)
	{
		$('#checklist input, #checklist button').attr('disabled', 'disabled');
		setInputAreaBool(1);
	}
	else
	{
		$('#checklist input, #checklist button').removeAttr('disabled', 'disabled');
		setInputAreaBool(hideInputAreaBool);
	}
}

function setReportPassClient(val)
{	
	reportPassClient = val;
	
	if(val)
	{
		setInputAreaBool(hideInputAreaBool);
		$('#checklist input, #checklist button').removeAttr('disabled', 'disabled');
	}
	else
	{
		setInputAreaBool(1);
		$('#checklist input, #checklist button').attr('disabled', 'disabled');
	}
}

function checklistChange(checkbox)
{
	if(reportCreator && !$(checkbox).attr('disabled') && checkbox.checked == true)
		return false;
}

function CheckTrackNumber(val)
{
	var error = 0;
	if(val)
	{
		if(isNaN(parseInt(String(val))))
		{
			$('#track_number_link_li input').val('');
			error = 1;
		}
		else
		{
			if(parseInt(String(val)) != val)
			{
				$('#track_number_link_li input').val(parseInt(String(val)));
				error = 1;
			}
		}
		if(val.length > 14)
		{
			$('#track_number_link_li input').val(val.substr(0, 14));
			error = 1;
		}
		else if(val.length < 14)
			error = 1;
		else
			error = 0;
	}
	
	if(error)
		$('#track_number_link_li .track_number_error').show();
	else
		$('#track_number_link_li .track_number_error').hide();
}

function isNumeric(n)
{
  return !isNaN(parseFloat(n)) && isFinite(n);
}


function ToggleDate()
{
	if($('#send_ado_li input:checked').length)
		$('#send_ado_date_li').removeClass('hide');
	else
		$('#send_ado_date_li').addClass('hide');
}

function ReportNameEdit()
{
	if($('#report_name').hasClass('hide'))
	{
		$('#report_name')
			.removeClass('hide')
			.addClass('show');
		
		$('#report_name_edit_block')
			.removeClass('show')
			.addClass('hide');
		
		var inp = $('#report_name_edit');
		
		
		if(inp.val())
			var defVal = inp.val();
		else
			var defVal = inp.prop('placeholder');
		
		$('#report_name span').html(defVal);
	}
	else if($('#report_name').hasClass('show'))
	{
		$('#report_name')
			.removeClass('show')
			.addClass('hide');
		
		$('#report_name_edit_block')
			.removeClass('hide')
			.addClass('show');
			
		$('body').click(function(event)
		{
			if($(event.target).prop('id') != 'report_name_edit' && $(event.target).prop('id') != 'report-name-edit-button')
			{
				ReportNameEdit();
				$('body').unbind('click');

				$.post(
					$('#table_form').attr('action'),
					{
						'report-name-new' : $('#report_name_edit').val()
					}
				);
			}
		});
	}
}

function TogglePrepayment()
{
	if($('#prepayment:checked').length)
		$('#invoices_select').removeClass('hide');
	else
		$('#invoices_select').addClass('hide');
}

function SetCrmInvoicesSetVals(invoices, inputs)
{
	var array = [];

	if($('#prepayment').prop("checked"))
	{
		$(inputs).each(function(i, v){
			array.push($(v).val());
		});
	}
	
	for (var id in invoices)
	{
		if($.inArray(invoices[id].id, array) > -1)
			invoices[id].selected = 'Y';
		else
			invoices[id].selected = 'N';
	}
	

	$('.itees_task_crm_client_link').click(function()
	{
		if(typeof(obCrm) == 'object') {
			obCrm[this.id].Open();
			
			if(obCrm[this.id].onSaveListeners.length == 0)
			{
				obCrm[this.id].AddOnSaveListener(function(arElements) {
					$('#selected-clients').remove();
					InvoicesSumFormate(arElements, inv_price);
				});
			}
		}
		return false;
	});

	SetCrmInvoices(invoices);
	
	if(typeof(obCrm) == 'object')
	{
		if(obCrm['itees_task_crm_client_link'].onSaveListeners.length == 0)
		{
			obCrm['itees_task_crm_client_link'].AddOnSaveListener(function(arElements)
			{
				$('#selected-clients').remove();
				InvoicesSumFormate(arElements, inv_price);
			});
		}
	}
}

function SetCrmInvoices(invoices, inv_price)
{
	var actions = {
		'ok': "Ok",
		'cancel': "������",
		'close': "�������",
		'wait': "�����",
		'noresult': "� ���������, �� ��� ��������� ������ ������ �� �������",
		'add': "������� ���� � CRM",
		'edit': "��������",
		'search': "�����",
		'last': "���������",
		'company': "�����"
	}

	/* $('.itees_task_crm_client_link').click(function()
	{
		if(typeof(obCrm) == 'object') {
			obCrm[this.id].Open();
			
			if(obCrm[this.id].onSaveListeners.length == 0)
			{
				obCrm[this.id].AddOnSaveListener(function(arElements) {
					$('#selected-clients').remove();
					InvoicesSumFormate(arElements, inv_price);
				});
			}
		}
		return false;
	}); */
	
	$('.itees_task_crm_client_link').each(function()
	{
		if(typeof(CRM) != 'undefined') {
			CRM.Set(BX($(this).attr('id')), 'invoice_id', '', invoices, false, true, ['lead'], actions);
		}
	});
}

function InvoicesSumFormate(arSelInv, arAllInvPrice)
{
	var sumStr = '';
	if(Object.keys(arSelInv.lead).length)
	{
		var allSumm = 0;
		var paidSumm = 0;
		var sumStr = '';
		
		for (var id in arSelInv.lead)
		{
			allSumm = allSumm + Number(arAllInvPrice[arSelInv.lead[id].id].PRICE);
			paidSumm = paidSumm + Number(arAllInvPrice[arSelInv.lead[id].id].SUM_PAID);
		}

		if(allSumm > 0)
			sumStr = '����� ������ �� ����������:<br> ' + XFormatPrice(allSumm) + ',<br> �� ��� �������� ' + XFormatPrice(paidSumm);

		if(paidSumm == Number($('input[name=nogarant_summ]').val()))
			$('input[name=payed]').prop( "checked", "checked" );
		else
			$('input[name=payed]').removeProp( "checked", "checked" );
	}
	
	$('#sum_paid').html(sumStr);
}

function XFormatPrice(_number) 
{
    var decimal=0;
    var separator=' ';
    var decpoint = '.';
    var format_string = '# ���';
 
    var r=parseFloat(_number)
 
    var exp10=Math.pow(10,decimal);// �������� � ����������� ���������
    r=Math.round(r*exp10)/exp10;// ��������� �� ������������ ����� ������ ����� �������
 
    rr=Number(r).toFixed(decimal).toString().split('.');
 
	if(rr.length == 1)
		rr[1] = '00';

    b=rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1"+separator);
 
    r=(rr[1]?b+ decpoint +rr[1]:b);
    return format_string.replace('#', r);
}