<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(intval($arResult['REPORT_ID']) > 0){?>
<?global $APPLICATION;?>
<?$GLOBALS["DETAIL_REPORT_TEMPLATE_FOLDER"] = $templateFolder;?>
<?/*if($_REQUEST["download"] == 'true'){?>
	<?LocalRedirect('/itees/support_reports/reports/pdf.php?id='.$arResult['REPORT_ID'].'&download');?>
<?}*/?>
<?if(isset($_REQUEST["print"])){?>
<script type="text/javascript">
	//BX.ready(function() {
		window.open('/itees/support_reports/reports/pdf.php?id=<?=$arResult['REPORT_ID']?>', "", "width=200, height=100");
		//location.assign('/itees/support_reports/reports/pdf.php?id=<?=$arResult['REPORT_ID']?>');
		//location.assign('<?=$APPLICATION->GetCurPageParam("", Array("print"))?>');
	//});
</script>
<?//LocalRedirect($APPLICATION->GetCurPageParam());
}?>
<?
$colspan = 3;

if($APPLICATION->get_cookie("show_menu") == 1)
	$colspan++;
?>

<?
function print_childs($arTask, $padding, &$time, &$counter, &$arResult)
{
	global $APPLICATION;
	
	if(is_array($arTask["CHILDS"]))
	{
		foreach($arTask["CHILDS"] as $arChild)
		{
			$counter++;
			?>
			<?if(intval($arChild["ID"]) > 0):?>
			<tr id="t_<?=$arChild["ID"]?>" <?if($arChild["MINUTES"] == 0 && $arResult['SHOW_ZERO_TIME_TASKS']  != '��'):?>style="display:none"<?$counter--?><?endif?>>
				<td class="task_id" style="text-align:center;">
					�<?=$counter?><br /><?=$arChild["ID"]?>
					<input type="hidden" name="counter[<?=$arChild["ID"]?>]" value="<?=$counter?>" class="counter" />
					<?if($arChild["MINUTES"] == 0):?>
						<input type="hidden" name="hidden[<?=$arChild["ID"]?>]" value="Y" />
					<?else:?>
						<input type="hidden" name="hidden[<?=$arChild["ID"]?>]" value="N" />
					<?endif?>
					<?if($arChild["UF_TASK_GARANT"] > 0):?>
						<input type="hidden" name="garant[<?=$arChild["ID"]?>]" value="Y" class="garant" />
					<?else:?>
						<input type="hidden" name="garant[<?=$arChild["ID"]?>]" value="N" class="garant" />
					<?endif?>
				</td>
				<textarea style="display:none" class="input_date input_date1_<?=$counter?>" name="date_start[<?=$arChild["ID"]?>]"><?=substr($arChild["CREATED_DATE"], 0, 10);?></textarea>
				<textarea style="display:none" class="input_date input_date2_<?=$counter?>" name="date_close[<?=$arChild["ID"]?>]"><?=substr($arChild["CLOSED_DATE"], 0, 10);?></textarea>
				<td class="task_name" style="padding-left:<?=$padding?>em">
					<a href='/workgroups/group/<?=$arChild["GROUP_ID"]?>/tasks/task/view/<?=$arChild["ID"]?>/' onclick='if(taskIFramePopup.isLeftClick(event)) {taskIFramePopup.view(<?=$arChild["ID"]?>); return false;}'><?=$arChild["TITLE"]?></a>
					<input type="hidden" name="orig_task[<?=$arChild["ID"]?>]" value="<?=$arChild["TITLE"]?>" />
					<?
					if(!empty($arChild['CREATED_BY_NAME']))
					{
						?><br><span class="resp_info">�����������: <?=$arChild['CREATED_BY_NAME'];?></span><?
					}

					if(!empty($arChild['RESPONSIBLE_ID_NAME']))
					{
						?><br><span class="resp_info">�������������: <?=$arChild['RESPONSIBLE_ID_NAME'];?></span><?
					}
					?>
				</td>
				<td>
					<?if(isset($arChild["TITLE_PRINT"]))
						$title = $arChild["TITLE_PRINT"];
					else
						$title = $arChild["TITLE"];?>
					<span class="input_task" data="input_task_<?=$counter?>" onclick="showInputArea(this, true)"><?=preg_replace("/\n/", "<br>", $title)?>&nbsp;</span>
					<textarea class="input_task input_task_<?=$counter?>" name="task[<?=$arChild["ID"]?>]"><?=$title?></textarea>
				</td>
				<?if($APPLICATION->get_cookie("show_menu") == 1):?>
				<td class="menu_td" style="position:relative;">
					<a href="javascript: void(0)" class="task-menu-button custom" onclick="return ShowMenuPopupCustom(this);" title="����"><i class="task-menu-button-icon"></i></a>
					<ul class="popup_menu_custom">
						<li><a class='delete_report' href="javascript: void(0)" data-id="<?=$arChild["ID"]?>" data-url="<?=$GLOBALS["DETAIL_REPORT_TEMPLATE_FOLDER"]?>/ajax/delete_task_from_report.php?ID=" onclick="DeleteFromReport(this, '<?=$GLOBALS["APPLICATION"]->GetCurPageParam()?>')">������� �� ������</a><img src='/bitrix/templates/bitrix24/components/itees/simple.component/support.tasks/images/ajax-loader.gif' class="loader" style="position:absolute; right:0.5em; top:0.5em;" alt='' /></li>
						<li><a class='download_xls_timereport' href="/dev/time_report.php?id=<?=$arChild["ID"]?>">������� ��������� �����</a></li>
					</ul>
				</td>
				<?endif?>
				<td class="nowrap">
					<?if(isset($arChild["DURATION"]))
						$duration = $arChild["DURATION"];
					else
						$duration = intval($arChild["MINUTES"]/60).' � '.$arChild["MINUTES"]%60 . ' �';?>
					
					<?
					$ESTIMATE_MINUTES = intval($arChild["TIME_ESTIMATE"])/60;
					$ELAPSED_MINUTES = $arChild["MINUTES"];
					if($ESTIMATE_MINUTES > $ELAPSED_MINUTES)
						$MINUTES = $ESTIMATE_MINUTES;
					else
						$MINUTES = $ELAPSED_MINUTES;
					?>
					
					<span class="input_time time_h" data="time_h_<?=$counter?>" onclick="showInputArea(this)"><?=intval($MINUTES/60)?>&nbsp;</span>
					<textarea class="input_time time_h time_h_<?=$counter?>" name="time_h[<?=$arChild["ID"]?>]" style="display:none"><?=intval($MINUTES/60)?></textarea> �
					<span class="input_time time_m" data="time_m_<?=$counter?>" onclick="showInputArea(this)"><?=($MINUTES%60)?>&nbsp;</span>
					<textarea class="input_time time_m time_m_<?=$counter?>" name="time_m[<?=$arChild["ID"]?>]" style="display:none"><?=($MINUTES%60)?></textarea>�
				</td>
				<td class="estimate_time nowrap">
					<?=intval($arChild["TIME_ESTIMATE"]/(60*60))?> � <?=((intval($arChild["TIME_ESTIMATE"])/60)%(60))?> �
				</td>
				<td class="grades_time nowrap">
					<?if($arChild['UF_TASK_GARANT'] == 0)
					{
						$GLOBALS['TOTAL_NO_GARANT_GRADES'] += $arChild["MINUTES_GRADES"];
					}?>
					<?=intval($arChild["MINUTES_GRADES"]/60)?> � <?=($arChild["MINUTES_GRADES"]%60)?> �
				</td>
				<td class="duration nowrap">
					<?if(isset($arChild["DURATION"]))
						$duration = $arChild["DURATION"];
					else
						$duration = intval($arChild["MINUTES"]/60).' � '.$arChild["MINUTES"]%60 . ' �';?>
					<?=intval($arChild["~MINUTES"]/60)?> � <?=($arChild["~MINUTES"]%60)?> �
				</td>
				<td>
					<?$price = 0;?>
					<span class="input_price input_total_price" data="input_total_price<?=$counter?>" onclick="showInputArea(this)"><?=$price?>&nbsp;</span>
					<textarea class="input_price input_total_price input_total_price<?=$counter?>" name="price[<?=$arChild["ID"]?>]" style="display:none"><?=$price?></textarea>
				</td>
				<td>
					<?$hour_price = 0;?>
					<a class="calcprice" href="#" onclick="calcPrice(this); return false;" style="display:inline-block; margin-right:8px;"><-</a>
					<div style="display:inline-block; width:3em; text-align:center">
					<span class="input_price input_hour_price" data="input_hour_price_<?=$counter?>" onclick="showInputArea(this)"><?=$hour_price?>&nbsp;</span>
					<textarea class="input_price input_hour_price input_hour_price_<?=$counter?>" name="hour_price[<?=$arChild["ID"]?>]" style="display:none"><?=$hour_price?></textarea>
					</div>
					<a href="#" onclick="forAll(this); return false;" title="��������� ��� ����">=</a>
				</td>
				<td>
					<?if(!empty($arChild["COMMENT"]))
						$comment = $arChild["COMMENT"];
					else
						$comment = ($arChild["UF_TASK_GARANT"] == 1) ? "����������� ������" : "";?>
					<span class="input_comment" data="input_comment_<?=$counter?>" onclick="showInputArea(this, true)"><?=$comment?>&nbsp;</span>
					<textarea class="input_comment input_comment_<?=$counter?>" name="comment[<?=$arChild["ID"]?>]" style="display:none"><?=preg_replace("/\n/", "<br>", $comment)?></textarea>
				</td>
				<td>
					<a href="#" class="plus" onclick="addRow(this); return false;">+</a>
				</td>
			</tr>
			<?$time += $MINUTES;//$arChild["MINUTES"];?>
			<?else:?>
			<?$counter--?>
			<?endif?>
			<?
			print_childs($arChild, $padding + 0, $time, $counter, $arResult);
		}
	}
}
?>

<br>
<div class="report-name-block-bg">
	<div class="report-name-block">
		<div class="report-name show" id="report_name">
			<span><?=!empty($arResult['REPORT_NAME']) ? $arResult['REPORT_NAME'] : $arResult['NAME']?></span>
			<div id="report-name-edit-button" class="report-name-edit" onClick="ReportNameEdit();"></div>
		</div>
		<div id="report_name_edit_block" class="report-name-edit-block hide">
			<input id="report_name_edit" placeholder="<?=$arResult['NAME']?>" type="text" name="report-name-new" value="<?=$arResult['REPORT_NAME']?>">
		</div>
		<div class="clear"></div>
		<span class="small">(�������� ������ ������������ � ������ ������� �������)</span>																														   
	</div>
	
	<div class="report-status">
		<div class="report-status-flex">
			<div>
				<?$APPLICATION->IncludeComponent(
					"itees:support.reports.status",
					"",
					Array(
						"REPORT_ID" => $arResult["ID"],
						"REPORT_IBLOCK_ID" => 59,
						"REPORT_STATUS_CODE" => "REPORT_STATUS",
						"REPORT_STATUS_IBLOCK" => 576
					)
				);?>
			</div>
		</div>
	</div>   
</div>

<form action="<?=$APPLICATION->GetCurPageParam()?>" method="post" id="table_form">

<table class="report">
	<tr>
		<th style="width: 50px;" rowspan="2">ID</th>
		<th style="width: 17%;" rowspan="2">�������� ������</th>
		<th style="width: 17%;" rowspan="2">�������� ������ ��� ������</th>
		<th style="width:20px;" rowspan="2"></th>
		<th style="width:300px;" colspan="4">�����</th>
		<th rowspan="2">���������</th>
		<th rowspan="2">��������� ����</th>
		<th rowspan="2">�����������</th>
		<th rowspan="2" style="width:20px; text-align:left !important;"><a href="#" class="plus" onclick="addFirstRow(this); return false;">+</a></th>
	</tr>
	<tr>
		<th>�����</th>
		<th>�������</th>
		<th>������</th>
		<th>����</th>
	</tr>
	<?$flag == false; $time = 0; $c = 0; $counter = 0;?>
	<?if(!count($arResult["NEW_TASKS"])):?>
	<?foreach($arResult["PROPERTY_TASK_ID_VALUE"] as $arTask):?>
		<?$counter++?>
		<?if($arTask["UF_TASK_GARANT"] == 0 && $flag == false && $c > 0):?>
			<tr class="total">
				<td colspan="<?=$colspan?>">����� �� ����������� �������:</td>
				<td colspan="2">

					<span class="garant_time garant_time_h" data="garant_time_h" onclick="showInputArea(this)"><?=intval($time/60);?></span>
					<input class="garant_time_h" type="text" name="garant_time_h" value="<?=intval($time/60)?>" style="display:none; width:20px;" /> �
					<span class="garant_time garant_time_m" data="garant_time_m" onclick="showInputArea(this)"><?=($time%60);?></span>
					<input class="garant_time_m" type="text" name="garant_time_m" value="<?=($time%60)?>" style="display:none; width:20px;" /> �

					<input type="hidden" name="garant_time_sort" value="<?=(($counter-1) * 100 + 99)?>" />
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><a href="#" class="plus" onclick="addFirstRow(this); return false;" style="font-weight:normal;">+</a></td>
			</tr>
			<?$flag = true; $time = 0;?>
		<?endif?>
		
		<?if($arTask["UF_TASK_GARANT"] == 1):?>
			<?$c++?>
		<?endif?>
	
	<?if(intval($arTask["ID"]) > 0):?>
	<tr id="t_<?=$arTask["ID"]?>" <?if($arTask["MINUTES"] == 0 && $arResult['SHOW_ZERO_TIME_TASKS'] != '��'):?>style="display:none"<?$counter--?><?endif?>>
		<td class="task_id" style="text-align:center;">
			�<?=$counter?><br /><?=$arTask["ID"]?>
			<input type="hidden" name="counter[<?=$arTask["ID"]?>]" value="<?=$counter?>" class="counter" />
			<?if($arTask["MINUTES"] == 0):?>
				<input type="hidden" name="hidden[<?=$arTask["ID"]?>]" value="Y" />
			<?else:?>
				<input type="hidden" name="hidden[<?=$arTask["ID"]?>]" value="N" />
			<?endif?>
			<?if($arTask["UF_TASK_GARANT"] > 0):?>
				<input type="hidden" name="garant[<?=$arTask["ID"]?>]" value="Y" class="garant" />
			<?else:?>
				<input type="hidden" name="garant[<?=$arTask["ID"]?>]" value="N" class="garant" />
			<?endif?>
		</td>
		<textarea style="display:none" class="input_date input_date1_<?=$counter?>" name="date_start[<?=$arTask["ID"]?>]"><?=substr($arTask["CREATED_DATE"], 0, 10);?></textarea>
		<textarea style="display:none" class="input_date input_date2_<?=$counter?>" name="date_close[<?=$arTask["ID"]?>]"><?=substr($arTask["CLOSED_DATE"], 0, 10);?></textarea>
		<td class="task_name">
			<a href='/workgroups/group/<?=$arTask["GROUP_ID"]?>/tasks/task/view/<?=$arTask["ID"]?>/' onclick='if(taskIFramePopup.isLeftClick(event)) {taskIFramePopup.view(<?=$arTask["ID"]?>); return false;}'><?=$arTask["TITLE"]?></a>
			<input type="hidden" name="orig_task[<?=$arTask["ID"]?>]" value="<?=$arTask["TITLE"]?>" />
			<?
			if(!empty($arTask['CREATED_BY_NAME']))
			{
				?><br><span class="resp_info">�����������: <?=$arTask['CREATED_BY_NAME'];?></span><?
			}

			if(!empty($arTask['RESPONSIBLE_ID_NAME']))
			{
				?><br><span class="resp_info">�������������: <?=$arTask['RESPONSIBLE_ID_NAME'];?></span><?
			}
			?>
		</td>
		<td>
			<?if(isset($arTask["TITLE_PRINT"]))
				$title = $arTask["TITLE_PRINT"];
			else
				$title = $arTask["TITLE"];?>
			<span class="input_task" data="input_task_<?=$counter?>" onclick="showInputArea(this, true)"><?=preg_replace("/\n/", "<br>", $title)?>&nbsp;</span>
			<textarea class="input_task input_task_<?=$counter?>" name="task[<?=$arTask["ID"]?>]"><?=$title?></textarea>
		</td>
		<?if($APPLICATION->get_cookie("show_menu") == 1):?>
		<td class="menu_td" style="position:relative;">
			<a href="javascript: void(0)" class="task-menu-button custom" onclick="return ShowMenuPopupCustom(this);" title="����"><i class="task-menu-button-icon"></i></a>
			<ul class="popup_menu_custom">
				<li><a class='delete_report' href="javascript: void(0)" data-id="<?=$arTask["ID"]?>" data-url="<?=$templateFolder?>/ajax/delete_task_from_report.php?ID=" onclick="DeleteFromReport(this, '<?=$GLOBALS["APPLICATION"]->GetCurPageParam()?>')">������� �� ������</a><img src='/bitrix/templates/bitrix24/components/itees/simple.component/support.tasks/images/ajax-loader.gif' class="loader" style="position:absolute; right:0.5em; top:0.5em;" alt='' /></li>
				<li><a class='download_xls_timereport' href="/dev/time_report.php?id=<?=$arTask["ID"]?>">������� ��������� �����</a></li>
			</ul>
		</td>
		<?endif?>
		<td class="nowrap">
			<?if(isset($arTask["DURATION"]))
				$duration = $arTask["DURATION"];
			else
				$duration = intval($arTask["MINUTES"]/60).' � '.$arTask["MINUTES"]%60 . ' �';?>
			
			<?
			$ESTIMATE_MINUTES = intval($arTask["TIME_ESTIMATE"])/60;
			$ELAPSED_MINUTES = $arTask["MINUTES"];
			if($ESTIMATE_MINUTES > $ELAPSED_MINUTES)
				$MINUTES = $ESTIMATE_MINUTES;
			else
				$MINUTES = $ELAPSED_MINUTES;
			?>
					
			<span class="input_time time_h" data="time_h_<?=$counter?>" onclick="showInputArea(this)"><?=intval($MINUTES/60)?>&nbsp;</span>
			<textarea class="input_time time_h time_h_<?=$counter?>" name="time_h[<?=$arTask["ID"]?>]" style="display:none"><?=intval($MINUTES/60)?></textarea> �
			<span class="input_time time_m" data="time_m_<?=$counter?>" onclick="showInputArea(this)"><?=($MINUTES%60)?>&nbsp;</span>
			<textarea class="input_time time_m time_m_<?=$counter?>" name="time_m[<?=$arTask["ID"]?>]" style="display:none"><?=($MINUTES%60)?></textarea>�
		</td>
		<td class="estimate_time nowrap">
			<?=intval($arTask["TIME_ESTIMATE"]/3600)?> � <?=($arTask["TIME_ESTIMATE"]%60)?> �
		</td>
		<td class="grades_time nowrap">
			<?if($arTask['UF_TASK_GARANT'] == 0)
			{
				$GLOBALS['TOTAL_NO_GARANT_GRADES'] += $arTask["MINUTES_GRADES"];
			}?>
			<?=intval($arTask["MINUTES_GRADES"]/60)?> � <?=($arTask["MINUTES_GRADES"]%60)?> �
		</td>
		<td class="duration nowrap">
			<?if(isset($arTask["DURATION"]))
				$duration = $arTask["DURATION"];
			else
				$duration = intval($arTask["MINUTES"]/60).' � '.$arTask["MINUTES"]%60 . ' �';?>
			<?=intval($arTask["~MINUTES"]/60)?> � <?=($arTask["~MINUTES"]%60)?> �
		</td>
		<td>
			<?$price = 0;?>
			<span class="input_price input_total_price" data="input_total_price<?=$counter?>" onclick="showInputArea(this)"><?=$price?>&nbsp;</span>
			<textarea class="input_price input_total_price input_total_price<?=$counter?>" name="price[<?=$arTask["ID"]?>]" style="display:none"><?=$price?></textarea>
		</td>
		<td>
			<?$hour_price = 0;?>
			<a class="calcprice" href="#" onclick="calcPrice(this); return false;" style="display:inline-block; margin-right:8px;"><-</a>
			<div style="display:inline-block; width:3em; text-align:center">
			<span class="input_price input_hour_price" data="input_hour_price_<?=$counter?>" onclick="showInputArea(this)"><?=$hour_price?>&nbsp;</span>
			<textarea class="input_price input_hour_price input_hour_price_<?=$counter?>" name="hour_price[<?=$arTask["ID"]?>]" style="display:none"><?=$hour_price?></textarea>
			</div>
			<a href="#" onclick="forAll(this); return false;" title="��������� ��� ����">=</a>
		</td>
		<td>
			<?if(!empty($arTask["COMMENT"]))
				$comment = $arTask["COMMENT"];
			else
				$comment = ($arTask["UF_TASK_GARANT"] == 1) ? "����������� ������" : "";?>
			<span class="input_comment" data="input_comment_<?=$counter?>" onclick="showInputArea(this, true)"><?=preg_replace("/\n/", "<br>", $comment)?>&nbsp;</span>
			<textarea class="input_comment input_comment_<?=$counter?>" name="comment[<?=$arTask["ID"]?>]" style="display:none"><?=$comment?></textarea>
		</td>
		<td>
			<a href="#" class="plus" onclick="addRow(this); return false;">+</a>
		</td>
	</tr>
	<?$time += $MINUTES;//$arTask["MINUTES"];?>
	<?else:?>
	<?$counter--?>
	<?endif?>

	<?print_childs($arTask, 1, $time, $counter, $arResult)?>
	<?endforeach?>
	<tr class="total">
		<td colspan="<?=$colspan?>">����� �� <?if($arTask["UF_TASK_GARANT"] == 0):?>�������������<?else:?>�����������<?endif?> �������:</td>
		<td colspan="2">
			<?if($arTask["UF_TASK_GARANT"] == 0):?>

			<span class="nogarant_time nogarant_time_h" data="nogarant_time_h" onclick="showInputArea(this)"><?=intval($time/60);?></span>
			<input class="nogarant_time_h" type="text" name="nogarant_time_h" value="<?=intval($time/60)?>" style="display:none; width:20px;" /> �
			<span class="nogarant_time nogarant_time_m" data="nogarant_time_m" onclick="showInputArea(this)"><?=($time%60);?></span>
			<input class="nogarant_time_m" type="text" name="nogarant_time_m" value="<?=($time%60)?>" style="display:none; width:20px;" /> �

			<input type="hidden" name="nogarant_time_sort" value="<?=($counter * 100 + 99)?>" />
			<?else:?>

			<span class="garant_time garant_time_h" data="garant_time_h" onclick="showInputArea(this)"><?=intval($time/60);?></span>
			<input class="garant_time_h" type="text" name="garant_time_h" value="<?=intval($time/60)?>" style="display:none; width:20px;" /> �
			<span class="garant_time garant_time_m" data="garant_time_m" onclick="showInputArea(this)"><?=($time%60);?></span>
			<input class="garant_time_m" type="text" name="garant_time_m" value="<?=($time%60)?>" style="display:none; width:20px;" /> �

			<input type="hidden" name="garant_time_sort" value="<?=($counter * 100 + 99)?>" />
			<?endif?>
		</td>
		<td><?=intval($GLOBALS['TOTAL_NO_GARANT_GRADES']/60)?> � <?=$GLOBALS['TOTAL_NO_GARANT_GRADES']%60?> �</td>
		<td></td>
		<td>
			<?if($arTask["UF_TASK_GARANT"] == 0):?>
			<span class="nogarant_summ" data="nogarant_summ" onclick="showInputArea(this)">0</span>
			<input class="nogarant_summ" type="text" name="nogarant_summ" value="0" style="display:none;" />
			<?endif?>
		</td>
		<td>
		<?if($arTask["UF_TASK_GARANT"] == 0):?>
			<a href="#" onclick="calcAllPrices(); return false;" style="display:none; margin-right:8px;" title="���������� ��������� �� ��������� ����"><=</a>
		<?endif?>
		</td>
		<td></td>
		<td></td>
	</tr>
	<?else:?>
	<?printNewTasks($arResult, $counter, $colspan)?>
	<?endif?>
</table>

<div style="clear:both; margin:20px 0; position: relative;">
<?/*<input type="submit" value="�����������" name="print" id="print_b" />*/?>
<a class="button_link print" href="/itees/support_reports/reports/html.php?id=<?=$arResult['REPORT_ID']?>" target="_blank">�����������</a><span class="download_select_arrow">&nbsp;</span>
<a class="button_link download" href="/itees/support_reports/reports/pdf.php?id=<?=$arResult['REPORT_ID']?>&download">������� PDF</a><span class="download_select_arrow_2">&nbsp;</span>
<span class="download_select_menu">
<a target="_blank" href="/itees/support_reports/reports/html.php?id=<?=$arResult['REPORT_ID']?>&stamp=1">� �������</a><br>
<a target="_blank" href="/itees/support_reports/reports/html.php?id=<?=$arResult['REPORT_ID']?>">��� ������</a>
</span>
<span class="download_select_menu_2">
<a target="_blank" href="/itees/support_reports/reports/pdf.php?id=<?=$arResult['REPORT_ID']?>&download&stamp=1">� �������</a><br>
<a target="_blank" href="/itees/support_reports/reports/pdf.php?id=<?=$arResult['REPORT_ID']?>&download">��� ������</a>
</span>
<script>
	$(".download_select_arrow").click(function() {
		$(".download_select_menu").toggle();
		$(".download_select_arrow").toggleClass('arrow_bg');
	});
	$(".download_select_arrow_2").click(function() {
		$(".download_select_menu_2").toggle();
		$(".download_select_arrow_2").toggleClass('arrow_bg');
	});
</script>

<input class="input_button save report_save" type="submit" value="��������� �����" name="save" />
<?if(!$arResult['PAPER']){?>
<input class="input_button reset" type="submit" value="�������� �����" name="clear" style="float: right;<?if(!count($arResult["NEW_TASKS"])):?>display:none;<?endif?>" onclick="return confirm('��������! �������� ����������. �� �������, ��� ������ ������������ �������� ��������� ������?');" />
<?}?>
<?/*<button type="button" onclick="$('#print_b').before('<input type=\'hidden\' name=\'download\' value=\'true\' id=\'download\' />'); $('#print_b').click(); $('#download').remove()">�������</button>*/?>
</div>

</form>
<div class="itees_settings_block">
	<form method="post" action="<?=$APPLICATION->GetCurPageParam()?>" class="ajax_form" id="print_option">
		<h3>��������� ������</h3>
		<img src="<?=$GLOBALS["DETAIL_REPORT_TEMPLATE_FOLDER"]?>/images/loader.gif" alt="" class="itees_loader" />
		<ul class="panel">
			<li><label><input type="checkbox" name="price_x" <?=(($arResult['SHOW_PRICE'] == 1) || ($arResult["CHEKED_MARK"] == 'Y')) ? "checked='checked'" : ""?> onchange="$(this).parents('form').submit();" />�������� ��������� ��� ������</label></li>
			<?/*li><label><input type="checkbox" name="hour_price_x" <?=($arResult['SHOW_HOUR_PRICE'] == 1) ? "checked='checked'" : ""?> onchange="$(this).parents('form').submit();" />�������� ��������� ����</label></li*/?>
			<li class="itees_hidden"><script>//$('.itees_hidden').css('display','none')</script><input type="hidden" name="set_option" value="1" /><button type="submit">���������</button></li>
		</ul>
	</form>
	<hr>
	<form method="post" action="<?=$APPLICATION->GetCurPageParam()?>" class="ajax_form" id="report_option">
		<h3>��������� ������</h3><img src="<?=$GLOBALS["DETAIL_REPORT_TEMPLATE_FOLDER"]?>/images/loader.gif" alt="" class="itees_loader" />
		<ul class="panel">
			<li style="margin: 10px 5px"> ���� ������: <div style="">
<?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "",
     "INPUT_NAME" => "report_date",
     "INPUT_VALUE" => $arResult['REPORT_DATE'],
     "SHOW_TIME" => "Y",
     "HIDE_TIMEBAR" => "Y"
	)
);?></div>
			</li>
			<?/*li>
				<input type="hidden" name="report_option" value="1" />
				<label><input onchange="$('button', $(this).parents('form')).click()" type="checkbox" name="zero_time" value="��" <?=($arResult['SHOW_ZERO_TIME_TASKS']  == '��') ? "checked='checked'" : ""?> />�������� ������ � ������� ��������</label>
			</li*/?>
			<li class="itees_hidden"><script>$('.itees_hidden').css('display','none')</script><button type="submit" name="report_option">���������</button></li>
		</ul>
	</form>
		
		<div class="itees_crm_block">
			<div class="itees_crm_block-line"></div>
			<h3>CRM</h3>
			<p>
				������:<br />
				<?=$arResult['CLIENT_URL']?>
			</p>
			<p>
				������:<br />
				<?if (count($arResult['DEALS']) > 0):?>
				<ul>
				<?foreach($arResult['DEALS'] as $arDeal):?>
					<li>
						<a href="/crm/deal/show/<?=$arDeal['ID'];?>/" target="_blank"><?=$arDeal['TITLE'];?></a> - <?=$arDeal['OPPORTUNITY'];?> ���.
					</li>
				<?endforeach;?>
				</ul>
				<?else:?>
					<p style="font-size:12px;color:#777">�� ������� ������ ������ �� �������</p>
				<?endif;?>
			</p>
		</div>
</div>



<script>
	var hideInputAreaBool = '<?=$arResult['CHECKLIST_CHECKED']?>';
	var reportPassClient = '<?=$arResult['REPORT_PASS_CLIENT_START']?>';
	var reportCreator = '<?=$arResult['REPORT_CREATOR']?>';
	
	function custom_handlers() {
		//$('input[name=report_date], input[name=order_id], input[name=act], input[name=payed], input[name=paper], input[name=paper_returns], input[name=paper_date]').change(function() {
		$('input[name=report_date], input[name=order_id], input[name=act]').change(function() {
			$(this).parents('form').submit();
		});
		$('input[name=paper]').change(function() {
			if(!confirm("�������� ������ �������� ��������� ����� ����������. �� �������, ��� ������ ���������� ������ ��������?")) {
				$('input[name=paper]').prop('checked', false);
				return false;
			}
		});
	}
	
	$(function() {
		
		setInputAreaBool(hideInputAreaBool);
		startReportPassClient(reportPassClient);
		
		custom_handlers();
		$('.ajax_form').submit(function() {
			var form = this;
			$('.itees_loader', this).show();
			var url = $(this).attr('action');
			var id = $(this).attr('id');

			$.post(url, $('#'+id).serialize(), function(data) {
				
				if($(form).hasClass('invoice_update'))
					var inputs = $('input[name^=invoice_id]');
				
				if(!($(form).hasClass('no-update'))) {
					$('#'+id).html($('#'+id, data).html());
				}
				else
					$('.itees_loader', this).hide();

				if(!($(form).hasClass('no-reload'))) {
					$('#table_form').html($('#table_form', data).html());
				}
				$('.itees_hidden').hide();

				setInputAreaBool($('#CHECKLIST_CHECKED', data).val());
				
				if(id == 'report_pass_client')
				{
					$('#1c').html($('#1c', data).html());
					setReportPassClient($('#id_report_pass_client', data).val());
				}
				
				if(id == 'checklist')
					$('#report_pass_client').html($('#report_pass_client', data).html());
			
				if($(form).hasClass('invoice_update'))
					SetCrmInvoicesSetVals(invoices, inputs);
			
				custom_handlers();
				setHandlers();
			});

			return false;
		});
	});
</script>

<div class="itees_settings_block">
	<h3>�����������</h3>
	<form method="post" action="<?=$APPLICATION->GetCurPageParam()?>" class="ajax_form no-reload" id="1c">
		<img src="<?=$GLOBALS["DETAIL_REPORT_TEMPLATE_FOLDER"]?>/images/loader.gif" alt="" class="itees_loader" />
		<ul>
			<li><span style="display:inline-block; width:125px;">����� ������ � 1�:</span><br><input type="text" name="order_id" value="<?=$arResult['ORDER_ID']?>" /></li>
			<?if(!empty($arResult['INVOICE_ID'])){?>
				<li><a href="/crm/invoice/show/<?=$arResult['INVOICE_ID']?>/" target="blank"><?=$arResult['INVOICE_ACCOUNT_NUMBER']?></a></li>
			<?}?>
			<li><span style="display:inline-block; width:125px;">����� ���� � 1�:</span><br><input type="text" name="act" value="<?=$arResult['ACT']?>" /></li>
			<?if($arResult['DELIVERY_DOC_NUM']){?><li><div>����� ��������� ����������: <?=$arResult['DELIVERY_DOC_NUM'] ? $arResult['DELIVERY_DOC_NUM'] : '�� ��������'?></div></li><?}?>
		</ul>
		<div class="itees_hidden"><script>$('.itees_hidden').css('display','none')</script><button type="submit">����������</button></div>
	</form>
	<hr>
		<h3></h3>
	<form method="post" action="<?=$APPLICATION->GetCurPageParam()?>" class="ajax_form no-reload invoice_update" id="client_work">
		<input type="hidden" name="client_work" value="1" />
		<img src="<?=$GLOBALS["DETAIL_REPORT_TEMPLATE_FOLDER"]?>/images/loader.gif" alt="" class="itees_loader" />
		<ul>
		
<?
global $USER;
if($USER->GetID() == 502 || $USER->GetID() == 1151 || $USER->GetID() == 478)
{

	if(!empty($arResult['INVOICE']))
	{
	?>
		<li>
			<label>
				<input
					id="prepayment"
					type="checkbox"
					name="prepayment"
					onChange="TogglePrepayment();"
					<?if($arResult['PREPAYMENT']){?>checked="checked"<?}?>
					> ���� ����������
			</label>
		</li>
		<li>
			<div id="invoices_select" class="itees_task_crm_select_client <?if(empty($arResult['PREPAYMENT'])){?>hide<?}?>" style="margin:10px 0;">
				<div style="background:#eaeff0; padding:10px; border-radius:5px;">
				<table cellpadding="0" cellspacing="0" class="field_crm" id="selected-clients">
					<tr>
						<td class="field_crm_entity">
						</td>
					</tr>
				</table>
				<div class="itees_task_crm_button_open">
					<a class="itees_task_crm_client_link" id="itees_task_crm_client_link" href="#"><?=GetMessage('SUPPORT_ADD')?></a>
				</div>
				</div>
			</div>
			<div id="sum_paid">
				<?if(!empty($arResult['INVOICES'])){?>
					����� ������ �� ����������:<br> <?=CurrencyFormat($arResult['INVOICE_ALL_PAID_SUM'], "RUB");?>,<br> �� ��� �������� <?=CurrencyFormat($arResult['INVOICE_PAID_SUM'], "RUB");?>
				<?}?>
			</div>
			<script type="text/javascript">
				var invoices = <?=CUtil::PhpToJsObject($arResult['INVOICE'])?>;
				var inv_price = <?=CUtil::PhpToJsObject($arResult['JS_INV_PRICE'])?>;

				SetCrmInvoices(invoices, inv_price);
				
				$(document).ready(function()
				{
					if(typeof(obCrm) == 'object')
					{
						if(obCrm['itees_task_crm_client_link'].onSaveListeners.length == 0)
						{
							obCrm['itees_task_crm_client_link'].AddOnSaveListener(function(arElements) {
								$('#selected-clients').remove();
								InvoicesSumFormate(arElements, inv_price);
							});
						}
					}

					$('.itees_task_crm_client_link').click(function()
					{
						if(typeof(obCrm) == 'object') {
							obCrm[this.id].Open();
							
							if(obCrm[this.id].onSaveListeners.length == 0)
							{
								obCrm[this.id].AddOnSaveListener(function(arElements) {
									$('#selected-clients').remove();
									InvoicesSumFormate(arElements, inv_price);
								});
							}
						}
						return false;
					});

				});
			</script>
		</li>
		<li><hr></li>
	<?
	}
}
?>
			<li><label><input type="checkbox" name="payed" <?if($arResult['PAYED']){?>checked="checked"<?}?>> ������� ���������</label></li>
			<li><hr></li>
			<li><label><input type="checkbox" name="paper" <?if($arResult['PAPER']){?>checked="checked" disabled="disabled"<?}?>> ��������� �������� ��������</label></li>
			<li style="margin: 10px 5px"> ���� �������� ���������: <div style="">
<?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "",
     "INPUT_NAME" => "paper_date",
     "INPUT_VALUE" => $arResult['PAPER_DATE'],
     "SHOW_TIME" => "Y",
     "HIDE_TIMEBAR" => "Y"
	)
);?></div>
			</li>
			<li>
				<label>
					<input type="checkbox" name="paper_returns" <?if($arResult['PAPER_RETURNS']){?>checked="checked"<?}?>> �������� �������� ��������
				</label>
			</li>
			<li id="track_number_link_li">
				<label>
					���� �����:<br>
					<input
						type="text"
						name="track_number"
						value="<?=$arResult['TRACK_NUMBER']?>"
						onInput="CheckTrackNumber($('#track_number_link_li input').val());"
						onFocusout="CheckTrackNumber($('#track_number_link_li input').val());"
					>
					<div class="track_number_error">������ �����, 14 ������</div>
					<div>
						<?if(!empty($arResult['TRACK_NUMBER'])){?>
							<a href="https://www.pochta.ru/tracking#<?=$arResult['TRACK_NUMBER']?>" target="_blank">���������� ������ �� ����� �����</a>
						<?}?>
					</div>
				</label>
			</li>
			<li><hr></li>
			<li id="send_ado_li">
				<label>
					<input
						type="checkbox"
						name="send_ado"
						<?if($arResult['SEND_ADO']){?>checked="checked"<?}?>
						onChange="ToggleDate();"
						> ��������� �� ���
				</label>
			</li>
			<li id="send_ado_date_li" class="<?if(empty($arResult['SEND_ADO'])){?>hide<?}?>"> ���� �������� �� ���:
				<div>
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.calendar",
						"",
						Array(
							"SHOW_INPUT" => "Y",
							"FORM_NAME" => "",
							"INPUT_NAME" => "send_ado_date",
							"INPUT_VALUE" => $arResult['SEND_ADO_DATE'],
							"SHOW_TIME" => "Y",
							"HIDE_TIMEBAR" => "Y"
						)
					);?>
				</div>
			</li>
		</ul>
		<?/*<div class="itees_hidden"><script>//$('.itees_hidden').css('display','none')</script>*/?><?/*</div>*/?>
			<button class="input_button save w_100" type="submit">���������</button>
	</form>
	
	<?
	if(!empty($arResult['REPORT_PASS_CLIENT_SHOW']))	
	{
	?>
		<hr>
		<h3>����� �������</h3>
		<br>
		<?if(empty($arResult['REPORT_GARANT'])){?>
			<form method="post" action="<?=$APPLICATION->GetCurPageParam()?>" class="ajax_form no-reload" id="report_pass_client">
				<input id="id_report_pass_client" type="hidden" name="report_pass_client" value="<?=$arResult['REPORT_PASS_CLIENT']?>" />
					<?if($arResult['REPORT_PASS_CLIENT']){?>
						<span>��������� ��������������<br>������ ����������</span>
					<?}else{?>
						<span class="c_red">����� ������������ ���<br>�������������� �����������</span>
					<?}?>
					<br><br>
					<input
						class="input_button pass_client w_100 <?if(!$arResult['REPORT_PASS_CLIENT_DISABLED'] && !$arResult['REPORT_PASS_CLIENT']){?>lock<?}?>"
						type="submit"
						value="<?=$arResult['REPORT_PASS_CLIENT_NAME']?>"
						name="save"
						<?if(!$arResult['CHECKLIST_CHECKED']){?>disabled="disabled"<?}elseif($arResult['REPORT_PASS_CLIENT_DISABLED']){?>disabled="disabled"<?}?>
					>
			</form>
			<?if($arResult['REPORT_PASS_CLIENT_DISABLED']){?><br><span>��� �������������� ������<br>���������� � ������������.</span><?}?>
		<?}else{?>
			� ������ ������ ����������� ������.<br>
			�� ���� �� ��������� ��������� �����.
		<?}?>
	<?
	}
	?>
	
</div>

<?if(!empty($arResult['CHECKLIST_RIGHTS']) && $arResult['CHECKLIST_RIGHTS'] != 'D' && !empty($arResult['CHECKLIST'])){?>
	<div class="itees_settings_block checklist">
		<h3>��������</h3>
		<?if($arResult['CHECKLIST_RIGHTS'] == 'W'){?>
			<form method="post" action="<?=$APPLICATION->GetCurPageParam()?>" class="ajax_form no-reload" id="checklist">
				<input type="hidden" name="check" value="1" />
				<img src="<?=$GLOBALS["DETAIL_REPORT_TEMPLATE_FOLDER"]?>/images/loader.gif" alt="" class="itees_loader" />
		<?}else{
			$disabled = 'disabled';
		}
		if($arResult['CHECKLIST_CHECKED'])
		{?>
			<div class="report_check">����� ��������</div>
			<span class="checklist_show" onclick="showChecklist();">�������� ������ ���-�����</span>
			<?
			$hide = 'hide';
		}?>
			<input type="hidden" name="CHECKLIST_CHECKED" id="CHECKLIST_CHECKED" value="<?=$arResult['CHECKLIST_CHECKED']?>">
			<div class="trig <?=$hide?>">
				<ul>
					<?foreach($arResult['CHECKLIST'] as $propVal){?>
						<li>
							<label>
								<input onClick="return checklistChange(this);" type="checkbox" name="checklist[]" value='<?=$propVal['ID']?>' <?if($propVal['CHECKED']){?>checked="checked"<?}?> <?=$disabled?>><?=$propVal['VALUE']?>
								<?if(!empty($arResult['CHECKLIST_USER'][$propVal['ID']])){?>
									<br><span class="checklist_user <?if($propVal['CHECKED']){?>green<?}else{?>red<?}?>"><?=$arResult['CHECKLIST_USER'][$propVal['ID']]?></span>
								<?}?>
							</label>
							<input type="hidden" name="checklist_user[]" value='<?=$GLOBALS['USER']->GetId()?>'>
							<input type="hidden" name="checklist_user_descr[]" value='<?=$propVal['ID']?>'>
						</li>
					<?}?>
				</ul>
			</div>
		<?if($arResult['CHECKLIST_RIGHTS'] == 'W'){?>
				<button class="trig input_button save <?=$hide?>" type="submit">���������</button>
			</form>
		<?}?>
	</div>
<?}?>

	<?$APPLICATION->IncludeComponent(
		"as:reports.report.roles",
		"flat",
		Array(
			"REPORT_ID" => $arResult["ID"],
			"NAME_TEMPLATE" => empty($arParams['NAME_TEMPLATE']) ? CSite::GetNameFormat(false) : str_replace(array("#NOBR#","#/NOBR#"), array("",""), $arParams["NAME_TEMPLATE"]),
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "180",
			"CACHE_TYPE" => "A",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "AUTO",
			"PATH_TO_USER_PROFILE" => "/company/personal/user/#user_id#/"
		)
	);?>
<div style="clear:both;"></div>
<div class="report-comments">
<h3>����������</h3>
<?$APPLICATION->IncludeComponent("bitrix:forum.comments", "bitrix24", array(
		"FORUM_ID" => 37,
		"ENTITY_TYPE" => "RC",
		"ENTITY_ID" => intval($arResult['REPORT_ID']),
		"ENTITY_XML_ID" => "REPORT_".intval($arResult['REPORT_ID']),
		"POST_FIRST_MESSAGE" => "Y",
		"URL_TEMPLATES_PROFILE_VIEW" => '/company/personal/user/#user_id#/',
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600000",
		"IMAGE_HTML_SIZE" => 400,
		"MESSAGES_PER_PAGE" => 20,
		"PAGE_NAVIGATION_TEMPLATE" => "arrows",
		"DATE_TIME_FORMAT" => CDatabase::DateFormatToPHP(FORMAT_DATETIME),
		"PATH_TO_SMILE" => '/bitrix/images/forum/smile/',
		"EDITOR_CODE_DEFAULT" => "N",
		"SHOW_MODERATION" => "Y",
		"SHOW_AVATAR" => "Y",
		"SHOW_RATING" => 'N',
		"RATING_TYPE" => 'like',
		"SHOW_MINIMIZED" => "N",
		"USE_CAPTCHA" => "N",
		'PREORDER' => 'N',
		"SHOW_LINK_TO_FORUM" => "N",
		"SHOW_SUBSCRIBE" => "N",
		"FILES_COUNT" => 10,
		"SHOW_WYSIWYG_EDITOR" => "Y",
		"AUTOSAVE" => true,
		"PERMISSION" => 'M',
		"NAME_TEMPLATE" => '#LAST_NAME# #NAME#',
		"MESSAGE_COUNT" => 3,
	)
);?>
</div>
<?
/*$APPLICATION->IncludeComponent("bitrix:forum.comments", "", array(
		"FORUM_ID" => 37,
		"ENTITY_TYPE" => "s1",
		"ENTITY_ID" => intval($arResult['REPORT_ID']),
		"ENTITY_XML_ID" => "REPORT_".intval($arResult['REPORT_ID']),
		"POST_FIRST_MESSAGE" => "Y",
		//"URL_TEMPLATES_PROFILE_VIEW" => $arParams['PATH_TO_USER_PROFILE'],
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600000",
		"IMAGE_HTML_SIZE" => 400,
		"MESSAGES_PER_PAGE" => 20,
		"PAGE_NAVIGATION_TEMPLATE" => "arrows",
		"DATE_TIME_FORMAT" => CDatabase::DateFormatToPHP(FORMAT_DATETIME),
		//"PATH_TO_SMILE" => $arParams['PATH_TO_FORUM_SMILE'],
		"EDITOR_CODE_DEFAULT" => "N",
		"SHOW_MODERATION" => "Y",
		"SHOW_AVATAR" => "Y",
		"SHOW_RATING" => 'N',
		//"RATING_TYPE" => $arParams['RATING_TYPE'],
		"SHOW_MINIMIZED" => "N",
		"USE_CAPTCHA" => "N",
		'PREORDER' => 'N',
		"SHOW_LINK_TO_FORUM" => "N",
		"SHOW_SUBSCRIBE" => "N",
		"FILES_COUNT" => 10,
		"SHOW_WYSIWYG_EDITOR" => "Y",
		"AUTOSAVE" => true,
		//"PERMISSION" => $arParams['PERMISSION'],
		//"NAME_TEMPLATE" => $arParams["NAME_TEMPLATE"],
		"MESSAGE_COUNT" => 3,
	)
);*/?>
<?}?>
<?
//s($arResult["NEW_TASKS"]);
function printNewTasks(&$arResult, &$counter, $colspan)
{
	global $APPLICATION;
	foreach($arResult["NEW_TASKS"] as $k => $arNewTask):?><?//s($arNewTask,497);?>
	<?$zero_time = false;?><?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0 && $arNewTask["PROPERTY_ELAPSED_TIME_VALUE"] == 0){?><?$zero_time = true;?><?}?>
	<?if($arNewTask["NAME"] == "����� �� ����������� �������"):?>
	<tr class="total">
		<td colspan="<?=$colspan?>"><?=$arNewTask["NAME"]?>:</td>
		<td colspan="2">
			<?$total_time =$arNewTask["PROPERTY_ELAPSED_TIME_VALUE"];?>

			<span class="garant_time garant_time_h" data="garant_time_h" onclick="showInputArea(this)"><?=intval($total_time/60);?></span>
			<input class="garant_time_h" type="text" name="garant_time_h" value="<?=intval($total_time/60)?>" style="display:none; width:20px;" /> �
			<span class="garant_time garant_time_m" data="garant_time_m" onclick="showInputArea(this)"><?=($total_time%60);?></span>
			<input class="garant_time_m" type="text" name="garant_time_m" value="<?=($total_time%60)?>" style="display:none; width:20px;" /> �

			<input type="hidden" name="garant_time_sort" value="<?=($counter * 100 + 99)?>" />
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td><a href="#" class="plus" onclick="addFirstRow(this); return false;" style="font-weight:normal;">+</a></td>
	</tr>
	<?elseif($arNewTask["NAME"] == "����� �� ������������� �������"):?>
	<tr class="total">
		<td colspan="<?=$colspan?>"><?=$arNewTask["NAME"]?>:</td>
		<td colspan="2">
			<?$total_time =$arNewTask["PROPERTY_ELAPSED_TIME_VALUE"];?>
			<span class="nogarant_time nogarant_time_h" data="nogarant_time_h" onclick="showInputArea(this)"><?=intval($total_time/60);?></span>
			<input class="nogarant_time_h" type="text" name="nogarant_time_h" value="<?=intval($total_time/60)?>" style="display:none; width:20px;" /> �
			<span class="nogarant_time nogarant_time_m" data="nogarant_time_m" onclick="showInputArea(this)"><?=($total_time%60);?></span>
			<input class="nogarant_time_m" type="text" name="nogarant_time_m" value="<?=($total_time%60)?>" style="display:none; width:20px;" /> �
			<input type="hidden" name="nogarant_time_sort" value="<?=($counter * 100 + 99)?>" />
		</td>
		<td><?=intval($GLOBALS['TOTAL_NO_GARANT_GRADES']/3600)?> � <?=round($GLOBALS['TOTAL_NO_GARANT_GRADES']/60 - intval($GLOBALS['TOTAL_NO_GARANT_GRADES']/3600) * 60)?> �</td>
		<td><input type="hidden" name="summ_changed_by_hands" value="<?=($arNewTask["PROPERTY_SUMM_CHANGED_BY_HANDS_VALUE"] == '��')?'Y':'N'?>" /><input type="hidden" name="nogarant_summ_2" value="<?=$arNewTask["PROPERTY_PRICE_VALUE"]?>" /></td>
		<td>
			<span <?if($arNewTask["PROPERTY_SUMM_CHANGED_BY_HANDS_VALUE"] == '��'){?>style="color:red"<?}?> class="nogarant_summ" data="nogarant_summ" onclick="SummChangeByHandsHandler(); showInputArea(this);"><?=$arNewTask["PROPERTY_PRICE_VALUE"]?>&nbsp;</span>
			<input class="nogarant_summ" type="text" name="nogarant_summ" value="<?=$arNewTask["PROPERTY_PRICE_VALUE"]?>" style="display:none;" />
			<?if($arNewTask["PROPERTY_PRICE_VALUE"] == 0) {?>
			<script>
				$(document).ready(function() {
					$("textarea.input_price").change();
				});
			</script>
			<?}?>
		</td>
		<td>
			<a href="#" onclick="calcAllPrices(); return false;" style="display:none; margin-right:8px;" title="���������� ��������� �� ��������� ����"><=</a>
		</td>
		<td></td>
		<td></td>
	</tr>
	<?else:?>
	<?$counter++;?>
	<?if(!$zero_time){?>
	<?$not_zero_time_counter++;?>
	<?}?>
	<tr <?if($zero_time){?>style="background:#ddd;"<?}?>>
		<td class="task_id" style="text-align:center;">
		<?if(!$zero_time){?>
			�<?=$not_zero_time_counter?><?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><br /><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?endif?>
		<?}?>
			<input type="hidden" name="counter[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" value="<?=$counter?>" class="counter" />
			<?if($arResult["~"][$arNewTask["PROPERTY_TASK_ID_VALUE"]]["MINUTES"] == 0):?>
				<input type="hidden" name="hidden[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" value="Y" />
			<?else:?>
				<input type="hidden" name="hidden[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" value="N" />
			<?endif?>
			<?if($arNewTask["PROPERTY_GARANT_VALUE"] == "Y"):?>
				<input type="hidden" name="garant[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" value="Y" class="garant" />
			<?else:?>
				<input type="hidden" name="garant[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" value="N" class="garant" />
			<?endif?>
		<?if($zero_time){?>
			<span style="color:#f00; font-size:.75em;">�� � ������</span>
		<?}?>
		</td>
		<td class="task_name">
			<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?>
			<a href='/workgroups/group/<?=$arResult["~"][$arNewTask["PROPERTY_TASK_ID_VALUE"]]["GROUP_ID"]?>/tasks/task/view/<?=$arResult["~"][$arNewTask["PROPERTY_TASK_ID_VALUE"]]["ID"]?>/' onclick='if(taskIFramePopup.isLeftClick(event)) {taskIFramePopup.view(<?=$arResult["~"][$arNewTask["PROPERTY_TASK_ID_VALUE"]]["ID"]?>); return false;}'><?=$arResult["~"][$arNewTask["PROPERTY_TASK_ID_VALUE"]]["TITLE"]?></a>
			<input type="hidden" name="orig_task[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" value="<?=$arResult["~"][$arNewTask["PROPERTY_TASK_ID_VALUE"]]["TITLE"]?>" />
			<?endif?>
			<?
			if(!empty($arResult["~"][$arNewTask["PROPERTY_TASK_ID_VALUE"]]['CREATED_BY_NAME']))
			{
				?><br><span class="resp_info">�����������: <?echo $arResult["~"][$arNewTask["PROPERTY_TASK_ID_VALUE"]]['CREATED_BY_NAME'];?></span><?
			}

			if(!empty($arResult["~"][$arNewTask["PROPERTY_TASK_ID_VALUE"]]['RESPONSIBLE_ID_NAME']))
			{
				?><br><span class="resp_info">�������������: <?echo $arResult["~"][$arNewTask["PROPERTY_TASK_ID_VALUE"]]['RESPONSIBLE_ID_NAME'];?></span><?
			}
			?>
		</td>
		<td>
			<?$title = $arNewTask["PROPERTY_TASK_NAME_PRINT_VALUE"];?>
			<span class="input_task" data="input_task_<?=$counter?>" onclick="showInputArea(this, true)"><?=preg_replace("/\n/", "<br>", $title)?></span>
			<textarea class="input_task input_task_<?=$counter?>" name="task[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]"><?=$title?></textarea>
		</td>
		<?if($APPLICATION->get_cookie("show_menu") == 1):?>
		<td class="menu_td" style="position:relative;">
			
			<a href="javascript: void(0)" class="task-menu-button custom" onclick="return ShowMenuPopupCustom(this);" title="����"><i class="task-menu-button-icon"></i></a>
			<ul class="popup_menu_custom">
			<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?>
				<li class='li_delete_report'><a class='delete_report' href="javascript: void(0)" data-id="<?=$arResult["~"][$arNewTask["PROPERTY_TASK_ID_VALUE"]]["ID"]?>" data-url="<?=$GLOBALS["DETAIL_REPORT_TEMPLATE_FOLDER"]?>/ajax/delete_task_from_report.php?ID=" onclick="DeleteFromReport(this, '<?=$GLOBALS["APPLICATION"]->GetCurPageParam()?>')">������� �� ������</a><img src='/bitrix/templates/bitrix24/components/itees/simple.component/support.tasks/images/ajax-loader.gif' class="loader" style="position:absolute; right:0.5em; top:0.5em;" alt='' /></li>
			<?else:?>	
				<li class='li_delete_row'><a class='delete_row' href="javascript: void(0)" data-id="<?=$arNewTask["ID"]?>" data-url="<?=$GLOBALS["DETAIL_REPORT_TEMPLATE_FOLDER"]?>/ajax/delete_task_from_print_form.php?ID=" onclick="DeleteFromPrintForm(this, '<?=$GLOBALS["APPLICATION"]->GetCurPageParam()?>')">������� ������</a><img src='/bitrix/templates/bitrix24/components/itees/simple.component/support.tasks/images/ajax-loader.gif' class="loader" style="position:absolute; right:0.5em; top:0.5em;" alt='' /></li>
			<?endif?>
			<?if($arNewTask["PROPERTY_GARANT_VALUE"] != 'Y'){?>
				<li class='li_garant'><a class='garant' href="javascript: void(0)" data-id="<?=$arNewTask["ID"]?>" data-url="<?=$GLOBALS["DETAIL_REPORT_TEMPLATE_FOLDER"]?>/ajax/garant.php?ID=" onclick="Garant(this, '<?=$GLOBALS["APPLICATION"]->GetCurPageParam()?>')">������� �����������</a><img src='/bitrix/templates/bitrix24/components/itees/simple.component/support.tasks/images/ajax-loader.gif' class="loader" style="position:absolute; right:0.5em; top:0.5em;" alt='' /></li>
			<?}else{?>
				<li class='li_nogarant'><a class='nogarant' href="javascript: void(0)" data-id="<?=$arNewTask["ID"]?>" data-url="<?=$GLOBALS["DETAIL_REPORT_TEMPLATE_FOLDER"]?>/ajax/nogarant.php?ID=" onclick="Garant(this, '<?=$GLOBALS["APPLICATION"]->GetCurPageParam()?>')">������� �������������</a><img src='/bitrix/templates/bitrix24/components/itees/simple.component/support.tasks/images/ajax-loader.gif' class="loader" style="position:absolute; right:0.5em; top:0.5em;" alt='' /></li>
			<?}?>
			<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?>
				<li><a class='download_xls_timereport' href="/dev/time_report.php?id=<?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?>">������� ��������� �����</a></li>
			<?endif?>
			</ul>
			
		</td>
		<?endif?>
		<td class="nowrap">
			<?$duration = $arNewTask["PROPERTY_ELAPSED_TIME_VALUE"];?>
			<span class="input_time time_h" data="time_h_<?=$counter?>" onclick="showInputArea(this)"><?=intval($duration/60)?>&nbsp;</span>
			<textarea class="input_time time_h time_h_<?=$counter?>" name="time_h[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" style="display:none"><?=intval($duration/60)?></textarea> �
			<?
				$modulo = ($duration%60)%5;
				$correction = 0;
				/*if($modulo < 2.5)
				{
					$correction = -$modulo;
				}
				else*/
				{
					//if($modulo > 0)
					//	$correction = -$modulo + 5;
				}
			?>
			<span class="input_time time_m" data="time_m_<?=$counter?>" onclick="showInputArea(this)"><?=($duration%60 + $correction)?>&nbsp;</span>
			<textarea class="input_time time_m time_m_<?=$counter?>" name="time_m[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" style="display:none"><?=($duration%60 + $correction)?></textarea>�
			<?$duration_for_summ_calculation = intval($duration/60)*60 + $duration%60 + $correction;?>
		</td>
		<td class="estimate_time nowrap">
			<?$TIME_ESTIMATE = $arResult['~'][$arNewTask['PROPERTY_TASK_ID_VALUE']]['TIME_ESTIMATE'];
			if($TIME_ESTIMATE == 0 && $arNewTask['PROPERTY_ESTIMATE_TIME_VALUE'] > 0) {$TIME_ESTIMATE = $arNewTask['PROPERTY_ESTIMATE_TIME_VALUE']*60;}?>
			<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] == 0){?>
				<span class="input_time time_m" data="time_he_<?=$counter?>" onclick="showInputArea(this)"><?=intval($TIME_ESTIMATE/3600)?>&nbsp;</span>
				<textarea class="time_input estimate_time<?=$counter?> time_m time_he_<?=$counter?>" name="estimate_time_h[n_<?=$arNewTask["ID"]?>]" style="display:none"><?=intval($TIME_ESTIMATE/3600)?></textarea> �
				<span class="input_time time_m" data="time_me_<?=$counter?>" onclick="showInputArea(this)"><?=round($TIME_ESTIMATE/60 - intval($TIME_ESTIMATE/3600) * 60)?>&nbsp;</span>
				<textarea class="time_input estimate_time<?=$counter?> time_h time_m time_me_<?=$counter?>" name="estimate_time_m[n_<?=$arNewTask["ID"]?>]" style="display:none"><?=round($TIME_ESTIMATE/60 - intval($TIME_ESTIMATE/3600) * 60)?></textarea> �
			<?}else{?>
				<?=intval($TIME_ESTIMATE/3600)?> � <?=round($TIME_ESTIMATE/60 - intval($TIME_ESTIMATE/3600) * 60)?> �
			<?}?>
		</td>
		<td class="grades_time nowrap">
			<?
			$TIME_GRADES = $arNewTask["ELAPSED_TIME_GRADES"];
			if($TIME_GRADES == 0 && $arNewTask['PROPERTY_GRADE_TIME_VALUE'] > 0) {$TIME_GRADES = $arNewTask['PROPERTY_GRADE_TIME_VALUE']*60;}
			if($arNewTask["PROPERTY_GARANT_VALUE"] == 'N')
			{
				$GLOBALS['TOTAL_NO_GARANT_GRADES'] += $TIME_GRADES;
			}
			?>
			<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] == 0){?>
				<span class="input_time time_m" data="time_hg_<?=$counter?>" onclick="showInputArea(this)"><?=intval($TIME_GRADES/3600)?>&nbsp;</span>
				<textarea class="time_input grade_time<?=$counter?> time_m time_hg_<?=$counter?>" name="grade_time_h[n_<?=$arNewTask["ID"]?>]" style="display:none"><?=intval($TIME_GRADES/3600)?></textarea> �
				<span class="input_time time_m" data="time_mg_<?=$counter?>" onclick="showInputArea(this)"><?=round($TIME_GRADES/60 - intval($TIME_GRADES/3600) * 60)?>&nbsp;</span>
				<textarea class="time_input grade_time<?=$counter?> time_h time_m time_mg_<?=$counter?>" name="grade_time_m[n_<?=$arNewTask["ID"]?>]" style="display:none"><?=round($TIME_GRADES/60 - intval($TIME_GRADES/3600) * 60)?></textarea> �
			<?}else{?>
				<?=intval($arNewTask["ELAPSED_TIME_GRADES"]/3600)?> � <?=round($arNewTask["ELAPSED_TIME_GRADES"]/60 - intval($arNewTask["ELAPSED_TIME_GRADES"]/3600) * 60)?> �
			<?}?>
		</td>
		<td class="duration nowrap">
			<?$TIME_REAL = $arNewTask["ELAPSED_TIME"];
			if($TIME_REAL == 0 && $arNewTask['PROPERTY_REAL_TIME_VALUE'] > 0) {$TIME_REAL = $arNewTask['PROPERTY_REAL_TIME_VALUE']*60;}?>
			<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] == 0){?>
				<span class="input_time time_m" data="time_hr_<?=$counter?>" onclick="showInputArea(this)"><?=intval($TIME_REAL/3600)?>&nbsp;</span>
				<textarea class="time_input real_time<?=$counter?> time_m time_hr_<?=$counter?>" name="real_time_h[n_<?=$arNewTask["ID"]?>]" style="display:none"><?=intval($TIME_REAL/3600)?></textarea> �
				<span class="input_time time_m" data="time_mr_<?=$counter?>" onclick="showInputArea(this)"><?=round($TIME_REAL/60 - intval($TIME_REAL/3600) * 60)?>&nbsp;</span>
				<textarea class="time_input real_time<?=$counter?> time_h time_m time_mr_<?=$counter?>" name="real_time_m[n_<?=$arNewTask["ID"]?>]" style="display:none"><?=round($TIME_REAL/60 - intval($TIME_REAL/3600) * 60)?></textarea> �
			<?}else{?>
				<?=intval($arNewTask["ELAPSED_TIME"]/3600)?> � <?=round($arNewTask["ELAPSED_TIME"]/60 - intval($arNewTask["ELAPSED_TIME"]/3600) * 60)?> �
			<?}?>
		
		</td>
		<?if($zero_time and false){?>
		<td colspan="2">
			<span style="color:#f00; font-size:.75em;">��� ���������� ������ � ����� ������� �� ��� �����</span>
			<?$price = $arNewTask["PROPERTY_PRICE_VALUE"];?>
			<textarea style="display:none;" class="input_price input_total_price input_total_price_<?=$counter?>" name="price[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" style="display:none"><?=$price?></textarea>
			<textarea style="display:none;" class="input_price input_hour_price input_hour_price_<?=$counter?>" name="hour_price[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" style="display:none"><?=$hour_price?></textarea>
		</td>
		<?}else{?>
		<td class="centered" style="position:relative;">
			<?if($zero_time){?>
			<div class="zero-time-alert zero-time-alert-<?=$counter?>" style="position:absolute; width:100px; height:30px; top:0; left:0; background:#ddd; display:none;">
				<script>
					$(document).ready(function(){
						var $zero_time_alert_block_<?=$counter?> = $('.zero-time-alert-<?=$counter?>');
						var height = $zero_time_alert_block_<?=$counter?>.closest('td').height();
						var width = $zero_time_alert_block_<?=$counter?>.closest('td').width() + $zero_time_alert_block_<?=$counter?>.closest('td').next().width();
						$zero_time_alert_block_<?=$counter?>.height(height + 5);
						$zero_time_alert_block_<?=$counter?>.width(width);
						$zero_time_alert_block_<?=$counter?>.click(function() {return false;});
						$zero_time_alert_block_<?=$counter?>.show();
						
						var $zero_time_alert_span_<?=$counter?> = $('span', $zero_time_alert_block_<?=$counter?>);
						$zero_time_alert_span_<?=$counter?>.css('display', 'block');
						$zero_time_alert_span_<?=$counter?>.css('padding', ((height - $zero_time_alert_span_<?=$counter?>.height())/2)+'px '+'0');
					});
				</script>
				<span style="color:#f00; font-size:.75em;">��� ���������� ������ � ����� ������� �� ��� �����</span>
			</div>
			<?}?>
			<?$price = $arNewTask["PROPERTY_PRICE_VALUE"];?>
			<?/*if($price == 0){
				$view_price = $arNewTask["PROPERTY_HOUR_PRICE_VALUE"] * $duration_for_summ_calculation/60;
			}*/?>
			<span class="input_price input_total_price" data="input_total_price_<?=$counter?>" onclick="showInputArea(this)"><?if($price == 0){?><?=$view_price?><?}else{?><?=$price?><?}?>&nbsp;</span>
			<textarea class="input_price input_total_price input_total_price_<?=$counter?>" name="price[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" style="display:none"><?=$price?></textarea>
		</td>
		<td>
			<?$hour_price = $arNewTask["PROPERTY_HOUR_PRICE_VALUE"];?>
			<a class="calcprice" href="#" onclick="calcPrice(this); return false;" style="display:inline-block; margin-right:8px;"><-</a>
			<div style="display:inline-block; width:3em; text-align:center">
			<span class="input_price input_hour_price" data="input_hour_price_<?=$counter?>" onclick="showInputArea(this)"><?=$hour_price?>&nbsp;</span>
			<textarea class="input_price input_hour_price input_hour_price_<?=$counter?>" name="hour_price[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" style="display:none"><?=$hour_price?></textarea>
			</div>
			<a class="equally" href="#" onclick="forAll(this); return false;" title="��������� ��� ����">=</a>
		</td>
		<?}?>
		<td>
			<?$comment = $arNewTask["PROPERTY_COMMENT_VALUE"];?>
			<span class="input_comment" data="input_comment_<?=$counter?>" onclick="showInputArea(this, true)"><?=preg_replace("/\n/", "<br>", $comment)?>&nbsp;</span>
			<textarea class="input_comment input_comment_<?=$counter?>" name="comment[<?if($arNewTask["PROPERTY_TASK_ID_VALUE"] > 0):?><?=$arNewTask["PROPERTY_TASK_ID_VALUE"]?><?else:?>n_<?=$arNewTask["ID"]?><?endif?>]" style="display:none"><?=$comment?></textarea>
		</td>
		<td>
			<a href="#" class="plus" onclick="addRow(this); return false;">+</a>
		</td>
	</tr>
	<?endif?>
	<?endforeach;
}
?>