<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(intval($arParams["IBLOCK_ID"])){
	//	��� ��������� ������ "�������� �������", ���� � ���������� ������������ ��� ���������
	CModule::IncludeModule('iblock'); 

	foreach($arResult["ITEMS"] as $value){

		$arButtons = CIBlock::GetPanelButtons(
			$value["IBLOCK_ID"], 
			$value["ID"], 
			$value["IBLOCK_SECTION_ID"], 
			Array(
				"RETURN_URL" => $arReturnUrl, 
				"SECTION_BUTTONS" => false, 
			) 
		); 
		unset($arButtons["configure"]["edit_element"]);		//������� ������ "�������������" 
		unset($arButtons["configure"]["delete_element"]);	//������� ������ "�������" 
	} 

	if ($APPLICATION->GetShowIncludeAreas() )
	{
		$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
	}
}
?>