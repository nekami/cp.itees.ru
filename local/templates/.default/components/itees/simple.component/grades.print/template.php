<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle($arResult['USER']['LAST_NAME']." ".$arResult['USER']['NAME'].": �����");
?>
<?if(strlen($arResult['SORY']) > 0):?>
	<p>
		<a href="/company/personal/user/<?=$arResult['USER']['ID']?>/">
			<?=$arResult['USER']['LAST_NAME'];?> <?=$arResult['USER']['NAME'];?>
		</a>
	</p>
	<div><?=$arResult['SORY']?></div>
<?else:?>
<script type="text/javascript" src="/bitrix/js/main/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var saved = $("#save_comment");
	var user_id = <?=$arResult['USER']['ID'];?>;
	$('input[type=submit]').click(function(event){
		var textarea = $("textarea[name=faq]");
	//	alert(textarea.val());
		$.post('/dev/ajax/grade_notes.php', {"what_is_missing": textarea.val(), "user_id": user_id}, function(data){
	// alert(data);
			textarea.html(data);
			saved.css("display","inline")
		});
		return false;
	});
});
</script>
<div id="grades_wrapper">
	<?$APPLICATION->IncludeComponent(
		"itees:grades.change",
		"user",
		Array(
			"CACHE_GROUPS" => "N",
			"CACHE_TIME" => "3600",
			"CACHE_TYPE" => "A",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "AUTO",
			"IBLOCK_GRADE_HISTORY_ID" => "171",
			"IBLOCK_GRADE_ID" => "66",
			"IBLOCK_TYPE" => "grades",
			"USER_ID" => $arResult["USER"]["ID"]
		)
	);?>
<pre><?/*print_r($arResult)*/?></pre>
	<table cellspacing="0">
		<?if(strlen($arResult['USER']['UF_BITRIX_PROFILE']) > 0 || strlen($arResult['USER']['UF_CERTIF_PROFILE']) > 0):?>
			<tr>
				<td class="title-cols">
					������ ����������:
				</td>
				<td>
					<?if(strlen($arResult['USER']['UF_BITRIX_PROFILE'])):?>
						<a target="_blank" href="<?=$arResult['USER']['UF_BITRIX_PROFILE']?>">dev.1c-bitrix.ru</a>
					<?endif;?>
						<br />
					<?if(strlen($arResult['USER']['UF_CERTIF_PROFILE'])):?>
						<a target="_blank" href="<?=$arResult['USER']['UF_CERTIF_PROFILE']?>">certifications.ru</a>
					<?endif;?>
				
				</td>
			</tr>
		<?endif;?>
		<tr>
			<td class="main-title-cols" colspan="2">������� ���������</td>
		</tr>
		<tr>
			<td class="title-cols">�����:</td>
			<td><?=$arResult['CURRENT_GRADE']['NAME'];?></td>
		</tr>
		<tr>
			<td class="title-cols">����������� ��������� ����:</td>
			<td><?=$arResult['CURRENT_GRADE']['PROPERTY_RATE_CHANGE_HOURS_VALUE'];?>%</td>
		</tr>
		<tr>
			<td class="title-cols">�����:</td>
			<td><?=$arResult['CURRENT_GRADE']['PROPERTY_RATE_SALARY_VALUE'] * $arResult['BASE_SALARY'];?> RUB</td>
		</tr>
		<?if (!empty($arResult['USER']['UF_GRADE_DATE_LAST'])):?>
			<tr>
				<td class="title-cols">�������:</td>
				<td><?=ConvertTimeStamp(MakeTimeStamp($arResult['USER']['UF_GRADE_DATE_LAST']),"SHORT");?></td>
			</tr>
		<?endif;?>
		<tr>
			<td class="title-cols">��������:</td>
			<td><?=$arResult['CURRENT_GRADE']['DETAIL_TEXT'];?></td>
		</tr>
			<?if(strlen($arResult['NEXT_GRADE']['NAME']) > 0):?>
			<tr>
				<td class="main-title-cols" colspan="2">��������� ���������</td>
			</tr>
			<tr>
				<td class="title-cols">�����:</td>
				<td><?=$arResult['NEXT_GRADE']['NAME'];?></td>
			</tr>
			<tr>
				<td class="title-cols">����������� ��������� ����:</td>
				<td><?=$arResult['NEXT_GRADE']['PROPERTY_RATE_CHANGE_HOURS_VALUE'];?>%</td>
			</tr>
			<tr>
				<td class="title-cols">��������:</td>
				<td><?=$arResult['NEXT_GRADE']['DETAIL_TEXT'];?></td>
			</tr>
			<tr>
				<td class="title-cols">����������� ������:</td>
				<td><?=$arResult['NEXT_GRADE']['~PROPERTY_SKILLS_VALUE']['TEXT'];?></td>
			</tr>
			<tr>
				<td class="title-cols">����������</br> ��� �������� �� �������:</td>
				<td><?=$arResult['NEXT_GRADE']['~PROPERTY_LEVEL_MOVE_NEEDS_VALUE']['TEXT'];?></td>
			</tr>
			<tr>
				<td class="title-cols">���������� �������:</td>
				<td>
					
						<?if(is_array($arResult['USER_CERTIFICATES'])):?>
						<ul>
						
						<?foreach($arResult['USER_CERTIFICATES'] as $certificate):?>
							<li><a target="_blank" href='<?=$certificate['PROPERTY_LINK_VALUE']?>'><?=$certificate['NAME']?></a>; </li>
						<?endforeach?>
						</ul>
						<?else:?>
							<?=$arResult['USER_CERTIFICATES'];?>
						<?endif;?>
					
				</td>
			</tr>
		<?endif;?>
		<?if(strlen($arResult['USER']['UF_WHAT_IS_MISSING']) > 0 or ($USER->GetID() == $arResult['HEAD']['UF_HEAD'] || $USER->GetID() == $arResult['HEAD_HEAD']['UF_HEAD'])):?>
			<tr>
				<td class="title-cols">���������� ������������:</td>
				<td>
			<?if($USER->GetID() != $arResult['USER']['ID'] and($USER->GetID() == $arResult['HEAD']['UF_HEAD'] || $USER->GetID() == $arResult['HEAD_HEAD']['UF_HEAD'])):?>
				<form action="/dev/ajax/grade_notes.php" method="POST">
					<textarea name='faq' cols='50' rows='5'><?=$arResult['USER']['UF_WHAT_IS_MISSING'];?></textarea>
					<p><input type="submit" value="���������" />&nbsp;<span id='save_comment'>���������</span></p>
				</form>
			<?else:?>
					<?=$arResult['USER']['UF_WHAT_IS_MISSING'];?>
			<?endif;?>
				</td>
			</tr>
		<?endif;?>
	</table>
</div>
<?endif;?>

