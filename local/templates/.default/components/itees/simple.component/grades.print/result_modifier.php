<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $USER;
error_reporting(E_ERROR);
$arResult['MY_PAGE'] = "/company/personal/user/".$USER->GetID()."/";
CModule::IncludeModule("iblock");

$userCertificates = array();

$arFilter = array("ID" => intval($_GET['user_id']));
$arParams["SELECT"] = array(
		"UF_GRADE_ID",
		"UF_CERTIFICATES",
		"UF_WHAT_IS_MISSING",
		"UF_GRADE_DATE_LAST",
		"UF_DEPARTMENT",
		"UF_BITRIX_PROFILE",
		"UF_CERTIF_PROFILE"
	);
//�������� �������� ������������
$arRes = CUser::GetList($by, $desc, $arFilter, $arParams);
$GRADE_ID = false;
if($arUserProp = $arRes->GetNext(true,false))
{
		$GRADE_ID = $arUserProp["UF_GRADE_ID"];
}
$arResult['USER'] = $arUserProp;
$countDepatment = count($arUserProp["UF_DEPARTMENT"]);

//�������� �������� �������������
$ibSectId = $arUserProp["UF_DEPARTMENT"][$countDepatment-1];

$department = SingleDepartment::getDepartmentArProperty($ibSectId);
$arResult['HEAD'] = $department->depProp;

if($arResult['HEAD']['UF_USE_GRADES'] > 0)
{
	// �������� ������������ ������������ ))
	if(intval($arResult['HEAD']['IBLOCK_SECTION_ID'] > 1))
	{
		$ibSectRes2 = CIBlockSection::GetList(
			array("ID"=>"ASC"),
			array(
				"ID"=>intval($arResult['HEAD']['IBLOCK_SECTION_ID']),
				"IBLOCK_ID"=>5
			),
			false,
			array(
				"IBLOCK_SECTION_ID",
				"UF_HEAD"
			),
			false
		);
		if($head2 = $ibSectRes2->GetNext(true,false))
		{
			$arResult['HEAD_HEAD'] = $head2;
		}
		
	}
	
	//��������� ������� �� ������� ������� ��������� �������
	if (GetUserGrade($_GET['user_id'], date('d.m.Y',time())))
	{
		$GRADE_ID = GetUserGrade($_GET['user_id'], date('d.m.Y',time()));
		
		$date2 = ConvertDateTime(date('d.m.Y',time()), "YYYY-MM-DD");
		
		$grade_date = CIBlockElement::GetList(
			Array("PROPERTY_DATE" => "DESC"),
			Array("IBLOCK_CODE" => "user_grade", "<=PROPERTY_DATE" => $date2, "PROPERTY_USER" => $_GET['user_id'], "ACTIVE" => "Y"),
			false,
			false,
			Array("ID", "PROPERTY_DATE", "PROPERTY_RATE_CHANGE_HOURS")
		);
		while($arRes = $grade_date->GetNext())
		{
			$arResult['USER']['RATE_CHANGE_HOURS'] = $arRes['PROPERTY_RATE_CHANGE_HOURS_VALUE'];
			$arResult['USER']['UF_GRADE_DATE_LAST'] = $arRes['PROPERTY_DATE_VALUE'];
		}
	}

	if(strlen($GRADE_ID) > 0)
	{	
		$arResult['GradeID'] = intval($GRADE_ID);
		//�������� ������� �����
		$arFilter = array(
			"ID" =>intval($GRADE_ID),
			"IBLOCK_ID" => Config::$GRADE_IBLOCK_ID
		);

		$db_elements = CIblockElement::GetList(
			array("SORT" => "ASC"),
			$arFilter,
			false,
			false,
			array("ID", "IBLOCK_ID", "SORT", "NAME", "PROPERTY_SERTIFICATES_ID","DETAIL_TEXT","PROPERTY_RATE_CHANGE_HOURS","PROPERTY_RATE_SALARY")
		);
		
		if($grade = $db_elements->GetNext(true,false))
		{
			$arResult['CURRENT_GRADE'] = $grade;
		}

		if(!empty($arResult['USER']['RATE_CHANGE_HOURS']))
			$arResult['CURRENT_GRADE']['PROPERTY_RATE_CHANGE_HOURS_VALUE'] = $arResult['USER']['RATE_CHANGE_HOURS'];
		
		//�������� ��������� �����
		$db_elements = CIblockElement::GetList(
			array("SORT" => "ASC"),
			array(
				"IBLOCK_ID" => Config::$GRADE_IBLOCK_ID,
				//"SORT" => intval($arResult['CURRENT_GRADE']['SORT']) + 10
				">SORT" => $arResult['CURRENT_GRADE']["SORT"]
			),
			false,
			false,
			array("ID", "IBLOCK_ID", "NAME", "PROPERTY_SERTIFICATES_ID","DETAIL_TEXT","PROPERTY_SKILLS","PROPERTY_RATE_CHANGE_HOURS","PROPERTY_LEVEL_MOVE_NEEDS")
		);
		
		if($grade = $db_elements->GetNext())
		{	
			$arResult['NEXT_GRADE'] = $grade;
		}
		
		//����������� ����� ������������ �� �������.

		$ravenstvo = false;
		$arrCertificate = array_diff($arResult['NEXT_GRADE']['PROPERTY_SERTIFICATES_ID_VALUE'],$arResult['USER']['UF_CERTIFICATES']);
		
		if(!count($arrCertificate))
		{
			$arResult['USER_CERTIFICATES'] = "��� �����������, ����������� ��� ���������� ������, ��������.";
		}
		else
		{
			
			$db_elements = CIblockElement::GetList(
				array("SORT" => "ASC"),
				array(
					"ID" =>$arrCertificate,
					"IBLOCK_ID" => Config::$CERTIFICATE_IBLOCK_ID
				),
				false,
				false,
				array("ID", "IBLOCK_ID", "NAME", "PROPERTY_SERTIFICATES_ID","PROPERTY_LINK")
			);
			while($certificate = $db_elements->GetNext(true,false))
			{
				$userCertificates[$certificate['ID']] = $certificate;
			}
			$arResult['USER_CERTIFICATES'] = $userCertificates;
		
		}
		
		
		
		//�������� �����
		CModule::includeModule('ws.projectsettings');
		$arResult['BASE_SALARY'] = WS_PSettings::getFieldValue('BASE_SALARY');
		
		
	}
	else
	{
		$arResult['SORY'] = "<b>����� �� ����������.</b>";
	}

	
}
else
{
	$arResult['SORY'] = "� ����������� ������������� <b>\"". $arResult['HEAD']['NAME'] ."\"</b> ������ �� ��������������.";
}

// saving template name to cache array
$arResult["__TEMPLATE_FOLDER"] = $this->__folder;

// writing new $arResult to cache file
$this->__component->arResult = $arResult;

?>