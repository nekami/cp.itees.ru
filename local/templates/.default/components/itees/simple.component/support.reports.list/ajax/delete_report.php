<?
define('NO_KEEP_STATISTIC', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule("iblock");

$result = '0';

if(intval($_REQUEST["ID"]) > 0)
{
	if(CModule::IncludeModule("iblock"))
	{
		$res = CIBlockElement::GetList(Array(), Array("ID" => intval($_REQUEST["ID"]), "IBLOCK_CODE" => 'support_reports'), false, false, Array("ID"));
		if($ob = $res->GetNext())
		{
			if(CIBlockElement::Delete(intval($_REQUEST["ID"])))
				$result = '1';
		}
	}
}

echo $result;
?>