<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arParams["ARCHIVE"] != "Y"):?>
<?
$APPLICATION->SetAdditionalCSS('/bitrix/js/crm/css/crm.css');
$APPLICATION->AddHeadScript('/bitrix/js/crm/crm.js');

$obParser = new CTextParser;
?>

<script type="text/javascript">
var companies = <?=CUtil::PhpToJsObject($arResult['COMPANY'])?>;
var actions = {
	'ok': '<?=GetMessage('SUPPORT_OK')?>',
	'cancel': '<?=GetMessage('SUPPORT_CANCEL')?>',
	'close': '<?=GetMessage('SUPPORT_CLOSE')?>',
	'wait': '<?=GetMessage('SUPPORT_SEARCH')?>',
	'noresult': '<?=GetMessage('SUPPORT_NORESULT')?>',
	'add': '<?=GetMessage('SUPPORT_ADD')?>',
	'edit': '<?=GetMessage('SUPPORT_EDIT')?>',
	'search': '<?=GetMessage('SUPPORT_SEARCH')?>',
	'last': '<?=GetMessage('SUPPORT_LAST')?>',
	'company': '<?=GetMessage('SUPPORT_COMPANIES')?>'
}

$(function() {
	
// ������ �� �������
	
	$('.itees_task_crm_client_link').click(function() {
		if(typeof(obCrm) == 'object') {
			obCrm[this.id].Open();
			obCrm[this.id].AddOnSaveListener(function(arElements) {
				//var client = arElements.company[0].id;
				$('#selected-clients').remove();
			});
		}
		return false;
	});
	
	$('.itees_task_crm_client_link').each(function() {
		if(typeof(CRM) != 'undefined') {
			CRM.Set(
				BX($(this).attr('id')),
				'client_id',
				'',
				companies,
				false,
				true,
				['company'],
				actions
			);
		}
	});
});
</script>
<?/*<form action="<?$APPLICATION->GetCurPageParam()?>" method="post">
	<div style="float:left;">
		<?=GetMessage('ITEES_SUPPORT_GROUP_IN')?>
	</div>
	<div style="margin-left:130px;">
		<label><input type="radio" name="group" value="1" <?=($APPLICATION->get_cookie("group") == 1) ? "checked='checked'" : ""?> /><?=GetMessage('ITEES_SUPPORT_GROUP_CLIENT')?></label><br/>
		<label><input type="radio" name="group" value="2" <?=($APPLICATION->get_cookie("group") == 2) ? "checked='checked'" : ""?> /><?=GetMessage('ITEES_SUPPORT_GROUP_MONTH')?></label>
	</div>
	<div>
		<button type="submit"><?=GetMessage('ITEES_SUPPORT_APPLY')?></button>
	</div>
</form>*/?>
<?if(1 || $APPLICATION->get_cookie("group") == 2){?>
<form action="<?$APPLICATION->GetCurPageParam()?>" method="post" style="position:relative; margin:40px 0 20px 0; padding: 15px 20px; border-radius:10px; background:url('/dev/images/filter-bg.gif');">
	<div style="font-weight:bold; font-size:12px; position:absolute; top:-28px; left:0px; border-radius:10px; background:url('/dev/images/filter-bg.gif'); padding:10px 20px 10px 20px; line-height:2em;">������</div>
	<div class="itees_task_crm_select_client">
		<span class="filter_prop_name">������</span>
		<div style="background:#eaeff0; padding:10px; border-radius:5px;">
		<table cellpadding="0" cellspacing="0" class="field_crm" id="selected-clients">
			<tr>
				<td class="field_crm_entity">
					<?$first = true;
					foreach ($arResult['SELECTED_COMPANIES'] as $entityId => $entity):
						echo (!$first? '': '').'<a href="'.$entity['ENTITY_LINK'].'" id="crm-company-'.$entityId.'">'.htmlspecialcharsbx($entity['ENTITY_TITLE']).'</a>'; $first = false;
						?><input type="hidden" name="client_id[]" value="<?=$entityId?>" id="crm-company-input-<?=$entityId?>" />
						<span class="crm-element-item-delete" id="deleted-crm-itees_task_crm_client_link_client_id-block-item-<?=$entityId?>-company" onclick="$('#crm-company-<?=$entityId?>, #crm-company-input-<?=$entityId?>, #deleted-crm-itees_task_crm_client_link_client_id-block-item-<?=$entityId?>-company').remove(); return false;"></span>
						<?
					endforeach;?>
				</td>
			</tr>
		</table>
		<div class="itees_task_crm_button_open">
			<a class="itees_task_crm_client_link" id="itees_task_crm_client_link" href="#"><?=GetMessage('SUPPORT_ADD')?></a>
		</div>
		</div>
	</div>
	
	<div class="itees_task_crm_select_client">
		<span class="filter_prop_name">����������� ��������</span>

		<span class="taskView__SelectContainer">						
		
			<select name="SELF_COMPANY">

				<option value="all"><?=GetMessage('NOT_SELECTED')?></option>
			
				<? foreach($arResult["COMPANY"] as $keyCompany => $valueCompany) { ?> 
				
					<option value="<?=$keyCompany?>" <? if (($_GET["SELF_COMPANY"] == $keyCompany) && ($_GET["SELF_COMPANY"] != "")) { ?> selected <? } ?>>
						<?=$valueCompany?>
					</option>
					
				<? } ?>			
			</select>
		
		</span>
		
	</div>
	
	<div>
		<button type="submit" class="by-button" style="margin-left:0;"><?=GetMessage('ITEES_SUPPORT_APPLY')?></button>
		<button type="button" class="by-button" onclick="location.assign('<?=$APPLICATION->GetCurPage()?>'); <? unset($_GET["SELF_COMPANY"]); ?>; return false;"><?=GetMessage('ITEES_SUPPORT_CANCEL_FILTER')?></button>
	</div>
</form>
<?}?>
<?endif?>

<script type="text/javascript">

/*$(function() {
	$("body").mousemove(function() {
		$(".apply").each(function() {
			if($("a", $(this).siblings("div.box")).html() == "<?=GetMessage('ITEES_SUPPORT_CHANGE_CLIENT')?>")
				$(this).show();
			else
				$(this).hide();
		});
	});
});*/
var report_id = 0;

// ���� ���������� ������

function OnSaveListenerCustom(arElements) {
	$(".apply", $("#menu_"+report_id)).show();
}

	function ToggleTasks(client) {
		if($(".client_"+client).is(":hidden")) {
			$(".client_"+client).show();
			$("#arrow_"+client).removeClass("task-project-folding-closed");
		}
		else {
			$(".client_"+client).hide();
			$("#arrow_"+client).addClass("task-project-folding-closed");
		}
	}
	
	function ShowMenuPopupCustom(obj) {
		var $menu = $(obj).siblings("ul");
		$menu.show();
		$("body").prepend("<div class='back'></div>");
		
		$(".back").click(function() {
			$(".popup_menu_custom, .back").hide();
		});
		
		return false;
	}
	
	function DeleteReport(obj) {
		$(obj).siblings(".loader").show();
		var trclass = $(obj).closest("tr").attr("class");
		$.ajax({
			url: $(obj).attr("data-url"),
			cache: false,
			success: function(result){
				if(result == "1") {
					if($("tr."+trclass).length == 1)
						$(obj).closest("tr").prev("tr").remove();
					$(obj).closest("tr").remove();
					$(".popup_menu_custom, .back").hide();
				}
			}
		});
		
		return false;
	}
	
	$(document).ready(function() {
		/*$("a.delete_report").each(function() {
			$(this).click(function() {
				if(confirm("<?=GetMessage('ITEES_SUPPORT_DELETE_CONFIRM')?>")) {
					DeleteReport(this);
				}
			});
		});*/
		
		$('a.delete_report').click(function() {
				if(confirm("<?=GetMessage('ITEES_SUPPORT_DELETE_CONFIRM')?>")) {
					DeleteReport(this);
				}
		});
	});
</script>
<?if($APPLICATION->get_cookie("group") == 1 || $arParams["ARCHIVE"] == "Y"):?>
<div style="margin: 20px 0">
<?=$arResult["NAV_STRING"]?>
</div>
<table class="reports">
	<thead>
	<tr>
		<th class="task-title-column" style="width:1px !important;"></th>
		<th class="task-title-column"><?=GetMessage('ITEES_SUPPORT_REPORT')?></th>
		<?/*?><th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_REPORT_STATUS')?></th><?*/?>																		   
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_DATE_CREATE')?></th>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_CREATOR')?></th>
		<?if($arParams["ARCHIVE"] != "Y"){?>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_REPORT_TIME')?></th>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_FACT_TIME')?></th>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_SUMM')?></th>
		<th colspan="2" class="report-title-column"><?=GetMessage('ITEES_SUPPORT_NUMBER')?></th>
		<?/*<th class="task-title-column"><?=GetMessage('ITEES_SUPPORT_ACT')?></th>*/?>
		<?}else{?>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_EDIT_DATE')?></th>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_EDITOR')?></th>
		<?}?>
	</tr>
	</thead>
	<?foreach($arResult['CLIENTS'] as $key => $arClient):?>
	<?if($key > 0){?>
		<?if($arParams["ARCHIVE"] != "Y"){?>
		<tr>
			<td colspan="<?=($arParams["ARCHIVE"] == "Y")?"6":"9"?>" class="task-project-column">
				<div class="task-project-column-inner">
					<div class="task-project-name">
						<span class="task-project-folding" id="arrow_<?=$key?>" onclick="ToggleTasks('<?=$key?>'); return false;"></span>
						<a class="task-project-name-link" href="#" onclick="ToggleTasks('<?=$key?>'); return false;">
							<?$client = GetClientTitle($key);?><?if(empty($client)){?><?$client=$key?><?}?>
							<?=$client?>
						</a>
					</div>
				</div>
			</td>
		</tr>
		<?}?>
	<?}?>
	<?foreach($arClient as $arReport):?>
	<?$jobReportType = $arResult['REPORT_TYPE_ALL'][$arReport["PROPERTY_REPORT_TYPE_ENUM_ID"]] == "job" ? true : false?>
	<?if(!preg_match("/".GetMessage('ITEES_SUPPORT_ARCHIVE')."/", $arReport["NAME"])):?>
	<?if($arParams["ARCHIVE"] != "Y"):?>
	<tr class="client_<?=$key?>" id="menu_<?=$arReport["ID"]?>">
		<td style="position:relative;">
			<a href="javascript: void(0)" class="task-menu-button custom" onclick="return ShowMenuPopupCustom(this);" title="<?=GetMessage('ITEES_SUPPORT_MENU')?>" style="margin-left:13px;"><i class="task-menu-button-icon"></i></a>
			<ul class="popup_menu_custom">
				<li class="inline_a">
					<form id="form-<?=$arReport["ID"]?>" action="<?$APPLICATION->GetCurPage()?>" method="post">
					<div id="crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>_UF_CLIENT-text-box" style="display:inline;" class="box">
					<!--a class='set_client' href='javascript: void(0)' onclick="obCrm[this.id].Open(); obCrm[this.id].AddOnSaveListener(OnSaveListenerCustom); report_id=<?=$arReport["ID"]?>" id="crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>" style="display:inline;"><?=GetMessage('ITEES_SUPPORT_CHANGE_CLIENT')?></a-->
					<a class='set_client' href='javascript: void(0)' id="crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>" style="display:inline;"><?=GetMessage('ITEES_SUPPORT_CHANGE_CLIENT')?></a>
					</div>&nbsp;&nbsp;&nbsp;<a href="#confirm" onclick="setClient<?=$arReport["ID"]?>()" style="display:none;" class="apply"><?=GetMessage('ITEES_SUPPORT_APPLY')?></a>
					<div id="crm-crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>_UF_CLIENT-input-box" style="display:none;"><input type="text" name="UF_CLIENT"></div>
					<input type="hidden" name="UF_REPORT" value="<?=$arReport["ID"]?>" />
					</form>
				</li>
				<li><a class='delete_report' href="javascript: void(0)" data-url="<?=$templateFolder?>/ajax/delete_report.php?ID=<?=$arReport["ID"]?>"><?=GetMessage('ITEES_SUPPORT_DELETE_REPORT')?></a><img src='<?=$templateFolder?>/images/loader.gif' class="loader" style="position:absolute; right:0.5em; top:0.5em;" alt='' /></li>
			</ul>
			
			
<script type="text/javascript">
	function setClient<?=$arReport["ID"]?>() {
		$("#form-<?=$arReport["ID"]?>").submit();
	}
	
	var _BX_CRM_FIELD_INIT_<?=$arReport["ID"]?> = function()
	{	
		if(typeof(CRM) == 'undefined')
		{
			BX.loadCSS('/bitrix/js/crm/css/crm.css');
			BX.loadScript('/bitrix/js/crm/crm.js', _BX_CRM_FIELD_INIT_<?=$arReport["ID"]?>);
			return;
		}

		CRM.Set(
			BX('crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>'),
			'UF_CLIENT',
			'',
			<?echo CUtil::PhpToJsObject($arResult['ELEMENT']);?>,
			false,
			false,
			['company'],
			{
				'ok': '<?=GetMessage('ITEES_SUPPORT_OK')?>',
				'cancel': '<?=GetMessage('ITEES_SUPPORT_CANCEL')?>',
				'close': '<?=GetMessage('ITEES_SUPPORT_CLOSE')?>',
				'wait': '<?=GetMessage('ITEES_SUPPORT_SEARCH')?>',
				'noresult': '<?=GetMessage('ITEES_SUPPORT_NO_RESULT')?>',
				'add' : '<?=GetMessage('ITEES_SUPPORT_CHANGE_CLIENT')?>',
				'edit' : '<?=GetMessage('ITEES_SUPPORT_CHANGE_CLIENT')?>',
				'search' : '<?=GetMessage('ITEES_SUPPORT_SEARCH')?>',
				'last' : '<?=GetMessage('ITEES_SUPPORT_LAST')?>'
			}
		);
	};

	/*BX.ready(
		function() {
			_BX_CRM_FIELD_INIT_<?=$arReport["ID"]?>();
		}
	);*/
	BX.ready(function(){
		jQuery('#crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>').click(function(){
			_BX_CRM_FIELD_INIT_<?=$arReport["ID"]?>();
			obCrm[this.id].Open(); 
			obCrm[this.id].AddOnSaveListener(OnSaveListenerCustom); 
			report_id=<?=$arReport["ID"]?>;
		});
	});
</script>






		</td>
		<td class="report-name"><a href="<?=$arReport["ID"]?>"><?=preg_replace("/ - .*/", "", $arReport["NAME"])?></a></td>
		<?/*?><td>
			<span class="main-grid-cell-content">
				<?$APPLICATION->IncludeComponent(
					"itees:support.reports.status",
					"",
					Array(
						"REPORT_ID" => $arReport["ID"],
						"REPORT_IBLOCK_CODE" => "support_reports",
						"REPORT_STATUS_CODE" => "REPORT_STATUS",
						"REPORT_STATUS_IBLOCK" => "support_report_status"
					)
				);?>
			</span>
		</td><?*/?>
		<td class="report_date"><?=$arReport["DATE_CREATE"]?></td>
		<td><?=$arReport["USER"]?></td>
		<td style="padding-left:20px;"><?=intval($arReport["REPORT_TIME"]/60)?><?=GetMessage('ITEES_SUPPORT_H')?>&nbsp;<?=$arReport["REPORT_TIME"]%60?><?=GetMessage('ITEES_SUPPORT_M')?></td>
		<td style="padding-left:20px;"><?=intval($arReport["ELAPSED_TIME"]/60)?><?=GetMessage('ITEES_SUPPORT_H')?>&nbsp;<?=$arReport["ELAPSED_TIME"]%60?><?=GetMessage('ITEES_SUPPORT_M')?></td>
		<td><?=intval($arReport["PRICE"])?> <?=GetMessage('ITEES_SUPPORT_R')?>.</td>
		<td class="report_act"><?=$arReport["PROPERTY_ORDER_ID_VALUE"]?></td>
		<td class="report_act"><?=$arReport["PROPERTY_ACT_VALUE"]?></td>
	</tr>
	<?endif?>
	<?elseif($arParams["ARCHIVE"] == "Y"):?>
	<?
	$menu_td = "
	<td style='position:relative;'>
		<a href='javascript: void(0)' class='task-menu-button custom' onclick='return ShowMenuPopupCustom(this);' title='????' style='margin-left:13px;'><i class='task-menu-button-icon'></i></a>
		<ul class='popup_menu_custom'>
			<li><a class='delete_report' href='javascript: void(0)' data-url='".$templateFolder."/ajax/delete_report.php?ID=".$arReport["ID"]."'>".GetMessage('ITEES_SUPPORT_DELETE_ARCHIVE')."</a><img src='".$templateFolder."/images/loader.gif' class='loader' style='position:absolute; right:0.5em; top:0.5em;' alt='' /></li>
		</ul>
	</td>";
	
	$arch_tr[] = "
	<tr class='client_a' id='menu_".$arReport["ID"]."'>
		".$menu_td."
		<td class='report-name'><a href='{$arReport['ID']}/'>{$arReport['NAME']}</a></td>
		<td>{$arReport['DATE_CREATE']}</td>
		<td>{$arReport['USER']}</td>
		<td>{$arReport['TIMESTAMP_X']}</td>
		<td>{$arReport['MODIFIED_BY']}</td>
	</tr>
	";
	?>
	<?endif?>
	<?endforeach?>
	<?endforeach?>
	<?if($arParams["ARCHIVE"] == "Y"):?>
	<tr>
		<td colspan="6" class="task-project-column">
			<div class="task-project-column-inner">
				<div class="task-project-name">
					<span class="task-project-folding" id="arrow_a" onclick="ToggleTasks('a'); return false;"></span>
					<a class="task-project-name-link" href="#" onclick="ToggleTasks('a'); return false;">
						<?=GetMessage('ITEES_SUPPORT_TASK_ARCHIVES')?>
					</a>
				</div>
			</div>
		</td>
	</tr>
	<?foreach($arch_tr as $tr):?>
	<?=$tr?>
	<?endforeach?>
	<?endif?>
</table>

<?elseif(1 || $APPLICATION->get_cookie("group") == 2):?>
<div style="margin: 20px 0">
<?=$arResult["NAV_STRING"]?>
</div>
<table class="reports">
	<thead>
	<tr>
		<th class="task-title-column" style="width:1px !important;"></th>
		<th class="task-title-column"><?=GetMessage('ITEES_SUPPORT_REPORT')?></th>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_REPORT_STATUS')?></th>																		   
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_DATE_CREATE')?></th>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_CREATOR')?></th>
		<?if($arParams["ARCHIVE"] != "Y"):?>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_REPORT_TIME')?></th>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_FACT_TIME')?></th>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_SUMM')?></th>
		<th colspan="2" class="report-title-column"><?=GetMessage('ITEES_SUPPORT_NUMBER')?></th>
		<?/*<th class="task-title-column"><?=GetMessage('ITEES_SUPPORT_ACT')?></th>*/?>
		<?else:?>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_EDIT_DATE')?></th>
		<th class="report-title-column"><?=GetMessage('ITEES_SUPPORT_EDITOR')?></th>
		<?endif?>
	</tr>
	</thead>
	<?foreach($arResult['MONTHS'] as $key => $arClient):?>
	<?if($key > 0):?>
	<?if($arParams["ARCHIVE"] != "Y"):?>
	<tr>
		<td colspan="5" class="task-project-column">
			<div class="task-project-column-inner">
				<div class="task-project-name">
					<span class="task-project-folding" id="arrow_<?=preg_replace("/\./", "_", $key)?>" onclick="ToggleTasks('<?=preg_replace("/\./", "_", $key)?>'); return false;"></span>
					<a class="task-project-name-link" href="#" onclick="ToggleTasks('<?=preg_replace("/\./", "_", $key)?>'); return false;">
						<?=substr($key, 3)?>
					</a>
				</div>
			</div>
		</td>
		<td colspan="1" class="task-project-column">
			<?=intval($arResult['MONTHS_REPORT_TIME'][$key]/60)?><?=GetMessage('ITEES_SUPPORT_H')?>&nbsp;<?=$arResult['MONTHS_REPORT_TIME'][$key]%60?><?=GetMessage('ITEES_SUPPORT_M')?>
		</td>
		<td colspan="1" class="task-project-column">
			<?=intval($arResult['MONTHS_ELAPSED_TIME'][$key]/60)?><?=GetMessage('ITEES_SUPPORT_H')?>&nbsp;<?=$arResult['MONTHS_ELAPSED_TIME'][$key]%60?><?=GetMessage('ITEES_SUPPORT_M')?>
		</td>
		<td class="task-project-column"><?=$arResult['MONTHS_PRICE'][$key]?>&nbsp;�.</td>
		<td class="task-project-column"><?=GetMessage('ITEES_SUPPORT_BILL')?></td>
		<td class="task-project-column"><?=GetMessage('ITEES_SUPPORT_ACT')?></td>
	</tr>
	<?endif?>
	<?endif?>
	<?foreach($arClient as $arReports):?>
	<?foreach($arReports as $arReport):?>
	<?$jobReportType = $arResult['REPORT_TYPE_ALL'][$arReport["PROPERTY_REPORT_TYPE_ENUM_ID"]] == "job" ? true : false;?>
	<?if(!preg_match("/".GetMessage('ITEES_SUPPORT_ARCHIVE')."/", $arReport["NAME"])):?>
	<?if($arParams["ARCHIVE"] != "Y"):?>
	<tr class="client_<?=preg_replace("/\./", "_", $key)?>" id="menu_<?=$arReport["ID"]?>" style="<?if((($arReport["PRICE"] == 0 && $arReport["NOGARANT"]) || $arReport["ALERT"]) && !$jobReportType){?>background:#fed;<?}elseif($arReport["PROPERTY_PAYED_VALUE"] == 'Y'){?>background:#dfd;<?}?>">
		<td style="position:relative;">
			<a href="javascript: void(0)" class="task-menu-button custom" onclick="return ShowMenuPopupCustom(this);" title="<?=GetMessage('ITEES_SUPPORT_MENU')?>" style="margin-left:13px;"><i class="task-menu-button-icon"></i></a>
			<ul class="popup_menu_custom">
				<li class="inline_a">
					<form id="form-<?=$arReport["ID"]?>" action="<?$APPLICATION->GetCurPage()?>" method="post">
					<div id="crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>_UF_CLIENT-text-box" style="display:inline;" class="box">
					<!--a class='set_client' href='javascript: void(0)' onclick="obCrm[this.id].Open(); obCrm[this.id].AddOnSaveListener(OnSaveListenerCustom); report_id=<?=$arReport["ID"]?>" id="crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>" style="display:inline;"><?=GetMessage('ITEES_SUPPORT_CHANGE_CLIENT')?></a-->
					<a class='set_client' href='javascript: void(0)' id="crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>" style="display:inline;"><?=GetMessage('ITEES_SUPPORT_CHANGE_CLIENT')?></a>
					</div>&nbsp;&nbsp;&nbsp;<a href="#confirm" onclick="setClient<?=$arReport["ID"]?>()" style="display:none;" id="confirm-<?=$arReport["ID"]?>" class="apply"><?=GetMessage('ITEES_SUPPORT_APPLY')?></a>
					<div id="crm-crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>_UF_CLIENT-input-box" style="display:none;"><input type="text" name="UF_CLIENT"></div>
					<input type="hidden" name="UF_REPORT" value="<?=$arReport["ID"]?>" />
					</form>
				</li>
				<li><a class='delete_report' href="javascript: void(0)" data-url="<?=$templateFolder?>/ajax/delete_report.php?ID=<?=$arReport["ID"]?>"><?=GetMessage('ITEES_SUPPORT_DELETE_REPORT')?></a><img src='<?=$templateFolder?>/images/loader.gif' class="loader" style="position:absolute; right:0.5em; top:0.5em;" alt='' /></li>
			</ul>


<script type="text/javascript">
	function setClient<?=$arReport["ID"]?>() {
		$("#form-<?=$arReport["ID"]?>").submit();
	}
	
	var _BX_CRM_FIELD_INIT_<?=$arReport["ID"]?> = function()
	{	
		if(typeof(CRM) == 'undefined')
		{
			BX.loadCSS('/bitrix/js/crm/css/crm.css');
			BX.loadScript('/bitrix/js/crm/crm.js', _BX_CRM_FIELD_INIT_<?=$arReport["ID"]?>);
			return;
		}

		CRM.Set(
			BX('crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>'),
			'UF_CLIENT',
			'',
			<?echo CUtil::PhpToJsObject($arResult['ELEMENT']);?>,
			false,
			false,
			['company'],
			{
				'ok': '<?=GetMessage('ITEES_SUPPORT_OK')?>',
				'cancel': '<?=GetMessage('ITEES_SUPPORT_CANCEL')?>',
				'close': '<?=GetMessage('ITEES_SUPPORT_CLOSE')?>',
				'wait': '<?=GetMessage('ITEES_SUPPORT_SEARCH')?>',
				'noresult': '<?=GetMessage('ITEES_SUPPORT_NO_RESULT')?>',
				'add' : '<?=GetMessage('ITEES_SUPPORT_CHANGE_CLIENT')?>',
				'edit' : '<?=GetMessage('ITEES_SUPPORT_CHANGE_CLIENT')?>',
				'search' : '<?=GetMessage('ITEES_SUPPORT_SEARCH')?>',
				'last' : '<?=GetMessage('ITEES_SUPPORT_LAST')?>'
			}
		);
	};

	/*BX.ready(
		function() {
			_BX_CRM_FIELD_INIT_<?=$arReport["ID"]?>();
		}
	);*/
	BX.ready(function(){
		jQuery('#crm-task-edit-form-uf-client-open-<?=$arReport["ID"]?>').click(function(){
			_BX_CRM_FIELD_INIT_<?=$arReport["ID"]?>();
			obCrm[this.id].Open(); 
			obCrm[this.id].AddOnSaveListener(OnSaveListenerCustom); 
			report_id=<?=$arReport["ID"]?>;
		});
	});
</script>




		</td>
		<td class="report-name">
			<a href="<?=$arReport["ID"]?>/"><?=(GetClientTitle($arReport["PROPERTY_CLIENT_ID_VALUE"]) == '')?' - ':GetClientTitle($arReport["PROPERTY_CLIENT_ID_VALUE"])?></a>
			<?
			if(!empty($arReport['PROPERTY_REPORT_NAME_VALUE']))
			{
				$obParser->html_cut($arItem["PREVIEW_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
			?>
				<br>
				<span class="sub_name"><?=$obParser->html_cut($arReport['PROPERTY_REPORT_NAME_VALUE'], 60)?></span>
			<?
			}
			?>	
		</td>
		<td>
			<span class="main-grid-cell-content">
				<?$APPLICATION->IncludeComponent(
					"itees:support.reports.status",
					"",
					Array(
						"REPORT_ID" => $arReport["ID"],
						"REPORT_IBLOCK_CODE" => "support_reports",
						"REPORT_STATUS_CODE" => "REPORT_STATUS",
						"REPORT_STATUS_IBLOCK" => "support_report_status"
					)
				);?>
			</span>
		</td>
		<td class="report_date"><?=$arReport["DATE_CREATE"]?></td>
		<td><?=$arReport["USER"]?></td>
		<td style="padding-left:20px;"><?=intval($arReport["REPORT_TIME"]/60)?><?=GetMessage('ITEES_SUPPORT_H')?> <?=$arReport["REPORT_TIME"]%60?><?=GetMessage('ITEES_SUPPORT_M')?></td>
		<td style="padding-left:20px;"><?=intval($arReport["ELAPSED_TIME"]/60)?><?=GetMessage('ITEES_SUPPORT_H')?> <?=$arReport["ELAPSED_TIME"]%60?><?=GetMessage('ITEES_SUPPORT_M')?></td>
		<td style="position:relative;">
			<?=intval($arReport["PRICE"])?> <?=GetMessage('ITEES_SUPPORT_R')?>.
			<?
			if(!$arReport['SAVED'])
			{
				?><span class="rep_red">!</span><?
			}
			elseif($arReport["PROPERTY_PAYED_VALUE"] == 'Y')
			{
				?><span class="rep_green">$</span><?
			}
			elseif(!empty($arReport["PROPERTY_INVOICE_ID_VALUE"]))
			{
				?><span class="rep_blue">C</span><?
			}
			elseif(!empty($arReport['CHECKLIST_RESULT']))
			{
				?><span class="rep_green">O</span><?
			}
			?>
		</td>
		<td class="report_act"><?=$arReport["PROPERTY_ORDER_ID_VALUE"]?></td>
		<td class="report_act"><?=$arReport["PROPERTY_ACT_VALUE"]?></td>
	</tr>
	<?endif?>
	<?endif?>
	<?endforeach?>
	<?endforeach?>
	<?endforeach?>
</table>
<?endif?>
<div style="margin: 20px 0">
<?=$arResult["NAV_STRING"]?>
</div>