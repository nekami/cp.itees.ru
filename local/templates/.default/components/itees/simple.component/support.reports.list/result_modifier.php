<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/*ini_set('error_reporting', -1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);*/

if(!isset($arParams['REPORTS_COUNT']) || intval($arParams['REPORTS_COUNT']) == 0)
	$arParams['REPORTS_COUNT'] = 50;
else
	$arParams['REPORTS_COUNT'] = intval($arParams['REPORTS_COUNT']);
	
if(isset($_REQUEST['PAGEN_1']) && intval($_REQUEST['PAGEN_1']) > 0)
	$iNumPage = intval($_REQUEST['PAGEN_1']);
else
	$iNumPage = 1;

/*if($APPLICATION->get_cookie("group") === '')
{
	$APPLICATION->set_cookie("group", 1, time()+60*60*24*30*12*10);
	LocalRedirect($APPLICATION->GetCurPageParam("", array("group")));
}

if(isset($_REQUEST["group"]))
{
	if($_REQUEST["group"] == '1')
	{
		$APPLICATION->set_cookie("group", 1, time()+60*60*24*30*12*10);
	}
	elseif($_REQUEST["group"] == '2')
	{
		$APPLICATION->set_cookie("group", 2, time()+60*60*24*30*12*10);
	}
	LocalRedirect($APPLICATION->GetCurPageParam("", array("group")));
}*/

//if($APPLICATION->get_cookie("group") == 1)
{
//	$arOrder = Array("PROPERTY_CLIENT_NAME" => "ASC");
}
//else
{
	$arOrder = Array("PROPERTY_REPORT_DATE" => "DESC", "PROPERTY_REPORT_DATE_2" => "DESC");
}



if(CModule::IncludeModule('tasks') && CModule::IncludeModule('crm') && CModule::IncludeModule('iblock'))
{
	// ------------ ��������� ������ �������� ��� ������������ ������ ������ �������� -------------------
	
	/*$arCompanyTypeList = CCrmStatus::GetStatusListEx('COMPANY_TYPE');
	$arCompanyIndustryList = CCrmStatus::GetStatusListEx('INDUSTRY');

	$obRes = CCrmCompany::GetList(Array('ID' => 'DESC'), Array("COMPANY_TYPE" => "CUSTOMER"), $arSelect = Array('ID', 'TITLE', 'COMPANY_TYPE', 'INDUSTRY', 'LOGO'));
	while($arRes = $obRes->Fetch())
	{
		$arImg = Array();
		if(!empty($arRes['LOGO']) && !isset($arFiles[$arRes['LOGO']]))
		{
			if(intval($arRes['LOGO']) > 0)
				$arImg = CFile::ResizeImageGet($arRes['LOGO'], Array('width' => 25, 'height' => 25), BX_RESIZE_IMAGE_EXACT);

			$arFiles[$arRes['LOGO']] = $arImg['src'];
		}
		
		if(isset($arResult['SELECTED'][$arRes['SID']]))
		{
			unset($arResult['SELECTED'][$arRes['SID']]);
			$sSelected = 'Y';
		}
		else
			$sSelected = 'N';
		
		$arDesc = Array();
		if(isset($arCompanyTypeList[$arRes['COMPANY_TYPE']]))
			$arDesc[] = $arCompanyTypeList[$arRes['COMPANY_TYPE']];
		if(isset($arCompanyIndustryList[$arRes['INDUSTRY']]))
			$arDesc[] = $arCompanyIndustryList[$arRes['INDUSTRY']];
		
		$arResult['COMPANY'][$arRes['ID']] = Array(
			'~title' => $arRes['TITLE'],
			'title' => (str_replace(Array(';', ','), ' ', $arRes['TITLE'])),
			'desc' => implode(', ', $arDesc),
			'id' => $arRes['ID'],
			'url' => CComponentEngine::MakePathFromTemplate(COption::GetOptionString('crm', 'path_to_company_show'),
				Array(
					'company_id' => $arRes['ID']
				)
			),
			'image' => $arImg['src'],
			'type'  => 'company',
			'selected' => $sSelected
		);
	}*/
}


$obPropChecklist = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 59, "CODE" => "CHECKLIST"));
while($arPropChecklistVal = $obPropChecklist->Fetch())
	$AR_CHECKLIST[$arPropChecklistVal['ID']] = $arPropChecklistVal;

$arTaskIds = $arReportIds = Array();

$arFilter = Array("IBLOCK_CODE" => 'support_reports');
if($arParams["ARCHIVE"] != "Y")
{
	$arFilter["!PROPERTY_CLIENT_ID"] = Array(0, false);
	
	if(isset($_REQUEST['client_id']) && intval($_REQUEST['client_id']) > 0 || is_array($_REQUEST['client_id']))
	{
		if(!is_array($_REQUEST['client_id']))
			$_REQUEST['client_id'] = Array(intval($_REQUEST['client_id']));
	
		$arFilter["PROPERTY_CLIENT_ID"] = $arResult['SELECTED_CLIENTS'] = $_REQUEST['client_id'];
		
		if (count($arResult['SELECTED_CLIENTS']))
		{
			$dbRes = CCrmCompany::GetList(Array('TITLE'=>'ASC'), array('ID' => $arResult['SELECTED_CLIENTS']));			
			while ($arRes = $dbRes->Fetch())
			{
				$arResult['SELECTED_COMPANIES'][$arRes['ID']] = Array(
					'ENTITY_TITLE' => $arRes['TITLE'],
					'ENTITY_LINK' => CComponentEngine::MakePathFromTemplate(COption::GetOptionString('crm', 'path_to_company_show'), array('company_id' => $arRes['ID']))
				);
			}
		}
	}
}
else
{
	$arFilter["PROPERTY_CLIENT_ID"] = Array(0, false);
}

// ������ �� ����������� ���������

if (!empty($_POST["SELF_COMPANY"]))
{
	$_GET['SELF_COMPANY'] = $_POST["SELF_COMPANY"];
}

if (!empty($_GET["SELF_COMPANY"]) && ($_GET["SELF_COMPANY"] != "all"))
{
	$arFilter["=PROPERTY_SELF_COMPANY"] = $_GET['SELF_COMPANY'] === 'empty' ? false : $_GET['SELF_COMPANY'];
}

$userInfo = [];

$res = CIBlockElement::GetList($arOrder, $arFilter, false, Array("iNumPage" => $iNumPage, "nPageSize" => $arParams['REPORTS_COUNT'], 'bShowAll' => false), Array("NAME", "TIMESTAMP_X", "MODIFIED_BY", "ID", "NAME", "DATE_CREATE", "CREATED_BY", "PROPERTY_CLIENT_ID", "PROPERTY_TASK_ID", "PROPERTY_REPORT_DATE", "PROPERTY_REPORT_DATE_2", "PROPERTY_ORDER_ID", "PROPERTY_ACT", "PROPERTY_PAYED", "PROPERTY_CHECKLIST", "PROPERTY_INVOICE_ID", "PROPERTY_REPORT_NAME", "PROPERTY_REPORT_TYPE"));

while($arFields = $res->GetNext())
{		
	/*$rsUser = CUser::GetByID($arFields["CREATED_BY"]);
	$arUser = $rsUser->Fetch();
	$arFields["USER"] = $arUser["LAST_NAME"]." ".$arUser["NAME"];
	$arFields["MODIFIED_BY"] = GetUserName($arFields["MODIFIED_BY"]);*/
	$userInfo[$arFields["CREATED_BY"]] = null;
	$arFields["USER"] = &$userInfo[$arFields["CREATED_BY"]];
	$userInfo[$arFields["MODIFIED_BY"]] = null;
	$arFields["MODIFIED_BY"] = &$userInfo[$arFields["MODIFIED_BY"]];
	
	$arFields['ELAPSED_TIME'] = 0;
	
	$arFields['CHECKLIST_RESULT'] = checklistChecked($AR_CHECKLIST, $arFields['PROPERTY_CHECKLIST_VALUE']);
	//s($arFields);
	$arReportIds[$arFields['ID']] = $arFields['ID'];
	$arResult['CLIENTS'][$arFields["PROPERTY_CLIENT_ID_VALUE"]][] = $arFields;
	
	if(empty($arFields["PROPERTY_REPORT_DATE_VALUE"]))
	{
		$date = $arFields["PROPERTY_REPORT_DATE_2_VALUE"];
	}
	else
	{
		$stmp = MakeTimeStamp($arFields["PROPERTY_REPORT_DATE_VALUE"]);
		$month = date('m', $stmp);
		$year = date('Y', $stmp);
		$first_day_stmp = mktime(0, 0, 0, $month, 1, $year);
		$date = ConvertTimeStamp($first_day_stmp, "SHORT");
	}
	
	/*if($arFields['ID'] == 358852)
	{
		s($arFields["PROPERTY_REPORT_DATE_VALUE"]);
	}*/
	
	if(!empty($arFields["PROPERTY_REPORT_DATE_2_VALUE"]))
	{
		$arResult['MONTHS'][$date][$arFields["PROPERTY_CLIENT_ID_VALUE"]][] = $arFields;
	}
	foreach($arFields["PROPERTY_TASK_ID_VALUE"] as $taskId)
	{
		$arTaskIds[$taskId] = $taskId;
	}
}

$userInfoBy = 'id';
$userInfoOrder = 'asc';

$userInfoResult = CUser::GetList($userInfoBy, $userInfoOrder, ['ID' => array_keys($userInfo)], ['FIELDS ' => ['ID', 'NAME', 'LAST_NAME']]);

while($info = $userInfoResult->Fetch()){
	$userInfo[$info['ID']] = $info['LAST_NAME'] . ' ' . $info['NAME'];
}

if(count($arResult['CLIENTS']) || count($arResult['MONTHS']))
{

$arResult["NAV_STRING"] = $res->GetPageNavStringEx($navComponentObject, '', 'modern1', 'Y');

$res = CTaskElapsedTime::GetList(
	Array(), 
	Array("TASK_ID" => $arTaskIds)
);

while($arTaskTime = $res->GetNext())
{
	foreach($arResult['CLIENTS'] as $k1 => $arClient)
	{
		foreach($arClient as $k2 => $arReport)
		{
			foreach($arReport['PROPERTY_TASK_ID_VALUE'] as $taskId)
			{
				if($taskId == $arTaskTime['TASK_ID'])
				{
					$modulo = $arTaskTime['MINUTES']%5;
					$correction = 0;
					if($modulo > 0)
						$correction = -$modulo + 5;
					$arResult['CLIENTS'][$k1][$k2]['ELAPSED_TIME'] += $arTaskTime['MINUTES'];
					$arResult['CLIENTS'][$k1][$k2]['ELAPSED_TIME2'] += $arTaskTime['MINUTES'] + $correction;
				}
			}
		}
	}
	
	foreach($arResult['MONTHS'] as $k1 => $arClient)
	{
		foreach($arClient as $k2 => $arReports)
		{
			foreach($arReports as $k3 => $arReport)
			{
				foreach($arReport['PROPERTY_TASK_ID_VALUE'] as $taskId)
				{
					if($taskId == $arTaskTime['TASK_ID'])
					{
						$modulo = $arTaskTime['MINUTES']%5;
						$correction = 0;
						if($modulo > 0)
							$correction = -$modulo + 5;
						$arResult['MONTHS'][$k1][$k2][$k3]['ELAPSED_TIME'] += $arTaskTime['MINUTES'];
						$arResult['MONTHS'][$k1][$k2][$k3]['ELAPSED_TIME2'] += $arTaskTime['MINUTES'] + $correction;
						$arResult['MONTHS_ELAPSED_TIME'][$k1] += $arTaskTime['MINUTES'];
						$arResult['MONTHS_ELAPSED_TIME2'][$k1] += $arTaskTime['MINUTES'] + $correction;
					}
				}
			}
		}
	}
}

$res = CIBlockElement::GetList(
	Array(),
	Array("IBLOCK_CODE" => 'reports_print', "CODE" => Array('nogarant_time', 'garant_time'), 'PROPERTY_report' => $arReportIds),
	false,
	false,
	Array("ID", 'CODE', 'PROPERTY_report', 'PROPERTY_elapsed_time', "PROPERTY_price", "PROPERTY_garant")
);
while($arFields = $res->GetNext())
{
	foreach($arResult['CLIENTS'] as $k1 => $arClient)
	{
		foreach($arClient as $k2 => $arReport)
		{
			if($arReport['ID'] == $arFields['PROPERTY_REPORT_VALUE'])
			{
				$arResult['CLIENTS'][$k1][$k2]['REPORT_TIME'] += $arFields['PROPERTY_ELAPSED_TIME_VALUE'];
				if($arFields['CODE'] == 'nogarant_time')
					$arResult['CLIENTS'][$k1][$k2]['PRICE'] = $arFields['PROPERTY_PRICE_VALUE'];
			}
		}
	}
	
	foreach($arResult['MONTHS'] as $k1 => $arClient)
	{
		foreach($arClient as $k2 => $arReports)
		{
			foreach($arReports as $k3 => $arReport)
			{
				if($arReport['ID'] == $arFields['PROPERTY_REPORT_VALUE'])
				{
					$arResult['MONTHS'][$k1][$k2][$k3]['REPORT_TIME'] += $arFields['PROPERTY_ELAPSED_TIME_VALUE'];
					if($arFields['CODE'] == 'nogarant_time')
					{
						$arResult['MONTHS'][$k1][$k2][$k3]['PRICE'] = $arFields['PROPERTY_PRICE_VALUE'];
						$arResult['MONTHS'][$k1][$k2][$k3]['PRICE'] = $arFields['PROPERTY_PRICE_VALUE'];
						$arResult['MONTHS_PRICE'][$k1] += $arFields['PROPERTY_PRICE_VALUE'];
					}
					if($arFields['PROPERTY_GARANT_VALUE'] == 'N')
					{
						$arResult['MONTHS'][$k1][$k2][$k3]['NOGARANT'] = true;
					}
					
					$arResult['MONTHS'][$k1][$k2][$k3]['SAVED'] = true;
				}
			}
		}
	}
}

$res = CIBlockElement::GetList(
	Array(),
	Array("IBLOCK_CODE" => 'reports_print', "!CODE" => Array('nogarant_time', 'garant_time'), 'PROPERTY_report' => $arReportIds, 'ACTIVE' => 'Y'),
	false,
	false,
	Array("ID", 'CODE', 'PROPERTY_report', 'PROPERTY_elapsed_time', "PROPERTY_price", "PROPERTY_garant", "PROPERTY_hour_price")
);

while($arFields = $res->GetNext())
{	
	foreach($arResult['MONTHS'] as $k1 => $arClient)
	{
		foreach($arClient as $k2 => $arReports)
		{
			foreach($arReports as $k3 => $arReport)
			{
				if($arReport['ID'] == $arFields['PROPERTY_REPORT_VALUE'])
				{
					if($arFields['PROPERTY_GARANT_VALUE'] == 'N')
					{
						$arResult['MONTHS'][$k1][$k2][$k3]['NOGARANT'] = true;
						if($arFields['PROPERTY_ELAPSED_TIME_VALUE'] != 0 && $arFields['PROPERTY_PRICE_VALUE'] == 0 && $arFields['PROPERTY_HOUR_PRICE_VALUE'] == 0)
						{
							$arResult['MONTHS'][$k1][$k2][$k3]['ALERT'] = true;
						}
					}
				}
			}
		}
	}
}

foreach($arResult['CLIENTS'] as $k1 => $arClient)
{
	foreach($arClient as $k2 => $arReport)
	{
		if($arResult['CLIENTS'][$k1][$k2]['REPORT_TIME'] == 0)
		{
			$arResult['CLIENTS'][$k1][$k2]['REPORT_TIME'] = $arReport['ELAPSED_TIME2'];
			$modulo = $arResult['CLIENTS'][$k1][$k2]['REPORT_TIME']%5;
			$correction = 0;
			if($modulo > 0)
				$correction = -$modulo + 5;
			$arResult['CLIENTS'][$k1][$k2]['REPORT_TIME'] += $correction;
		}
	}
}

foreach($arResult['MONTHS'] as $k1 => $arClient)
{
	foreach($arClient as $k2 => $arReports)
	{
		foreach($arReports as $k3 => $arReport)
		{
			if($arResult['MONTHS'][$k1][$k2][$k3]['REPORT_TIME'] == 0)
			{
				$arResult['MONTHS'][$k1][$k2][$k3]['REPORT_TIME'] = $arReport['ELAPSED_TIME2'];
				$modulo = $arResult['MONTHS'][$k1][$k2][$k3]['REPORT_TIME']%5;
				$correction = 0;
				if($modulo > 0)
					$correction = -$modulo + 5;
				$arResult['MONTHS'][$k1][$k2][$k3]['REPORT_TIME'] += $correction;
			}
			
			$arResult['MONTHS_REPORT_TIME'][$k1] += $arResult['MONTHS'][$k1][$k2][$k3]['REPORT_TIME'];
		}
	}
}
}
/*if(isset($_REQUEST['test']))
{
	$arFilter = Array("IBLOCK_CODE" => 'support_reports', "!PROPERTY_CLIENT_ID" => Array(0, false));
	$res = CIBlockElement::GetList(Array(), $arFilter, false,false, Array("ID", "NAME", "PROPERTY_CLIENT_ID", "PROPERTY_CLIENT_NAME"));
	while($arFields = $res->GetNext())
	{
		CIBlockElement::SetPropertyValuesEx($arFields['ID'], 59, Array("CLIENT_NAME" => GetClientTitle($arFields['PROPERTY_CLIENT_ID_VALUE'])));
		//s(GetClientTitle($arFields['PROPERTY_CLIENT_ID_VALUE']));
		//s($arFields['ID']);
	}
}*/

function GetClientTitle($client_id)
{
	CModule::IncludeModule("crm");
	
	$res = CCrmCompany::GetList($arOrder = Array('DATE_CREATE' => 'DESC'), $arFilter = Array("ID" => $client_id, "CHECK_PERMISSIONS" => "N"), $arSelect = Array("TITLE"));
	while($arTask = $res->GetNext())
	{
		return $arTask["TITLE"];
	}
}

function GetUserName($user_id)
{
	$rsUser = CUser::GetByID($user_id);
	$arUser = $rsUser->Fetch();
	return $arUser["LAST_NAME"] ." " . $arUser["NAME"];
}

/*$arCompanyTypeList = CCrmStatus::GetStatusListEx('COMPANY_TYPE');
$arCompanyIndustryList = CCrmStatus::GetStatusListEx('INDUSTRY');
$arSelect = array('ID', 'TITLE', 'COMPANY_TYPE', 'INDUSTRY', 'LOGO');
$obRes = CCrmCompany::GetList(Array('ID' => 'DESC'), Array("COMPANY_TYPE" => "CUSTOMER"), $arSelect);

while ($arRes = $obRes->Fetch())
{
	$arImg = array();
	if(!empty($arRes['LOGO']) && !isset($arFiles[$arRes['LOGO']]))
	{
		if(intval($arRes['LOGO']) > 0)
			$arImg = CFile::ResizeImageGet($arRes['LOGO'], array('width' => 25, 'height' => 25), BX_RESIZE_IMAGE_EXACT);

		$arFiles[$arRes['LOGO']] = $arImg['src'];
	}
	
	if(isset($arResult['SELECTED'][$arRes['SID']]))
	{
		unset($arResult['SELECTED'][$arRes['SID']]);
		$sSelected = 'Y';
	}
	else
		$sSelected = 'N';
	
	$arDesc = Array();
	if(isset($arCompanyTypeList[$arRes['COMPANY_TYPE']]))
		$arDesc[] = $arCompanyTypeList[$arRes['COMPANY_TYPE']];
	if(isset($arCompanyIndustryList[$arRes['INDUSTRY']]))
		$arDesc[] = $arCompanyIndustryList[$arRes['INDUSTRY']];
	
	$arResult['ELEMENT'][] = Array(
		'title' => (str_replace(array(';', ','), ' ', $arRes['TITLE'])),
		'desc' => implode(', ', $arDesc),
		'id' => $arRes['ID'],
		'url' => CComponentEngine::MakePathFromTemplate(COption::GetOptionString('crm', 'path_to_company_show'),
			array(
				'company_id' => $arRes['ID']
			)
		),
		'image' => $arImg['src'],
		'type'  => 'company',
		'selected' => $sSelected
	);
}*/

/**** ����� ������� *****/

if(isset($_POST['UF_CLIENT']) && isset($_POST['UF_REPORT']) && intval($_POST['UF_CLIENT']) > 0 && intval($_POST['UF_REPORT']) > 0)
{
	$UF_CLIENT = intval($_POST['UF_CLIENT']);
	$UF_REPORT = intval($_POST['UF_REPORT']);
	
	
	$res = CIBlockElement::GetByID($UF_REPORT);
	if($arEl = $res->GetNext())
	{
		$reports_iblock = false;
		$res = CIBlock::GetList(Array(), Array('CODE' => 'support_reports', 'ACTIVE' => 'Y'), false, false, Array('ID'));
		if($arIblock = $res->GetNext())
		{
			$reports_iblock = intval($arIblock['ID']);
			CIBlockElement::SetPropertyValuesEx($UF_REPORT, $reports_iblock, Array("CLIENT_ID" => $UF_CLIENT));
			
			preg_match("/^(.*) - (.*)$/", $arEl['NAME'], $arMatches);
			
			CModule::IncludeModule("crm");

			$res = CCrmCompany::GetList($arOrder = Array(), $arFilter = Array("ID" => $UF_CLIENT, "CHECK_PERMISSIONS" => "N"), $arSelect = Array("TITLE"));
			while($arTask = $res->GetNext())
			{
				$title = $arTask['TITLE'];
			}
			
			$arFields = Array('NAME' => $arMatches[1].' - '.$title);
			$el = new CIBlockElement;
			$el->Update($UF_REPORT, $arFields);
			
			LocalRedirect($APPLICATION->GetCurPage());
		}
	}
}

// ------------- ��������� ������ �� ���� ---------------- ���� ������� �� �������� ������. ��, ��. ��� ����������. ��, ��� ������, ���� �����...
if($arParams['ARCHIVE'] == 'Y')
{
	function cmp4365($a, $b) 
	{
		$name_a = $a['NAME'];
		$name_b = $b['NAME'];
		
		preg_match('/[�-��-߸�]+\ ([0-9]{2,4})\.([0-9]{1,2})/', $name_a, $arMatchesA);
		preg_match('/[�-��-߸�]+\ ([0-9]{2,4})\.([0-9]{1,2})/', $name_b, $arMatchesB);
		
		$year_a = $arMatchesA[1];
		$year_b = $arMatchesB[1];
		$month_a = $arMatchesA[2];
		$month_b = $arMatchesB[2];
		
		if($year_a > $year_b)
		{
			return -1;
		}
		elseif($year_a < $year_b)
		{
			return 1;
		}
		else
		{
			if($month_a > $month_b)
			{
				return -1;
			}
			elseif($month_a < $month_b)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}

	foreach($arResult['CLIENTS'] as $k1 => &$arClient)
	{
		
		usort($arClient, "cmp4365");
	}
	// ------------- ------------------ ----------------
}

function checklistChecked($arChecklistProp, $arChecklistVals)
{
	if(!array_diff_key($arChecklistProp, $arChecklistVals) && !empty($arChecklistVals))
		return true;
	else
		return false;
}

// ������ � �������� �������� ��� ������ �� �������� � ������ �� ��������� ���������
// �������� ������ �� ������� ��������, ������� ���� � ������ $arrSelfCompany

$obSelfCompany = \Bitrix\Crm\CompanyTable::getList([
										'select'  => ['ID', 'TITLE'], 
										'filter'  => ['=IS_MY_COMPANY' => "Y"],
										'order'   => ['TITLE' => 'ASC']
									  ]);
									  
while ($SelfCompany = $obSelfCompany->fetch()) 
{
	$arResult["COMPANY"][$SelfCompany['ID']] = $SelfCompany['TITLE'];
}
$arResult["COMPANY"]['empty'] = '�� �����������';

//������� ���� ��������� ����� ������
$arrReportTypes = array();
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_CODE"=>"support_reports", "CODE"=>"REPORT_TYPE"));
while($enum_fields = $property_enums->GetNext())
{
  $arrReportTypes[$enum_fields["ID"]] = $enum_fields["XML_ID"];
}
$arResult['REPORT_TYPE_ALL'] = $arrReportTypes;