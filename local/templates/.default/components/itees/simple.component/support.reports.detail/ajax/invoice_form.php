<?
$ajax = !!$_POST['reportId'];
$ID = $_POST['reportId'] ? $_POST['reportId'] : $arResult['ID'];
$canEdit = $_POST['canEdit'] ? $_POST['canEdit'] : $arResult['CAN_EDIT_INVOICE'];
foreach ( $arResult['INVOICES'] as $inv ) {
	$curInv[] = "I_".$inv;
}
if ($arResult['INVOICE_ID']) $curInv[] = "I_".$arResult['INVOICE_ID'];
if ($_POST['company']) $curInv = [];
$company = $_POST['company'] ? $_POST['company'] : $arResult['PROPERTY_CLIENT_ID_VALUE'];

$inv = Array( 
	'ID' => "I_".$ID, 
	'ENTITY_ID' => 'INVOICE', 
	'FIELD_NAME' => 'INVOICE', 
	'USER_TYPE_ID' => 'crm', 
	'XML_ID' => '', 
	'SORT' => 100, 
	'MULTIPLE' => 'Y', 
	'MANDATORY' => 'N', 
	'SHOW_FILTER' => 'Y', 
	'SHOW_IN_LIST' => 'Y', 
	'EDIT_IN_LIST' => /*$canEdit*/'Y', 
	'IS_SEARCHABLE' => 'Y', 
	'SETTINGS' => Array(
		'INVOICE' => 'Y',
	),
	'EDIT_FORM_LABEL' => '�����',
	'LIST_COLUMN_LABEL' => '�����',
	'LIST_FILTER_LABEL' => '�����',
	'ERROR_MESSAGE' => '',
	'HELP_MESSAGE' => '',
	'USER_TYPE' => Array( 
		'USER_TYPE_ID' => 'report', 
		'CLASS_NAME' => 'CUserTypeCrm', 
		'DESCRIPTION' => '�������� � ������', 
		'BASE_TYPE' => 'string' 
	), 
	'VALUE' => array_unique($curInv), 
	'FIELD_NAME_ORIG' => 'INVOICE',
	'~FIELD_NAME' => 'INVOICE',
	'SAVE_BUTTON' => 'crm_save',
	'CANCEL_BUTTON' => 'crm_cancel',
	'COMPANY' => $company ? $company : 0,
	'GENERATED_INVOICE' => $arResult['INVOICE_ID'] ? $arResult['INVOICE_ID'] : 0
);

if ($ajax) {
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	GLOBAL $APPLICATION;
	$APPLICATION->RestartBuffer();
	CModule::IncludeModule("tasks");
}
if ($company) {
	\Bitrix\Tasks\Util\UserField\UI::showEdit($inv, array());
} else {
	echo "� ������ �� ��������� ��������";
}
if ($ajax) die();
?>
