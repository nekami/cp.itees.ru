<?
define('NO_KEEP_STATISTIC', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule("iblock");
CModule::IncludeModule("tasks");
CModule::IncludeModule("crm");

/*$dbRes = CCrmRole::GetRelation();
$perm = false; $error = false;
while($arRel = $dbRes->Fetch())
{
	if($arRel["ROLE_ID"] == 5)
	{
		$rel = substr($arRel["RELATION"], 0, 1);
		$id = substr($arRel["RELATION"], 1);
		if($rel == "G")
		{
			if(in_array($id, $GLOBALS["USER"]->GetUserGroupArray()))
			{
				$perm = true;
			}
		}
		elseif($rel == "U")
		{
			if($GLOBALS["USER"]->GetId() == $id)
			{
				$perm = true;
			}
		}
		elseif($rel == "D")
		{
			$res = CUser::GetById($GLOBALS["USER"]->GetId());
			if($arUser = $res->Fetch())
			{
				if(in_array($id, $arUser["UF_DEPARTMENT"]))
				{
					$perm = true;
				}
			}
		}
	}
}*/


// ---------------------------- ��������� ����� ����� �� ��������� ----------------------------------

$arGroups = false;
$res = CIBlockElement::GetList(Array(), Array('IBLOCK_CODE' => 'support groups', 'ACTIVE' => 'Y'), false, false, Array('CODE'));
while($arGroup = $res->GetNext())
{
	$groupId = intval($arGroup['CODE']);
	
	if($groupId > 0)
		$arGroups[] = $groupId;
}

// --------------------------------------------------------------------------------------------------




if($GLOBALS["USER"]->IsAuthorized() /*&& (in_array(21, $GLOBALS["USER"]->GetUserGroupArray()) || in_array(5, $GLOBALS["USER"]->GetUserGroupArray()) || in_array(9, $GLOBALS["USER"]->GetUserGroupArray()) || $perm == true) */&& !empty($_REQUEST["ID"]))
{
	$arRequestTasks = explode(",", $_REQUEST["ID"]);
	
	foreach($arRequestTasks as $k => $arReqTask)
	{
		if($arReqTask == "")
			unset($arRequestTasks[$k]);
	}
	
	if(!count($arRequestTasks))
		$arRequestTasks = false;
	
	$res1 = CTasks::GetList(
		Array(), 
		Array("GROUP_ID" => $arGroups, "ID" => $arRequestTasks, "CHECK_PERMISSIONS" => "N"),
		Array("ID", "CREATED_DATE", "PARENT_ID")
	);
	
	while($arTask = $res1->GetNext()) // ������ ���������� � ��������� � ������ ���������
	{
		if(intval($arTask["PARENT_ID"]) > 0)
		{
			$rep_id = 0;
			$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 59, "PROPERTY_TASK_ID" => $arTask["ID"]), false, false, Array("ID", "PROPERTY_TASK_ID"));
			if($arRes23 = $res->GetNext())
			{
				$rep_id = $arRes23['ID'];
			}
			
			$rep_id_parent = 0.1;
			$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 59, "PROPERTY_TASK_ID" => $arTask["PARENT_ID"]), false, false, Array("ID", "PROPERTY_TASK_ID"));
			if($arRes233 = $res->GetNext())
			{
				$rep_id_parent = $arRes233['ID'];
			}
			
			if($rep_id == $rep_id_parent)
				$arRequestTasks[] = $arTask["PARENT_ID"];
		}
	}
	
	$res1 = CTasks::GetList(
		Array(), 
		Array("GROUP_ID" => $arGroups, "ID" => $arRequestTasks, "CHECK_PERMISSIONS" => "N"),
		Array("ID", "CREATED_DATE")
	);
	//s($arRequestTasks);die();
	$arTasks = Array();
	while($arTask = $res1->GetNext()) // ������ ���������� � ��������� � ������ ���������
	{
		$el = new CIBlockElement;
		$PROP = array();
		$elId = false;
		
		$res2 = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "PROPERTY_task_id" => $arTask["ID"]), false, false, Array("ID"));
		if($arRes2 = $res2->GetNext())
		{
			CIBlockElement::Delete($arRes2["ID"]);
		}
			
		$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 59, "PROPERTY_TASK_ID" => $arTask["ID"]), false, false, Array("ID", "PROPERTY_TASK_ID"));
		if($arRes = $res->GetNext())
		{
			$elId = $arRes["ID"];
			foreach($arRes["PROPERTY_TASK_ID_VALUE"] as $task_id)
			{
				if(!in_array($task_id, $arRequestTasks))
					$PROP[288][]["VALUE"] = $task_id;
			}

			$arFields = Array();
			$obTask = new CTasks;
			$arFields["UF_REPORTS"] = Array('');
			if(!$obTask->Update($arTask["ID"], $arFields, array('USER_ID' => 1)))
				$error = true;
			
			$arLoadProductArray = Array(
				"MODIFIED_BY"    => $GLOBALS["USER"]->GetID(),
				"PROPERTY_VALUES"=> $PROP,
			);

			if(empty($PROP[288])) {
				CIBlockElement::Delete($elId);
			}
			else {
				CIBlockElement::SetPropertyValuesEx($elId, 59, array('TASK_ID' => $PROP[288]));
			}
			
			if(empty($PROP[288]))
				die("2");
		}
	}
}
else
{
	die("������ �������");
}

if(!$error)
	echo "1";
?>