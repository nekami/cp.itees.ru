<?
define('NO_KEEP_STATISTIC', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
global $USER;

if( isset($_POST['iblock_element_id']) && CModule::IncludeModule('crm') && CModule::IncludeModule('iblock')) {
	$elId = intval($_POST['iblock_element_id']);
	$arSelect = Array("PROPERTY_CLIENT_ID", "PROPERTY_TASK_ID", "CREATED_BY");
	$arFilter = Array("IBLOCK_ID"=>59, "ID"=>$elId);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	$ob = $res->GetNext();
	$tasks = unserialize($ob['~PROPERTY_TASK_ID_VALUE']);
	
	$obCompanys = CCrmCompany::GetListEx(
		array(),
		array('ID' => $ob['PROPERTY_CLIENT_ID_VALUE']),
		false,
		false,
		array('TITLE')
	);
	$arCompany = $obCompanys->GetNext(false, false);
	
	$crm = new CCrmDeal;
	$arCrmFields = array(
		"TITLE" => $arCompany['TITLE'].' - ��������� ����� - '.FormatDate("f Y", MakeTimeStamp(date('d.m.Y'))),
		"CLOSEDATE" => date('d.m.Y'),
		"STAGE_ID" => "C6:NEW",
		"OPPORTUNITY" => "",
		"COMPANY_ID" => $ob['PROPERTY_CLIENT_ID_VALUE'],
		"AUTHOR_ID" => $ob['CREATED_BY'],
		"CATEGORY_ID" => 6, //ID  ��������� ������� ���������
		"UF_REPORT_LINK2" => '##a##/itees/support_reports/reports/'.$elId.'/##a##', //��������� � ���������
		"UF_REPORT_LINK" => $elId, //��������� � ���������
		"UF_AUTO_GENERATED" => "Y" //���� ������������� ������
	);
	$crmResult = $crm->Add($arCrmFields, $bUpdateSearch = true, $options = array());
	$uf_fields = array('D_'.$crmResult); //id ������ ��� ������ � ���� ����� UF_CRM_TASK
	
	CIBlockElement::SetPropertyValuesEx($elId, 59, Array('DEAL_ID' => $crmResult));
	
	if (CModule::IncludeModule("tasks")) {
		$arTasks = array();
		foreach($tasks['VALUE'] as $task) {
			$oTaskItem = CTaskItem::getInstance($task, $USER->getId());
			$arUfFields = array(
				"UF_CRM_TASK" => array_merge($oTaskItem['UF_CRM_TASK'], $uf_fields)
			);
			$oTaskItem->update($arUfFields);
		}
	}
}
?>