<?
define('STOP_STATISTICS', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
global $DB, $APPLICATION, $USER;
if(!function_exists('__CrmInvoiceListEndResponse'))
{
	function __CrmInvoiceListEndResponse($result)
	{
		$GLOBALS['APPLICATION']->RestartBuffer();
		header('Content-Type: application/x-javascript; charset='.LANG_CHARSET);
		if(!empty($result))
		{
			echo CUtil::PhpToJSObject($result);
		}
		require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
		die();
	}
}

if (!CModule::IncludeModule('crm'))
{
	return;
}

$userPerms = CCrmPerms::GetCurrentUserPermissions();
if(!CCrmPerms::IsAuthorized())
{
	return;
}

$action = isset($_REQUEST['ACTION']) ? $_REQUEST['ACTION'] : '';
$companyId = isset($_REQUEST['COMPANY']) ? intval($_REQUEST['COMPANY']) : 0;
if (isset($_REQUEST['MODE']) && $_REQUEST['MODE'] === 'SEARCH')
{
	if($userPerms->HavePerm('INVOICE', BX_CRM_PERM_NONE, 'READ'))
	{
		return;
	}

	\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

	CUtil::JSPostUnescape();
	$APPLICATION->RestartBuffer();

	// Limit count of items to be found
	$nPageTop = 50;		// 50 items by default
	if (isset($_REQUEST['LIMIT_COUNT']) && ($_REQUEST['LIMIT_COUNT'] >= 0))
	{
		$rawNPageTop = (int) $_REQUEST['LIMIT_COUNT'];
		if ($rawNPageTop === 0)
			$nPageTop = false;		// don't limit
		elseif ($rawNPageTop > 0)
			$nPageTop = $rawNPageTop;
	}

	$search = trim($_REQUEST['VALUE']);
	$multi = isset($_REQUEST['MULTI']) && $_REQUEST['MULTI'] == 'Y'? true: false;
	$arFilter = array('UF_COMPANY_ID' => $companyId);
	if ($_REQUEST['SELECTED']) $arFilter["!ID"] = $_REQUEST['SELECTED'];
	if (is_numeric($search))
		$arFilter['ID'] = (int) $search;
	else if (preg_match('/(.*)\[(\d+?)\]/i'.BX_UTF_PCRE_MODIFIER, $search, $arMatches))
	{
		$arFilter['ID'] = (int) $arMatches[2];
		$arFilter['%ORDER_TOPIC'] = trim($arMatches[1]);
		$arFilter['LOGIC'] = 'OR';
	}
	else
		$arFilter['%ORDER_TOPIC'] = $search;

	$arInvoiceStatusList = CCrmStatus::GetStatusListEx('INVOICE_STATUS');
	$arSelect = array('ID', 'ORDER_TOPIC', 'STATUS_ID', 'PRICE', 'ACCOUNT_NUMBER', 'SUM_PAID');
	$arOrder = array('ORDER_TOPIC' => 'ASC');
	$arData = array();
	$obRes = CCrmInvoice::GetList($arOrder, $arFilter, false, (intval($nPageTop) > 0) ? array('nTopCount' => $nPageTop) : false, $arSelect);
	$arFiles = array();
	while ($arRes = $obRes->Fetch())
	{
		$arData[] =
			array(
				'id' => $multi? 'I_'.$arRes['ID']: $arRes['ID'],
				'url' => CComponentEngine::MakePathFromTemplate(COption::GetOptionString('crm', 'path_to_invoice_show'),
					array(
						'invoice_id' => $arRes['ID']
					)
				),
				'title' => $arRes['ACCOUNT_NUMBER'],
				'desc' => $arRes['ORDER_TOPIC'],
				'type' => 'invoice',
				'statuses' => CCrmViewHelper::RenderInvoiceStatusControl(
					array(
						'PREFIX' => "{$arRes['ID']}_PROGRESS_BAR_",
						'ENTITY_ID' => $arRes['ID'],
						'CURRENT_ID' => $arRes['STATUS_ID'],
						'SERVICE_URL' => '/bitrix/components/bitrix/crm.invoice.list/list.ajax.php',
						'READ_ONLY' => true
					)
				),
				'price' => $arRes['PRICE'],
				'paid' => $arRes['SUM_PAID'],
				'deletable' => $arRes['ID'] === $generaitedInvoice ? "N" : "",
			)
		;
	}

	__CrmInvoiceListEndResponse($arData);
}
?>