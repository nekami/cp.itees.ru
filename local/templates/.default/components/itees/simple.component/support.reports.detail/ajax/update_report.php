<?
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC","Y");
define("NO_AGENT_CHECK", true);
define("DisableEventsCheck", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (\Bitrix\Main\Loader::includeModule("iblock"))
{
	if ( isset($_POST['elementId']) && $_POST['update'] === "Y" ) {
		$ELEMENT_ID = $_POST['elementId'];
		
		$reportType = $_POST['reportType'];
		if ($reportType) {
			CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array('REPORT_TYPE'=>$reportType));
			echo json_encode(array("el"=>$ELEMENT_ID, "REPORT_TYPE"=>$reportType));
			die();
		}
		
		$updateCRM = $_POST['updateCRM'];
		$ownComp = $_POST['curComp'];
		$respons = [];
		$propArr = Array();
		
		foreach ($_POST['data']['contact'] as  $cont) {
			$crmContacts[] = substr($cont['id'], 2);
		}
		
		if ($_POST['updateInvoice'] === "Y") {
			foreach ($_POST['crmInvoice'] as $inv) {
				$crmInvoice[] = $inv['id'];
			}
			$propArr['INVOICES'] = $crmInvoice ? $crmInvoice : false;
		}
		
		if ($updateCRM != "false") {
			$propArr['CLIENT_ID'] = substr($_POST['data']['company'][0]['id'], 3);
			$propArr['CRM_CONTACTS'] = $crmContacts ? $crmContacts : false;
		}
		
		if (isset($ownComp)) $propArr['SELF_COMPANY'] = $respons['own_company'] = $ownComp; 
		
		if ( $_POST['updateDeal'] === "Y" && $updateCRM != "false" ) {
			if (\Bitrix\Main\Loader::includeModule('crm')) 
			{
				$entity = new CCrmDeal(false);//true - проверять права на доступ
				
				$nul = array('UF_REPORT_LINK' => "");
				$res = CCrmDeal::GetList(array(),array('UF_REPORT_LINK' => $ELEMENT_ID),array('ID'),false);
				while($ar_res = $res->GetNext()) {
					$entity->update($ar_res['ID'], $nul);
				}
				
				if (isset($_POST['data']['deal'])) $propArr['DEAL_ID'] = substr($_POST['data']['deal'][0]['id'], 2);
				$dealId = $propArr['DEAL_ID'] ? $ELEMENT_ID  : "";
				
				$fields = array( 
					'UF_REPORT_LINK' => $dealId
				); 
				$entity->update($propArr['DEAL_ID'], $fields); 
			}
		}
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, $propArr);
	}$respons['props'] = $propArr;
}
echo json_encode($respons);
?>