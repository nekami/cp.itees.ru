<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array('jquery'));
global $APPLICATION;

$APPLICATION->SetAdditionalCSS('/bitrix/js/crm/css/crm.css');
$APPLICATION->AddHeadScript('/bitrix/js/crm/crm.js');

if(!empty($arResult['CLIENT_URL']))
{
	$title = FormatDate("f", MakeTimeStamp($arResult['REPORT_MONTH']))." ".date('Y', MakeTimeStamp($arResult['REPORT_MONTH']))." - ".$arResult['CLIENT_URL'];
	?>
	<script>
		$('h1').html('<?=$title?>');
	</script>
	<?
}

if(!empty($arResult['CLIENT_ASSIGNED']))
{
	?>
	<script>
		$('.pagetitle-content-topEnd .pagetitle-content-topEnd-corn')
			.html('���������, ������������� �� �������: <?=$arResult['CLIENT_ASSIGNED']?>');
		$('.pagetitle-content-topEnd, .pagetitle-content-topEnd-corn').show();
	</script>
	<?
}

$APPLICATION->SetTitle($arResult["NAME"]);
?>