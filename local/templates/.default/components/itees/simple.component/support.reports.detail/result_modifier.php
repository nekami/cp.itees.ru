<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

include("invoice_add.php");

global $APPLICATION;

//if(isset($_REQUEST["id"]))
//	$report_id = $_REQUEST["id"];

if(isset($_REQUEST["report_id"]))
	$report_id = $_REQUEST["report_id"];

if(isset($report_id) && $report_id > 0 && $_REQUEST["set_option"] == 1)
{
	if(isset($_REQUEST["price_x"]))
	{
		CIBlockElement::SetPropertyValuesEx(intval($report_id), 59, Array('SHOW_PRICE' => 4569));
	}
	else
	{
		CIBlockElement::SetPropertyValuesEx(intval($report_id), 59, Array('SHOW_PRICE' => false));
	}
	
	if(isset($_REQUEST["hour_price_x"]))
	{
		CIBlockElement::SetPropertyValuesEx(intval($report_id), 59, Array('SHOW_HOUR_PRICE' => 4570));
	}
	else
	{
		CIBlockElement::SetPropertyValuesEx(intval($report_id), 59, Array('SHOW_HOUR_PRICE' => false));
	}
}

if(isset($report_id) && $report_id > 0 && isset($_REQUEST["check"]))
{
	$db_props = CIBlockElement::GetProperty(59, intval($report_id), array("sort" => "asc"), Array("CODE" => "CHECKLIST_USER"));
	while($arProp = $db_props->Fetch())
	{
		if(!empty($arProp['VALUE']))
			$arPropChecklistUser[$arProp['VALUE']] = $arProp['DESCRIPTION'];
	}
	
	$arChecklistPropIds = array();
	
	$db_props = CIBlockElement::GetProperty(59, intval($report_id), array("sort" => "asc"), Array("CODE" => "CHECKLIST"));
	while($arProp = $db_props->Fetch())
	{
		if(!empty($arProp['VALUE']))
			$arPropChecklist[] = $arProp['VALUE'];
		
		$arChecklistPropIds[] = $arProp['ID'];
	}

	$arChecklistUser = array_combine($_REQUEST["checklist_user_descr"], $_REQUEST["checklist_user"]);

	/* $arChecklistVals = array();
	
	$res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 59, "ID" => intval($report_id)), false, false, array("ID", "PROPERTY_CHECKLIST"));
	if($arElem = $res->GetNext(false, false))
	{
		$arChecklistVals = array_keys($arElem['PROPERTY_CHECKLIST_VALUE']);
	}
	
	$difRes = array_diff($arChecklistPropIds, $arChecklistVals);
	
	if(!empty($difRes))
	{ */
		foreach($arChecklistUser as $key => $user_id)
		{
			if((in_array($key, $_REQUEST["checklist"]) && !in_array($key, $arPropChecklist)) || (!in_array($key, $_REQUEST["checklist"]) && in_array($key, $arPropChecklist)))
			{
				$arChecklistUserSave[] = array("VALUE" => $key, "DESCRIPTION" => $user_id);
			}
			elseif(!empty($arPropChecklistUser[$key]))
			{ 
				$arChecklistUserSave[] = array("VALUE" => $key, "DESCRIPTION" => $arPropChecklistUser[$key]);
			}
		}

		CIBlockElement::SetPropertyValuesEx(intval($report_id), false, array("CHECKLIST" => $_REQUEST["checklist"], "CHECKLIST_USER" => $arChecklistUserSave));
	/* } */
}

	// ---------------------------- ��������� ����� ����� �� ��������� ----------------------------------
	
$arGroups = false;
$res = CIBlockElement::GetList(Array(), Array('IBLOCK_CODE' => 'support groups', 'ACTIVE' => 'Y'), false, false, Array('CODE'));
while($arGroup = $res->GetNext(false, false))
{
	$groupId = intval($arGroup['CODE']);
	
	if($groupId > 0)
		$arGroups[] = $groupId;
}

$arProjectGroups = GetProjectGroups();

$arGroups = array_merge($arGroups, $arProjectGroups);
	
	// --------------------------------------------------------------------------------------------------
$res = CIBlockElement::GetList(
	Array(),
	Array("IBLOCK_ID" => 59, "ID" => intval($report_id)),
	false,
	false,
	Array(
		"ID",
		"CODE",
		"NAME",
		"CREATED_BY",
		"PROPERTY_REPORT_DATE",
		"PROPERTY_ORDER_ID",
		"PROPERTY_SHOW_ZERO_TIME_TASKS",
		"PROPERTY_SHOW_PRICE",
		"PROPERTY_SHOW_HOUR_PRICE",
		"PROPERTY_ACT",
		"PROPERTY_PAYED",
		"PROPERTY_PAPER",
		"PROPERTY_PAPER_RETURNS",
		"PROPERTY_PAPER_DATE",
		"PROPERTY_CHECKLIST",
		"PROPERTY_CHECKLIST_USER",
		"PROPERTY_REPORT_PASS_CLIENT",
		"PROPERTY_INVOICE_ID",
		"PROPERTY_SEND_ADO",
		"PROPERTY_SEND_ADO_DATE",
		"PROPERTY_TRACK_NUMBER",
		"PROPERTY_REPORT_NAME",
		"PROPERTY_REPORT_DATE",
		"PROPERTY_PREPAYMENT",
		"PROPERTY_INVOICES",
		"PROPERTY_CRM_CONTACTS",
		"PROPERTY_SELF_COMPANY",
		"PROPERTY_REPORT_TYPE"
	)
);
while($ar_fields1 = $res->GetNext(false, false))
{
	$PREPAYMENT = $ar_fields1["PROPERTY_PREPAYMENT_VALUE"];
	$INVOICES = $ar_fields1["PROPERTY_INVOICES_VALUE"];
	
	$SHOW_PRICE = ($ar_fields1["PROPERTY_SHOW_PRICE_VALUE"] == 'Y')?true:false;
	$SHOW_HOUR_PRICE = ($ar_fields1["PROPERTY_SHOW_HOUR_PRICE_VALUE"] == 'Y')?true:false;

	$NAME = $ar_fields1["PROPERTY_REPORT_NAME_VALUE"];
	
	$report_date = $ar_fields1["PROPERTY_REPORT_DATE_VALUE"];
	if(empty($report_date))
	{
		if(preg_match("/.* ([0-9]{1,2})\.([0-9]{4})/", $ar_fields1["CODE"], $arDateMatches))
		{
			$month = $arDateMatches[1];
			if(strlen($month) == 1)
				$month = "0".$month;
			$year = $arDateMatches[2];
			$ts = MakeTimeStamp("01.".$month.".".$year, "DD.MM.YYYY");
			$days = date("t", $ts);
			if(strlen($days) == 1)
				$days = "0".$days;

			$report_date = $days.".".$month.".".$year;
		}
	}
	
	$ORDER_ID = $ar_fields1["PROPERTY_ORDER_ID_VALUE"];
	$PAYED = ($ar_fields1["PROPERTY_PAYED_VALUE"] == 'Y')?true:false;
	$PAPER = ($ar_fields1["PROPERTY_PAPER_VALUE"] == 'Y')?true:false;
	$PAPER_RETURNS = ($ar_fields1["PROPERTY_PAPER_RETURNS_VALUE"] == 'Y')?true:false;
	$PAPER_DATE = $ar_fields1["PROPERTY_PAPER_DATE_VALUE"];
	$ACT = $ar_fields1["PROPERTY_ACT_VALUE"];

	$SHOW_ZERO_TIME_TASKS = $ar_fields1["PROPERTY_SHOW_ZERO_TIME_TASKS_VALUE"];
	
	$AR_CHECKLIST_VALS = $ar_fields1['PROPERTY_CHECKLIST_VALUE'];

	$CREATOR = $ar_fields1['CREATED_BY'];
	
	$CHECKLIST_USER = array(
		'VALUE' => $ar_fields1['PROPERTY_CHECKLIST_USER_VALUE'],
		'USER' => $ar_fields1['PROPERTY_CHECKLIST_USER_DESCRIPTION']
	);
	
	$REPORT_PASS_CLIENT = $ar_fields1['PROPERTY_REPORT_PASS_CLIENT_VALUE'];
	$INVOICE_ID = $ar_fields1['PROPERTY_INVOICE_ID_VALUE'];
	
	$SEND_ADO = $ar_fields1['PROPERTY_SEND_ADO_VALUE'];
	$SEND_ADO_DATE = $ar_fields1['PROPERTY_SEND_ADO_DATE_VALUE'];
	$TRACK_NUMBER = $ar_fields1['PROPERTY_TRACK_NUMBER_VALUE'];
	
	$REPORT_MONTH = $ar_fields1['PROPERTY_REPORT_DATE_VALUE'];
	$OWN_COMPANY = $ar_fields1['PROPERTY_SELF_COMPANY_VALUE'];
	$REPORT_TYPE = $ar_fields1['PROPERTY_REPORT_TYPE_ENUM_ID'];
}

if(isset($_REQUEST["report-name-new"]))
{
	if(trim(mb_convert_encoding($_REQUEST["report-name-new"], LANG_CHARSET, "auto")) != $NAME)
	{	
		$NAME = htmlspecialcharsbx(trim(mb_convert_encoding($_REQUEST["report-name-new"], 'CP1251', 'UTF-8')));	
		CIBlockElement::SetPropertyValuesEx(intval($report_id), 59, Array('REPORT_NAME' => $NAME));
	}
}

if(isset($_REQUEST["report_date"]))
{
	$ELEMENT_ID = intval($report_id);
	CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("REPORT_DATE" => $_REQUEST["report_date"]));
	$report_date = htmlspecialcharsbx($_REQUEST["report_date"]);
}

if(isset($_REQUEST["order_id"]))
{
	$ELEMENT_ID = intval($report_id);
	$order_id = mb_convert_encoding($_REQUEST["order_id"], LANG_CHARSET, "auto");
	CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("ORDER_ID" => $order_id));
	$ORDER_ID = htmlspecialcharsbx($order_id);
}

if(isset($_REQUEST["client_work"]))
{
	$ELEMENT_ID = intval($report_id);
	if($_REQUEST["payed"])
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("PAYED" => 4612));
		$PAYED = true;
	}
	else
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("PAYED" => false));
		$PAYED = false;
	}
	if($_REQUEST["paper"])
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("PAPER" => 4613));
		$PAPER = true;
	}
	else
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("PAPER" => false));
		$PAPER = false;
	}
	if($_REQUEST["paper_returns"])
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("PAPER_RETURNS" => 4614));
		$PAPER_RETURNS = true;
	}
	else
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("PAPER_RETURNS" => false));
		$PAPER_RETURNS = false;
	}
	
	if(isset($_REQUEST["paper_date"]))
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("PAPER_DATE" => $_REQUEST["paper_date"]));
		$PAPER_DATE = htmlspecialcharsbx($_REQUEST["paper_date"]);
	}
	
	
	if($_REQUEST["send_ado"])
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("SEND_ADO" => 5732));
		$SEND_ADO = true;
	}
	else
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("SEND_ADO" => false));
		$SEND_ADO = false;
		unset($_REQUEST["send_ado_date"]);
	}
	
	if($_REQUEST["send_ado_date"])
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("SEND_ADO_DATE" => $_REQUEST["send_ado_date"]));
		$SEND_ADO_DATE = $_REQUEST["send_ado_date"];
	}
	else
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("SEND_ADO_DATE" => false));
		$SEND_ADO_DATE = false;
	}
	
	if($_REQUEST["track_number"])
	{
		if(strlen(trim($_REQUEST["track_number"])) == 14)
		{
			CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("TRACK_NUMBER" => $_REQUEST["track_number"]));
			$TRACK_NUMBER = $_REQUEST["track_number"];
		}
	}
	else
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("TRACK_NUMBER" => false));
		$TRACK_NUMBER = false;
	}
	
	
	if($_REQUEST["prepayment"])
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("PREPAYMENT" => 5733));
		$PREPAYMENT = true;
	}
	else
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("PREPAYMENT" => false));
		$PREPAYMENT = false;
		unset($_REQUEST["invoice_id"]);
	}
	
	if($_REQUEST["invoice_id"])
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("INVOICES" => $_REQUEST["invoice_id"]));
		$INVOICES = $_REQUEST["invoice_id"];
	}
	else
	{
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("INVOICES" => false));
		$INVOICES = false;
	}
	
}

if(isset($_REQUEST["act"]))
{
	$ELEMENT_ID = intval($report_id);
	$act = mb_convert_encoding($_REQUEST["act"], LANG_CHARSET, "auto");
	CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("ACT" => $act));
	$ACT = htmlspecialcharsbx($act);
}

if(isset($_REQUEST["zero_time"]))
{
	$ELEMENT_ID = intval($report_id);
	CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("SHOW_ZERO_TIME_TASKS" => 4549));
	$SHOW_ZERO_TIME_TASKS = '��';
}
elseif(isset($_REQUEST["report_option"]))
{
	$ELEMENT_ID = intval($report_id);
	CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("SHOW_ZERO_TIME_TASKS" => 0));
	$SHOW_ZERO_TIME_TASKS = false;
}
	
if(isset($_POST["ajax"]))
{
	foreach ($_POST as $name => $value)
	{
		if(!is_array($value))
		{
			$_REQUEST[$name] = mb_convert_encoding($value, LANG_CHARSET, "auto");
			$_POST[$name] = $_REQUEST[$name];
		}
		else
		{
			convert_custom($_POST[$name]);
			convert_custom($_REQUEST[$name]);
		}
	}
}

foreach($_POST["task"]["new"] as $k => $val)
{
	if($_REQUEST["task"]["new"][$k] == '' && 
	$_REQUEST["date_start"]["new"][$k] == '' && 
	$_REQUEST["date_close"]["new"][$k] == '' &&
	$_REQUEST["time_h"]["new"][$k] == '' &&
	$_REQUEST["time_m"]["new"][$k] == '' &&
	$_REQUEST["price"]["new"][$k] == '' &&
	$_REQUEST["hour_price"]["new"][$k] == '' &&
	$_REQUEST["comment"]["new"][$k] == ''
	)
	{
		unset($_REQUEST["task"]["new"][$k]);
		unset($_REQUEST["date_start"]["new"][$k]);
		unset($_REQUEST["date_close"]["new"][$k]);
		unset($_REQUEST["time_h"]["new"][$k]);
		unset($_REQUEST["time_m"]["new"][$k]);
		unset($_REQUEST["price"]["new"][$k]);
		unset($_REQUEST["hour_price"]["new"][$k]);
		unset($_REQUEST["comment"]["new"][$k]);
		unset($_REQUEST["counter"]["new"][$k]);
		unset($_REQUEST["garant"]["new"][$k]);
		
		unset($_POST["task"]["new"][$k]);
		unset($_POST["date_start"]["new"][$k]);
		unset($_POST["date_close"]["new"][$k]);
		unset($_POST["time_h"]["new"][$k]);
		unset($_POST["time_m"]["new"][$k]);
		unset($_POST["price"]["new"][$k]);
		unset($_POST["hour_price"]["new"][$k]);
		unset($_POST["comment"]["new"][$k]);
		unset($_POST["counter"]["new"][$k]);
		unset($_POST["garant"]["new"][$k]);
	}
}

function convert_custom(&$ar)
{
	foreach ($ar as $name => $value)
	{
		if(!is_array($value))
		{
			$ar[$name] = mb_convert_encoding($value, LANG_CHARSET, "auto");
		}
		else
		{
			convert_custom($ar[$name]);
		}
	}
}

$APPLICATION->set_cookie("show_menu", 1, time()+60*60*24*30*12*10);

if($APPLICATION->get_cookie("price") === "")
{
	$APPLICATION->set_cookie("price", 1, time()+60*60*24*30*12*10);
}

if(isset($_REQUEST["clear"]))
{
	$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "PROPERTY_report" => $report_id), false, false, Array("ID"));
	while($ar_fields = $res->GetNext(false, false))
	{
		CIBlockElement::Delete($ar_fields["ID"]);
	}
	
	LocalRedirect($APPLICATION->GetCurPageParam());
	die();
}

$res = CIBlockElement::GetList(Array(), Array('IBLOCK_ID' => 66, "ACTIVE" => 'Y'), false, false, Array("ID", "PROPERTY_RATE_CHANGE_HOURS"));
while($arFields = $res->Fetch())
{
	$arGrades[$arFields['ID']] = $arFields;
}

$rsUsers = CUser::GetList(($by="id"), ($order="desc"), Array("ACTIVE" => 'Y'), Array("FIELDS" => Array("ID", "NAME", "LAST_NAME")));
while($arUser = $rsUsers->Fetch())
{
	/* if(!empty($report_id) && !empty($report_date))
		$gradeDate = $report_date;
	else
		$gradeDate = date('d.m.Y');

	$userHistGrade = GetUserGrade($arUser['ID'], $gradeDate);

	if(!empty($userHistGrade))
		$arUser['TIME_FACTOR'] = $arGrades[$userHistGrade]['PROPERTY_RATE_CHANGE_HOURS_VALUE'] / 100;
	else
		$arUser['TIME_FACTOR'] = 1;
		
	if(empty($arUser['TIME_FACTOR']))
		$arUser['TIME_FACTOR'] = $arGrades[$arUser['UF_GRADE_ID']]['PROPERTY_RATE_CHANGE_HOURS_VALUE'] / 100; */

	$arUser['TIME_FACTOR'] = GetUserGradeTimeFactor($arUser['ID'], $gradeDate);
		
	$arUsers[$arUser['ID']] = $arUser;
}

if(isset($_REQUEST["save"]) || isset($_REQUEST["print"]))
{
	if(isset($_REQUEST["garant_time_h"]) || isset($_REQUEST["garant_time_m"]))
	{
		$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "CODE" => "garant_time", "PROPERTY_report" => $report_id), false, false, Array("ID"));
		if($ar_fields = $res->GetNext(false, false))
		{
			$el = new CIBlockElement;

			$elapsed_time = $_REQUEST["garant_time_h"]*60 + $_REQUEST["garant_time_m"];
			$counter =  $_REQUEST["garant_time_sort"];
			
			$PROP = array();
			$PROP["report"] = $report_id;
			$PROP["elapsed_time"] = $elapsed_time;

			$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(),
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID"      => 62,
				"PROPERTY_VALUES"=> $PROP,
				"NAME"           => "����� �� ����������� �������",
				"ACTIVE"         => "Y",
				"SORT"					 => $counter,
				"CODE" => "garant_time"
				);
			$el->Update($ar_fields["ID"], $arLoadProductArray);
		}
		else
		{
			$el = new CIBlockElement;

			$elapsed_time = $_REQUEST["garant_time_h"]*60 + $_REQUEST["garant_time_m"];
			$counter =  $_REQUEST["garant_time_sort"];
			
			$PROP = array();
			$PROP["report"] = $report_id;
			$PROP["elapsed_time"] = $elapsed_time;

			$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(),
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID"      => 62,
				"PROPERTY_VALUES"=> $PROP,
				"NAME"           => "����� �� ����������� �������",
				"ACTIVE"         => "Y",
				"SORT"					 => $counter,
				"CODE" => "garant_time"
				);
			$el->Add($arLoadProductArray);
		}
	}
	
	
	
	foreach($_REQUEST["task"] as $k => $val)
	{
		if($k != 'new')
		{
			if(preg_match("/^n_/", $k))
			{
				$k = preg_replace("/^n_/", "", $k);
				$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "ID" => $k, "PROPERTY_report" => $report_id), false, false, Array("ID"));
				if($ar_fields = $res->GetNext(false, false))
				{
					$el = new CIBlockElement;

					$date_create = $_REQUEST["date_start"]["n_".$k];
					$date_close = $_REQUEST["date_close"]["n_".$k];
					$elapsed_time = intval($_REQUEST["time_h"]["n_".$k]) * 60 + intval($_REQUEST["time_m"]["n_".$k]);
					$estimate_time = intval($_REQUEST["estimate_time_h"]["n_".$k]) * 60 + intval($_REQUEST["estimate_time_m"]["n_".$k]);
					$grade_time = intval($_REQUEST["grade_time_h"]["n_".$k]) * 60 + intval($_REQUEST["grade_time_m"]["n_".$k]);
					$real_time = intval($_REQUEST["real_time_h"]["n_".$k]) * 60 + intval($_REQUEST["real_time_m"]["n_".$k]);
					$counter =  round(floatval($_REQUEST["counter"]["n_".$k]), 2) * 100;
					
					$PROP = array();
					$PROP["report"] = $report_id;
					$PROP["task_id"] = "0";
					$PROP["date_create"] = $date_create;
					$PROP["date_close"] = $date_close;
					$PROP["task_name"] = "";
					$PROP["task_name_print"] = $_REQUEST["task"]["n_".$k];
					$PROP["elapsed_time"] = $elapsed_time;
					$PROP["estimate_time"] = $estimate_time;
					$PROP["grade_time"] = $grade_time;
					$PROP["real_time"] = $real_time;
					$PROP["comment"] = $_REQUEST["comment"]["n_".$k];
					$PROP["price"] = $_REQUEST["price"]["n_".$k];
					$PROP["garant"] = $_REQUEST["garant"]["n_".$k];
					$PROP["hour_price"] = $_REQUEST["hour_price"]["n_".$k];

					$arLoadProductArray = Array(
						"MODIFIED_BY"    => $USER->GetID(),
						"IBLOCK_SECTION_ID" => false,
						"IBLOCK_ID"      => 62,
						"PROPERTY_VALUES"=> $PROP,
						"NAME"           => "New! ".$_REQUEST["task"]["n_".$k],
						"ACTIVE"         => "Y",
						"SORT"					 => $counter
						);
					$el->Update($ar_fields["ID"], $arLoadProductArray);
				}
			}
			else
			{
				$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "PROPERTY_task_id" => $k, "PROPERTY_report" => $report_id), false, false, Array("ID"));
				if($ar_fields = $res->GetNext(false, false))
				{
					$el = new CIBlockElement;
					$date_create = $_REQUEST["date_start"][$k];
					$date_close = $_REQUEST["date_close"][$k];
					$elapsed_time = intval($_REQUEST["time_h"][$k]) * 60 + intval($_REQUEST["time_m"][$k]);
					$counter =  round(floatval($_REQUEST["counter"][$k]), 2) * 100;
					
					$PROP = array();
					$PROP["report"] = $report_id;
					$PROP["task_id"] = $k;
					$PROP["date_create"] = $date_create;
					$PROP["date_close"] = $date_close;
					$PROP["task_name"] = $_REQUEST["orig_task"][$k];
					$PROP["task_name_print"] = $_REQUEST["task"][$k];
					$PROP["elapsed_time"] = $elapsed_time;
					$PROP["comment"] = $_REQUEST["comment"][$k];
					$PROP["garant"] = $_REQUEST["garant"][$k];
					$PROP["price"] = $_REQUEST["price"][$k];
					$PROP["hour_price"] = $_REQUEST["hour_price"][$k];

					$arLoadProductArray = Array(
						"MODIFIED_BY"    => $USER->GetID(),
						"IBLOCK_SECTION_ID" => false,
						"IBLOCK_ID"      => 62,
						"PROPERTY_VALUES"=> $PROP,
						"NAME"           => $_REQUEST["orig_task"][$k],
						"ACTIVE"         => ($_REQUEST["hidden"][$k] == "Y") ? "N" : "Y",
						"SORT"					 => $counter
						);

					$el->Update($ar_fields["ID"], $arLoadProductArray);
					
					
				}
				else
				{
					$el = new CIBlockElement;

					$date_create = $_REQUEST["date_start"][$k];
					$date_close = $_REQUEST["date_close"][$k];
					$elapsed_time = intval($_REQUEST["time_h"][$k]) * 60 + intval($_REQUEST["time_m"][$k]);
					$counter =  round(floatval($_REQUEST["counter"][$k]), 2) * 100;
					
					$PROP = array();
					$PROP["report"] = $report_id;
					$PROP["task_id"] = $k;
					$PROP["date_create"] = $date_create;
					$PROP["date_close"] = $date_close;
					$PROP["task_name"] = $_REQUEST["orig_task"][$k];
					$PROP["task_name_print"] = $_REQUEST["task"][$k];
					$PROP["elapsed_time"] = $elapsed_time;
					$PROP["comment"] = $_REQUEST["comment"][$k];
					$PROP["garant"] = $_REQUEST["garant"][$k];
					$PROP["price"] = $_REQUEST["price"][$k];
					$PROP["hour_price"] = $_REQUEST["hour_price"][$k];

					$arLoadProductArray = Array(
						"MODIFIED_BY"    => $USER->GetID(),
						"IBLOCK_SECTION_ID" => false,
						"IBLOCK_ID"      => 62,
						"PROPERTY_VALUES"=> $PROP,
						"NAME"           => $_REQUEST["orig_task"][$k],
						"ACTIVE"         => ($_REQUEST["hidden"][$k] == "Y") ? "N" : "Y",
						"SORT"					 => $counter
						);

					$el->Add($arLoadProductArray);
					
				}
			}
		}
		else
		{
			foreach($val as $k1 => $val1)
			{
				$el = new CIBlockElement;
				
				$date_create = $_REQUEST["date_start"]["new"][$k1];
				$date_close = $_REQUEST["date_close"]["new"][$k1];
				$elapsed_time = intval($_REQUEST["time_h"]["new"][$k1]) * 60 + intval($_REQUEST["time_m"]["new"][$k1]);
				$counter =  floatval($_REQUEST["counter"]["new"][$k1]) * 100;
				
				$PROP = array();
				$PROP["report"] = $report_id;
				$PROP["task_id"] = "0";
				$PROP["date_create"] = $date_create;
				$PROP["date_close"] = $date_close;
				$PROP["task_name"] = "";
				$PROP["task_name_print"] = $_REQUEST["task"]["new"][$k1];
				$PROP["elapsed_time"] = $elapsed_time;
				$PROP["comment"] = $_REQUEST["comment"]["new"][$k1];
				$PROP["garant"] = $_REQUEST["garant"]["new"][$k1];
				$PROP["price"] = $_REQUEST["price"]["new"][$k1];
				$PROP["hour_price"] = $_REQUEST["hour_price"]["new"][$k1];

				$arLoadProductArray = Array(
					"MODIFIED_BY"    => $USER->GetID(),
					"IBLOCK_SECTION_ID" => false,
					"IBLOCK_ID"      => 62,
					"PROPERTY_VALUES"=> $PROP,
					"NAME"           => "New! ".$_REQUEST["task"]["new"][$k1],
					"ACTIVE"         => "Y",
					"SORT"					 => $counter
					);
				
				
				$el->Add($arLoadProductArray);
			}
		}
	}
	
	if(isset($_REQUEST["nogarant_time_h"]) || isset($_REQUEST["nogarant_time_m"]))
	{
		$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "CODE" => "nogarant_time", "PROPERTY_report" => $report_id), false, false, Array("ID"));
		if($ar_fields = $res->GetNext(false, false))
		{
			$el = new CIBlockElement;

			$elapsed_time = $_REQUEST["nogarant_time_h"]*60 + $_REQUEST["nogarant_time_m"];
			$counter =  $_REQUEST["nogarant_time_sort"];
			
			$PROP = array();
			$PROP["report"] = $report_id;
			$PROP["elapsed_time"] = $elapsed_time;
			$PROP["price"] = $_REQUEST["nogarant_summ"];
			
			if($_REQUEST["summ_changed_by_hands"] == 'Y')
				$PROP["summ_changed_by_hands"] = 4548;
			else
			//if(intval($_REQUEST["nogarant_summ"]) == 0)
				$PROP["summ_changed_by_hands"] = false;
				
			if($PROP["price"] == 0)
				$PROP["summ_changed_by_hands"] = false;

			$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(),
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID"      => 62,
				"PROPERTY_VALUES"=> $PROP,
				"NAME"           => "����� �� ������������� �������",
				"ACTIVE"         => "Y",
				"SORT"					 => $counter,
				"CODE" => "nogarant_time"
				);
			$el->Update($ar_fields["ID"], $arLoadProductArray);
		}
		else
		{
			$el = new CIBlockElement;

			$elapsed_time = $_REQUEST["nogarant_time_h"]*60 + $_REQUEST["nogarant_time_m"];
			$counter =  $_REQUEST["nogarant_time_sort"];
			
			$PROP = array();
			$PROP["report"] = $report_id;
			$PROP["elapsed_time"] = $elapsed_time;
			$PROP["price"] = $_REQUEST["nogarant_summ"];
			
			if($_REQUEST["summ_changed_by_hands"] == 'Y')
				$PROP["summ_changed_by_hands"] = 4548;
			else
			//if(intval($_REQUEST["nogarant_summ"]) == 0)
				$PROP["summ_changed_by_hands"] = false;
				
			if($PROP["price"] == 0)
				$PROP["summ_changed_by_hands"] = false;

			$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(),
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID"      => 62,
				"PROPERTY_VALUES"=> $PROP,
				"NAME"           => "����� �� ������������� �������",
				"ACTIVE"         => "Y",
				"SORT"					 => $counter,
				"CODE" => "nogarant_time"
				);
			$el->Add($arLoadProductArray);
		}
	}
	
	if(!isset($_REQUEST["print"]))
		LocalRedirect($APPLICATION->GetCurPageParam());
}

$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 59, "ID" => intval($report_id)), false, false, Array("ID", "NAME", "DATE_CREATE", "CREATED_BY", "PROPERTY_CLIENT_ID", "PROPERTY_TASK_ID","PROPERTY_DEAL_ID", "PROPERTY_CRM_CONTACTS", "PROPERTY_CRM_CONTACTS", "PROPERTY_INVOICES"));
if($arFields = $res->GetNext(false, false))
{
	$arTasks = $arFields["PROPERTY_TASK_ID_VALUE"];
	if(!empty($arTasks))
	{
		//foreach($arFields["PROPERTY_TASK_ID_VALUE"] as $arTask)
		$res2 = CTasks::GetList(
			Array("UF_TASK_GARANT" => "DESC", "ID" => "ASC"), 
			Array("GROUP_ID" => $arGroups, "ID" => $arTasks, "CHECK_PERMISSIONS" => "N"),
			Array("ID", "TITLE", "GROUP_ID", "PARENT_ID", "CREATED_DATE", "CLOSED_DATE", "TIME_ESTIMATE", "UF_CLIENT", "UF_TASK_GARANT", "CREATED_BY", "RESPONSIBLE_ID","UF_CRM_TASK")
		);

		$arFields["PROPERTY_TASK_ID_VALUE"] = Array();
		while($arTask = $res2->GetNext(false, false))
		{
			$arFields["PROPERTY_TASK_ID_VALUE"][$arTask["ID"]] = $arTask;
			$arFields["PROPERTY_TASK_ID_VALUE"][$arTask["ID"]]["MINUTES"] = 0;

			if(!empty($arUsers[$arTask['CREATED_BY']]))
				$arFields["PROPERTY_TASK_ID_VALUE"][$arTask["ID"]]['CREATED_BY_NAME'] = CUser::FormatName('#LAST_NAME# #NAME#', $arUsers[$arTask['CREATED_BY']]);
			
			if(!empty($arUsers[$arTask['RESPONSIBLE_ID']]))
				$arFields["PROPERTY_TASK_ID_VALUE"][$arTask["ID"]]['RESPONSIBLE_ID_NAME'] = CUser::FormatName('#LAST_NAME# #NAME#', $arUsers[$arTask['RESPONSIBLE_ID']]);
		}
		
		$res3 = CTaskElapsedTime::GetList(
			Array(),
			Array("TASK_ID" => $arTasks)
		);

		$elapsedTime = 0;
		while($arElapsed = $res3->Fetch())
		{
			$arFields["PROPERTY_TASK_ID_VALUE"][$arElapsed["TASK_ID"]]["MINUTES"] += $arElapsed["MINUTES"];
			$arFields["PROPERTY_TASK_ID_VALUE"][$arElapsed["TASK_ID"]]["MINUTES_GRADES"] += $arElapsed['MINUTES'] * $arUsers[$arElapsed['USER_ID']]['TIME_FACTOR'];
		}
		
		foreach($arFields["PROPERTY_TASK_ID_VALUE"] as $k => $arTask)
		{
			$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 62, "PROPERTY_task_id" => $k, "PROPERTY_report" => $report_id), false, false, Array(
				"ID",
				"PROPERTY_report",
				"PROPERTY_task_id",
				"PROPERTY_date_create",
				"PROPERTY_date_close",
				"PROPERTY_task_name",
				"PROPERTY_task_name_print",
				"PROPERTY_elapsed_time",
				"PROPERTY_comment",
			));
			if($ar_fields = $res->GetNext(false, false))
			{
				$arFields["PROPERTY_TASK_ID_VALUE"][$k]["TITLE"] = $ar_fields["PROPERTY_TASK_NAME_VALUE"];
				$arFields["PROPERTY_TASK_ID_VALUE"][$k]["TITLE_PRINT"] = $ar_fields["PROPERTY_TASK_NAME_PRINT_VALUE"];
				$arFields["PROPERTY_TASK_ID_VALUE"][$k]["CREATED_DATE"] = $ar_fields["PROPERTY_DATE_CREATE_VALUE"];
				$arFields["PROPERTY_TASK_ID_VALUE"][$k]["CLOSED_DATE"] = $ar_fields["PROPERTY_DATE_CLOSE_VALUE"];
				$arFields["PROPERTY_TASK_ID_VALUE"][$k]["COMMENT"] = $ar_fields["PROPERTY_COMMENT_VALUE"];
				$arFields["PROPERTY_TASK_ID_VALUE"][$k]["DURATION"] = $ar_fields["PROPERTY_ELAPSED_TIME_VALUE"];
				
				$modulo = $arFields["PROPERTY_TASK_ID_VALUE"][$k]["DURATION"]%5;
				$correction = 0;
				/*if($modulo < 2.5)
				{
					$correction = -$modulo;
				}
				else*/
				{
					if($modulo > 0)
						$correction = -$modulo + 5;
				}
				
				$arFields["PROPERTY_TASK_ID_VALUE"][$k]["DURATION"] += $correction;
			}
			
			$modulo = $arFields["PROPERTY_TASK_ID_VALUE"][$k]["MINUTES"]%5;
			$correction = 0;
			/*if($modulo < 2.5)
			{
				$correction = -$modulo;
			}
			else*/
			{
				if($modulo > 0)
					$correction = -$modulo + 5;
			}
			
			$arFields["PROPERTY_TASK_ID_VALUE"][$k]["~MINUTES"] = $arFields["PROPERTY_TASK_ID_VALUE"][$k]["MINUTES"];
			$arFields["PROPERTY_TASK_ID_VALUE"][$k]["MINUTES"] += $correction;
		}

		$arFields["~"] = $arFields["PROPERTY_TASK_ID_VALUE"];

		// ���������� ��������� � ������������ ������
		$c = 0;
		do
		{
			$flag = false;
			$ararNoReports0 = $arFields["PROPERTY_TASK_ID_VALUE"];
			foreach($arFields["PROPERTY_TASK_ID_VALUE"] as $key => $arTask)
			{
				if(intval($arTask["PARENT_ID"]) > 0)
				{
					if(GetChildsLevel($arTask["ID"], $ararNoReports0) > 0)
					{
						$flag = true;
					}
					else
					{
						if(
							($arTask["UF_TASK_GARANT"] == 1 && $arFields["PROPERTY_TASK_ID_VALUE"][$arTask["PARENT_ID"]]["UF_TASK_GARANT"] == 1) ||
							($arTask["UF_TASK_GARANT"] == 0 && $arFields["PROPERTY_TASK_ID_VALUE"][$arTask["PARENT_ID"]]["UF_TASK_GARANT"] == 0)
						)
						{
							$arFields["PROPERTY_TASK_ID_VALUE"][$arTask["PARENT_ID"]]["CHILDS"][] = $arTask;
							unset($arFields["PROPERTY_TASK_ID_VALUE"][$key]);
						}
						else
						{
						
						}
					}
				}
			}
			//if($c> 999)
			//	s("warning");
		}
		while($flag && $c++ < 1000); // ����������� �������. ��� ������ ��������!
	}
	
	$arFilter =  Array("IBLOCK_ID" => 62, "PROPERTY_report" => $report_id);
	//if($SHOW_ZERO_TIME_TASKS != '��')
	//	$arFilter["ACTIVE"] = 'Y';
	$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, Array(
		"*",
		"PROPERTY_report",
		"PROPERTY_task_id",
		"PROPERTY_date_create",
		"PROPERTY_date_close",
		"PROPERTY_task_name",
		"PROPERTY_task_name_print",
		"PROPERTY_elapsed_time",
		"PROPERTY_estimate_time",
		"PROPERTY_grade_time",
		"PROPERTY_real_time",
		"PROPERTY_comment",
		"PROPERTY_garant",
		"PROPERTY_price",
		"PROPERTY_hour_price",
		"PROPERTY_summ_changed_by_hands",
	));
	$arNewTasks = Array();
	$arTasks01 = false;
	while($ar_fields = $res->GetNext(false, false))
	{
		$modulo = $ar_fields["PROPERTY_ELAPSED_TIME_VALUE"]%5;
		$correction = 0;
		/*if($modulo < 2.5)
		{
			$correction = -$modulo;
		}
		else*/
		{
			if($modulo > 0)
				$correction = -$modulo + 5;
		}
		$ar_fields["PROPERTY_ELAPSED_TIME_VALUE"] += $correction;
		$arNewTasks[] = $ar_fields;
		//s($ar_fields);
		if(intval($ar_fields['PROPERTY_TASK_ID_VALUE']) > 0)
			$arTasks01[$ar_fields['ID']] = $ar_fields['PROPERTY_TASK_ID_VALUE'];
	}
	
	$res = CTaskElapsedTime::GetList(
		Array(), 
		Array("TASK_ID" => $arTasks01)
	);
	
	while($arTaskTime = $res->GetNext(false, false))
	{
		$arTaskTime01[$arTaskTime['TASK_ID']] += $arTaskTime['SECONDS'];
		$arTaskTimeGrades[$arTaskTime['TASK_ID']] += $arTaskTime['SECONDS'] * $arUsers[$arTaskTime['USER_ID']]['TIME_FACTOR'];
	}
	
	/*$res = CTasks::GetList(
		Array("TITLE" => "ASC"), 
		Array("ID" => $arTasks01)
	);

	while($arTask12345 = $res->GetNext())
	{
		$arTaskTime02[$arTask12345['ID']] = $arTask12345['TIME_ESTIMATE'];
	}*/
	
	foreach($arNewTasks as &$arTask02)
	{
		$arTask02['ELAPSED_TIME'] = $arTaskTime01[$arTask02['PROPERTY_TASK_ID_VALUE']];
		$arTask02['ELAPSED_TIME_GRADES'] = $arTaskTimeGrades[$arTask02['PROPERTY_TASK_ID_VALUE']];
		//$arTask02['TIME_ESTIMATE'] = $arTaskTime02[$arTask02['PROPERTY_TASK_ID_VALUE']];
	}
	
	$arFields["NEW_TASKS"] = $arNewTasks;
	
	//asort($arFields["PROPERTY_TASK_ID_VALUE"]);
	$arFields['ACT'] = $ACT;
	
	CModule::IncludeModule('sale');
	if($ORDER_ID)
	{
		//var_dump($ORDER_ID);
		$rsSales = CSaleOrder::GetList(array("ID" => "ASC"), Array("ID" => $ORDER_ID));
		if($arSales = $rsSales->Fetch()){
			$arOrder = $arSales;
		}
		else {
			$rsSales = CSaleOrder::GetList(array("ID" => "ASC"), Array("ACCOUNT_NUMBER" => $ORDER_ID));
			if($arSales = $rsSales->Fetch()){
				$arOrder = $arSales;
			}
		}
		//$arOrder = CSaleOrder::GetByID($ORDER_ID);
		$arFields['DELIVERY_DOC_NUM'] = $arOrder['DELIVERY_DOC_NUM'];
	}
	$arFields['ORDER_ID'] = $ORDER_ID;
	$arFields['PAYED'] = $PAYED;
	$arFields['PAPER'] = $PAPER;
	$arFields['PAPER_RETURNS'] = $PAPER_RETURNS;
	$arFields['PAPER_DATE'] = $PAPER_DATE;
	$arFields['SHOW_ZERO_TIME_TASKS'] = '��';//$SHOW_ZERO_TIME_TASKS;
	
	$arResult = $arFields;
}

$arResult['REPORT_DATE'] = $report_date;
$arResult['SHOW_PRICE'] = $SHOW_PRICE;
$arResult['SHOW_HOUR_PRICE'] = $SHOW_HOUR_PRICE;

$arResult['SEND_ADO'] = $SEND_ADO;
$arResult['SEND_ADO_DATE'] = $SEND_ADO_DATE;
$arResult['TRACK_NUMBER'] = $TRACK_NUMBER;

$arResult['REPORT_NAME'] = $NAME;
$arResult['REPORT_MONTH'] = $REPORT_MONTH;

$arResult['PREPAYMENT'] = $PREPAYMENT;
$arResult['INVOICES'] = $INVOICES;

$arResult['OWN_COMPANY'] = $OWN_COMPANY;
$arResult['REPORT_TYPE'] = $REPORT_TYPE;

//������� ���� ��������� ����� ������
$arrReportTypes = array();
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_CODE"=>"support_reports", "CODE"=>"REPORT_TYPE"));
while($enum_fields = $property_enums->GetNext())
{
  $arrReportTypes[$enum_fields["ID"]] = $enum_fields;
}
$arResult['REPORT_TYPE_ALL'] = $arrReportTypes;

function GetChildsLevel($task_id, $arTasks, &$level = Array())
{
	$level[] = '';
	foreach($arTasks as $arTask)
	{
		if($arTask["PARENT_ID"] == $task_id)
		{
			GetChildsLevel($arTask["ID"], $arTasks, $level);
		}
	}
	
	return count($level) - 1;
}																			
//s($arResult,497);

$arResult['REPORT_ID'] = $report_id;

$arResult['REPORT_GARANT'] = true;

if (count($arResult["NEW_TASKS"])) {
	foreach ($arResult["NEW_TASKS"] as $my_key)
	{

		if(
			$my_key['PROPERTY_GARANT_VALUE'] == 'N'
		&&
			(
				(
					!empty($my_key["PROPERTY_TASK_ID_VALUE"])
				&&
					!empty($my_key["PROPERTY_ELAPSED_TIME_VALUE"])
				)
			||
				!empty($my_key['PROPERTY_PRICE_VALUE'])
			)
		)
		{
			$arResult['REPORT_GARANT'] = false;
		}

		if ($my_key['PROPERTY_TASK_ID_VALUE'] > 0 && $my_key['PROPERTY_PRICE_VALUE'] > 0) {
			$arResult["CHEKED_MARK"] = 'Y';
			continue;
		}
	}
}
else $arResult["CHEKED_MARK"] = 'N';

//����� �� ���� "��������"
$accountantDep = 314;
$chiefs = array();
$arDeps = CIntranetUtils::GetUserDepartments($CREATOR);
$arDepsStructure = CIntranetUtils::GetStructure();

$chiefs = getChiefs($arDeps, $arDepsStructure['DATA']);

$arResult['CHECKLIST_RIGHTS'] = 'D';

if(in_array($GLOBALS['USER']->GetId(), $chiefs) || $USER->IsAdmin())
{
	$arResult['CHECKLIST_RIGHTS'] = 'W';
}
elseif(in_array($GLOBALS['USER']->GetId(), $arDepsStructure['DATA'][$accountantDep]['EMPLOYEES']) || $GLOBALS['USER']->GetId() == $CREATOR)
{
	$arResult['CHECKLIST_RIGHTS'] = 'R';
}

$obPropChecklist = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 59, "CODE" => "CHECKLIST"));
while($arPropChecklistVal = $obPropChecklist->Fetch())
{
	if(!empty($AR_CHECKLIST_VALS[$arPropChecklistVal['ID']]))
		$arPropChecklistVal['CHECKED'] = true;
	
	$AR_CHECKLIST[$arPropChecklistVal['ID']] = $arPropChecklistVal;
}

if(!empty($CHECKLIST_USER['VALUE']))
{
	$strUsers = implode(" | ", array_unique($CHECKLIST_USER['USER']));

	$obUsers = CUser::GetList(($by="ID"), ($order="desc"), array("ID" => $strUsers), array('FIELDS' => array('ID', 'NAME', 'LAST_NAME')));
	while($arUser = $obUsers->Fetch())
		$arChecklistUser[$arUser['ID']] = $arUser['NAME'].' '.$arUser['LAST_NAME'];

	foreach($CHECKLIST_USER['VALUE'] as $key => $val)
		$CHECKLIST_USER['FORMATE_VALUE'][$val] = $arChecklistUser[$CHECKLIST_USER['USER'][$key]];
	
	unset($CHECKLIST_USER['VALUE'], $CHECKLIST_USER['USER']);
	
	$arResult['CHECKLIST_USER'] = $CHECKLIST_USER['FORMATE_VALUE'];
}

$arResult['CHECKLIST'] = $AR_CHECKLIST;

if(!array_diff_key($AR_CHECKLIST, $AR_CHECKLIST_VALS) && !empty($AR_CHECKLIST_VALS))
	$arResult['CHECKLIST_CHECKED'] = true;

if(!$arResult['CHECKLIST_CHECKED'] && $GLOBALS['USER']->GetId() == $CREATOR && !$USER->IsAdmin())
{
	$arResult['CHECKLIST_RIGHTS'] = 'W';
	$arResult['REPORT_CREATOR'] = true;
}

$arInvoiceInfo = CCrmInvoice::GetByID($INVOICE_ID);

$arResult['INVOICE_STATUS'] = $arInvoiceInfo['STATUS_ID'];

if(in_array($GLOBALS['USER']->GetId(), $arDepsStructure['DATA'][$accountantDep]['EMPLOYEES']) || $USER->IsAdmin())
{
	if(empty($REPORT_PASS_CLIENT) || (!empty($REPORT_PASS_CLIENT) && (in_array($GLOBALS['USER']->GetId(), getChiefs(array($accountantDep), $arDepsStructure['DATA'])) || $USER->IsAdmin()) || in_array($GLOBALS['USER']->GetId(), $arDepsStructure['DATA'][$accountantDep]['EMPLOYEES'])))
	{
		$arResult['REPORT_PASS_CLIENT_SHOW'] = true;
		
		if(
			!empty($REPORT_PASS_CLIENT)
		&&
			in_array($GLOBALS['USER']->GetId(), $arDepsStructure['DATA'][$accountantDep]['EMPLOYEES'])
		&&
			!empty($arInvoiceInfo['STATUS_ID'])
		&&
			$arInvoiceInfo['STATUS_ID'] != 'N'
		)
		{
			$arResult['REPORT_PASS_CLIENT_DISABLED'] = true;
		}
	}

	if(isset($report_id) && $report_id > 0 && isset($_REQUEST["report_pass_client"]))
	{
		$val = '';
		if(!empty($_REQUEST["report_pass_client"]))
		{
			$property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => 59, "CODE" => "REPORT_PASS_CLIENT"));
			$enum_fields = $property_enums->Fetch();
			$val = $enum_fields['ID'];
		}
		
		CIBlockElement::SetPropertyValuesEx(intval($report_id), false, array('REPORT_PASS_CLIENT' => $val));
		
		$REPORT_PASS_CLIENT = $_REQUEST["report_pass_client"];
	}

	if(!empty($arResult['NEW_TASKS']) && !empty($_REQUEST["report_pass_client"]) /* && ($_REQUEST["save"] == "��������� ��������������") */)
	{
		$productId_pc = 359781;		
		$productId_h = 359823;
		
		$obProduct = CIBlockElement::GetByID($productId_pc);
		$arProduct_pc = $obProduct->GetNext(false, false);
		$arProduct_pc['MEASURE_CODE'] = 796;
		
		$obProduct = CIBlockElement::GetByID($productId_h);
		$arProduct_h = $obProduct->GetNext(false, false);
		$arProduct_h['MEASURE_CODE'] = 356;
		
		//356 - � - h
		//796 - �� - pc

			//$arInvoiceProducts = CCrmInvoice::GetProductRows($INVOICE_ID);

		if(empty($INVOICE_ID) || (!empty($INVOICE_ID) && ($arInvoiceInfo["STATUS_ID"] == 'N' || $arInvoiceInfo["STATUS_ID"] == 'Q')))
		{
			$arInvoice = Array(
				"save" => true,
				"ORDER_TOPIC" => $arInvoiceInfo['ORDER_TOPIC'] ? $arInvoiceInfo['ORDER_TOPIC'] : $arResult['NAME'],
				"STATUS_ID" => $arInvoiceInfo['STATUS_ID'] ? $arInvoiceInfo['STATUS_ID'] : "N",
				"DATE_BILL" => $arInvoiceInfo['DATE_BILL'] ? $arInvoiceInfo['DATE_BILL'] : date('d.m.Y'),
				"RESPONSIBLE_ID" => $arInvoiceInfo['RESPONSIBLE_ID'] ? $arInvoiceInfo['RESPONSIBLE_ID'] : $arResult['CREATED_BY'],
				"PRIMARY_ENTITY_TYPE" => "COMPANY",
				"COMPANY" => $arInvoiceInfo['UF_COMPANY_ID'] ? $arInvoiceInfo['UF_COMPANY_ID'] : $arResult['PROPERTY_CLIENT_ID_VALUE'],
				"PAY_SYSTEM_ID" => $arInvoiceInfo['PAY_SYSTEM_ID'] ? $arInvoiceInfo['PAY_SYSTEM_ID'] : 12,
				"DATE_INSERT" => $arInvoiceInfo["DATE_INSERT"],
				"ID" => $INVOICE_ID,
				"ACCOUNT_NUMBER" => $arInvoiceInfo['ACCOUNT_NUMBER'],
				"UF_DEAL_ID" => $arResult['PROPERTY_DEAL_ID_VALUE']
			);

			$arInvoiceProduct = array();
			$counter = 0;
			$InvoiceName = '';

			foreach($arResult['NEW_TASKS'] as $task)
			{
				if(!empty($arResult['PROPERTY_TASK_ID_VALUE'][$task['PROPERTY_TASK_ID_VALUE']]['CHILDS']))
				{
					$arInvoice["ORDER_TOPIC"] = $task['PROPERTY_TASK_NAME_PRINT_VALUE'];
					continue;
				}
				elseif(($task['PROPERTY_GARANT_VALUE'] == 'Y') || ($task['CODE'] == 'garant_time') || empty($task['PROPERTY_TASK_NAME_PRINT_VALUE']))
					continue;
				
				if(!empty($task['PROPERTY_PRICE_VALUE']))
					$hp = $task['PROPERTY_PRICE_VALUE'].'_pc_'.rand();
				elseif(!empty($task['PROPERTY_HOUR_PRICE_VALUE']))
					$hp = $task['PROPERTY_HOUR_PRICE_VALUE'].'_h';
				else
					$hp = '0';

				if(empty($arInvoiceProduct[$hp]))
				{
					$arInvoiceProduct[$hp] = Array(
						'PRODUCT_NAME' => !empty($task['PROPERTY_PRICE_VALUE']) ? $task['PROPERTY_TASK_NAME_PRINT_VALUE'] : $arProduct_h['NAME'],
						'PRODUCT_ID' => !empty($task['PROPERTY_PRICE_VALUE']) ? $arProduct_pc['ID'] : $arProduct_h['ID'],
						'MEASURE_CODE' => !empty($task['PROPERTY_PRICE_VALUE']) ? $arProduct_pc['MEASURE_CODE'] : $arProduct_h['MEASURE_CODE'],
						'QUANTITY' => (!empty($task['PROPERTY_PRICE_VALUE']) || empty($task["PROPERTY_ELAPSED_TIME_VALUE"])) ? 1 : $task["PROPERTY_ELAPSED_TIME_VALUE"],
						'PRICE' => !empty($task['PROPERTY_PRICE_VALUE']) ? $task['PROPERTY_PRICE_VALUE'] : $task['PROPERTY_HOUR_PRICE_VALUE']
					);
				}
				else
					$arInvoiceProduct[$hp]['QUANTITY'] = $arInvoiceProduct[$hp]['QUANTITY'] + ($task["PROPERTY_ELAPSED_TIME_VALUE"] ? $task["PROPERTY_ELAPSED_TIME_VALUE"] : 1);
			}
			
			foreach($arInvoiceProduct as $key => $product)
			{
				$product['QUANTITY'] = ceil($product['QUANTITY']/60);
				$arInvoiceProductNew[] = $product;
			}
			
			unset($arInvoiceProduct);

			$arInvoice['INVOICE_PRODUCT_DATA'] = $arInvoiceProductNew;

			$invoiceID = ReportInvoiceAdd($arInvoice);
			
			if(empty($INVOICE_ID) || $INVOICE_ID != $invoiceID)
			{
				CIBlockElement::SetPropertyValuesEx(intval($report_id), false, array('INVOICE_ID' => $invoiceID));
				$INVOICE_ID = $invoiceID;
			}
		}
	}
}

if(!empty($arInvoiceInfo['ACCOUNT_NUMBER']))
	$arResult['INVOICE_ACCOUNT_NUMBER'] = $arInvoiceInfo['ACCOUNT_NUMBER'];
elseif(!empty($INVOICE_ID))
{
	$arInvoiceInfo = CCrmInvoice::GetByID($INVOICE_ID);
	if(!empty($arInvoiceInfo['ACCOUNT_NUMBER']))
		$arResult['INVOICE_ACCOUNT_NUMBER'] = $arInvoiceInfo['ACCOUNT_NUMBER'];
}

$arResult['INVOICE_ID'] = $INVOICE_ID;

if(!empty($REPORT_PASS_CLIENT))
{
	$arResult['REPORT_PASS_CLIENT'] = false;
	$arResult['REPORT_PASS_CLIENT_NAME'] = '��������� ��������������';
}
else
{
	$arResult['REPORT_PASS_CLIENT'] = true;
	$arResult['REPORT_PASS_CLIENT_NAME'] = '������� ����';
}

$arResult['REPORT_PASS_CLIENT_START'] = $REPORT_PASS_CLIENT;


function getChiefs($arDeps, $arDepsStructure)
{
	foreach($arDeps as $dep)
	{
		if(!empty($arDepsStructure[$dep]['UF_HEAD']))
			$chiefs[$arDepsStructure[$dep]['UF_HEAD']] = $arDepsStructure[$dep]['UF_HEAD'];
		
		if(!empty($arDepsStructure[$dep]['IBLOCK_SECTION_ID']))
			$chiefs = array_merge($chiefs, getChiefs(array($arDepsStructure[$dep]['IBLOCK_SECTION_ID']), $arDepsStructure));
	}
	
	return array_unique($chiefs);
}

CModule::IncludeModule('crm');
$obCompanys = CCrmCompany::GetListEx(
	array(),
	array('ID' => $arResult['PROPERTY_CLIENT_ID_VALUE']),
	false,
	false,
	array('TITLE', 'ASSIGNED_BY_ID')
);
if($arCompany = $obCompanys->GetNext(false, false))
{
	if(!empty($arCompany['TITLE']))
	{
		$arResult['CLIENT_NAME'] = $arCompany['TITLE'];
		$arResult['CLIENT_URL'] = '<a href="/crm/company/show/'.$arCompany['ID'].'/" target="_blank">'.$arCompany['TITLE'].'</a>';
	}
	
	if(!empty($arCompany['ASSIGNED_BY_ID']))
	{
		if(!empty($arUsers[$arCompany['ASSIGNED_BY_ID']]))
			$arUser = $arUsers[$arCompany['ASSIGNED_BY_ID']];
		else
		{
			$rsUsers = CUser::GetList(
				($by="id"),
				($order="desc"),
				array("ID" => $arCompany['ASSIGNED_BY_ID']),
				array("FIELDS" => array("ID", "NAME", "LAST_NAME"))
			);
			$arUser = $rsUsers->Fetch();
		}
		
		$arResult['CLIENT_ASSIGNED'] = '<a href="/company/personal/user/'.$arUser['ID'].'/" target="_blank">'.CUser::FormatName('#LAST_NAME# #NAME#', $arUser).'</a>';
	}
}

$paidSum = $allPaidSum = 0;

$obRes = CCrmInvoice::GetList(
	array('DATE_INSERT' => 'DESC'),
	array('UF_COMPANY_ID' => $arResult['PROPERTY_CLIENT_ID_VALUE']),
	false,
	false,
	array('ACCOUNT_NUMBER', 'ID', 'ORDER_TOPIC', 'SUM_PAID', 'DATE_INSERT', 'PAYED', 'PRICE', 'STATUS_ID')
);
while($arRes = $obRes->Fetch())
{
	if(in_array($arRes['ID'], $arResult['INVOICES']))
	{
		if($arRes['STATUS_ID'] == 'P')
			$paidSum = $paidSum + $arRes['SUM_PAID'];
		
		$allPaidSum = $allPaidSum + $arRes['PRICE'];
	
		$sSelected = 'Y';
	}
	else
		$sSelected = 'N';
	
	$arDesc = Array();
	
	$arDesc[] = $arRes['ORDER_TOPIC'];

	if(!empty($arRes['PRICE']))
		$arDesc[] = CurrencyFormat($arRes['PRICE'], 'RUB');
	
	if(!empty($arRes['DATE_INSERT']))
		$arDesc[] = date('d.m.Y', strtotime($arRes['DATE_INSERT']));
	
	$arResult['INVOICE'][] = Array(
		'title' => $arRes['ACCOUNT_NUMBER'],
		'desc' => implode(', ', $arDesc),
		'id' => $arRes['ID'],
		'url' => CComponentEngine::MakePathFromTemplate(COption::GetOptionString('crm', 'path_to_invoice_show'), Array('invoice_id' => $arRes['ID'])),
		'type'  => 'lead',
		'selected' => $sSelected,
		'payed' => ($arRes['PAYED'] == 'Y') ? 1 : 0,
		'sum_paid' => $arRes['SUM_PAID'],
		'price' => $arRes['PRICE']
	);
	
	$arResult['JS_INV_PRICE'][$arRes['ID']] = array('PRICE' => $arRes['PRICE'], 'SUM_PAID' => $arRes['SUM_PAID']);
}

$arResult['INVOICE_PAID_SUM'] = $paidSum;
$arResult['INVOICE_ALL_PAID_SUM'] = $allPaidSum;

//s($arResult['NEW_TASKS'], 502);


$res = CCrmDeal::GetList(array(),array('ID'=>$arResult['PROPERTY_DEAL_ID_VALUE'],),array(),false);
while($ar_res = $res->GetNext()) {
	$arResult['DEALS'][] = $ar_res;
}

//����� ����������� ������������
$rsUser = CUser::GetByID($arResult["CREATED_BY"]);
$arUser = $rsUser->Fetch();
$userDep = $arUser["UF_DEPARTMENT"];

$depIds = array();
foreach ($userDep as $dep) {
	$nav = CIBlockSection::GetNavChain(false, $dep);
	while($arSectionPath = $nav->GetNext()){
		$depIds[] = $arSectionPath["ID"];
	}
}

$depHeads = array();
$arFilter = array('IBLOCK_ID' => 5, 'ID' => $depIds);
$rsSections = CIBlockSection::GetList(array("SORT"=>"ASC"), $arFilter, false, array("UF_HEAD", "IBLOCK_ID"));
while ($arSection = $rsSections->GetNext())
	$depHeads[] = $arSection["UF_HEAD"];

$arResult["CAN_EDIT"] = (!isset($arResult['CHECKLIST_CHECKED']) && ($USER->getId() === $arResult['CREATED_BY'] || in_array($USER->getId(), $depHeads)) ) ? "Y" : "N"; //�������� �� �� ������ �� ����� ��� ��������������
$arResult["CAN_EDIT_INVOICE"] = ($USER->getId() === $arResult['CREATED_BY'] || in_array($USER->getId(), $depHeads) || $USER->getId() === COption::GetOptionString("itees.performance_sheet", "general_buh", "DEFAULT_VALUE")) ? "Y" : "N";

//����������� ��������
$ownCompanyes = array();

$obRes = CCrmCompany::GetListEx(
	array('ID' => 'DESC'),
	array('IS_MY_COMPANY' => 'Y'),
	false,
	array(),
	array('ID', 'TITLE')
);

while ($arRes = $obRes->Fetch()) {
	$ownCompanyes[$arRes['ID']] = $arRes;
}

$arResult["OWN_COMPANYES"] = $ownCompanyes;
unset($ownCompanyes);

$arResult["CAN_EDIT_OWN_COMP"] = ((!isset($arResult['CHECKLIST_CHECKED']) && ($USER->getId() === $arResult['CREATED_BY'] || in_array($USER->getId(), $depHeads))) || (isset($arResult['CHECKLIST_CHECKED']) && ($USER->getId() === $arResult['CREATED_BY'] || in_array($USER->getId(), $depHeads)) && !$arResult["OWN_COMPANY"]) ) ? "Y" : "N";
$arResult["CAN_EDIT_TYPE"] = ((!isset($arResult['CHECKLIST_CHECKED']) && ($USER->getId() === $arResult['CREATED_BY'] || in_array($USER->getId(), $depHeads))) || (isset($arResult['CHECKLIST_CHECKED']) && ($USER->getId() === $arResult['CREATED_BY'] || in_array($USER->getId(), $depHeads))) ) ? "Y" : "N";

//������ ������������� ������
$newInvoices = [
	"SUMM" => 0,
	"ID" => [],
];
$invoiceIds = $arResult['INVOICES'];
$invoiceIds[] = $arResult['INVOICE_ID'];

$obRes = CCrmInvoice::GetList(
	array('DATE_INSERT' => 'DESC'),
	array('ID' => array_unique($invoiceIds)),
	false,
	false,
	array(/*'ACCOUNT_NUMBER', 'ID', 'ORDER_TOPIC', 'SUM_PAID', 'DATE_INSERT', 'PAYED', 'PRICE', 'STATUS_ID'*/)
);
while($arRes = $obRes->Fetch())
{
	$arRes["STATUS"] = CCrmViewHelper::RenderInvoiceStatusControl(
		array(
			'PREFIX' => "{$arRes['ID']}_PROGRESS_BAR_",
			'ENTITY_ID' => $arRes['ID'],
			'CURRENT_ID' => $arRes['STATUS_ID'],
			'SERVICE_URL' => '/bitrix/components/bitrix/crm.invoice.list/list.ajax.php',
			'READ_ONLY' => true
		)
	);
	$newInvoices["SUMM"] += $arRes['PRICE'];
	$newInvoices["PAID"] += $arRes['SUM_PAID'];
	$newInvoices["ID"][$arRes["ID"]] = $arRes;
	unset($arRes);
}

$arResult["NEW_INVOICES"] = $newInvoices;
?>