<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

include_once $_SERVER["DOCUMENT_ROOT"].'/local/php_interface/include/learning_search.php';

use \Bitrix\Main\Loader,
    \Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();

if(!empty($request->getQuery('search'))) {

    $arResult['SEARCH'] = $request->getQuery('search');
    
    Loader::includeModule('search');
    Loader::includeModule('learning');

    $search = new CSearch();
    $search->Search([
        'MODULE_ID' => 'learning',
        'QUERY' => $arResult['SEARCH']
    ]);

    while($item = $search->fetch()) {

        $itemId = explode('_', $item['ITEM_ID']);
        
        $url = LearningSearch::getLessonUrl($item, $itemId);
        if(!empty($url)) {
            $arResult['ITEMS'][$itemId[2]] = [
                'URL' => $url,
                'TITLE' => $item['TITLE'],
                'BODY_FORMATED' => $item['BODY_FORMATED']
            ];
        }
    }

}