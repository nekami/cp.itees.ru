<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

define('IBLOCK_HISTORY_GRADES',171);
define('IBLOCK_GRADES',66);
define('IBLOCK_DEPARTAMENT',5);
define('ITEES_GROUP_ID',17);

//Пользователи
$order = array('sort' => 'desc');
$filter = array(
	'ACTIVE' => 'Y',
	'GROUPS_ID' => ITEES_GROUP_ID
);
$rsUsers = CUser::GetList($by='last_name', $order, $filter,array('SELECT' => array('UF_GRADE_ID','UF_DEPARTMENT'), 'FIELDS' => array('ID','NAME','LAST_NAME'))); 

$preUser = array();
$idList = array();
$arUser = array();
$preGrades = array();
$arFields = array();

while ($users = $rsUsers->Fetch())  {
	$preUser[] = $users;
}

foreach ($preUser as $user) {
	$arUser[$user['ID']] = array(
		'ID' => $user['ID'],
		'NAME' => $user['NAME'],
		'LAST_NAME' => $user['LAST_NAME'],
		'UF_GRADE_ID' => $user['UF_GRADE_ID'],
		'UF_DEPARTMENT' => $user['UF_DEPARTMENT']
	);
	$idList[] = $user['ID'];
}

//Грейды
$arFilter = array(
	'IBLOCK_ID' => IBLOCK_GRADES,
	'ACTIVE' => 'Y'
);
$arSelect = array(
	'ID',
	'NAME',
	'PROPERTY_RATE_CHANGE_HOURS'
);
$rg = CIBlockElement::GetList(array('PROPERTY_DATE' => 'ASC'), $arFilter, false, false, $arSelect);
while($gr = $rg->Fetch()) {
	$preGrades[$gr['ID']] = $gr;
}

foreach ($preGrades as $grade) {
	$arGrades[$grade['ID']]['ID'] = $grade['ID'];
	$arGrades[$grade['ID']]['NAME'] = $grade['NAME'];
	$arGrades[$grade['ID']]['RATE_CHANGE_HOURS'] = $grade['PROPERTY_RATE_CHANGE_HOURS_VALUE'];
}

foreach ($arUser as $key=>$user) {
	if (!isset($user['GRADE_NAME']) && !empty($user['UF_GRADE_ID'])) {
		$arUser[$key]['GRADE_NAME'] = $arGrades[$user['UF_GRADE_ID']]['NAME'];
		//$arUser[$key]['RATE_CHANGE_HOURS'] = ($arGrades[$user['UF_GRADE_ID']]['RATE_CHANGE_HOURS'] > 5)? $arGrades[$user['UF_GRADE_ID']]['RATE_CHANGE_HOURS']/100: $arGrades[$user['UF_GRADE_ID']]['RATE_CHANGE_HOURS'];
	}
}

//История грейдов
$rate = "";
$arFilter = array(
	'IBLOCK_ID' => IBLOCK_HISTORY_GRADES,
	'ACTIVE' => 'Y'
);
$arSelect = array(
	'PROPERTY_GRADE.PROPERTY_RATE_CHANGE_HOURS',
	'PROPERTY_GRADE.NAME',
	'PROPERTY_GRADE.ID',
	'PROPERTY_USER',
	'PROPERTY_RATE_CHANGE_HOURS'
);
$res = CIBlockElement::GetList(array('PROPERTY_DATE' => 'ASC'), $arFilter, false, false, $arSelect);
while($ob = $res->Fetch()) {
	$arFields[] = $ob;
}
foreach ($arFields as $field) {
	if(isset($field["PROPERTY_RATE_CHANGE_HOURS_VALUE"]) && !empty($field["PROPERTY_RATE_CHANGE_HOURS_VALUE"]) && $field["PROPERTY_RATE_CHANGE_HOURS_VALUE"] > 0 && $field["PROPERTY_RATE_CHANGE_HOURS_VALUE"] != "" && $field["PROPERTY_RATE_CHANGE_HOURS_VALUE"] != null)
		$arUser[$field['PROPERTY_USER_VALUE']]['RATE_CHANGE_HOURS'] = $field["PROPERTY_RATE_CHANGE_HOURS_VALUE"]/100;
	//$arUser[$field['PROPERTY_USER_VALUE']]['RATE_CHANGE_HOURS'] = GetUserGradeTimeFactor($field['PROPERTY_USER_VALUE'],'');
	$arUser[$field['PROPERTY_USER_VALUE']]['GRADE_NAME'] = $field['PROPERTY_GRADE_NAME'];
	$arUser[$field['PROPERTY_USER_VALUE']]['REAL_GRADE_ID'] = $field['PROPERTY_GRADE_ID'];
}

//Подразделения
$arFilter = array(
	'IBLOCK_ID' => IBLOCK_DEPARTAMENT,
	'ACTIVE' => 'Y'
);
$arSelect = array(
	'ID',
	'NAME'
);
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, $arSelect);
while($dep = $db_list->Fetch()) {
	$arDep[$dep['ID']]['ID'] = $dep['ID'];
	$arDep[$dep['ID']]['NAME'] = $dep['NAME'];
}

$arResult['DEPARTAMENTS'] = $arDep;

foreach ($arUser as $key=>&$user) {
	if (!isset($user['RATE_CHANGE_HOURS'])) {
		if (isset($user['REAL_GRADE_ID']))
			$user['RATE_CHANGE_HOURS'] = $arGrades[$user['REAL_GRADE_ID']]['RATE_CHANGE_HOURS']/100;
		else
			$user['RATE_CHANGE_HOURS'] = $arGrades[$user['UF_GRADE_ID']]['RATE_CHANGE_HOURS']/100;
	}
	/*if (isset($user['GRADE_NAME'])) {*/
		foreach ($user['UF_DEPARTMENT'] as $departaments) {
			$arResult['USERS'][$departaments][$key] = $user;
			if (isset($user['REAL_GRADE_ID']))
				$arResult['USERS'][$departaments][$key]['GRADE_BASE'] = $arGrades[$user['REAL_GRADE_ID']]['RATE_CHANGE_HOURS']/100;
			else
				$arResult['USERS'][$departaments][$key]['GRADE_BASE'] = $arGrades[$user['UF_GRADE_ID']]['RATE_CHANGE_HOURS']/100;
		}
	/*}*/
}
