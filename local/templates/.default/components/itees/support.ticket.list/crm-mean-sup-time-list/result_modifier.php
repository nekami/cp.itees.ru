<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(CModule::IncludeModule('crm')){
	$prefixEntityCRM = array(
		'L' => CCrmOwnerType::Lead,
		'D' => CCrmOwnerType::Deal,
		'C' => CCrmOwnerType::Contact,
		'CO' => CCrmOwnerType::Company,
	);
	
	$entityCRMName = array(
		'L' => array(),
		'D' => array(),
		'C' => array(),
		'CO' => array(),
	);
		
	foreach($arResult['TICKETS'] as $ticket){
		if(is_array($ticket['UF_CLIENTS_CRM'])){
			foreach($ticket['UF_CLIENTS_CRM'] as $clientCRM){
				$client = explode('_', $clientCRM);
				$entityCRMName[$client[0]][$client[1]] = null;
			}
		}
	}
	
	foreach($entityCRMName as $key => $entity){
		switch($key){
			case 'L':
			$className = 'CCrmLead';
			$nameField = 'TITLE';
			break;
			
			case 'D':
			$className = 'CCrmDeal';
			$nameField = 'TITLE';
			break;
			
			case 'C':
			$className = 'CCrmContact';
			$nameField = 'FULL_NAME';
			break;
			
			case 'CO':
			$className = 'CCrmCompany';
			$nameField = 'TITLE';
			break;			
		}		
		
		$entityResult = $className::GetListEx(
			array(),
			array('ID' => array_keys($entity), 'CHECK_PERMISSIONS' => 'N'),
			false,
			false,
			array('ID', $nameField)
		);
		
		while($e = $entityResult->Fetch()){
			if(isset($e['TITLE'])){
				$name = $e['TITLE'];
			} elseif(isset($e['FULL_NAME'])) {
				$name = $e['FULL_NAME'];
			}
			
			$entityCRMName[$key][$e['ID']] = $name;
		}		
	}
	
	$ticketList = array();	
	
	foreach($arResult['TICKETS'] as $ticketKey => $ticket){
		if(is_array($ticket['UF_CLIENTS_CRM']) && count($ticket['UF_CLIENTS_CRM'])){			
			foreach($ticket['UF_CLIENTS_CRM'] as $clientCRM){
				$client = explode('_', $clientCRM);				
				$urlClient = CCrmOwnerType::GetShowUrl($prefixEntityCRM[$client[0]], $client[1]);
				$ticket['UF_COMPANY_LINK'] .= '<br><a href="' . $urlClient . '" target="_blank">' . $entityCRMName[$client[0]][$client[1]] . '</a>';
			}
			
			$arResult['TICKETS'][$ticketKey]['UF_COMPANY_LINK'] = $ticket['UF_COMPANY_LINK'];			
		}
		
		$ticketList[$ticket['ID']] = $arResult['TICKETS'][$ticketKey];	
	}
	
	foreach($arResult['ROWS'] as $keyRow => $row){		
		$arResult['ROWS'][$keyRow]['data'] = $ticketList[$row['data']['ID']];
	}	
}
?>