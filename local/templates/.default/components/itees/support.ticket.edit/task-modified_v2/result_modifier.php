<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	//������� ������������� �� ����������
	$rsResponsible = CTicket::GetResponsible();
	while($resp = $rsResponsible->Fetch(false, false))
	{
		$arResponsibles[$resp['REFERENCE_ID']] = $resp['REFERENCE'];
	}
	$arResult['DICTIONARY']['RESPONSIBLE'] = $arResponsibles;
	
	//���������� ������ �� �������� ������� � crm, ���� ���������� "UF_COMPANY"
	
	if(!empty($arResult['TICKET']['UF_COMPANY']))
	{//RewriteFile($_SERVER["DOCUMENT_ROOT"]."/log123.php", print_r($arResult,1));
		CModule::IncludeModule('crm');
		$arContMails = array();
		$arContact = CCrmContact::GetByID($arResult['TICKET']['UF_COMPANY']);
		//s($arContact,497);
		if(!empty($arContact['COMPANY_TITLE']))
		{
			$arResult['TICKET']['COMPANY_LINK'] = '<a href="/crm/contact/show/'.$arContact['ID'].'/">'.$arContact['COMPANY_TITLE'].'</a>';
			//��� ���������
			$arResult['TICKET']['COMPANY_MANAGER_NAME'] = $arContact['ASSIGNED_BY_NAME'].' '.$arContact['ASSIGNED_BY_LAST_NAME'];
			$arResult['TICKET']['COMPANY_MANAGER_ID'] = $arContact['ASSIGNED_BY_ID'];
			$arResult['TICKET']['COMPANY'] = $arContact;
			
			$res = CCrmFieldMulti::GetList(array('ID' => 'asc'), array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $arContact['ID']));
			while($ar = $res->Fetch())
			{
				$arContMails[$ar['ID']] = $ar['VALUE'];
			}
			
			$arResult['TICKET']['COMPANY']['MAILS'] = $arContMails;
			
		}
	}
	
	//������ ������������ "���������� ���������" � ��������� ��������
	if(!CModule::IncludeModule("iblock"))
		return false;

	$rsSect = CIBlockSection::GetList(array(), array('IBLOCK_ID' => 5, 'ID' => 1332), false,array("ID", "UF_HEAD"));
	if($arSect = $rsSect->Fetch(false, false))
	{
		$arResult['TICKET']['COMPANY_MANAGERS_BOSS'] = $arSect["UF_HEAD"];
	}
	//s($arResult['TICKET']['COMPANY_MANAGERS_BOSS'],497);
	//���������� ������ �� ������ � ������� ��������� ���������
	if(!empty($arResult['TICKET']['UF_TASK']))
	{
		CModule::IncludeModule('tasks');
		$arTasks = array();
		$obTasks = CTasks::GetList(array(), array('ID' => $arResult['TICKET']['UF_TASK']),array(), $arParams = array());
		while($arTask = $obTasks->Fetch())
		{
			$arTasks[$arTask['ID']] = $arTask;
		}
		$arResult['TASKS'] = $arTasks;
	}
	
	if(is_array($arResult['TICKET']['UF_CLIENTS_CRM']) && count($arResult['TICKET']['UF_CLIENTS_CRM'])){
		$prefixEntityCRM = array(
			'L' => CCrmOwnerType::Lead,
			'D' => CCrmOwnerType::Deal,
			'C' => CCrmOwnerType::Contact,
			'CO' => CCrmOwnerType::Company,
		);
	
		$entityCRMName = array(
			'L' => array(),
			'D' => array(),
			'C' => array(),
			'CO' => array(),
		);
		
		foreach($arResult['TICKET']['UF_CLIENTS_CRM'] as $clientCRM){
				$client = explode('_', $clientCRM);
				$entityCRMName[$client[0]][$client[1]] = null;
		}
		
		foreach($entityCRMName as $key => $entity){
			switch($key){
				case 'L':
				$className = 'CCrmLead';
				$nameField = 'TITLE';
				break;
			
				case 'D':
				$className = 'CCrmDeal';
				$nameField = 'TITLE';
				break;
			
				case 'C':
				$className = 'CCrmContact';
				$nameField = 'FULL_NAME';
				break;
			
				case 'CO':
				$className = 'CCrmCompany';
				$nameField = 'TITLE';
				break;			
			}
		
			$entityResult = $className::GetListEx(
				array(),
				array('ID' => array_keys($entity), 'CHECK_PERMISSIONS' => 'N'),
				false,
				false,
				array('ID', $nameField)
			);
		
			while($e = $entityResult->Fetch()){
				if(isset($e['TITLE'])){
					$name = $e['TITLE'];
				} elseif(isset($e['FULL_NAME'])) {
					$name = $e['FULL_NAME'];
				}
			
				$entityCRMName[$key][$e['ID']] = $name;
			}		
		}
		
		$arResult['TICKET']['CLIENTS_CRM_LINK'] = array();
		
		foreach($arResult['TICKET']['UF_CLIENTS_CRM'] as $clientCRM){
				$client = explode('_', $clientCRM);
				$urlClient = CCrmOwnerType::GetShowUrl($prefixEntityCRM[$client[0]], $client[1]);
				$arResult['TICKET']['CLIENTS_CRM_LINK'][$clientCRM] = '<a href="' . $urlClient . '" target="_blank">' . $entityCRMName[$client[0]][$client[1]] . '</a>';
		}	
	}
        
        //�������� ������ ��������
        $arStatuses = Array();
        $arFilter = Array(
                "LID" => SITE_ID,
                "TYPE" => "S",
        );
        $by = "s_c_sort";
        $sort = "asc";
        $rsStatus = CTicketDictionary::GetList($by, $sort, $arFilter, $is_filtered); 
        while($arRes = $rsStatus->GetNext()) {
                $arStatuses[] = $arRes;
        }
        foreach($arStatuses as $status)
        {
                $arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields ("TEH_SUPPORT_STATUS", $status["ID"]);
                if($arUF["UF_TS_STATUS_FINAL"]["VALUE"])
                {
                    $arResult["STATUSES"][$status["ID"]]["DESCR"] = $status["DESCR"];
                    $arResult["STATUSES"][$status["ID"]]["ID"] = $status["ID"];
                    $arResult["STATUSES"][$status["ID"]]["NAME"] = $status["NAME"];
                    $arResult["STATUSES"][$status["ID"]]["REFERENCE"] = $status["REFERENCE"];
                    $arResult["STATUSES"][$status["ID"]]["REFERENCE_ID"] = $status["REFERENCE_ID"];
                }
                    
        }
        
        
?>