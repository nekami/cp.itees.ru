<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$uf_sonet_com_doc = Array(
    "FIELD_NAME" => "UF_SONET_COM_DOC",
    "USER_TYPE_ID" => "disk_file",
    "SORT" => 100,
    "MULTIPLE" => "Y",
    "MANDATORY" => "N",
    "SHOW_FILTER" => "N",
    "SHOW_IN_LIST" => "N",
    "EDIT_IN_LIST" => "Y",
    "IS_SEARCHABLE" => "Y",
    "SETTINGS" => Array
    (
        "IBLOCK_ID" => 0,
        "SECTION_ID" => 0,
        "UF_TO_SAVE_ALLOW_EDIT" => ""
    ),
    "EDIT_FORM_LABEL" => "UF_SONET_COM_DOC",
    "LIST_COLUMN_LABEL" => "",
    "LIST_FILTER_LABEL" => "",
    "ERROR_MESSAGE" => "",
    "HELP_MESSAGE" => "",
    "VALUE" => "",
    "~EDIT_FORM_LABEL" => "UF_SONET_COM_DOC",
    "USER_TYPE" => Array
    (
        "USER_TYPE_ID" => "disk_file",
    ),
);
$formParams = array(
    "FORM_ID" => "form1",
    "SHOW_MORE" => "Y",
    "PARSER" => Array(
        "Bold", "Italic", "Underline", "Strike", "ForeColor",
        "FontList", "FontSizeList", "RemoveFormat", "Quote",
        "Code", "CreateLink",
        "Image", "UploadFile",
        "InputVideo",
        "Table", "Justify", "InsertOrderedList",
        "InsertUnorderedList",
        "Source", "MentionUser", "Spoiler"),
    "BUTTONS" => Array(
        "UploadFile",
        "CreateLink",
        "InputVideo",
        "Quote",
        "MentionUser"
    ),
    "MAX_FILE_SIZE" => COption::GetOptionString("forum", "file_max_size", 5242880),
    "INPUT_NAME" => "UF_SONET_COM_DOC",
    "TEXT" => Array(
        "NAME" => "MESSAGE",
        "VALUE" => " ",
        "HEIGHT" => "80px",
    ),
    "UPLOAD_WEBDAV_ELEMENT" => $uf_sonet_com_doc,
    "UPLOAD_FILE_PARAMS" => array("width" => 400, "height" => 400),

    "SMILES" => 4,//$smiles,
    "LHE" => array(
        "id" => "MESSAGE",
        "documentCSS" => "body {color:#434343;}",
        "fontFamily" => "'Helvetica Neue', Helvetica, Arial, sans-serif",
        "fontSize" => "12px",
        "bInitByJS" => true,
        'bbCode' => true,
        "height" => 80
    )
);
?>


<p>��������</p>
<div class="feed-comments-block" style="display: block; background-color:  #eef2f4;padding: 5px;">
    <!--<div style="display: none">
        <input name="UF_SONET_COM_DOC" size="30" type="file">
    </div>-->
    <?= bitrix_sessid_post(); ?>
    <input type="hidden" name="sonet_log_comment_logid" id="sonet_log_comment_logid" value="">
    <input type="hidden" name="bbcode_val" id="bbcode_val" value="<?=$formParams["LHE"]["bbCode"]?>">
    <? $APPLICATION->IncludeComponent(
        "bitrix:main.post.form",
        ".default",
        $formParams,
        false,
        array(
            "HIDE_ICONS" => "N"
        )
    ); ?>
    <input type="hidden" name="cuid" id="upload-cid" value=""/>
</div>
<script type="text/javascript">

    //overwrite BXHtmlEditor
    (function(window) {
        if (window.BXHtmlEditor)
        {
            var BXHtmlEditor = window.BXHtmlEditor;
            BXHtmlEditor.OnBeforeUnload = function()
            {
                return;


            };
            window.BXHtmlEditor = BXHtmlEditor;
            window.onbeforeunload = BXHtmlEditor.OnBeforeUnload;
        }
        BX.onCustomEvent(window, "OnBXHtmlEditorInit");
        top.BXHtmlEditorAjaxResponse = {};
    })(window);

</script>

