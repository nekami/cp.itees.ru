<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? CJSCore::Init(array("jquery")); ?>

<?= ShowError($arResult["ERROR_MESSAGE"]); ?>

<script>
    if ((-1 < window.location.search.indexOf("TICKET_ID")) && (-1 < window.location.search.indexOf("MESSAGE_ID")) && !isNaN('<?=$_REQUEST['ID']?>') && <?=$_REQUEST['ID']?> != 0) {
        history.pushState({}, '', '?ID=<?=$arResult['TICKET']['ID']?>&edit=1');
    }
</script>

<script type="text/javascript">BX.loadCSS('<?=CUtil::JSEscape($this->GetFolder());?>/style.css');</script>
<? CUtil::InitJSCore(array('jquery', 'ajax', 'popup', 'grid')); ?>

<? //global $USER;?>
<script>
    var SUP_CONTACT = <?= '"'.GetMessage('SUP_CONTACT').'"';?>;
    var SUP_EMPLOYEE_TS = <?= '"'.GetMessage('SUP_EMPLOYEE_TS').'"';?>;
    var SUP_TICKET = <?= '"'.GetMessage('SUP_TICKET').'"';?>;
    var SUP_NOT_SEL_MESS = <?= '"'.GetMessage('SUP_NOT_SEL_MESS').'"';?>;
    var SUP_JS_MESS_CONT_EMPTY = <?= '"'.GetMessage('SUP_JS_MESS_CONT_EMPTY').'"';?>;
    var SUP_JS_MESS_FILL = <?= '"'.GetMessage('SUP_JS_MESS_FILL').'"';?>;

    var SUP_MESS_COPY_POPUP_TITLE = <?= '"'.GetMessage("SUP_MESS_COPY_POPUP_TITLE").'"';?>;
    var SUP_CLOSE_POPUP = <?= '"'.GetMessage("SUP_CLOSE_POPUP").'"';?>;
    var SUP_GO_TO_CONTACT = <?= '"'.GetMessage("SUP_GO_TO_CONTACT").'"';?>;

    BX.message["SUP_FULL_DESCR"] = <?= '"'.GetMessage("SUP_FULL_DESCR").'"';?>;
    
    BX.message["TICKET_CLOSE_DESCR"] = "<?=GetMessage("TICKET_CLOSE_DESCR")?>";
    BX.message["TICKET_CLOSE_BUTTON"] = "<?=GetMessage("TICKET_CLOSE_BUTTON")?>";
    BX.message["TICKET_CLOSE_BUTTON_CANCEL"] = "<?=GetMessage("TICKET_CLOSE_BUTTON_CANCEL")?>";



    //������ ���������
    var arrMessages = <?=CUtil::PhpToJsObject($arResult["MESSAGES_FOR_TASK"])?>;
    //���������� � ������
    var arrTicket = <?=CUtil::PhpToJsObject($arResult["TICKET"])?>;
    //����������� �������
    var statuses = <?=CUtil::PhpToJsObject($arResult["STATUSES"])?>;
</script>

<?

function  __GetDropDown($TYPE, &$TICKET_DICTIONARY)
{
    $arReturn = Array();

    if (array_key_exists($TYPE, $TICKET_DICTIONARY)) {
        foreach ($TICKET_DICTIONARY[$TYPE] as $key => $value) {
            $arReturn["REFERENCE"][] = $value["REFERENCE"];
            $arReturn["REFERENCE_ID"][] = $key;
        }
    }

    return $arReturn;
}

    if(!empty($_REQUEST['UF_COMPANY_ID']))$uf_company_id = $_REQUEST['UF_COMPANY_ID'];
    elseif(!empty($arResult['TICKET']['UF_COMPANY_ID']))$uf_company_id = $arResult['TICKET']['UF_COMPANY_ID'];
    elseif(!empty($arResult['TICKET']['COMPANY']['COMPANY_ID']))$uf_company_id = $arResult['TICKET']['COMPANY']['COMPANY_ID'];

    $uf_company_id = str_replace("CO_","",$uf_company_id);

$isTicketClosed = strlen($arResult["TICKET"]["DATE_CLOSE"]) > 0;
$TICKET_SLA = (intval($str_SLA_ID) > 0) ? $str_SLA_ID : CTicketSLA::GetForUser($TICKET_SITE);
$TICKET_DICTIONARY = CTicketDictionary::GetDropDownArray($TICKET_SITE, $TICKET_SLA);
?>

<div class="but_panel">

    <a href="/services/support/"><?= GetMessage("SUP_LIST") ?></a>

    <div class="mess_alert_block">
        <?= GetMessage("SUP_SEL_MESS_ALERT") ?>
    </div>

    <div class="but_block" style="<? if (empty($arResult['TICKET']['ID'])) {
        echo "display: none;";
    } ?>">
        <a class="add_task webform-small-button webform-small-button-blue task-list-toolbar-create" href=""
           onclick="menuOpenClose(); return false;">
            <span class="webform-small-button-left"></span>
            <span class="webform-small-button-icon"></span>
            <span class="webform-small-button-text"><?= GetMessage("SUP_TASK_CREATE") ?></span>
            <span class="webform-small-button-right"></span>
        </a>		

        <div id="task-menu-popup" class="popup-window">

            <div class="popup-window-content" id="popup-window-content-task-menu-popup--1">
                <div class="menu-popup" style="display: block;">
                    <div class="menu-popup-items">
                        <a id="sel_first_mess" title="<?= GetMessage("SUP_ESTIMATION") ?>"
                           class="menu-popup-item menu-popup-item-create" href=""
                           onclick="CreateTaskAll(138,454);
											   return false;"><span
                                class="menu-popup-item-left"></span><span
                                class="menu-popup-item-icon"></span><span
                                class="menu-popup-item-text"><?= GetMessage("SUP_ESTIMATION") ?></span><span
                                class="menu-popup-item-right"></span></a>
                        <a id="sel_first_mess" title="<?= GetMessage("SUP_TO_WORK") ?>"
                           class="menu-popup-item menu-popup-item-create" href=""
                           onclick="CreateTaskAll(35,<?= $uf_company_id ?>);
                               return false;"><span
                                class="menu-popup-item-left"></span><span
                                class="menu-popup-item-icon"></span><span
                                class="menu-popup-item-text"><?= GetMessage("SUP_TO_WORK") ?></span><span
                                class="menu-popup-item-right"></span></a>
                        <a id="sel_first_mess" title="<?= GetMessage("SUP_IDEA") ?>"
                           class="menu-popup-item menu-popup-item-create" href=""
                           onclick="CreateTaskAll(48,738);
											   return false;"><span
                                class="menu-popup-item-left"></span><span
                                class="menu-popup-item-icon"></span><span
                                class="menu-popup-item-text"><?= GetMessage("SUP_IDEA") ?></span><span
                                class="menu-popup-item-right"></span></a>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?
if (!empty($arResult["TICKET"])):
    if (!empty($arResult["ONLINE"])) {
        ?>
        <p>
            <? $time = intval($arResult["OPTIONS"]["ONLINE_INTERVAL"] / 60) . " " . GetMessage("SUP_MIN"); ?>
            <?= str_replace("#TIME#", $time, GetMessage("SUP_USERS_ONLINE")); ?>:<br/>
            <? foreach ($arResult["ONLINE"] as $arOnlineUser): ?>
                <small>(<?= $arOnlineUser["USER_LOGIN"] ?>) <?= $arOnlineUser["USER_NAME"] ?>
                    [<?= FormatDate($DB->DateFormatToPHP(CSite::GetDateFormat('FULL')), MakeTimeStamp($arOnlineUser["TIMESTAMP_X"])) ?>
                    ]
                </small><br/>
            <? endforeach ?>
        </p>
        <?
    }
    ?>

    <? if (!empty($arResult['TICKET']['ID'])) {
    ?>

    <div id="tasks_webdav_area">
        <table class="support-ticket-edit data-table">
            <tr>
                <th><? //GetMessage("SUP_TICKET")
                    ?>

                    <span class="tasks_webdav_area_title"><b><?= GetMessage("SUP_TICKET") ?>
                            �<?= $arResult["TICKET"]["ID"] ?>:
                            <?= $arResult["TICKET"]["TITLE"] ?></b></span>
                </th>
            </tr>
            <tr>
                <td>
                    <div class="ticket_short_info" style="">
                        <div style="float:right;width: 300px">
                            <?$APPLICATION->IncludeComponent(
                                "itees:simple.component",
                                "support.ticket.tasks.bind",
                                array(
                                    "CACHE_TYPE" => "N",
                                    "CACHE_TIME" => "3600",
                                    "CACHE_GROUPS" => "N",
                                    "USE_SEF" => "N",
                                    "TASKS" => $arResult['TASKS'],
                                    "GROUP_ID" => Array(19, 35, 114),
                                    //"MASSAGES" => $status_name,
                                    "TICKET_ID" => $arResult['TICKET']['ID']
                                ),
                                false
                            ); ?>
                        </div>
                        <div style="margin-right: 310px">
                            <?= GetMessage("SUP_SOURCE") . " / " . GetMessage("SUP_FROM") ?>:
                            <? if (strlen($arResult["TICKET"]["SOURCE_NAME"]) > 0): ?>
                                [<?= $arResult["TICKET"]["SOURCE_NAME"] ?>]
                            <? else: ?>
                                [web]
                            <? endif ?>

                            <? if (strlen($arResult["TICKET"]["OWNER_SID"]) > 0): ?>
                                <?= $arResult["TICKET"]["OWNER_SID"] ?>
                            <? endif ?>

                            <? if (intval($arResult["TICKET"]["OWNER_USER_ID"]) > 0): ?>
                                [<?= $arResult["TICKET"]["OWNER_USER_ID"] ?>]
                                (<?= $arResult["TICKET"]["OWNER_LOGIN"] ?>)
                                <?= $arResult["TICKET"]["OWNER_NAME"] ?>
                            <? endif ?>
                            <br/>

                            <?= GetMessage("SUP_CREATE") ?>
                            : <?= FormatDate($DB->DateFormatToPHP(CSite::GetDateFormat('FULL')), MakeTimeStamp($arResult["TICKET"]["DATE_CREATE"])) ?>

                            <? if (strlen($arResult["TICKET"]["CREATED_MODULE_NAME"]) <= 0 || $arResult["TICKET"]["CREATED_MODULE_NAME"] == "support"): ?>
                                [<?= $arResult["TICKET"]["CREATED_USER_ID"] ?>]
                                (<?= $arResult["TICKET"]["CREATED_LOGIN"] ?>)
                                <?= $arResult["TICKET"]["CREATED_NAME"] ?>
                            <? else: ?>
                                <?= $arResult["TICKET"]["CREATED_MODULE_NAME"] ?>
                            <? endif ?>
                            <br/>

                            <? if ($arResult["TICKET"]["DATE_CREATE"] != $arResult["TICKET"]["TIMESTAMP_X"]): ?>
                                <?= GetMessage("SUP_TIMESTAMP") ?>: <?= FormatDate($DB->DateFormatToPHP(CSite::GetDateFormat('FULL')), MakeTimeStamp($arResult["TICKET"]["TIMESTAMP_X"])) ?>

                                <? if (strlen($arResult["TICKET"]["MODIFIED_MODULE_NAME"]) <= 0 || $arResult["TICKET"]["MODIFIED_MODULE_NAME"] == "support"): ?>
                                    [<?= $arResult["TICKET"]["MODIFIED_USER_ID"] ?>]
                                    (<?= $arResult["TICKET"]["MODIFIED_BY_LOGIN"] ?>)
                                    <?= $arResult["TICKET"]["MODIFIED_BY_NAME"] ?>
                                <? else: ?>
                                    <?= $arResult["TICKET"]["MODIFIED_MODULE_NAME"] ?>
                                <? endif ?>

                                <br/>
                            <? endif ?>

                            <? if ($isTicketClosed): ?>
                                <?= GetMessage("SUP_CLOSE") ?>: <?= FormatDate($DB->DateFormatToPHP(CSite::GetDateFormat('FULL')), MakeTimeStamp($arResult["TICKET"]["DATE_CLOSE"])) ?>
                            <? endif ?>

                            <? if (strlen($arResult["TICKET"]["STATUS_NAME"]) > 0) : ?>
                                <?= GetMessage("SUP_STATUS") ?>: <span
                                    title="<?= $arResult["TICKET"]["STATUS_DESC"] ?>"><?= $arResult["TICKET"]["STATUS_NAME"] ?></span>
                                <br/>
                            <? endif; ?>

                            <? if (strlen($arResult["TICKET"]["CATEGORY_NAME"]) > 0): ?>
                                <?= GetMessage("SUP_CATEGORY") ?>: <span
                                    title="<?= $arResult["TICKET"]["CATEGORY_DESC"] ?>"><?= $arResult["TICKET"]["CATEGORY_NAME"] ?></span>
                                <br/>
                            <? endif ?>

                            <? if (strlen($arResult["TICKET"]["CRITICALITY_NAME"]) > 0) : ?>
                                <?= GetMessage("SUP_CRITICALITY") ?>: <span
                                    title="<?= $arResult["TICKET"]["CRITICALITY_DESC"] ?>"><?= $arResult["TICKET"]["CRITICALITY_NAME"] ?></span>
                                <br/>
                            <? endif ?>

                            <? if (intval($arResult["TICKET"]["RESPONSIBLE_USER_ID"]) > 0): ?>
                                <?= GetMessage("SUP_RESPONSIBLE") ?>: [<?= $arResult["TICKET"]["RESPONSIBLE_USER_ID"] ?>]
                                (<?= $arResult["TICKET"]["RESPONSIBLE_LOGIN"] ?>) <?= $arResult["TICKET"]["RESPONSIBLE_NAME"] ?>
                                <br/>
                            <? endif ?>

                            <? if (strlen($arResult["TICKET"]["SLA_NAME"]) > 0) : ?>
                                <?= GetMessage("SUP_SLA") ?>:
                                <span
                                    title="<?= $arResult["TICKET"]["SLA_DESCRIPTION"] ?>"><?= $arResult["TICKET"]["SLA_NAME"] ?></span>
                                <br/>
                            <? endif ?>

                            <? if (!empty($arResult['TICKET']['COMPANY_LINK'])) {
                                ?>
                                <?= GetMessage("SUP_CONTACT") ?>: <?= $arResult['TICKET']['COMPANY_LINK'] ?><br/>
                                <?
                            }
							global $USER;
							
							if(is_array($arResult['TICKET']['CLIENTS_CRM_LINK']) && count($arResult['TICKET']['CLIENTS_CRM_LINK'])){
								foreach($arResult['TICKET']['CLIENTS_CRM_LINK'] as $keyClientCRM => $clientCRMLink){
									if(preg_match('/L_/', $keyClientCRM)){
										echo '���: ';
									} elseif(preg_match('/D_/', $keyClientCRM)){
										echo '������: ';
									} elseif(preg_match('/C_/', $keyClientCRM)){
										echo '�������: ';
									} elseif(preg_match('/CO_/', $keyClientCRM)){
										echo '��������: ';
									}
									
									echo $clientCRMLink . '<br>';
								}
							}
							
                            if (!empty($arResult['TICKET']['COMPANY_MANAGER_NAME'])) {
                                ?>
                                <?= GetMessage("SUP_MANAGER") ?>: <?= $arResult['TICKET']['COMPANY_MANAGER_NAME'] ?>
                                <br/>
                            <? } ?>
                        </div>
                </td>
            </tr>
            <tr>
                <th><?= GetMessage("SUP_DISCUSSION") ?></th>
            </tr>
            <tr>
                <td>
                    <?= $arResult["NAV_STRING"] ?>
                    <div id="ticket-edit-panel" class="ticket-edit-message ticket-edit-panel">
                        <label class="message_select_all">
                            <input id="select_all" type="checkbox"
                                   onclick="ActAll($(this));"><?= GetMessage("SUP_FOR_ALL_MESSAGE"); ?>
                        </label>
                    </div>
                    <? foreach ($arResult["MESSAGES"] as $arMessage): ?>
                        <div class="ticket-edit-message" >
                            <a id="<?= $arMessage['ID'] ?>"></a>

                            <div class="support-float-quote">
                                <? /* ?>[&nbsp;<a href="javascript:void(0);" onclick="MessAct('<?=$arMessage["ID"]?>', 'COPY');"><?=GetMessage('SUP_MESS_COPY');?></a>&nbsp;]<? */
                                ?>
                                <? /*global $USER;if($USER->IsAdmin()){*/
                                ?>
                                [&nbsp;<a
                                    href="/services/support/?ID=0&edit=1&TICKET_ID=<?= $arResult['TICKET']['ID'] ?>&MESSAGE_ID=<?= $arMessage['ID'] ?>"><?= GetMessage('SUP_MESS_NEW'); ?></a>&nbsp;]
                                <? /*}*/
                                ?>
                                [&nbsp;<a href="javascript:void(0);"
                                          onclick="MessAct('<?= $arMessage["ID"] ?>', 'TRANSFER');"><?= GetMessage('SUP_MESS_TRANSFER'); ?></a>&nbsp;]

                                <? global $USER;
                                if ($USER->GetID() == 502) {
                                    ?>
                                    [&nbsp;<a href="javascript:void(0);"
                                              onclick="MessAct('<?= $arMessage["ID"] ?>', 'DELETE');"><?= GetMessage('SUP_MESS_DELETE'); ?></a>&nbsp;]
                                <? } ?>
                                [&nbsp;<a href="#postform"
                                          OnMouseDown="javascript:SupQuoteMessage('quotetd<? echo $arMessage["ID"]; ?>')"
                                          title="<?= GetMessage("SUP_QUOTE_LINK_DESCR"); ?>"><? echo GetMessage("SUP_QUOTE_LINK"); ?></a>&nbsp;]
                            </div>

                            <label class="message_select">
                                <input id="<?= $arMessage['ID'] ?>"
                                       type="checkbox"><?= GetMessage("SUP_SELECT_MESSAGE"); ?>
                            </label>

                            <div align="left">
                                <b><?= GetMessage("SUP_TIME") ?></b>: <?= FormatDate($DB->DateFormatToPHP(CSite::GetDateFormat('FULL')), MakeTimeStamp($arMessage["DATE_CREATE"])) ?>
                            </div>
                            <b><?= GetMessage("SUP_FROM") ?></b>:

                            <?= $arMessage["OWNER_SID"] ?>

                            <? if (intval($arMessage["OWNER_USER_ID"]) > 0): ?>
                                [<?= $arMessage["OWNER_USER_ID"] ?>]
                                (<?= $arMessage["OWNER_LOGIN"] ?>)
                                <?= $arMessage["OWNER_NAME"] ?>
                            <? endif ?>
                            <br/>

                            <?
                            if (!empty($arMessage["FILES"])) {
                                if (!empty($arMessage['MESS_FILE_TYPE_IMG'])) {
                                    ?>
                                    <div class="feed-com-files-title"><?= GetMessage("SUP_FOTO"); ?>:</div>
                                    <div style="margin-left: 60px;">
                                        <? $aImg = array("gif", "png", "jpg", "jpeg", "bmp");
                                        foreach ($arMessage["FILES"] as $arFile) {
                                            if (in_array(strtolower(GetFileExtension($arFile["NAME"])), $aImg)) {
                                                ?>
                                                <div
                                                    class="sup_mess_file"
                                                    style="
                                                        background: url(<?= $componentPath ?>/ticket_show_file.php?hash=<?= $arFile["HASH"] ?>&amp;lang=<?= LANG ?>&amp;) no-repeat center center;
                                                        background-size: <?= ($arFile['WIDTH'] < 69 || $arFile['HEIGHT'] < 69) ? 'contain' : 'cover' ?>;
                                                        "
                                                    id="sup-attach-image-<?= $arFile['ID'] ?>"
                                                    width="69"
                                                    height="69"
                                                    border="0"
                                                    title="<?= $arFile['NAME'] ?>"
                                                    alt="<?= $arFile['NAME'] ?>"
                                                    data-bx-viewer="image"
													data-viewer-type="image"
                                                    data-bx-title="<?= $arFile['NAME'] ?>"
                                                    data-bx-size="<?= CFile::FormatSize($arFile["FILE_SIZE"]) ?>"
                                                    data-bx-download="<?= $componentPath ?>/ticket_show_file.php?hash=<?= $arFile["HASH"] ?>&amp;lang=<?= LANG ?>&amp;action=download"
                                                    data-bx-storage=""
                                                    data-src="<?= $componentPath ?>/ticket_show_file.php?hash=<?= $arFile["HASH"] ?>&amp;lang=<?= LANG ?>&amp;action=download"
                                                    data-bx-width="<?= $arFile['WIDTH'] ?>"
                                                    data-bx-height="<?= $arFile['HEIGHT'] ?>"
                                                    data-bx-full="<?= $componentPath ?>/ticket_show_file.php?hash=<?= $arFile["HASH"] ?>&amp;lang=<?= LANG ?>&amp;"
                                                    data-bx-full-width="<?= $arFile['WIDTH'] ?>"
                                                    data-bx-full-height="<?= $arFile['HEIGHT'] ?>"
                                                    data-bx-full-size="<?= CFile::FormatSize($arFile["FILE_SIZE"]) ?>"
                                                    ></div>
                                                <?
                                            }
                                        }
                                        ?>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                    <?
                                }
                                if (!empty($arMessage['MESS_FILE_TYPE_FILE'])) {
                                    ?>
                                    <div class="feed-com-files-title"><?= GetMessage("SUP_FILES"); ?>:</div>
                                    <div style="margin: 3px 0 10px 60px;">
                                        <? foreach ($arMessage["FILES"] as $arFile) {
                                            if (!in_array(strtolower(GetFileExtension($arFile["NAME"])), $aImg)) {
                                                ?>
                                                <a title="<?= str_replace("#FILE_NAME#", $arFile["NAME"], GetMessage("SUP_DOWNLOAD_ALT")) ?>"
                                                   href="<?= $componentPath ?>/ticket_show_file.php?hash=<?= $arFile["HASH"] ?>&amp;lang=<?= LANG ?>&amp;action=download"><?= $arFile["NAME"] ?></a> (<?= CFile::FormatSize($arFile["FILE_SIZE"]) ?>)
                                                <br class="clear"/>
                                                <?
                                            }
                                        }
                                        ?>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                    <?
                                }
                            }
                            ?>
                            <br/>

                            <div id="quotetd<? echo $arMessage["ID"]; ?>"><?= $arMessage["MESSAGE"] ?></div>
                        </div>
                    <? endforeach ?>

                    <?= $arResult["NAV_STRING"] ?>

                </td>

            </tr>
        </table>
    </div>
    <script type="text/javascript">
        top.BX.viewElementBind(
            BX('tasks_webdav_area'),
            {},
			false
			//BX('sup-attach-image-<?= $arFile['ID'] ?>')
            /*function (node) {console.dir(node);
                return BX.type.isElementNode(node) && (node.getAttribute('data-bx-viewer') || node.getAttribute('data-bx-image'));
            }*/
        );
    </script>
    <br/>
<? } ?>
<? endif; ?>
<form name="form1" id="form1" method="post" action="<?= $arResult["REAL_FILE_PATH"] ?>" enctype="multipart/form-data"
      onsubmit="if(!BX('OWNER_SID').value){return false;}">
    <?= bitrix_sessid_post() ?>

    <input type="hidden" name="ID" value=<?= (empty($arResult["TICKET"]) ? 0 : $arResult["TICKET"]["ID"]) ?>/>
    <input type="hidden" name="edit" value="1">
    <input type="hidden" name="lang" value="<?= LANG ?>"/>
    <table class="support-ticket-edit-form data-table">

        <? if (empty($arResult["TICKET"]['ID'])) { ?>
            <tr>
                <th colspan="2"><?= GetMessage("SUP_TICKET") ?></th>
            </tr>
            <?
        } else { ?>
            <tr>
                <th colspan="2"><?= GetMessage("SUP_ANSWER") ?></th>
            </tr>
            <?
        }

        if (!empty($arResult['TICKET']['COMPANY'])) {
            ?>
            <tr>
                <td align="right" width="20%" nowrap><?= GetMessage("SUP_COMPANY") ?>:</td>
                <td width="80%" nowrap><a
                        href="/crm/company/show/<?= $arResult['TICKET']['COMPANY']['COMPANY_ID'] ?>/"><?= $arResult['TICKET']['COMPANY']['COMPANY_TITLE'] ?></a>
                </td>
            </tr>
            <tr>
                <td align="right" width="20%" nowrap><?= GetMessage("SUP_COM_CONTACT") ?>:</td>
                <td width="80%" nowrap><a
                        href="/crm/contact/show/<?= $arResult['TICKET']['COMPANY']['ID'] ?>/"><?= $arResult['TICKET']['COMPANY']['FULL_NAME'] ?></a>
                </td>
            </tr>
        <? } ?>
        <tr class="bb c_show">
            <td align="right" width="20%" nowrap></td>
            <td width="80%" nowrap><a id="AUTHOR_CHANGE_SHOW"
                                      href="javascript:void(0);"><?= GetMessage("SUP_CHANGE_AUTHOR") ?></a></td>
        </tr>


        <? if (empty($arResult['TICKET']['COMPANY'])) {?>

            <tr class="c_hide">
                <td align="right" width="20%" nowrap><?= GetMessage("SUP_COMPANY") ?>:</td>
                <td width="80%" nowrap>
                    <?

                    $crmUf = Array(
                        "FIELD_NAME" => "UF_COMPANY_ID",
                        "USER_TYPE_ID" => "crm",
                        "XML_ID" => "",
                        "SORT" => 100,
                        "MULTIPLE" => "N",
                        "MANDATORY" => "N",
                        "SHOW_FILTER" => "N",
                        "SHOW_IN_LIST" => "Y",
                        "EDIT_IN_LIST" => "Y",
                        "IS_SEARCHABLE" => "N",
                        "SETTINGS" => Array
                        (
                            "LEAD" => "N",
                            "CONTACT" => "N",
                            "COMPANY" => "Y",
                            "DEAL" => "N"
                        ),
                        "USER_TYPE" => Array
                        (
                            "USER_TYPE_ID" => "crm",
                            "CLASS_NAME" => "CUserTypeCrm",
                            "DESCRIPTION" => "�������� � ��������� CRM",
                            "BASE_TYPE" => "string"
                        ),

                        "VALUE" => Array
                        (
                            "0" => "CO_".$uf_company_id
                        ),

                        "FIELD_NAME_ORIG" => "UF_COMPANY_ID",
                    );

                    $APPLICATION->IncludeComponent(
                        'bitrix:system.field.edit' ,
                        $crmUf["USER_TYPE"]["USER_TYPE_ID"],
                        array(
                            "bVarsFromForm" => false,
                            "arUserField" => $crmUf,
                            // "form_name" => "task-form",
                            "form_name" => "form1",
                            // "usePrefix" => "N",
                            'SHOW_FILE_PATH'    => false,
                            'FILE_URL_TEMPLATE' => '/bitrix/components/bitrix/tasks.task.detail/show_file.php?fid=#file_id#'
                        ), null, array("HIDE_ICONS" => "Y")
                    );?>
                </td>
            </tr>

            <? /*?>
            <tr class="c_hide">
                <td align="right" width="20%" nowrap><?= GetMessage("SUP_COMPANY") ?>:</td>
                <td width="80%" nowrap>
                    <? $APPLICATION->IncludeComponent(
                        'bitrix:crm.field.filter',
                        'crm',
                        array(
                            'arUserField' => Array(
                                'USER_TYPE' => 'crm',
                                'FIELD_NAME' => 'UF_COMPANY_ID',
                                'MULTIPLE' => 'N',
                                'SETTINGS' => array('COMPANY' => 'Y'),
                                'VALUE' => isset($_REQUEST['UF_COMPANY_ID']) ? intval($_REQUEST['UF_COMPANY_ID']) : '',
                            ),
                            'bVarsFromForm' => false,
                            'form_name' => 'filter_form1'
                        ),
                        false,
                        array('HIDE_ICONS' => true)
                    ); ?>
                </td>
            </tr>

 <?
 */
 ?>
            <tr id="CONTACT_SEL_BOX" style="display: none;" class="c_hide_c">
                <td align="right" width="20%" nowrap><?= GetMessage("SUP_COM_CONTACT") ?>:</td>
                <td class="sel" width="80%" nowrap></td>
            </tr>
        <? } ?>
        <tr class="c_hide">
            <td align="right" width="20%" nowrap><?= GetMessage("SUP_SOURCE") ?>:</td>
            <td width="80%" nowrap>
                <?= SelectBoxFromArray("SOURCE_ID", __GetDropDown("SR", $TICKET_DICTIONARY), $arResult['TICKET']['SOURCE_ID'], "< web >", "OnChange=SelectSource() class='inputselect'"); ?>
            </td>
        </tr>
        <? if (!empty($arResult['TICKET']['COMPANY']['MAILS'])) { ?>
            <tr id="CONTACT_EMAIL_SEL_BOX" class="c_hide">
                <td align="right" width="20%" nowrap><?= GetMessage("SUP_CONTACT_MAIL") ?>:</td>
                <td class="sel" width="80%" nowrap>
                    <select id="CONTACT_EMAIL_SEL" onchange="ContMailSel($(this).val());">
                        <option>-------</option>
                        <? foreach ($arResult['TICKET']['COMPANY']['MAILS'] as $mKey => $mail) { ?>
                            <option value="<?= $mKey ?>"><?= $mail ?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <script>
                var arContact =
                <?=CUtil::PhpToJsObject(
                               array(
                                   'EMAIL' => $arResult['TICKET']['COMPANY']['MAILS'],
                                   'FULL_NAME' => $arResult['TICKET']['COMPANY']['FULL_NAME'],
                               )
                           )?>
            </script>
        <? }else{ ?>
            <tr id="CONTACT_EMAIL_SEL_BOX" style="display: none;" class="c_hide_c">
                <td align="right" width="20%" nowrap><?= GetMessage("SUP_CONTACT_MAIL") ?>:</td>
                <td class="sel" width="80%" nowrap></td>
            </tr>
        <? } ?>
        <tr class="c_hide" id="SEL_CONTACT_LIST_BOX">
            <td align="right" width="20%" nowrap><?= GetMessage("SUP_SELECTED_RECIPIENTS") ?>:</td>
            <td width="80%" nowrap id="SEL_CONTACT_LIST"></td>
        </tr>
        <tr class="bb c_hide">
            <td align="right" width="20%" nowrap><?= GetMessage("SUP_TICKET_AUTHOR") ?>:</td>
            <td width="80%" nowrap>
                <input readonly type="text" size="65" name="OWNER_SID" id="OWNER_SID"
                       value="<?= $arResult['TICKET']['OWNER_SID'] ?>">
                <?
                if (intval($arResult['TICKET']['OWNER_USER_ID']) > 0) {
                    $owner_name = $arResult['TICKET']['OWNER_NAME'];
					echo FindUserID("OWNER_USER_ID", $arResult['TICKET']['OWNER_USER_ID'], $owner_name); //� 18 ������ �������� ��������� �������� � ����������
                }
                if ($ID > 0):
                    ?><br>[&nbsp;<a
                    href="/bitrix/admin/ticket_list.php?set_filter=Y&lang=<?= LANGUAGE_ID ?>&<?= implode("&", $arAuthorFilter) ?>"><?= GetMessage("SUP_AUTHOR_TICKETS") ?></a>&nbsp;]<?
                endif;
                ?>
            </td>
        </tr>
        <? if (empty($arResult["TICKET"]['ID'])) { ?>
            <style>
                .c_hide {
                    display: table-row !important;
                }
                .c_show {
                    display: none !important;
                }
            </style>
        <? } else { ?>
            <tr class="bb c_hide">
                <td align="right" width="20%" nowrap></td>
                <td width="80%" nowrap><a id="AUTHOR_CHANGE_HIDE"
                                          href="javascript:void(0);"><?= GetMessage("SUP_CHANGE_AUTHOR_HIDE") ?></a>
                </td>
            </tr>
        <? } ?>
        <? if (!empty($arResult["TICKET"]['ID'])) { ?>
            <tr>
                <td align="right" width="20%" nowrap><?= GetMessage("SUP_ON_BEHALF_OWNER") ?>:</td>
                <td width="80%" nowrap>
                    <input type="checkbox"
                        <?= ($isTicketClosed) ? 'disabled' : '' ?>
                           name="ON_BEHALF_OWNER">
                </td>
            </tr>
        <? } ?>


        <SCRIPT LANGUAGE="JavaScript">
            <!--
            SelectSource();
            //-->
        </SCRIPT>
        <!---->
        <? global $USER;
        if (!empty($arResult["TICKET"]['ID'])) {
            ?>
            <tr>
                <td class="field-name border-none"><?= GetMessage("SUP_TITLE") ?>:</td>
                <td class="border-none"><input type="text" name="CHANGE_TITLE"
                                               value="<?= $arResult["TICKET"]["TITLE"] ?>"
                        <?= ($isTicketClosed) ? 'disabled' : '' ?>
                                               size="48" maxlength="255"/>
                </td>
            </tr>
            <?
        } else {
            ?>
            <tr>
                <td class="field-name border-none"><?= GetMessage("SUP_TITLE") ?>:</td>
                <td class="border-none"><input type="text" name="TITLE" value="<?
                    if (!empty($arResult['TICKET']['TITLE'])) {
                        echo $arResult['TICKET']['TITLE'];
                    } else {
                        echo htmlspecialcharsbx($_REQUEST["TITLE"]);
                    }
                    ?>" size="48" maxlength="255"/></td>
            </tr>
            <?
        }
        ?>

        <tr>
            <td class="field-name"><?= GetMessage("SUP_CRITICALITY") ?>:</td>
            <td>
                <?
                if (empty($arResult["TICKET"]) || strlen($arResult["ERROR_MESSAGE"]) > 0) {
                    if (strlen($arResult["DICTIONARY"]["CRITICALITY_DEFAULT"]) > 0 && strlen($arResult["ERROR_MESSAGE"]) <= 0)
                        $criticality = $arResult["DICTIONARY"]["CRITICALITY_DEFAULT"];
                    else
                        $criticality = htmlspecialcharsbx($_REQUEST["CRITICALITY_ID"]);
                } else
                    $criticality = $arResult["TICKET"]["CRITICALITY_ID"];
                ?>
                
                <?if($isTicketClosed):?>
                    <span><?=$arResult["DICTIONARY"]["CRITICALITY"][$criticality]?></span>
                    <input type="hidden" value="<?= $criticality ?>" name="CRITICALITY_ID"/>
                <?else:?> 
                    <select name="CRITICALITY_ID" id="CRITICALITY_ID">
                        <option value="">&nbsp;</option>
                        <? foreach ($arResult["DICTIONARY"]["CRITICALITY"] as $value => $option): ?>
                            <option value="<?= $value ?>"
                                    <? if ($criticality == $value): ?>selected="selected"<? endif ?>><?= $option ?></option>
                        <? endforeach ?>
                    </select>
                <? endif;?>
            </td>
        </tr>
        <tr>
            <td class="field-name"><?= GetMessage("SUP_CATEGORY") ?>:</td>
            <td>
                <?
                if($arResult["TICKET"]["CATEGORY_ID"] > 0)
                    $category = $arResult["TICKET"]["CATEGORY_ID"];
                elseif (strlen($arResult["DICTIONARY"]["CATEGORY_DEFAULT"]) > 0 && strlen($arResult["ERROR_MESSAGE"]) <= 0)
                    $category = $arResult["DICTIONARY"]["CATEGORY_DEFAULT"];
                else
                    $category = htmlspecialcharsbx($_REQUEST["CATEGORY_ID"]);
                ?>
                <?if($isTicketClosed):?>
                    <span><?=$arResult["DICTIONARY"]["CATEGORY"][$category]?></span>
                    <input type="hidden" value="<?= $category ?>" name="CATEGORY_ID"/>
                <?else:?>
                    <select name="CATEGORY_ID" id="CATEGORY_ID">
                        <option value="">&nbsp;</option>
                        <? foreach ($arResult["DICTIONARY"]["CATEGORY"] as $value => $option): ?>
                            <option value="<?= $value ?>"
                                    <? if ($category == $value): ?>selected="selected"<? endif ?>><?= $option ?></option>
                        <? endforeach ?>
                    </select>
                <? endif;?>
            </td>
        </tr>



        <tr>
            <td class="field-name"><?= GetMessage("SUP_RESPONSIBLE") ?>:</td>
            <td>
                <select name="RESPONSIBLE_USER_ID"
                        id="RESPONSIBLE_USER_ID" <?= ($isTicketClosed) ? 'disabled' : '' ?>>
                    <option value="">&nbsp;</option>
                    <? foreach ($arResult['DICTIONARY']['RESPONSIBLE'] as $respKey => $responsible) {
                        if ($arResult["TICKET"]["RESPONSIBLE_USER_ID"] == $respKey) {
                            ?>
                            <option value="<?= $respKey ?>" selected="selected"><?= $responsible ?></option><?
                        } else {
                            ?>
                            <option value="<?= $respKey ?>"><?= $responsible ?></option><?
                        }
                    }
                    ?>
                </select>
            </td>
        </tr>
        <? if (!$isTicketClosed): ?>
            <tr style="display: none">
                <td class="field-name"><?= GetMessage("SUP_CLOSE_TICKET") ?>:</td>
                <td><input type="checkbox" name="CLOSE" value="Y"
                           <? if ($arResult["TICKET"]["CLOSE"] == "Y"): ?>checked="checked" <? endif ?>/>
                </td>
            </tr>
        <? else: ?>
            <tr style="display: none">
                <td class="field-name"><?= GetMessage("SUP_OPEN_TICKET") ?>:</td>
                <td><input type="checkbox" name="OPEN" value="Y"
                           <? if ($arResult["TICKET"]["OPEN"] == "Y"): ?>checked="checked" <? endif ?>/>
                </td>
            </tr>
        <? endif; ?>

        <? if ($arParams['SHOW_COUPON_FIELD'] == 'Y' && $arParams['ID'] <= 0) { ?>
            <tr>
                <td class="field-name"><?= GetMessage("SUP_COUPON") ?>:</td>
                <td><input type="text" name="COUPON" value="<?= htmlspecialcharsbx($_REQUEST["COUPON"]) ?>" size="48"
                           maxlength="255"/>
                </td>
            </tr>
            <?
        }
        global $USER_FIELD_MANAGER;
        if (isset($arParams["SET_SHOW_USER_FIELD_T"])) {
            foreach ($arParams["SET_SHOW_USER_FIELD_T"] as $k => $v) {
                $v["ALL"]["VALUE"] = $arParams[$k];
                echo '<tr><td  class="field-name">' . htmlspecialcharsbx($v["NAME_F"]) . ':</td><td>';
                $APPLICATION->IncludeComponent(
                    'bitrix:system.field.edit',
                    $v["ALL"]['USER_TYPE_ID'],
                    array(
                        'arUserField' => $v["ALL"],
                    ),
                    null,
                    array('HIDE_ICONS' => 'Y')
                );
                echo '</td></tr>';
            }
        }
        ?>
        </tbody>
    </table>
    <br/>
    <?  if (!empty($_REQUEST['TICKET_ID'])) { ?>
        <input type="hidden" value="<?= $_REQUEST['TICKET_ID'] ?>" name="TICKET_ID"/>
        <?
    }
    if (!empty($_REQUEST['MESSAGE_ID'])) {
        ?>
        <input type="hidden" value="<?= $_REQUEST['MESSAGE_ID'] ?>" name="MESSAGE_ID"/>
    <? } ?>
    <?

    if (!$isTicketClosed){
        include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/inc_visual_editor.php");
     } ?>

        <input type="hidden" value="Y" name="apply"/>

        <br/>

    <div class="but_block" style="float: left;margin-right:30px;">
        <a class="webform-small-button webform-small-button-blue1 task-list-toolbar-create"
           href="<?=$_SERVER["SCRIPT_URL"]?>" >
            <span class="webform-small-button-arrow-left">&nbsp;&nbsp;&nbsp;</span>
            <span class="webform-small-button-icon"></span>
            <span class="webform-small-button-text"><?=GetMessage("SUP_BACK")?></span>
            <span class="webform-small-button-right"></span>
        </a>
    </div>
    <div class="but_block" style="float: left;margin-right:30px;">
        <a class="apply_ticket  add_task webform-small-button webform-small-button-blue task-list-toolbar-create"
           href="javascript:void(0);" onclick="sub(this);">
            <span class="webform-small-button-left"></span>
            <span class="webform-small-button-icon"></span>
            <span class="webform-small-button-text"><?=GetMessage("SUP_SEND")?></span>
            <span class="webform-small-button-right"></span>
        </a>
    </div>
        <? if ($_REQUEST["ID"]!=0) {
                if (!$isTicketClosed) { ?>
                <span class="webform-small-button webform-small-button-decline close_ticket  " onclick="sub(this);">
                    <span class="webform-small-button-text"><?=GetMessage("TICKET_CLOSE_BUTTON")?></span>
                </span>
            <? }else{?>

                <span class="webform-small-button webform-small-button-disable open_ticket  " onclick="sub(this);">
                    <span class="webform-small-button-text"><?=GetMessage("TICKET_OPEN_BUTTON")?></span>
                </span>
            <?}
        }
    ?>
    <input type="hidden" value="" name="STATUS_ID"/>
    
</form>
    
<div id="before-ticket-close" hidden>
    <?if(!empty($arResult["STATUSES"])):?>
        <div><?=GetMessage('TICKET_CLOSE_SET_STATUS_TEXT')?></div>
        <?foreach($arResult["STATUSES"] as $status):?>
            <p>
                <input type="radio" id="final-status-<?=$status["ID"]?>" name="final_status" value="<?=$status["ID"]?>">
                <label for="final-status-<?=$status["ID"]?>"><b><?=$status["NAME"]?></b></label>
            </p>
        <? endforeach;?>
    <? endif;?>
    <div id="status-error"><?=GetMessage("TICKET_CLOSE_STATUS_ERROR")?></div>
    <p><?=GetMessage('TICKET_CLOSE_SELECT_CATEGORY')?></p>
    <select name="close_category" id="close_category">
        <? foreach ($arResult["DICTIONARY"]["CATEGORY"] as $k => $cat):?>
            <option value="<?=$k?>" <?=$arResult["TICKET"]["CATEGORY_ID"]==$k ? 'selected': ''?>><?=$cat?></option>    
        <? endforeach;?> 
    </select>      
</div>

<div id="ajax-copy-mess"></div>
