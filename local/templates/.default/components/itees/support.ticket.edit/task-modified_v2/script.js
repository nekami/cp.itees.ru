
function AddFileInput()
{
	var counter = document.getElementById("files_counter").value;
	var table = document.getElementById("files_table_"+counter);

	document.getElementById("files_counter").value = ++counter;
	table.innerHTML += '<input name="FILE_'+counter+'" size="30" type="file"><br /><span id="files_table_'+counter+'"></span>';
}


function SupQuoteMessage(id)
{
	var selection;
	if (document.getSelection)
	{
		var selection = "" + document.getSelection();
		selection = selection.replace(/\r\n\r\n/gi, "_newstringhere_");
		selection = selection.replace(/\r\n/gi, " ");
		selection = selection.replace(/  /gi, "");
		selection = selection.replace(/_newstringhere_/gi, "\r\n\r\n");
	}
	else
	{
		selection = document.selection.createRange().text;
	}

	if (selection!="")
	{
		document.forms["form1"].elements["MESSAGE"].value += "<QUOTE>"+selection+"</QUOTE>\n";
	}
	else
	{
		var el = document.getElementById(id);
		var textData = (el.innerText) ? el.innerText : el.textContent;
		if(el)
		{
			var str = textData
			str = str.replace(/\r\n\r\n/gi, "_newstringhere_");
			str = str.replace(/\r\n/gi, " ");
			str = str.replace(/<br[^>]*>/gi, "");
			str = str.replace(/<\/p[^>]*>/gi, "\r\n");
			str = str.replace(/<li[^>]*>/gi, "\r\n");
			str = str.replace(/<[^>]*>/gi, " ");
			str = str.replace(/  /gi, "");
			str = str.replace(/_newstringhere_/gi, "\r\n");
			document.forms["form1"].elements["MESSAGE"].value += "<QUOTE>"+str+"</QUOTE>\n";
		}
	}
}

var QUOTE_open = 0;
var CODE_open = 0;
var B_open = 0;
var I_open = 0;
var U_open = 0;

var myAgent   = navigator.userAgent.toLowerCase();
var myVersion = parseInt(navigator.appVersion);
var myVersion = parseInt(navigator.appVersion);
var is_ie  = ((myAgent.indexOf("msie") != -1)  && (myAgent.indexOf("opera") == -1));
var is_nav = ((myAgent.indexOf('mozilla')!=-1) && (myAgent.indexOf('spoofer')==-1)
 && (myAgent.indexOf('compatible') == -1) && (myAgent.indexOf('opera')==-1)
 && (myAgent.indexOf('webtv')==-1) && (myAgent.indexOf('hotjava')==-1));

var is_win = ((myAgent.indexOf("win")!=-1) || (myAgent.indexOf("16bit")!=-1));
var is_mac = (myAgent.indexOf("mac")!=-1);


function insert_tag(thetag, objTextarea)
{
	var tagOpen = eval(thetag + "_open");
	if (tagOpen == 0)
	{
		if (DoInsert(objTextarea, "<"+thetag+">", "</"+thetag+">"))
		{
			eval(thetag + "_open = 1");
			eval("document.forms['form1'].elements['"+thetag+"'].value += '*'");
			//eval("document.form1." + thetag + ".value += '*'");
		}
	}
	else
	{
		DoInsert(objTextarea, "</"+thetag+">", "");
		//eval("document.form1." + thetag + ".value = ' " + eval(thetag + "_title") + " '");

		var buttonText = eval("document.forms['form1'].elements['"+thetag+"'].value");
		eval("document.forms['form1'].elements['"+thetag+"'].value = '"+(buttonText.slice(0,-1))+"'");

		eval(thetag + "_open = 0");
	}
}

function mozillaWr(textarea, open, close)
{
	var selLength = textarea.textLength;
	var selStart = textarea.selectionStart;
	var selEnd = textarea.selectionEnd;
	
	if (selEnd == 1 || selEnd == 2)
	selEnd = selLength;

	var s1 = (textarea.value).substring(0,selStart);
	var s2 = (textarea.value).substring(selStart, selEnd)
	var s3 = (textarea.value).substring(selEnd, selLength);
	textarea.value = s1 + open + s2 + close + s3;

	textarea.selectionEnd = 0;
	textarea.selectionStart = selEnd + open.length + close.length;
	return;
}


function DoInsert(objTextarea, Tag, closeTag)
{
	var isOpen = false;

	//if (closeTag=="")
		//isOpen = true;

	if ( myVersion >= 4 && is_ie && is_win && objTextarea.isTextEdit)
	{
		objTextarea.focus();
		var sel = document.selection;
		var rng = sel.createRange();
		rng.colapse;
		if ((sel.type=="Text" || sel.type=="None") && rng != null)
		{
			if (closeTag!="")
			{
				if (rng.text.length > 0) 
					Tag += rng.text + closeTag; 
				else
					isOpen = true;
			}
			rng.text = Tag;
		}
	}
	else
	{
		if (is_nav && document.getElementById)
		{
			if (closeTag!="" && objTextarea.selectionEnd > objTextarea.selectionStart)
			{
				mozillaWr(objTextarea, Tag, closeTag);
				isOpen = false;
			}
			else
			{
				mozillaWr(objTextarea, Tag, '');
				isOpen = true;
			}
		}
		else
		{
			objTextarea.value += Tag;
			isOpen = true;
		}

		//isOpen = true;
		//objTextarea.value += Tag;
	}



	objTextarea.focus();
	return isOpen;
} 

// Транслитерация

//var TRANSLIT_title = "<?=GetMessage("SUP_TRANSLIT")?>";
var TRANSLIT_way = 0;

var smallEngLettersReg = new Array(/e'/g, /ch/g, /sh/g, /yo/g, /jo/g, /zh/g, /yu/g, /ju/g, /ya/g, /ja/g, /a/g, /b/g, /v/g, /g/g, /d/g, /e/g, /z/g, /i/g, /j/g, /k/g, /l/g, /m/g, /n/g, /o/g, /p/g, /r/g, /s/g, /t/g, /u/g, /f/g, /h/g, /c/g, /w/g, /~/g, /y/g, /'/g);
var capitEngLettersReg = new Array( /E'/g, /CH/g, /SH/g, /YO/g, /JO/g, /ZH/g, /YU/g, /JU/g, /YA/g, /JA/g, /A/g, /B/g, /V/g, /G/g, /D/g, /E/g, /Z/g, /I/g, /J/g, /K/g, /L/g, /M/g, /N/g, /O/g, /P/g, /R/g, /S/g, /T/g, /U/g, /F/g, /H/g, /C/g, /W/g, /~/g, /Y/g, /'/g);
var smallRusLetters = new Array("э", "ч", "ш", "ё", "ё", "ж", "ю", "ю", "я", "я", "а", "б", "в", "г", "д", "е", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "щ", "ъ", "ы", "ь");
var capitRusLetters = new Array( "Э", "Ч", "Ш", "Ё", "Ё", "Ж", "Ю", "Ю", "\Я", "\Я", "А", "Б", "В", "Г", "Д", "Е", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Щ", "Ъ", "Ы", "Ь");

var smallEngLetters = new Array("e", "ch", "sh", "yo", "jo", "zh", "yu", "ju", "ya", "ja", "a", "b", "v", "g", "d", "e", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "c", "w", "~", "y", "'");
var capitEngLetters = new Array("E", "CH", "SH", "YO", "JO", "ZH", "YU", "JU", "YA", "JA", "A", "B", "V", "G", "D", "E", "Z", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "C", "W", "~", "Y", "'");
var smallRusLettersReg = new Array(/э/g, /ч/g, /ш/g, /ё/g, /ё/g,/ж/g, /ю/g, /ю/g, /я/g, /я/g, /а/g, /б/g, /в/g, /г/g, /д/g, /е/g, /з/g, /и/g, /й/g, /к/g, /л/g, /м/g, /н/g, /о/g, /п/g, /р/g, /с/g, /т/g, /у/g, /ф/g, /х/g, /ц/g, /щ/g, /ъ/g, /ы/g, /ь/ );
var capitRusLettersReg = new Array(/Э/g, /Ч/g, /Ш/g, /Ё/g, /Ё/g, /Ж/g, /Ю/g, /Ю/g, /Я/g, /Я/g, /А/g, /Б/g, /В/g, /Г/g, /Д/g, /Е/g, /З/g, /И/g, /Й/g, /К/g, /Л/g, /М/g, /Н/g, /О/g, /П/g, /Р/g, /С/g, /Т/g, /У/g, /Ф/g, /Х/g, /Ц/g, /Щ/g, /Ъ/g, /Ы/g, /Ь/);

// э, ч, ш, ё, ё,ж, ю, ю, я, я, а, б, в, г, д, е, з, и, й, к, л, м, н, о, п, р, с, т, у, ф, х, ц, щ, ъ, ы, ь
// e, ch, sh, yo, jo, zh, yu, ju, ya, ja, a, b, v, g, d, e, z, i, j, k, l, m, n, o, p, r, s, t, u, f, h, c, w, ~, y, '

function translit(objTextarea)
{
	var i;
	var textbody = objTextarea.value;
	var selected = false;

	if (objTextarea.isTextEdit)
	{
		objTextarea.focus();
		var sel = document.selection;
		var rng = sel.createRange();
		rng.colapse;
		if (sel.type=="Text" && rng != null)
		{
			textbody = rng.text;
			selected = true;
		}
	}

	if (textbody)
	{
		if (TRANSLIT_way==0) // латиница -> кириллица
		{
			for (i=0; i<smallEngLettersReg.length; i++) textbody = textbody.replace(smallEngLettersReg[i], smallRusLetters[i]);
			for (i=0; i<capitEngLettersReg.length; i++) textbody = textbody.replace(capitEngLettersReg[i], capitRusLetters[i]);
		}
		else // кириллица -> латиница
		{
			for (i=0; i<smallRusLetters.length; i++) textbody = textbody.replace(smallRusLettersReg[i], smallEngLetters[i]);
			for (i=0; i<capitRusLetters.length; i++) textbody = textbody.replace(capitRusLettersReg[i], capitEngLetters[i]);
		}
		if (!selected) objTextarea.value = textbody;
		else rng.text = textbody;
	}

	if (TRANSLIT_way==0) // латиница -> кириллица
	{
		//document.form1.TRANSLIT.value += " *";
		document.forms['form1'].elements['TRANSLIT'].value += " *";
		TRANSLIT_way = 1;
	}
	else // кириллица -> латиница
	{
		//document.form1.TRANSLIT.value = TRANSLIT_title;
		document.forms['form1'].elements['TRANSLIT'].value = document.forms['form1'].elements['TRANSLIT'].value.slice(0,-2);
		TRANSLIT_way = 0;
	}
	objTextarea.focus();
}

function isLeftClick(event)
{
	if (!event.which && event.button !== undefined)
	{
		if (event.button & 1)
			event.which = 1;
		else if (event.button & 4)
			event.which = 2;
		else if (event.button & 2)
			event.which = 3;
		else
			event.which = 0;
	}

	return event.which == 1 || (event.which == 0 && BX.browser.IsIE());
};



function AddPopupTask(e, group_id, params)
{
	group_id = group_id || 0;

	if (
		(navigator.userAgent.match(/iPhone/i)) ||
 		(navigator.userAgent.match(/iPad/i))
 	)
		return (false);

	if(!e) e = window.event;

	if (isLeftClick(e))
	{
		if (group_id > 0)
			params['GROUP_ID'] = group_id;

		window.descrToIframe = params["DESCRIPTION"];

		params["DESCRIPTION"]="...";
		var arr= Array();
		 arr.push("CO_"+params["UF_COMPANY_ID"]);
		if(params["UF_COMPANY_ID"])
		params["UF_CRM_TASK"]=JSON.stringify(arr);

		params["ADDIFRAMESCRIPT"]="Y";

		//console.log(params);

		taskIFramePopup.add(params);
		BX.PreventDefault(e);
	}
}



$(document).ready(function(){
	var ownerSidText = BX('OWNER_SID').value;

	if(ownerSidText)
	{
		mailsStrSplit(ownerSidText);
		var MAILS_STR_RC = ownerSidText;
	}else{
		//form_disabled
		$(".close_ticket").addClass("form_disabled");
		$(".open_ticket").addClass("form_disabled");
		$(".apply_ticket").addClass("form_disabled");
	}
	
	$('#AUTHOR_CHANGE_SHOW').click(function(){
		$('.c_show').hide();
		$('.c_hide').show();
	});
	
	$('#AUTHOR_CHANGE_HIDE').click(function(){
		$('.c_show').show();
		$('.c_hide, .c_hide_c').hide();
		
		mailsStrSplit(MAILS_STR_RC, 1);
	});
	
	var but_panel_pos = $('.but_panel').offset().top;
	panelPos(but_panel_pos);
	
	$(window).bind("scroll", function(){panelPos(but_panel_pos);});

	//document click handler
	$(this).click(function(e) {
		if ( 	!$(e.target).is('#task-menu-popup') &&
			!$(e.target).is('.add_task') &&
			!$(e.target).is('.webform-small-button-text') &&
			$('#task-menu-popup').css('display')  != 'none'
		)	menuOpenClose();
	});
});

function test(testStr)
{
	
	console.log(testStr);
	
}

function mailsStrSplit(mailsStr, cancel)
{
	var arMails = mailsStr.split(', ');
	var divContact = '';

	$(arMails).each(function(i, mail){
		if(mail.indexOf('<') + 1)
		{
			var mailWbkt = mail.replace('<', '(');
			mailWbkt = mailWbkt.replace('>', ')');
		}
		else
		{
			var mailWbkt = mail;
		}

		divContact = divContact + '<div data-id='+i+' id="'+mail+'">'+mailWbkt+' <span onClick="mDel($(this).parent()); test(this.parent);">x</span></div>';
	});

	BX('SEL_CONTACT_LIST').innerHTML = divContact;
	
	if(cancel)
	{
		BX('OWNER_SID').value = mailsStr;
	}
}

function mDel(elemP)
{
	var elemPId = elemP.attr('ID');
	elemP.remove();
	var owners = BX('OWNER_SID');
	var ownersVal = owners.value;
	
	var ownersValRep = mDelRep(ownersVal, elemPId);
	
	owners.value = ownersValRep;
}

function mDelRep(ownersVal, elemPId)
{
	if(ownersVal.indexOf(elemPId) + 1)
	{
		if(ownersVal.indexOf(elemPId+', ') + 1)
		{
			var ownersValRep = ownersVal.replace(elemPId+', ', '');
		}
		else if(ownersVal.indexOf(', '+elemPId) + 1)
		{
			var ownersValRep = ownersVal.replace(', '+elemPId, '');
		}
		else
		{
			var ownersValRep = ownersVal.replace(elemPId, '');
		}
	}
	
	return ownersValRep;
}

BX.addCustomEvent("onPopupClose", BX.delegate(function(command,params){
	if('CRM-crm-UF_COMPANY_ID-open-popup' == command.uniquePopupId)
	{
		ajaxCompany();
	}
}, this));

BX.addCustomEvent("onPopupClose", BX.delegate(function(command,params){
	//console.log(command.uniquePopupId);
	if('CRM-crm-form1-uf-company-id-open-popup' == command.uniquePopupId)
	{
		ajaxCompany();
	}
}, this));

function ajaxCompany()
{
	//console.log("cc");
	var input_box = $("#crm-crm-UF_COMPANY_ID-open_UF_COMPANY_ID-input-box input").val();
	//if(input_box == "undefined")
		//input_box = $('input[name="UF_COMPANY_ID"]').val();
	input_box = $("#crm-crm-form1-uf-company-id-open_UF_COMPANY_ID-input-box input").val();
	//console.log(input_box);
	if(input_box)
	{
		var params = null;
		params = {UF_COMPANY_ID: input_box};

		BX.ajax({
			url: '/dev/ajax/contact_list.php',
			data: params,
			method: 'POST',
			dataType: 'script',
			timeout: 60,
			async: true,
			processData: true,
			scriptsRunFirst: true,
			emulateOnload: true,
			start: true,
			cache: false,
			onsuccess: function(data){
				if(!$.isEmptyObject(arResult))
				{//console.log(arResult);
					ContactFormation(arResult);
				}
				else
				{
					ContactEmpty(input_box);
				}
			},
			onfailure: function(){}
		});
	}
	
	$('.crm-element-item-delete').bind('click', function(){
		$("#CONTACT_EMAIL_SEL_BOX, #CONTACT_SEL_BOX").hide();
		$("#CONTACT_EMAIL_SEL_BOX select, #CONTACT_SEL_BOX select").remove();
	});
}

function ContactEmpty(compId)
{
	$("#CONTACT_SEL_BOX .sel").html('<a title="'+SUP_JS_MESS_FILL+'" href="/crm/company/show/'+compId+'/" target="_blank">'+SUP_JS_MESS_CONT_EMPTY+'</a>');
	$("#CONTACT_SEL_BOX").show();
}

function ContactFormation(ajaxData)
{
	var options;
	var company_id;
	$.each(ajaxData, function(i, contact){
		if(contact.EMAIL)
		{
			options = options+'<option value="'+contact.ID+'">'+contact.FULL_NAME+'</option>';
		}
		company_id = contact.COMPANY_ID;
	});

	if(options)
	{
		var select = '<select id="CONTACT_SEL" onChange="ContSel($(this).val());"><option>-------</option>'+options+'</select>';
		$("#CONTACT_SEL_BOX").show();
		$("#CONTACT_SEL_BOX .sel").html(select);

		if($('#CONTACT_EMAIL_SEL_BOX .sel').length)
		{
			$("#CONTACT_EMAIL_SEL_BOX").hide();
		}
	}
	else
	{
		ContactEmpty(company_id);
	}
}

function ContSel(contact_id)
{
	var select = '<select id="CONTACT_EMAIL_SEL" onChange="ContMailSel($(this).val());"><option>-------</option>';
	
	if(!isNaN(parseFloat(contact_id)) && isFinite(contact_id) && arResult[contact_id]['EMAIL'])
	{
		$.each(arResult[contact_id]['EMAIL'], function(i, contact){
			if(contact){
				select = select+'<option value="'+i+'">'+contact+'</option>';
			}
		});
		
		var select = select+'</select>';
		$("#CONTACT_EMAIL_SEL_BOX").show();
		$("#CONTACT_EMAIL_SEL_BOX .sel").html(select);
		
		if($('a').is('#CONTACT_A'))
		{
			$("#CONTACT_A").attr('href', '/crm/contact/show/'+contact_id+'/');
		}
		else
		{
			$("#CONTACT_SEL").after("<a id='CONTACT_A' href='/crm/contact/show/"+contact_id+"/' target='_blank'>"+SUP_GO_TO_CONTACT+"</a>");
		}
	}
}

function ContMailSel(mailId)
{
	if(!$('#SOURCE_ID').val())
	{
		$("#SOURCE_ID option[value='14']").attr("selected", "selected");
		SelectSource();
	}
	
	if(mailId != '-------')
	{
		var ownerSid = $('#OWNER_SID');
		var ownerSidVal = ownerSid.val();
		
		if('undefined' != typeof arResult)
		{
			var contactId = $('#CONTACT_SEL').val();
			var mailStr = arResult[contactId]['EMAIL'][mailId];
			var elemFN = arResult[contactId]['FULL_NAME'];
		}
		else
		{
			var mailStr = arContact['EMAIL'][mailId];
			var elemFN = arContact['FULL_NAME'];
		}
		
		var elemMId = elemFN+' <'+mailStr+'>';

		if(!(ownerSidVal.indexOf(elemMId) + 1))
		{
			var divContact = '<div id="'+elemMId+'">'+elemFN+' ('+mailStr+') <span onClick="mDel($(this).parent());">x</span></div>';
		
			$('#SEL_CONTACT_LIST').append(divContact);
		
			if(ownerSidVal)
			{
				ownerSid.val(ownerSidVal+', '+elemMId);
			}
			else
			{
				ownerSid.val(elemMId);
			}
		}
		
		//document.getElementById('form_save').disabled = false;
		//document.getElementById('form_apply').disabled = false;
		$(".form_disabled").each(function(i) {
			//$(".webform-small-button").each(function(i) {
			$( this ).removeClass( "form_disabled" ).addClass( "form_enabled" );
		});

		//if ($(but).hasClass('apply_ticket')) {}
	}
}

function panelPos(but_panel_pos)
{
	var but_panel = $('.but_panel');
	if($(window).scrollTop() > but_panel_pos && !but_panel.hasClass('fixed'))
	{
		but_panel.addClass('fixed');
		if($('div').is("#bx-panel") && $('a').is(".bx-panel-pin-fixed"))
		{
			but_panel.addClass('fixed_top');
		}
	}
	else if($(window).scrollTop() < but_panel_pos && but_panel.hasClass('fixed'))
	{
		but_panel.removeClass('fixed').removeClass('fixed_top');
	}
}

//покажем/спр¤чем меню создани¤ задачи
function menuOpenClose()
{
	if($('#task-menu-popup').css('display') == 'none')
	{
		$('#task-menu-popup').show();
	}else{
		$('#task-menu-popup').hide();
	}
}

//нажали на "Выбрать сообщени¤", отобразим checkbox у сообщений
function showCheckbox()
{
	menuOpenClose();//спр¤чем меню
	$('.mess_alert_block, #ticket-edit-panel, .message_select').show();//отобразим checkbox
	$('#sel_mess').hide();//скроем "¬ыбрать сообщени¤"
	$('#of_sel_mess').css('display', 'block');
}

//формируем данные для создания задачи
function taskFormate(arrSelMessId,groupId,companyId)
{
	menuOpenClose();//спр¤чем меню

	groupId = groupId || 35;	//default
	companyId = companyId || "";	//default

	var ticketText = '';
	var files = [];
	
	$(arrSelMessId).each(function(i, mess)
	{
		//text mess
		if(arrMessages[mess]["SUPPORT_ANSWER"] == 'Y')
		{
			var persType = SUP_EMPLOYEE_TS;
		}
		else
		{
			var persType = SUP_CONTACT;
		}
		
		var messText = '[B]#'+arrMessages[mess]["MESS_NUMBER"]+' '+persType+' '+arrMessages[mess]['OWNER_USER_ID']['OWNER_NAME']+'[/B]\n'+arrMessages[mess]["TEXT"]+'\n\n';
		
		ticketText = ticketText + messText;
		
		//files list
		files = files.concat(arrMessages[mess]['FILE_ID']);

	});


	//copy files for task
	var files = copyFiles(files);

	//title
	var tTitle = html(arrTicket['TITLE'])+" ("+SUP_TICKET+" #"+ arrTicket['ID'] +")";

	var params = null;
	params = {TITLE: tTitle, DESCRIPTION: ticketText, FILES: files};
	//responsible
	if(arrTicket['COMPANY_MANAGER_ID'])
	{
		params['CREATED_BY'] = arrTicket['COMPANY_MANAGER_ID'];
	}


	if(groupId==138 || groupId==48)
		params['UF_COMPANY_ID'] = '';	//UF_COMPANY_ID
	else
		params['UF_COMPANY_ID'] = arrTicket.UF_COMPANY_ID;	//UF_COMPANY_ID

	params['UF_TICKET'] = arrTicket.ID;
	params['UF_CLIENT'] = companyId;	//UF_CLIENT


	AddPopupTask(event, groupId,  params);
}

function escapeHtml(text) {
	return text
		.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;")
		.replace(/"/g, "&quot;")
		.replace(/'/g, "&#039;");
}

function html(text) {
	return text
		.replace(/&lt;/g, "<")
		.replace(/&gt;/g, ">")
		.replace(/&quot;/g, '"')
		.replace(/&#039;/g, "'")
		.replace(/&amp;/g, "&");
}

//copy files for task
function copyFiles(files)
{

	var res;
	BX.ajax({
		url: '/dev/ajax/add_new_task.php',
		data: {'TICKET_FILES_ID':files},
		method: 'POST',
		dataType: "json",
		async: false,
		cache: false,
		onsuccess: function(data){
			res = data;
			return res;
		},
	});
	
	return res;
}

function CreateTaskAll(groupId,companyId)
{
	ActAll({0:{"checked":"checked"}});	//выбор всех сообщений вне зависимости от выбора пользователя

	var arrSelMessId = [];
	
	$('.message_select input:checked').each(function(){
		arrSelMessId.push($(this).prop('id'));
	});

	if($.isEmptyObject(arrSelMessId))
	{
		menuOpenClose();
		return false;
	}
	else
	{
		taskFormate(arrSelMessId,groupId,companyId);
	}
}

function ActAll(check)
{
	$('.message_select input[type=checkbox]').attr('checked', check[0]['checked']);
}

function SelectSource()
{
	var objSourceSelect, strSourceValue;
	objSourceSelect = document.form1.SOURCE_ID;

	strSourceValue = objSourceSelect[objSourceSelect.selectedIndex].value;
	document.getElementById("OWNER_SID").style.display = "none";
	document.getElementById("OWNER_SID").disabled = true;
	if (strSourceValue!="")
	{
		document.getElementById("OWNER_SID").disabled = false;
		document.getElementById("OWNER_SID").style.display = "inline";
	}
}

BX.ready(function(){
	var buttons = BX.findChildren(document.forms['form1'], {attr:{type:'submit'}});
	for (i in buttons)
	{
		BX.bind(buttons[i], "click", function(e) {
			setTimeout(function(){
				var _buttons = BX.findChildren(document.forms['form1'], {attr:{type:'submit'}});
				for (j in _buttons)
				{
					_buttons[j].disabled = true;
				}

			}, 30);
		});
	}
});

BX.ready(function(){
	if(typeof addAnswer !== 'undefined')
	{
		addAnswer.destroy();
	}

	addAnswer = new BX.PopupWindow("ticket_list", null,{
		content: BX('ajax-copy-mess'),
		closeIcon: {right: "20px", top: "10px"},
		titleBar: {content: BX.create("span", {html: '<b>'+SUP_MESS_COPY_POPUP_TITLE+'</b>', 'props': {'className': 'access-title-bar'}})},
		zIndex: 0,
		offsetLeft: 0,
		offsetTop: 0,
		draggable: {restrict: false},
		overlay: {},
		buttons: [
			new BX.PopupWindowButton({
				text: SUP_CLOSE_POPUP,
				className: "webform-button-link-cancel",
				events: {click: function(){
					this.popupWindow.close();
				}}
			})
		]
	});
});

function MessAct(m_elem, m_act)
{
	var params = null;
	params = {MESS_ID: m_elem, MESS_ACTION: m_act};

	BX.ajax({
		url: '/dev/ajax/ticket_list.php?MESS_ID='+m_elem+'&MESS_ACTION='+m_act,
		data: params,
		method: 'POST',
		dataType: 'html',
		timeout: 60,
		async: true,
		processData: true,
		scriptsRunFirst: true,
		emulateOnload: true,
		start: true,
		cache: false,
		onsuccess: function(data){
			BX('ajax-copy-mess').innerHTML = data;
			addAnswer.show();
			addAnswer.adjustPosition();
		},
		onfailure: function(){
			
		}
	});
}

$(document).ready(function(){
	//submit form Ctrl+Enter
    $("textarea").keydown(function(e) {
        if(e.ctrlKey && e.keyCode == 13) {
            this.form.setAttribute("method", "post");
            this.form.submit();
        }
    });

	//go to the last post
    if(window.location.hash)
	{
		if($('div').is("#bx-panel") && $('a').is(".bx-panel-pin-fixed")){
			var pad = 140;
		}
		else{
			var pad = 101;
		}

		var mesId = window.location.hash.substr(1);
	    var scroll_el = $('div.ticket-edit-message:last');
        if ($(scroll_el).length != 0){
			$('html, body').animate({ scrollTop: $(scroll_el).offset().top-pad }, 700);
        }
    }
	
	beforeTicketClose = new BX.PopupWindow("ticket_before_close", null,{
		content: BX('before-ticket-close'),
		closeIcon: {right: "20px", top: "10px"},
		titleBar: {content: BX.create("span", {html: BX.message["TICKET_CLOSE_DESCR"], 'props': {'className': 'ticket-close-title'}})},
		zIndex: 0,
		offsetLeft: 0,
		offsetTop: 0,
		draggable: {restrict: false},
		overlay: {},
		buttons: [
			new BX.PopupWindowButton({
				text: BX.message["TICKET_CLOSE_BUTTON"],
				className: "webform-small-button-accept",
				events: {click: function(){
					if($('input[name="final_status"]').length > 0)
					{
						if($('input[name="final_status"]:checked').length > 0)
						{
							$('input[name="STATUS_ID"]').val($('input[name="final_status"]:checked').val());
							$('select[name="CATEGORY_ID"]').val($('#close_category :selected').val());
							document.getElementById('form1').submit();
						}
						else
						{
							$('#status-error').show();
						}
						
					}
					else
					{
						$('select[name="CATEGORY_ID"]').val($('#close_category :selected').val());
						document.getElementById('form1').submit();
					}
				}}
			}),
			new BX.PopupWindowButton({
				text: BX.message["TICKET_CLOSE_BUTTON_CANCEL"],
				className: "webform-button-link-cancel",
				events: {click: function(){
					this.popupWindow.close();
				}}
			})
		]
	});
});

function sub(but) {
	if ($(but).hasClass('form_disabled')) {
		alert("Заполните обязательные поля!");
		return false;}
 
	if ($(but).hasClass('apply_ticket')) {document.getElementById('form1').submit();}
	if ($(but).hasClass('close_ticket')) 
	{
		beforeTicketClose.show();
		$('input[name="CLOSE"]').prop('checked', true); 
	}
	if ($(but).hasClass('open_ticket')) { 
		$('input[name="OPEN"]').prop('checked', true); 
		document.getElementById('form1').submit();
	}
	//return false;
}




